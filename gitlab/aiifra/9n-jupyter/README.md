# About

Dockerfiles for images to Open Virtual Service.

# Image Hierarchy

```
centos7 (official centos:7.4.1708)
└── centos7-base (jd yum mirror)
    └── centos7-minimal (tar, bzip2, which, make, git, net-tools, zlib-devel, wget, vim, nc, iperf3, telnet, makeself.sh)
        └── centos7-runtime-base-9n (jdk1.8.0_171, miniconda3 with python3.5.5, conda and pypi's jd mirror)
            └── centos7-runtime-hadoop-base-9n (hadoop 2.7.6 + 9n-hadoop-conf, 9n kerberos conf and 9n hdfs kerberos keytab)
                └── centos7-runtime-hadoop-9n-yarn (pyspark, Numpy, Pandas, Scipy, scikit.learn, Blaze, apriori)
```
