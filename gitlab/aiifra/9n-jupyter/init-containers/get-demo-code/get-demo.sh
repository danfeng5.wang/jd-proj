
bucket=http://storage.jd.local/jupyter-demo-code
#bucket=http://storage.jd.com/jupyter-demo-code
demoDir=/tmp/demo
files=""
for i in "$@"
    do
    case $i in
        -b=*|--bucket=*)
            bucket="${i#*=}"
            shift
            ;;
        -d=*|--demo-dir=*)
            demoDir="${i#*=}"
            shift
            ;;
        *)
            files="$files $i"
            shift
            ;;
    esac
done

echo $demoDir

ls $demoDir >/dev/null 2>&1
r=$?

if [ $r -ne 0 ]; then
    mkdir -p $demoDir
fi

echo $Files

for f in $files
do
    echo $bucket/$f
    wget -t 3 $bucket/$f -O $demoDir/$f #>/dev/null 2>&1
done
