import requests
import os
def delete_request(hub_url, token, username):
    auth_header = {
        'Authorization': 'token %s' % token
    }
    url = hub_url + '/users/%s/server' % username
    r = requests.delete(url, headers=auth_header)

if __name__ == '__main__':
    url = os.getenv('JUPYTERHUB_API_URL')
    token = os.getenv('JUPYTERHUB_API_TOKEN')
    username = os.getenv('JUPYTERHUB_USER')
    delete_request(url, token, username)
