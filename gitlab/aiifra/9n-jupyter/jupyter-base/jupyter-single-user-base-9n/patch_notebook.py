import notebook
from shutil import copyfile

def get_notebook_base_path():
    note_file = notebook.__file__.split('/')
    note_file.pop()
    return '/'.join(note_file)

def add_limit_file(nb_base_path):
    #nb_base_path = get_notebook_base_path()
    zmq_path = nb_base_path + ('/base/zmqhandlers.py')
    copyfile('./zmqhandlers.py', zmq_path)

def update_notebook_app(nb_base_path):
    #https://github.com/jupyter/notebook/commit/6debb0ae1a23a150517e1a034bac641566c71259
    app_path = nb_base_path + ('/notebookapp.py')
    copyfile('./notebookapp.py', app_path)

def main():
    base_path = get_notebook_base_path()
    # Add output limit function
    add_limit_file(base_path)
    # Update notebookapp.py
    update_notebook_app(base_path)

if __name__ == "__main__":
    main()
