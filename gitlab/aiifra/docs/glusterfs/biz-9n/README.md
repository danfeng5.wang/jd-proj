# 九数专用 GlusterFS 集群

## 集群信息

- **GlusterFS Version :** 3.12.15
- **Mount Point :** biz-9n-gfs.jd.local
- **Heketi API :** http://heketi.k8s-biz-9n.jd.local:8081

## 节点配置

COUNT | CPU | MEM | DISK
----- | --- | ---- | ----
30 | E5-2650v4 * 48 | 256G | 300G SAS * 2 (RAID1) + 6T SATA * 12 (NO RAID)

## 节点列表

IP |
--
11.3.77.2 |
11.3.77.5 |
11.3.77.7 |
11.3.77.10 |
11.3.77.11 |
11.3.77.35 |
11.3.77.37 |
11.3.77.38 |
11.3.77.39 |
11.3.77.41 |
11.3.77.42 |
11.3.77.66 |
11.3.77.67 |
11.3.77.68 |
11.3.77.69 |
11.3.76.41 |
11.3.76.42 |
11.3.76.43 |
11.3.76.67 |
11.3.76.74 |
11.3.76.75 |
11.3.76.98 |
11.3.76.169 |
11.3.76.171 |
11.3.76.194 |
11.3.76.196 |
11.3.76.198 |
11.3.76.200 |
11.3.76.201 |
11.3.76.226 |

## 网盘卷分配

Volume | Type | Size | Purpose
------ | ---- | ---- | -------
vol_3e7acde969650a611a2842ef67a734d4 | Distributed 1 | 100G | 存储 Pino HDFS 的 Kerberos Keytab
vol_c9b12d3704dec39d6bec7c67e1caea43 | Distributed 3 * Replicate 2 | 5T   | git-pino.jd.com 的 DB 和代码库
vol_9eeae5058a89555a09979441a2a886d1 | Distributed 8 | 20T  | 九数项目共享网盘
vol_831e2a3c64ba17e4687a38e6b6b2ec69 | Replicate 3 | 2T   | 测试网盘