# 9n-jupyter登录计费流程

> Note: 所有计费信息的发送(涉及到end time)均发生在delete singleuser pod的时候。start time为pod创建时的时间，end time为pod删除时间。所有billSN信息发生变动的情况均发生在用户通过9n.jd.com的接口打开jupyterlab的时候。

1. 普通用户登录
    1. jupyter singleuser未运行
        * 新建singleuser pod
        * 更新pod start time
        * user数据库中移除旧billSN，使用新billSN
    2. jupyter singleuser already running
        * 打开singleuser 用户空间
        * 原有billSN is None: 更新billSN为本次登录的billSN
        * 原有billSN not None：保留旧版billSN

2. admin or supporter协助
    1. 只进入用户空间进行协助(此过程不关闭or重启用户空间)
        * 此情况不会发送任何计费信息(无pod销毁过程)
        * billSN不变
    2. 开启用户空间并协助
        * 将用户billSN置为None
        1. 协助完成后用户接入
            * 不发送计费信息(不涉及pod销毁).
            * 用户通过9n接口登录，更新billSN. `Note: 如果用户直接刷新页面，不走登录流程，无法更新billSN`
        2. 协助完成后关闭
            * 不发送计费信息(billSN为None)
    3. 重启用户空间
        * 关闭时会发送计费信息(billSN为用户新打开用户空间时第一次登录的billSN)
        * 再次打开后续过程同2.2