# Jupyter with Taints and Tolerations

由于计算任务会消耗节点的大量资源, 在某些情况下甚至会用尽节点的内存和 cpu 从而影响 jupyter 服务的正常使用, 因此需要利用 kubernetes [Taints and Tolerations](https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/) 特性, 将 jupyter 与其他计算任务的节点隔离. 当前选取了 5 个 node 作为jupyter 的调度节点

- 11.3.176.227
- 11.3.176.231
- 11.3.177.100
- 11.3.177.8
- 11.3.178.9

1. taint and label node
   ```shell
   # Noschedule 是不允许调度, 而 PreferNoSchedule 相对来说 soft 一点, 具体效果就是默认不会调度到该节点
   # 但如果使用了 nodeSelector 或其他 scheduler hint 则允许调度. 具体可查看官方文档.
   # 目前方案采用的是 NoSchedule.
   kubectl taint nodes <node-name> key=value:NoSchedule or PreferNoSchedule
   kubectl label nodes <node-name> key=value
   # 验证 
   kubectl get nodes -l key=value
   NAME           STATUS    ROLES     AGE       VERSION
   11.3.176.227   Ready     <none>    136d      v1.10.3
   11.3.176.231   Ready     <none>    271d      v1.10.3
   11.3.177.100   Ready     <none>    271d      v1.10.3
   11.3.177.8     Ready     <none>    271d      v1.10.3
   11.3.178.9     Ready     <none>    271d      v1.10.3
   ```

2. 更新 jupyter

   1. 具体修改查看 [chart](http://git.jd.com/aiifit/9n-jupyter/commit/785e96075944f37c057327fcf27c3d4aec943e1d) 和 [jupyterhub](http://git.jd.com/aiifit/9n-jupyter/commit/a28693f18d6d1cd38763bd12080046305323dd3d) 这两个 commit.

   2. 根据第一步 taint 和 label node 的信息, 更新 jupyter values. 其中 jupyterhub 的 image 使用 mirror.jd.com/9n-jupyter/k8s-hub-jd-authenticator:v1.3.4 这个版本

    ```yaml
    # Note: nodeSelector 对应 node label, tolerations 对应 taint
    hub:
      ...
      nodeSelector:
        app: jupyter
    proxy:
      ...
      nodeSelector:
        app: jupyter
    singleuser:
      ...
      nodeSelector:
        app: jupyter
      tolerations:
        - key: app
          operator: Equal
          value: jupyter
          effect: NoSchedule
    ```
