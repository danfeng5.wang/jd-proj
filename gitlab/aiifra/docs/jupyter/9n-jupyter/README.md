# 9n-Jupyter文档

## 部署环境

### k8s info
1. master node: 11.3.77.8
2. namespace: 9n
3. ingress
    * name: 9n-jupyter 
    * host: 9n-editor.jd.com
4. helm info
    * tiller namespace: kube-system
    * release: 9n-editor

### Debug ENV
1. master node: 172.18.161.49(dev)
2. namespace: lichaolei4
3. ingress
    * name: jupyter-test
    * host: 9n-editor-test.jd.com
4. helm info
    * tiller namespace: lichaolei4
    * release: jupyter-test

## Code

1. jupyterlab dockerfile: http://git.jd.com/aiifit/9n-jupyter/tree/master/jupyter-base
2. jupyterhub dockerfile:
http://git.jd.com/aiifit/9n-jupyter/tree/jupyterhub/jupyterhub-base
3. chart: http://git.jd.com/aiifit/9n-jupyter/tree/master/chart
4. chart-value-file: http://git.jd.com/aiifit/9n-jupyter/blob/master/jupyterhub-9n-values.yaml
5. jupyterhub code:
http://git.jd.com/jupyter/jupyterhub/tree/auth-dev
6. notebook code: http://git.pino-ai.jd.com/ovs/notebook

## 主要添加功能

### Jupyterhub
1. 集成JD的认证流程：
    1. 登陆认证：ref: [登录态校验非JAVA应用接入](https://cf.jd.com/pages/viewpage.action?pageId=74188163)
    2. 角色信息认证：接口地址 http://9n.jd.com/api/getUserInfoBySignature 9n控制，详情见代码or咨询刘伟科

### Jupyterlab

1. 限制了notebook交互页面上output的最大字符数
2. 关闭了Terminal功能
3. cherr-pick了社区上的一个commit https://github.com/jupyter/notebook/commit/6debb0ae1a23a150517e1a034bac641566c71259 git.jd.com地址：http://git.pino-ai.jd.com/ovs/notebook/commit/32a641742f5a49f58bd756f09295f49cb3a624d2
4. 加入了PySpark kernel

### helm chart 

1. template支持配置JD认证的相关信息

### PySpark相关(TODO: @大佬)

## 更新相关

### Jupyterhub

1. After update jupyterhub code, build jupyterhub wheel package

    ```Shell
    cd $(PATH_TO_JUPYTERHUB_CODE)
    # Using python3
    # clean before build package
    rm -rf build/*
    rm -rf dist/*
    python setup.py bdist_wheel
    PACKAGE_PATH=$(PATH_TO_JUPYTERHUB_CODE)/dist/
    package=$(PACKAGE_PATH)/jupyterhub-0.8.1-py3-none-any.whl
    ```

2. update package in jupyterhub dockerfile
3. make build
4. update jupyterhub image version in chart-value-file
5. helm upgrade

### Jupyterlab

#### Update notebook code

1. 更新代码到git.jd.com
2. dockerfile部分, 由于涉及到notebook的改动比较少，在Dockerfile中，目前没有使用wheel package的方式去安装notebook， 因此是通过直接修改部分文件. 修改方式见：http://git.jd.com/aiifit/9n-jupyter/blob/update-notebook/jupyter-base/jupyter-single-user-base-9n/patch_notebook.py

#### Install extension

1. IN [dockerfile](http://git.jd.com/aiifit/9n-jupyter/blob/update-notebook/jupyter-base/jupyter-single-user-base-9n/jupyter-single-user-9n/Dockerfile) add `jupyter labextension install extension`