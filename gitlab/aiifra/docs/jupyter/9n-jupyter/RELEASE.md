# RELEASELOG for 9n-jupyter

## support bill

### helm chart update

* jupyterhub image: mirror.jd.com/9n-jupyter/k8s-hub-jd-authenticator:v1.2
* jupyter-singleuser image: mirror.jd.com/ovs/jupyter-single-user-9n:v1.08
* helm chart add singleuser.lifecycleHooks.preStop.exec.command:["python", "/usr/local/lib/delete.py"]
* Add bill config in hub.extraConfig.'c.JupyterHub.tornado_settings'

`TODO`: Update bill_domain in
[jupyterhub-9n-values.yaml](http://git.jd.com/aiifit/9n-jupyter/blob/jupyter-bill/jupyterhub-9n-values.yaml)

update singeluser image to v1.08
update jupyterhub image to v1.3

### dockerfile update

#### jupyterhub

Rebuild jupyterhub-0.8.1-py3-none-any.whl on jupyterhub branch bill-in-hub with commit
[3369ec162f0603bfd6eda99c6ca4378b9355b538](http://git.jd.com/jupyter/jupyterhub/commit/3369ec162f0603bfd6eda99c6ca4378b9355b538)

#### jupyter-singleuser

Add
[delete.py](http://git.jd.com/aiifit/9n-jupyter/blob/jupyter-bill/jupyter-base/jupyter-single-user-base-9n/delete.py) file used
in singleuser-lifecycleHooks to ensure send delete request to jupyterhub
everytime before delete a singleuser's pod. Jupyterhub will send bill
information after receive a delete request.

## add xgboost

### helm chart

update singleuser image to v1.09

### dockerfile update

#### jupyter-singleuser

add xgboost package
