# Jupyter ingress configuration

proxy-body-size: 因为 jupyter http 请求的 body size 可能超过 nginx 默认值. 这里设置为500m
proxy-connect-timeout, proxy-send-timeout, proxy-read-timeout: 目前 jupyter 启动需要经过 initContainer 获取 demo 代码. 可能耗费时间较长.
使用 nginx 默认值会出现 504 gateway timeout error. 为避免情况的发生, 现分别调整为60s, 180s, 180s.

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "60"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "180"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "180"
  name: 9n-jupyter
  namespace: biz-9n-jupyter
spec:
  rules:
  - host: 9n-editor.jd.com
    http:
      paths:
      - backend:
          serviceName: proxy-public
          servicePort: 80
```
