# Jupyter

## 目录结构

该目录中包含了目前支持的Jupyter环境的维护文档, 目前存在的环境有：

1. 9n-jupyter: 服务9n.jd.com用户(主要关注)
2. codelab: 服务组内用户
3. jupyter-official：用于实验社区版本的jupyter

每个目录主要包括：
1. 环境信息
    1. jupyter部署环境信息
    2. debug环境信息
3. 代码链接，包括dockerfile，helm chart，以及jupyter相关代码
4. 主要支持的功能，以及相较于官方版本主要修改的地方
5. 如何更新

## Jupyter overview

> Note: 目前各个环境中用的jupyter基本版本一样，只是在这个版本上做相应的修改。
> Jupyterhub version: 0.8.1
> Jupyterlab version: 0.32.1
> Notebook version: 5.4.1

### jupyter各组件相互关系

### Helm部署jupyter

ref: [zero-to-jupyterhub-k8s-doc](https://zero-to-jupyterhub.readthedocs.io/en/latest/)

github: [zero-to-jupyterhub-k8s-github](https://github.com/jupyterhub/zero-to-jupyterhub-k8s)