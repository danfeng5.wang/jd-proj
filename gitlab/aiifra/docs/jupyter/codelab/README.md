# Codelab

## 部署环境

### k8s info
1. master node: 172.18.161.59;172.18.162.12;172.18.162.13
2. namespace: jupyter
3. ingress
    * name: jupyter 
    * host: codelab.jd.com
4. helm info
    * tiller namespace: kube-system
    * release: jupyterhub
 
### Debug ENV
1. master node: 172.18.161.49(dev)
2. namespace: jupyter-codelab-debug
3. ingress
    * name: jupyter-codelab-debug
    * host: codelab-debug.local.jd.com
4. helm info
    * tiller namespace: lichaolei4
    * release: codelab-debug.local.jd.com

## Code

1. jupyterlab dockerfile: http://git.jd.com/jupyter/dockerfiles/tree/master/ubuntu-base
2. jupyterhub dockerfile: http://git.jd.com/jupyter/dockerfiles/tree/jupyterhub-base/ubuntu-base/jupyterhub-base
3. chart: http://git.jd.com/jupyter/charts/tree/master/jupyterhub
4. chart-value-file: http://git.jd.com/jupyter/charts/blob/master/jupyterhub.config.yaml
5. jupyterhub code: http://git.jd.com/jupyter/jupyterhub/tree/erp-dev

## 主要添加功能

### Jupyterhub
1. 集成erp认证
2. 支持用户自定义镜像和配置功能 

### Jupyterlab

1. 集成了tensorboard
2. 支持GPU版本的tensorflow和CUDA

### helm chart 

1. template支持配置erp认证接口
2. 支持配置jupyterlab启动镜像和启动配置

### Spark相关(TODO: @大佬)

## 更新相关

参考9n-jupyter文档
