# gluster cluster data securety solution
## Overview

- [VolumeSnapshots-快照模式](##VolumeSnapshots)
- [Geo-replication-主从增量备份 调试过程中。。。](##Geo-replication)

## VolumeSnapshots

gluster 会自动抓取快照，但是在恢复数据的时候需要管理员自己判断，此种数据恢复模式节省开支，但是不够自动化，没有做到实时性。

### 创建快照
```bash
root # gluster snapshot create snapshot-repvol repvol
root # gluster snapshot activate snapshot-repvol
root # gluster snapshot info snapshot-repvol
```
### 从快照恢复数据
```bash
root # gluster volume stop repvol
root # gluster snapshot restore snapshot-repvol
root # gluster volume start repvol
```

### 删除快照
```bash
root # gluster snapshot list
root # gluster snapshot delete snapshot-repvol
```

### features.uss 参数启用 [未验证]
很多的 volume使用者会不小心删除了一些东西，试途请求找回被删除的项目。如果在档案被删除之前保留有一份快照，那么就可以很轻松的从快照中还原

#### 启用 features.uss 参数 

```bash
 root # gluster volume set repvol features.uss enable
```
#### 在 Client 端存取
假设在Client中已经挂载gfs-01:/repvol到/mnt中，则存放Snapshot的方法是：

```bash
 root # ls /mnt/.snaps
 snapshot-repvol
```
.snaps 目录列出目前对于该Volume 所有的快照，可以到​​该目录中寻找必要的文件并取回。

## Geo-replication

官方文档参照 [Gluster docs](https://docs.gluster.org/en/v3/Administrator%20Guide/Geo%20Replication/)， 我们最好搭建一个和master配置完全一致的gluster cluster，显而易见，成本开销会很大。

根据官方文档描述 Geo-replication 模式可以理解为在两个volume之间做成mirror的形式，这两个volume是master-slave关系。

### Environment

|       cluster-1                     |       cluster-2                   |
| -------------------------           | ------------------------          |
|  192.168.1.9 (gluster1)             |  192.168.1.4 (gluster4)           |
|  192.168.1.10(gluster2)             |  192.168.1.5 (gluster5)           |
|  192.168.1.13(gluster3)             |  192.168.1.7 (gluster6)           |



- 配置用户及ssh密钥登录
在gluster4 gluster5 gluster6上做配置：

groupadd repgrp

useradd georep -g repgrp

echo ngaa.com.cn | passwd --stdin georep --force

- 在gluster1上做配置 ssh key,免秘钥登录：
```bash
ssh-keygen -f  /root/.ssh/id_rsa 
ssh-copy-id georep@gluster4
ssh-copy-id georep@gluster5
ssh-copy-id georep@gluster6
```

- 在gluster4 gluster5 gluster6 上配置：
```bash
mkdir -m 0711 /var/mountbroker-root
gluster system:: execute mountbroker opt mountbroker-root /var/mountbroker-root
gluster system:: execute mountbroker user georep safevol
gluster system:: execute mountbroker opt geo-replication-log-group repgrp
gluster system:: execute mountbroker opt rpc-auth-allow-insecure on
systemctl restart glusterd
```

- 在gluster1上配置：

```bash
gluster volume set all cluster.enable-shared-storage enable
gluster system:: execute gsec_create
gluster volume geo-replication datavol georep@gluster3::safevol create push-pem
```

- 在gluster4上使用密钥：

/usr/libexec/glusterfs/set_geo_rep_pem_keys.sh georep datavol safevol

- 配置数据同步

在gluster1上启用同步并查看：
```bash
gluster volume geo-replication datavol georep@gluster4::safevol config use_meta_volume true
gluster volume geo-replication datavol georep@gluster4::safevol start
gluster volume geo-replication datavol georep@gluster4::safevol status
```


