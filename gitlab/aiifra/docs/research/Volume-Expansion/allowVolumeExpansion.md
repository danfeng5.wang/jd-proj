# Let us expand kubernetes volume

This article is based on [kubernetes docs](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

In this case, you don’t need to delete and recreate a Pod or deployment that is using an existing PVC. Any in-use PVC automatically becomes available to its Pod as soon as its file system has been expanded. This feature has no effect on PVCs that are not in use by a Pod or deployment. You must create a Pod which uses the PVC before the expansion can complete.
## Overview

* feature support
* volume type 
* demo 

## feature support

- Kubernetes v1.8 alpha
- Kubernetes v1.11 beta

## volume type

you can expand the fllowing types of volumes:
- gcePersistentDisk
- awsElasticBlockStore
- Cinder
- glusterfs
- rbd
- Azure File
- Azure Disk
- Portworx

## demo

you just add **allowVolumeExpansion** field in your storage class,like this:

``` yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: gluster-vol-default
provisioner: kubernetes.io/glusterfs
parameters:
  resturl: "http://172.18.161.49:8080"
  restuser: ""
  secretNamespace: ""
  secretName: ""
allowVolumeExpansion: true
```
You can only resize volumes containing a file system if the file system is XFS, Ext3, or Ext4.

If you don't know you disk filesystem type, you can use the **parted** command to ensure your disk filesystem type,like this:

```bash
[wangdanfeng5@dev-workstation ~]$ sudo parted /dev/sdb
GNU Parted 3.1
Using /dev/sdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print list                                                       
Model: HP LOGICAL VOLUME (scsi)
Disk /dev/sdb: 600GB
Sector size (logical/physical): 512B/512B
Partition Table: loop
Disk Flags: 

Number  Start  End    Size   File system  Flags
 1      0.00B  600GB  600GB  xfs
```

last step, resize an in-use PersistentVolumeClaim and then enjoy it.

