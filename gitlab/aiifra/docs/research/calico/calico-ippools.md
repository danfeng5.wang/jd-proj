# Calico IPPools

ref: [IPPool](https://docs.projectcalico.org/master/reference/calicoctl/resources/ippool)

`NOTE: 事先说明, nodeSelector 功能官方文档还未更新, 但是代码部分支持了, 该文档仅作为参考, 最终以官网文档为主`

`目前, 官方已经更新了 3.5 版本, 支持 nodeSelector, 具体可参考`: [assigning-ip-address-topology](https://docs.projectcalico.org/v3.5/usage/assigning-ip-addresses-topology)

## with etcdv3 datastore type

[安装文档](https://docs.projectcalico.org/master/getting-started/kubernetes/installation/calico#installing-with-the-etcd-datastore)

| image name | version |
| --- | --- |
| calico/cni | v3.5.0-beta.0-0-gd2fc87a-amd64 |
| calico/node | v3.5.0-beta.0-2-gd7ef212-amd64 |
| calico/kube-controllers | v3.5.0-beta.0-0-ga003ffe-amd64 |

## ippool 配置与使用

### use ippool with nodeSelector

```Shell
(calicoctl 是自己编译的. 为了显示 selector, commit 6d6cc0e, version: v3.6.0-0.dev-9-g6d6cc0e)
calicoctl get ippools

Name                 CIDR              SELECTOR
default-ipv4-ippool  192.168.199.0/24  all()
```

新建的 pod 会默认分到该 CIDR 上. 此时, 创建新 ippool 并 disable 掉 default-ipv4-ippool(为什么要 disable 后续会说). 新 ippool yaml 如下.

```Yaml
apiVersion: projectcalico.org/v3
kind: IPPool
metadata:
  name: new-pool
spec:
  cidr: 192.168.222.0/24
  ipipMode: Always
  natOutgoing: true
  nodeSelector: app == 'calico'
```

```Shell
calicoctl apply -f ippools.yaml
calicoctl get ippools -o wide

NAME                  CIDR               NAT    IPIPMODE      DISABLED   SELECTOR
default-ipv4-ippool   192.168.199.0/24   true   CrossSubnet   true       all()
new-pool              192.168.222.0/24   true   Always        false      app == 'calico'
```

此时创建的 pod 不能正常获取到 ip, 显示`FailedCreatePodSandBox Failed create pod sandbox: rpc error: code = Unknown desc = NetworkPlugin cni failed to set up pod "testx_default" network: no configured Calico pools`. 原因是我们这里 disabled 了 default ippool, 而新建的 ippool 指定了 `nodeSelector: app == 'calico'`. nodeSelector 会自动匹配 node 的 label 从而进行 ip 的分配.

```Shell
kubectl edit node minikube
# add label with app = calico
...
metadata:
  labels:
    app: calico
...
```

修改 node label 之后, pod 便能正常获取 ip 了.

```Shell
testx   1/1     Running   0          6m    192.168.222.128   minikube
```

#### ippool 创建顺序与 nodeSelector 存在的一个问题(或者说是注意事项)

1. 多个 ippool match 到了同一个 node

    如果同一个 node match 了 多个 ippool 的 nodeSelector 会出现什么情况? 是 IP 在多个 pool 之间随机分配? 还是分配到最先创建的 ippools 上?

    这里证实一下. 这里新建一个 ippool new-pool-2, CIDR: 192.168.233.0/24; nodeSelector: app == 'calico'. 然后创建多个 pod. 发现这些 pod 的 ip 仍然分到了上一步创建的 new-pool 上. 说明 calico-cni 给 pod 分配 IP 是按照创建 ippool 的时间顺序分配的, 并不会随机分配. (这也是为什么在验证 new-pool 的时候需要 disable 掉 default-ipv4-pool).

2. enable old ippol

    那么如果 enable default-ipv4-pool. 此时的 pod IP 会分配到那个 pool 上呢? 答案是分配到 default-ipv4-pool 上, 也就是说 ip 的分配是按照 pool 的`创建时间`顺序进行, 而不是 `enable 时间`.

### use ippool with kubernetes annotations

ref: [IPAM Manipulation with Kubernetes Annotations](https://docs.projectcalico.org/master/reference/cni-plugin/configuration#specifying-ip-pools-on-a-per-namespace-or-per-pod-bass)

删掉上一步添加的 node label(app: calico). 但是在创建 pod 时指定 annotations

```Yaml
metadata:
  annotations:
    "cni.projectcalico.org/ipv4pools": "[\"new-pool\"]"
```

此时 pod 是可以正常获取到 ip 的. 那么, 如果指定 disabled 的 ippool 会有什么情况? try it.

```Yaml
metadata:
  annotations:
    "cni.projectcalico.org/ipv4pools": "[\"default-ipv4-ippool\"]"
```

此时, pod 不能正常获取 ip: `FailedCreatePodSandBox Failed create pod sandbox: rpc error: code = Unknown desc = NetworkPlugin cni failed to set up pod "testx-annotations-disabled_default" network: the given pool (192.168.199.0/24) does not exist, or is not enabled`. 很好. 可以放心的 disable ippool 了.

### with kuberentes datastore type

目前暂时不建议使用此 datastore, 因为 calico-ipam 暂时不支持 kubernetes datastore, 只能使用 `host-local` 模式. 因此不能通过 ippools 来分配 IP. 仅仅给出安装文档, 后续 calico-ipam 支持 kubernetes datastore 之后再做进一步更新.

[安装文档](https://docs.projectcalico.org/master/getting-started/kubernetes/installation/calico#installing-with-the-kubernetes-api-datastore50-nodes-or-less)

### 使用总结

1. 关于 datastore type, 目前 etcdv3 支持的最完善也最稳定, 唯一缺点是 calicoctl 需要配置 etcd 的 key 稍微麻烦一点. 而 kubernetes 不太推荐, 主要原因是不支持 calico-ipam, 其次是存在一些 bug, 对于配置出错的项也没有 check 机制(例如 nodeSelector).
2. 在使用 ippools 的时候, 如果不指定 nodeSelector 则默认为 all(). 如果存在一个 ippool, 它的创建时间最早且 nodeSelector 为 all(), 那么, 其他 pod 都会优先从该 pool 获取 IP. 因此在实际使用过程中, 如果发现创建的 ippool 不起作用, `则优先排查是否已经存在一个 nodeSelector 为 all 的 ippool`.
3. 如果需要为 pod 指定 ippool. 可以使用 annotations 为该 pod 指定 ippool, 这种用法更加灵活, 但需要人为(or 程序)显示指定.
4. Disable ippool 不会影响已经创建的 pod 以及对应的路由. 如果需要修改 pod ip, 则需要删除 pod 重建. 但是, `如果删除 ip pool, 则位于该 pool 上的 pod 网络会收到影响`:
   1. 无法访问外网: 原因是 calico 设置了两个 ipset(对于 ipv4, ipv6 会有另外的 ipset): `cali40masq-ipam-pools` 与 `cali40all-ipam-pools`, 其中 `masq-ipam-pools` 是包含了 `natOutgoing: true` 的 ippool 网段, 而 `all-ipam-pools` 则是包含了所有 ippool 网段. calico 设置的 iptables 规则会自动将原地址为 `cali40masq-ipam-pools` 且目的地址不在 `cali40all-ipam-pools` 中的网络请求进行 `MASQUERADE` 也就是根据 node ip 进行 SNAT. 而删除 ippool 意味着 pod ip 不再属于 `cali40masq-ipam-pools` 与 `calico40all-ipam-pools` 这两个 ipset, 从而无法进行 SNAT 也就不能访问外网了.
   2. 同一 node 上 pod 之间通信: 不受影响, 直接走本地路由不需要经过 tunl0
   3. 不同 node 上 pod 之间通信:
      1. disable ippool
         1. node 同网段: ipipMode crossSubnet:  可以 ping 同, 路由走 eth0, ipipMode always: 无法访问, 路由走 tunl0, 而 tunl0 的 ip 有 ippool 决定, disable 掉 ippol 会导致 tunl0 不可用.
         2. node 不同网段: 无法访问, 原因同上, tunl0 不可用.
      2. delete ippool
         1. 跨 node 的 pod 都无法通信(无论是否同网段), calico 删掉了跨 node 的路由表.
5. tunl0 的 IP: `每个节点的 tunl0 都会从 ip pool 中分配一个 IP`: 如果一个 node match 了两个 ippool, 那么 tunl0 的 ip 为最先创建的 ippool 给定的范围内. 如果 disable 掉第一个 ippool, 此时 pod 的 ip 会分配到第二个 ippool, 但 tunl0 的 ip 为空. 直到删除了第一个 ippool, tunl0 的 ip 才会从第二个 ippool 中获取. tunl0 是作为跨网段 node 之间 pod 通信的隧道, disable or delete ippool 都会对其造成影响.

### TIPs

记录一下查看路由过程中一些有意思的发现

1. calico 会将指向被删除 pod ip 的路由变为 blackhole 从而保证路由地址不可达(避免一些 bug). 但是暂不清楚 calico 是否会将释放的 ip 立刻会收到 ippool 中.
2. calico 使用了静态 erp 将网卡 mac 地址与 ip 信息绑定. 且 cali+ 网卡设置了 `proxy_arp` 用来响应容器内部的 arp 请求. 容器内部: 网关 ip (169.254.1.1) 绑定到 cali+ 网卡的 mac(ee:ee:ee:ee:ee:ee) 上. 容器外部: 容器 IP (也就是 pod ip) 绑定到容器内部的 eth0 的 mac 地址上.(cali+ 与 容器内部 eth0 为一对 veth 设备). 这么做的目的是为了将 cali+ 网卡作为网关使用, 从容器内部发出的数据发送到网关, 进过 arp 查询 169.254.1.1 下一跳的 mac 地址(cali+ 的 mac), 将数据包发送到 cali+ 的网卡上(veth 设备二层互通); 而 node 节点 arp 显示 pod ip 的下一条 mac 地址是容器内部的网卡 mac, 进入容器的流量会经过 cali+ 网卡然后转发到容器内部的网卡上. 另外, 在 node 上, calico 使用的是 point-to-point route interface, 所以 calico 的流量不会走到数据链路层, 因此不必担心 cali+ 的网卡地址(会经过 iptables 处理, 最终以物理机的数据包发出). ref: [Calico F&Q](https://docs.projectcalico.org/master/usage/troubleshooting/faq#why-does-my-container-have-a-route-to-16925411)
3. 本地 pod 的路由 flags 是 UH, 其他 node 上的 pod ip 路由 flags 为 UG.