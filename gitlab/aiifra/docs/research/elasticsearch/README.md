# ElasticSearch

## ElasticSearch 原理

**Lucene** 准实时反向索引 

## ElasticSearch 在九数人群挖掘场景的预研

[人群挖掘相关数据表](http://git.jd.com/aiifit/docs/blob/master/biz/9n/dw/hive_tables.md)

### 数据结构

作为 **在线实时服务** 引擎，所存数据需在不对查询性能产生过大负面影响的前提下尽可能简化，并针对引擎特性做查询友好性优化。

#### 用户属性

用户属性查询应使用 `app.app_ba_userprofile_prop_nonpolar_view_ext` 表所有字段，同时以 `user_log_acct` 作为 `doc` `id`。

#### 购物行为

购物行为的挖掘逻辑为：

1. 通过 `SKU ID` 及 `关键字` 圈定 `维度`
2. 在圈定的 `维度` 中，选出对符合 `价格区间` 的 `SKU` 产生过购物行为的 `PIN` 集合

因此，我们只需商品表 `app.index_resource_sku_data` 的 `sku_id`, `cid1`（一级类目）, `cid2`（二级类目）, `cid`（三级类目）, `brand_id`（品牌), `commprice` (价格)，以及和 `店铺` 有关的字段，同时以 `sku_id` 作为 `doc` `id`，方便定向查找。

举例如下：

```
{
    "sku_id": "12345",
    "cid1": "1",
    "cid2": "2",
    "cid": "3",
    "sku_name": "好东西",
    "brand_id": "123",
    "commprice": "100",
    "vendor_id“: "234"
}
```

行为表以 `sku_id` 作为 `doc` `id`，主要内容为对该 sku 产生行为的用户 PIN 数组。

举例如下：

```
{
    "sku_id": "12345",
    "cid1": "1",
    "cid2": "2",
    "cid": "3",
    "brand_id": "123",
    "commprice": "100",
    "vendor_id“: "234"
    "pins": [
        "zhaozhiyong",
        "licaixi",
        "wangmingming"
    ]
}
```

#### 广告行为

广告行为的挖掘逻辑为：

1. 通过 `SKU ID` 圈定 `维度`（SKU / 三级类目 / 品牌 / 店铺）
2. 在点击和曝光表中找出同所圈定 `维度` 相关联的 `PIN` 集合

该逻辑同 `购物行为` 的挖掘逻辑类似，不再赘述，广告行为表的索引亦参考相似逻辑创建，同样以 `sku_id` 作为 `doc` `id`。

举例如下：

```
{
    "sku_id": "12345",
    "cid": "3",
    "brand_id": "123",
    "vendor_id“: "234"
    "pins": [
        "zhaozhiyong",
        "licaixi",
        "wangmingming"
    ]
}
```

#### 搜索行为

搜索行为的挖掘逻辑为：

1. 根据关键词或类目找到 `PIN` 集合

该场景较简单，针对 `搜索关键词行为` 和 `搜索类目行为`，分别选用 `base64(搜索词)` 和 `cidx-cid` 作为 `doc` `id`，举例如下：

搜索关键词行为：

```
{
    "kwd": "好东西",
    "pins": [
        "zhaozhiyong",
        "licaixi",
        "wangmingming"
    ]
}
```

搜索类目行为：

```
{
    "pins": [
        "zhaozhiyong",
        "licaixi",
        "wangmingming"
    ]
}
```

### 性能测试

#### 用户属性

查询结构：

```
{
  "query": {
    "bool": {
      "must": [
        { "match": { "cpp_base_education": 3 } },
        { "match": { "csf_saletm_first_ord_dt": "2014-08-11" } },
        { "match": { "cfv_sens_comment": "L4-5" } },
        { "match": { "csf_medal_beauty": "V0" } },
        { "match": { "cvl_glob_community": -1.0 } },
        { "match": { "csf_sale_paytype": "00000c" } },
        { "match": { "cgp_cycl_lifecycle": "16" } }
      ]
    }
  }
}
```

执行耗时：

```
✔ 14:29:11 dev-workstation:~/sandbox/elasticsearch ⤾
% time curl -sSL -u ads_udmp:YTliYmUxMTQwOTk4N2Y3OWM1ZTJjNTYz -H "Content-Type: application/json" http://ads-es1.jd.local:9200/ads_udmp_userprofile/2018-08-22/_search -d @./search_must.json  >/dev/null
curl -sSL -u ads_udmp:YTliYmUxMTQwOTk4N2Y3OWM1ZTJjNTYz -H   -d  > /dev/null  0.00s user 0.00s system 0% cpu 0.944 total
✔ 14:29:15 dev-workstation:~/sandbox/elasticsearch ⤾
%
```

#### 购物行为

（待补充）

#### 广告行为

（待补充）

#### 搜索行为

以类目搜索为例，获得所有搜索过二级类目 **686** 的用户：

```
✔ 14:31:24 dev-workstation:~/sandbox/elasticsearch ⤾
% time curl -sSL -u ads_udmp:YTliYmUxMTQwOTk4N2Y3OWM1ZTJjNTYz http://ads-es1.jd.local:9200/ads_udmp_test/2018-08-22/cid2-686?pretty >/dev/null
curl -sSL -u ads_udmp:YTliYmUxMTQwOTk4N2Y3OWM1ZTJjNTYz  > /dev/null  0.01s user 0.01s system 9% cpu 0.139 total
✔ 14:31:34 dev-workstation:~/sandbox/elasticsearch ⤾
%
```

## 索引合并

为避免 `索引` 和 `分片` 过大影响 ES 故障恢复速度及检索效率，上述各类索引均至少 `按天` 分索引，对于需要做大范围查询的索引，需以一定粒度（如周和月）进行索引聚合。