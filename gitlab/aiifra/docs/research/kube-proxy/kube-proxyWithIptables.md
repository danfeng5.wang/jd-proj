# Kube-proxy with iptables

## iptables chain and table

Refer: [iptables-tutorial](https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html)

iptables 一共可以操作 5 张表: raw, mangle, nat, filter, security

* raw 用于配置数据包, raw 中的数据包不会被系统跟踪
* filter 用于过滤包, 与防火墙相关
* nat 网络地址转换
* mangle 用于对特定数据包的修改
* security 用于强制访问控制网络规则(例如 SELinux)

一般只会用到 filter 和 nat 表

1. table filter: 过滤进入 linux 本机的 package, 预设的 table
   1. Chain INPUT: 进入 linux 的 package
   2. Chain OUTPUT: linux 送出的 package
   3. Chain FORWARD: 转发 package, 但不进入本机, 与 nat 关系比较大
2. table nat: Network Address Translation, 进行来源与目的 ip address and port 转换.
   1. Chain PREROUTING: 进行路由判断之前要进行的规则 (修改目的地址 DNAT)
   2. Chain POSTROUTING: 进行路由判断之后要进行的规则 (修改源地址 SNAT)
   3. Chain OUTPUT: 与发送的 package 有关

   一些名词
   1. snat: 地址转换
   2. dnat: 目标地址转换
   3. pnat: 目标端口转换
   4. MASQUERADE: iptables 提供的功能, 自动根据本机 ip 做 SNAT

`NOTE: DROP和REJECT的区别：DROP是直接不让进入，而REJECT是先让进入然后再拒绝，DROP更安全，所以一般拒绝都用DROP。`

### package 在 iptables 中的流量走向

#### 目的地址是本机 (packet in put)

1. Comes in on the interface (eg: eth0)
2. raw: PREROUTING: 入包首先进过 raw 的 PREROUTING, 且发生在 connection tracking 之前.
3. connection tracking code 开始, 主要内容可以看 [The state machine](https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html#STATEMACHINE)
4. mangle: PREROUTING: 主要用于 mangling(损坏, 修改) packets. 例如修改 TOS 等.
5. `nat: PREROUTING`: 这个 chain 主要用于 DNAT. 避免在此进行过滤, 有可能会绕过某些情况
6. Routing decision. 路由决定
7. mangle: INPUT: 路由之后, mangle INPUT chain 用于 mangle packet.
8. `filter: INPUT`: 此时对所有本机 incomine packet 进行 filter.
9. Local process or application

#### 原地址是本机 (packet out put)

1. from local process/application
2. Routing decision, 决定出口, 同时会收集一些其他信息
3. raw: OUTPUT: 在 connection tracking 之前
4. connection tracking
5. mangle: OUTPUT: 不在此 chain 做 filter
6. `nat: OUTPUT`: 可以用于 NAT(网络地址转换) 来自 firewall 本身的 outgoing packets
7. Routing decision, 再次进行路由决定, 因为 mangle:OUTPUT 和 nat:OUTPUT 可能会改变 packet 的路由
8. `filter: OUTPUT`: filter 本地出去的 packets
9. mangle: POSTROUTING: 在被发出之前做的最后一次 mangle, 此 chain 会同时修改经过防火墙的 packet 和防火墙自身产生的 packet.
10. `nat: POSTROUTING`: 此 chain 做 SNAT, 建议不要在此做 filter. 因为即使 policy 是 drop, 也可能会有漏网之鱼

#### Forwarded packets (转发)

1. raw: PREROUTING: 同 'packet in'
2. 同 packet in step 3
3. mangle: PREROUTING: 同 'packet in' step 4
4. `nat: PREROUTING`: 同 'packet in' step 5
5. Routing decision. 同 'packet in' step 6, 决定是到本地还是转发
6. mangle: FORWARD:
7. `filter: FORWARD`: 做全部 filter . 所有转发的 packet 都会经过该 filter
8. mangle: POSTROUTING:
9. `nat POSTROUTING`: SNAT, 同时也是进行 Masquerading 的地方. 不要在此做 filter.

## kube-proxy with iptables chain

kube-proxy 在每个 node 都维护了 iptables chain, 下面是由 kube-proxy 维护并确保存在的 chain, rule and source

NOTE: -m conntrack 表示跟踪并记录连接状态. --ctstate 用于设置 conntrack match options. NEW 表示 packet 启动了一个新连接 or 在两个方向上都没有看见过该数据包的相关连接

> filter
1. table: filter, chain: 'KUBE-EXTERNAL-SERVICES', sourceChain: INPUT, comment: kubernetes externally-visible service portals, extraArgs: [-m, conntrack, --ctstate, new]
2. table: filter, chain: 'KUBE-SERVICES', sourceChain: OUTPUT, comment: kubernetes service portals, extraArgs: [-m, conntrack, --ctstate, new]
3. table: filter, chain: 'KUBE-FORWARD', sourceChain: FORWARD, comment: kubernetes forwarding portals

> nat
1. table: nat, chain: 'KUBE-SERVICES', sourceChain: OUTPUT, comment: kubernetes service portals
2. table: nat, chain: 'KUBE-SERVICES', sourceChain: PREROUTING, comment: kubernetes service portals
3. table: nat, chain: 'KUBE-POSTROUTING', sourceChain: POSTROUTING, comment: kubernetes postrouting portals

### kube-proxy filter chain

kube-proxy 设置了 filter 表中的 INPUT, OUTPUT 和 FORWARD chain.

KUBE-EXTERNAL-SERVICES chain 放到了 INPUT 之后, 任何新连接都会转到该 chain. (本来 INPUT 和 OUTPUT 都是用 KUBE-SERVICE chain 的, 但是如果 chain 的 rule 太多会导致严重的性能问题, 于是将 KUBE-SERVICES 拆开, 将 KUBE-EXTERNAL-SERVICES 放到 INPUT, 而将 KUBE-SERVICES 放到 OUTPUT, 详见: [kubernetes#56164](https://github.com/kubernetes/kubernetes/pull/56164))

KUBE-FORWARD chain 放到了 FORWARD 之后. KUBE-FORWARD 允许 标记了 0x4000/0x40000 的包进行转发, (但是大部分的机器将 FORWARD chain 的 policy 设为了 DROP)

KUBE-FIREWALL chain 分别 append 到了 INPUT 和 OUTPUT 之后, 该 chain 将标记为 0x8000/0x8000 的 packet 都 drop 掉

KUBE-SERVICES chain 放到了 OUTPUT 之后. 不做任何 policy 仅仅跟踪并记录连接

### kube-proxy nat chain

KUBE-SERVICES chain 放到了 PREROUTING 和 OUTPUT 之后, 负责将流量转发到具体的 KUBE-SVC-* 之后. KUBE-SVC-* 会 append KUBE-SEP-* chain, KUBE-SEP-* 进行 DNAT 以及 KUBE-MARK-MASQ 将包标记为 0x4000/0x4000.(用于 filter 的 KUBE-FORWARD 和 nat.POSTROUTING.KUBE-POSTROUTING 的 MASQUERADE). 如果没有匹配的规则, 会将流量转到 KUBE-NODEPORTS chain.

KUBE-POSTROUTING chain 放到了 POSTROUTING 之后, 标记 packet 0x4000/0x4000 然后转到 MASQUERADE 实现自动化 snat. (自动获取当前 IP 做 NAT)

KUBE-NODEPORTS chain 放到了 PREROUTING 最后, 用于接收 KUBE-SERVICES 都没有匹配的流量, ADDRTYPE match dst-type LOCAL, LOCAL 包含本机所有地址

## kube-proxy 控制的流量走向

### packet in

1. nat.PREROUTING
   1. nat.KUBE-SERVICES
      1. nat.KUBE-MARK-MASQ (注意, 在 clusterIP is not nil or kube-proxy 指定了 masqueradeAll, 每个 Service 才会生成此 chain, 目的是将集群外流向 Service 的流量包装成 kubenertes 内部的流量), 生成该 Rule 的代码如下, 详见: [kubernetes#24224](https://github.com/kubernetes/kubernetes/issues/24224):

            ```Golang
            if proxier.masqueradeAll {
                writeLine(proxier.natRules, append(args, "-j", string(KubeMarkMasqChain))...)
            } else if len(proxier.clusterCIDR) > 0 {
                // This masquerades off-cluster traffic to a service VIP.  The idea
                // is that you can establish a static route for your Service range,
                // routing to any node, and that node will bridge into the Service
                // for you.  Since that might bounce off-node, we masquerade here.
                // If/when we support "Local" policy for VIPs, we should update this.
                writeLine(proxier.natRules, append(args, "! -s", proxier.clusterCIDR, "-j", string(KubeMarkMasqChain))...)
            }
            ```

         1. masqueradeAll: mark all request to 0x4000/0x4000
         2. not masqueradeall but cluster IP is not nil: mark source !clusterCIDR to 0x4000/0x4000
      2. nat.KUBE-SVC-*: kube-proxy 利用 iptables 实现了 lb, 如果 service 有多个 endpoint, 则会根据 endpoint 的数量创建 KUBE-SEP-\*, 每个 KUBE-SEP-\* 的概率在创建的时候指定, -m statistic --mode random --probability xxx (有关 probability 的讨论可看: [Service distribution via iptables](https://groups.google.com/forum/#!topic/kubernetes-users/V2GgsIUrFlA))
         1. nat.KUBE-SEP-*
            1. DNAT
            2. KUBE-MARK-MASQ mark packet to 0x4000/0x4000 if source is the endpoint
   2. nat.KUBE-NODEPORTS
      1. nat.KUBE-SVC-* and after is the same as before
2. filter.INPUT
   1. filter.KUBE-EXTERNAL-SERVICES (same action with filter.OUTPUT.KUBE-SERVICES)
         1. match conntrack with ctstate NEW
         2. 如果有 service 不存在任何 endpoint 且 service type is NodePort or externalIP, 则新增一条 REJECT rule : -reject-with icmp-port-unreachable and -m addrtype with dst-type LOCAL
   2. filter.KUBE-FIREWALL
      1. packet with 0x8000/0x8000 will be DROP.

### packet out

1. nat.OUTPUT
   1. nat.KUBE-SERVICES
      1. nat.KUBE-MARK-MASQ (同 'packet in' nat.KUBE-MARK-MASQ)
      2. nat.KUBE-SVC-\*: kube-proxy 利用 iptables 实现了 lb, 如果 service 有多个 endpoint, 则会根据 endpoint 的数量创建 KUBE-SEP-\*, 每个 KUBE-SEP-\* 的概率在创建时指定, -m statistic --mode random --probability xxx, see code:

            ```Golang
            // Balancing rules in the per-service chain.
            args = append(args[:0], "-A", string(svcChain))
            proxier.appendServiceCommentLocked(args, svcNameString)
            if i < (n - 1) {
                // Each rule is a probabilistic match.
                args = append(args,
                    "-m", "statistic",
                    "--mode", "random",
                    "--probability", proxier.probability(n-i))
            }
            // The final (or only if n == 1) rule is a guaranteed match.
            args = append(args, "-j", string(endpointChain))
            writeLine(proxier.natRules, args...)
            ```

         1. DNAT
         2. KUBE-MARK-MASQ mark packet if source is the the endpoint
2. filter.OUTPUT
   1. filter.KUBE-SERVICES: (a service without endpint will create rule in this chain)
      1. REJECT with icmp-port-unreachable: match conntrack with cstate NEW and packets destination is service ip and dport is service port
   2. filter.KUBE-FIREWALL
      1. packet with 0x8000/0x8000 will be DROP.
3. nat.POSTROUTING
   1. nat.KUBE-POSTROUTING
      1. MASQUERADE: with packet 0x4000/0x4000 (do in nat.OUTPUT)

### packet forward

1. nat.PREROUTING (see 'packet in' setp 1)
   pass
2. filter.KUBE-FORWARD
   1. all packet with 0x4000/0x4000 will ACCEPT
   2. 如果 clusterCIDR is not nil
      1. packet from: clusterCIDR to: anywhere with conntrack with ctstate RELATED,ESTABLISHED will ACCEPT
      2. packet from: anywhere to: clusterCIDR with conntrack with ctstate RELATED,ESTABLISHED will ACCEPT
3. nat.POSTROUTING (see 'packet out' setep 3)
    pass
