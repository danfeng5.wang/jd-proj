---
- name: Install etcd from oss
  yum:
    name: "{{ etcd_rpm_package }}"
    state: present

- name: Copy TLS ca file
  copy:
    src: ../templates/auth/ca.pem
    dest: "{{ trusted_ca_file }}"
    owner: etcd
    group: etcd
    mode: 0600

- name: Copy TLS server key file
  copy:
    src: ../templates/auth/server-key.pem
    dest: "{{ key_file }}"
    owner: etcd
    group: etcd
    mode: 0600

- name: Copy TLS server cert file
  copy:
    src: ../templates/auth/server.pem
    dest: "{{ cert_file }}"
    owner: etcd
    group: etcd
    mode: 0600

- name: Create directory for etcd database
  file:
    path: "{{ data_dir }}"
    state: directory
    mode: 0700
    owner: etcd
    group: etcd

- name: Set etcd cluster name
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_NAME'
    line: 'ETCD_NAME="etcd-{{ inventory_hostname }}"'

- name: Set etcd listen peer url
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_LISTEN_PEER_URLS'
    line: 'ETCD_LISTEN_PEER_URLS="https://{{ inventory_hostname }}:2380"'

- name: Set etcd listen client url
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_LISTEN_CLIENT_URLS'
    line: 'ETCD_LISTEN_CLIENT_URLS="https://{{ inventory_hostname }}:2379,https://127.0.0.1:2379"'

- name: Set etcd quota backend size
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_QUOTA_BACKEND_BYTES'
    line: 'ETCD_QUOTA_BACKEND_BYTES="{{ quota_backend_bytes }}"'

- name: Set etcd max request size
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_MAX_REQUEST_BYTES'
    line: 'ETCD_MAX_REQUEST_BYTES="{{ max_request_bytes }}"'

- name: Set ETCD advertise peer url
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_INITIAL_ADVERTISE_PEER_URLS'
    line: 'ETCD_INITIAL_ADVERTISE_PEER_URLS="https://{{ inventory_hostname }}:2380"'

- name: Set ETCD advertise client url
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_ADVERTISE_CLIENT_URLS'
    line: 'ETCD_ADVERTISE_CLIENT_URLS="https://{{ inventory_hostname }}:2379"'

- name: Set ETCD initial cluster peers
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_INITIAL_CLUSTER'
    line: 'ETCD_INITIAL_CLUSTER="{{ peers }}"'

- name: Set ETCD initial cluster status
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_INITIAL_CLUSTER_STATE'
    line: 'ETCD_INITIAL_CLUSTER_STATE="new"'

- name: Set ETCD cert file directory
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_CERT_FILE'
    line: 'ETCD_CERT_FILE="{{ cert_file }}"'

- name: Set ETCD key file directory
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_KEY_FILE'
    line: 'ETCD_KEY_FILE="{{ key_file }}"'

- name: Set ETCD trusted ca file directory
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_TRUSTED_CA_FILE'
    line: 'ETCD_TRUSTED_CA_FILE="{{ trusted_ca_file }}"'

- name: Enable ETCD client cert auth
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_CLIENT_CERT_AUTH'
    line: 'ETCD_CLIENT_CERT_AUTH="true"'

- name: Enable ETCD peer auto tls
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_PEER_AUTO_TLS'
    line: 'ETCD_PEER_AUTO_TLS="true"'

- name: Enable ETCD compaction retention
  lineinfile:
    path: /etc/etcd/etcd.conf
    state: present
    regexp: '^[#]?ETCD_AUTO_COMPACTION_RETENTION'
    line: 'ETCD_AUTO_COMPACTION_RETENTION="0"'
