#!/bin/bash
# this script is used to configure minio, spark and hive for jupyter user! 
#
# @wangdanfeng5 11/13/2018

# Directory to find config artifacts
CONFIG_DIR="/tmp/config"
# SPARK_CONF_DIR="$"

# backup configuration
#
for f in core-site.xml hive-site.xml spark-defaults.conf; do
  if [[ -e ${CONFIG_DIR}/$f ]]; then
    cp ${CONFIG_DIR}/$f $SPARK_CONF_DIR/$f
  else
    echo "ERROR: Could not find $f in $CONFIG_DIR"
    exit 1
  fi
done

# check some system environment used flowing, or some configuration will be
# blank
#


# hadoop  core-site.xml
#
sed -i -E "s%__FS_S3A_ENDPOINT__%$ENV_HADOOP_S3_URL%g" $SPARK_CONF_DIR/core-site.xml
sed -i -E "s%__FS_S3A_ACCESS_KEY__%$ENV_MINIO_ACCESS_KEY%g" $SPARK_CONF_DIR/core-site.xml
sed -i -E "s%__FS_S3A_SECRET_KEY__%$ENV_MINIO_SECRET_KEY%g" $SPARK_CONF_DIR/core-site.xml


# hive-site.xml
#
sed -i -E "s%__HIVE_CONNECT_URL__%$ENV_HIVE_CONN_URL%g" $SPARK_CONF_DIR/hive-site.xml
sed -i -E "s%__HIVE_CONN_USERNAME__%$ENV_HIVE_CONN_USERNAME%g" $SPARK_CONF_DIR/hive-site.xml
sed -i -E "s%__HIVE_CONN_PASSWD__%$ENV_HIVE_CONN_PASSWD%g" $SPARK_CONF_DIR/hive-site.xml

# spark-defaults.conf
#
sed -i -E "s%__SPARK_CONN_URL__%$ENV_SPARK_CONN_URL%g" $SPARK_CONF_DIR/spark-defaults.conf
sed -i -E "s%__SPARK_DRIVER_CORES__%$ENV_SPARK_DRIVER_CORES%g" $SPARK_CONF_DIR/spark-defaults.conf
sed -i -E "s%__SPARK_DRIVER_MEM__%$ENV_SPARK_DRIVER_MEM%g" $SPARK_CONF_DIR/spark-defaults.conf
sed -i -E "s%__SPARK_EXE_CORES__%$ENV_SPARK_EXE_CORES%g" $SPARK_CONF_DIR/spark-defaults.conf
sed -i -E "s%__SPARK_EXE_MEM__%$ENV_SPARK_EXE_MEM%g" $SPARK_CONF_DIR/spark-defaults.conf

echo spark.driver.host $(hostname -i) >>$SPARK_CONF_DIR/spark-defaults.conf
