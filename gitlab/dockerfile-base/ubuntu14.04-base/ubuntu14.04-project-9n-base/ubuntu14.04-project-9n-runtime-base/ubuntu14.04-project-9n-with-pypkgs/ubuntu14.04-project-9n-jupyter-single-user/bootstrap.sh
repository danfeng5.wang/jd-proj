#!/bin/bash

set -e

ipython profile create pyspark
cp /etc/jupyter/00-pyspark-setup.py ~/.ipython/profile_pyspark/startup/

# do some configuration
#

/bin/bash  /tmp/config/config.sh

exec tini -- $*

