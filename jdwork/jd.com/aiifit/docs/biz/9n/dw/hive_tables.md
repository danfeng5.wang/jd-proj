# Hive Tables

## 表概览

### 用户视角

表描述 | 表名 | 用户 PIN 字段
----- | --- | ------------
人物属性 | app.app_ba_userprofile_prop_nonpolar_view_ext | user_log_acct
浏览行为 | model.user_info_browse_data_aggregation | user_id
购买行为 | ad.dmp_user_info_purchase_data_raw | user_id
关注行为 | ad.dmp_user_info_follow_sku_data_raw | user_id
加购行为 | ad.dmp_user_info_cart_data_raw | user_id
广告点击行为 | ad.ad_base_click | user_pin
广告曝光行为 | ad.ad_base_impression | user_pin
搜索关键词行为 | ad.dmp_user_info_searchkeyword_data_raw | user_id
搜索类目行为 | fdm.fdm_search_log | uid
PLUS会员查询 | fdm.fdm_plus_n_plus_pins_chain | pin
企业级用户查询 | gdm.gdm_m01_userinfo_enterprise_da | user_log_acct

### 商品视角
表描述 | 表名 | SKU 字段
----- | --- | -------
商品表 | app.index_resource_sku_data | sku_id

## 表结构

app.app_ba_userprofile_prop_nonpolar_view_ext

```
user_log_acct           string                  用户登录名
cpp_base_ulevel         string                  用户级别
cpp_base_sex            string                  性别
cpp_base_age            string                  用户年龄
cpp_base_income         string                  用户收入
cpp_base_marriage       string                  用户婚姻状况
cpp_base_education      int                     学历
cpp_base_profession     string                  职业
cpp_base_regprovince    string                  用户注册省份
cpp_base_regcity        string                  用户注册城市
cpp_base_regtime        string                  用户注册时间（年/月）
cpp_seni_haschild       string                  是否有小孩
cpp_seni_childage       string                  小孩年龄
cpp_seni_childsex       string                  孩子性别
cpp_seni_hascar         string                  是否有车
cpp_addr_province       string                  常用收货省份名称
cpp_addr_city           string                  常用收货市名称
cpp_addr_county         string                  常用收货县名称
csf_sale_paytype        string                  最常用支付方式
csf_sale_client         string                  最常用下单途径
csf_sale_lastclient     string                  最后一次下单途径
csf_sale_rebuy          string                  当年首次购情况
csf_sale_rebuy_lasty    string                  上一年购物情况
csf_saletm_first_ord_dt string                  第一单时间（年/月）
csf_saletm_first_ord_tm string                  首单距今时间
csf_saletm_last_ord_tm  string                  距今时间
csf_saletm_last_login_tm        string                  最后一次登录距今时间
csf_saletm_first_ord_tm_365     bigint                  一年内首次订单距今天数
csf_saletm_last_ord_tm_365      bigint                  一年内最后一次订单距今天数
csf_tmsale_1m_pctatm    double                  最近一个月客单价
csf_tmsale_2m_pctatm    double                  最近两个月客单价
csf_tmsale_3m_pctatm    double                  最近三个月客单价
csf_medal_mombaby       string                  母婴勋章等级：V1、V2、V3、V4
csf_medal_beauty        string                  个护化妆勋章等级:V1、V2、V3、V4
csf_medal_wine          string                  酒类勋章
cgp_action_type         string                  用户购物类型(用户购买类型模型)
cgp_action_active_type  int                     用户活跃度模型
cgp_action_grpcate      string                  用户品类分群模型
cgp_cust_purchpower     string                  购买力分段：1-5从高到低
cgp_cycl_lifecycle      string                  生命周期
cvl_rfm_all_group       string                  RFM全品类
cvl_rfm_all_score       double                  RFM全品类得分
cvl_rfm_pop             string                  POP服饰内衣RFM分组
cvl_rfm_pop_s           double                  POP服饰内衣RFM标准化得分
cvl_rfm_pop_super       string                  POP服饰内衣超级RFM分组
cvl_rfm_pop_super_s     double                  POP服饰内衣超级RFM标准化得分
cvl_rfm_pop_enlarged    string                  大服装品类RFM分组
cvl_rfm_pop_enlarged_s  double                  大服装品类RFM标准化得分
cvl_glob_valuegrp       string                  用户价值分组
cvl_glob_values         string                  用户价值标准得分
cvl_glob_loyalty        string                  用户忠诚度
cvl_glob_marginval      double                  毛利价值
cvl_glob_fmargval       double                  未来毛利价值
cvl_glob_platform       double                  平台影响力
cvl_glob_community      double                  社交关系影响力
cfv_sens_promotion      string                  用户促销敏感度(促销敏感度模型)
cfv_sens_comment        string                  用户评价敏感度(关注商品评价模型)
cfv_sens_colorfavo      string                  颜色偏好TOP1
cfv_cate_90dcate1       string                  最近90天的订单量最大一级品类
cfv_cate_90dcate2       string                  最近90天的订单量最大二级品类
cfv_cate_90dcate3       string                  最近90天的订单量最大三级品类
cfv_cate_60dcate1       string                  最近60天的订单量最大一级品类
cfv_cate_60dcate2       string                  最近60天的订单量最大二级品类
cfv_cate_60dcate3       string                  最近60天的订单量最大三级品类
cfv_cate_30dcate1       string                  最近30天的订单量最大一级品类
cfv_cate_30dcate2       string                  最近30天的订单量最大二级品类
cfv_cate_30dcate3       string                  最近30天的订单量最大三级品类
cfv_cate_cate1          string                  订单量最多的一级品类名称(品牌偏好模型)
cfv_cate_ordcate3       string                  品类偏好--订单
cfv_cate_viewcate3      string                  品类偏好--浏览
cfv_brand_favor         string                  品牌偏好
cpt_glob_bigelectric    string                  大家电模型
cpt_glob_cars           string                  潜在汽车用户模型
crk_glob_riskscore      double                  风险用户风险值
dt                      string
```

app.app_ba_userprofile_prop_nonpolar_view_ext

```
user_log_acct           string                  用户登录名
cpp_base_ulevel         string                  用户级别
cpp_base_sex            string                  性别
cpp_base_age            string                  用户年龄
cpp_base_income         string                  用户收入
cpp_base_marriage       string                  用户婚姻状况
cpp_base_education      int                     学历
cpp_base_profession     string                  职业
cpp_base_regprovince    string                  用户注册省份
cpp_base_regcity        string                  用户注册城市
cpp_base_regtime        string                  用户注册时间（年/月）
cpp_seni_haschild       string                  是否有小孩
cpp_seni_childage       string                  小孩年龄
cpp_seni_childsex       string                  孩子性别
cpp_seni_hascar         string                  是否有车
cpp_addr_province       string                  常用收货省份名称
cpp_addr_city           string                  常用收货市名称
cpp_addr_county         string                  常用收货县名称
csf_sale_paytype        string                  最常用支付方式
csf_sale_client         string                  最常用下单途径
csf_sale_lastclient     string                  最后一次下单途径
csf_sale_rebuy          string                  当年首次购情况
csf_sale_rebuy_lasty    string                  上一年购物情况
csf_saletm_first_ord_dt string                  第一单时间（年/月）
csf_saletm_first_ord_tm string                  首单距今时间
csf_saletm_last_ord_tm  string                  距今时间
csf_saletm_last_login_tm        string                  最后一次登录距今时间
csf_saletm_first_ord_tm_365     bigint                  一年内首次订单距今天数
csf_saletm_last_ord_tm_365      bigint                  一年内最后一次订单距今天数
csf_tmsale_1m_pctatm    double                  最近一个月客单价
csf_tmsale_2m_pctatm    double                  最近两个月客单价
csf_tmsale_3m_pctatm    double                  最近三个月客单价
csf_medal_mombaby       string                  母婴勋章等级：V1、V2、V3、V4
csf_medal_beauty        string                  个护化妆勋章等级:V1、V2、V3、V4
csf_medal_wine          string                  酒类勋章
cgp_action_type         string                  用户购物类型(用户购买类型模型)
cgp_action_active_type  int                     用户活跃度模型
cgp_action_grpcate      string                  用户品类分群模型
cgp_cust_purchpower     string                  购买力分段：1-5从高到低
cgp_cycl_lifecycle      string                  生命周期
cvl_rfm_all_group       string                  RFM全品类
cvl_rfm_all_score       double                  RFM全品类得分
cvl_rfm_pop             string                  POP服饰内衣RFM分组
cvl_rfm_pop_s           double                  POP服饰内衣RFM标准化得分
cvl_rfm_pop_super       string                  POP服饰内衣超级RFM分组
cvl_rfm_pop_super_s     double                  POP服饰内衣超级RFM标准化得分
cvl_rfm_pop_enlarged    string                  大服装品类RFM分组
cvl_rfm_pop_enlarged_s  double                  大服装品类RFM标准化得分
cvl_glob_valuegrp       string                  用户价值分组
cvl_glob_values         string                  用户价值标准得分
cvl_glob_loyalty        string                  用户忠诚度
cvl_glob_marginval      double                  毛利价值
cvl_glob_fmargval       double                  未来毛利价值
cvl_glob_platform       double                  平台影响力
cvl_glob_community      double                  社交关系影响力
cfv_sens_promotion      string                  用户促销敏感度(促销敏感度模型)
cfv_sens_comment        string                  用户评价敏感度(关注商品评价模型)
cfv_sens_colorfavo      string                  颜色偏好TOP1
cfv_cate_90dcate1       string                  最近90天的订单量最大一级品类
cfv_cate_90dcate2       string                  最近90天的订单量最大二级品类
cfv_cate_90dcate3       string                  最近90天的订单量最大三级品类
cfv_cate_60dcate1       string                  最近60天的订单量最大一级品类
cfv_cate_60dcate2       string                  最近60天的订单量最大二级品类
cfv_cate_60dcate3       string                  最近60天的订单量最大三级品类
cfv_cate_30dcate1       string                  最近30天的订单量最大一级品类
cfv_cate_30dcate2       string                  最近30天的订单量最大二级品类
cfv_cate_30dcate3       string                  最近30天的订单量最大三级品类
cfv_cate_cate1          string                  订单量最多的一级品类名称(品牌偏好模型)
cfv_cate_ordcate3       string                  品类偏好--订单
cfv_cate_viewcate3      string                  品类偏好--浏览
cfv_brand_favor         string                  品牌偏好
cpt_glob_bigelectric    string                  大家电模型
cpt_glob_cars           string                  潜在汽车用户模型
crk_glob_riskscore      double                  风险用户风险值
dt                      string
```

model.user_info_browse_data_aggregation

```
user_id                 string                  用户标识 pin或deviceid
user_id_type            int                     用户标识类型 1-pin 2-pc user id 4-mobile user id 8-imei 16-openudid 等
commprice               bigint                  商品价格,单位0.01元
sku_id                  bigint                  skuid
cid1                    bigint                  一级类目id
cid2                    bigint                  二级类目id
cid3                    bigint                  三级类目id
brand_id                string                  品牌id
vender_id               string                  店铺id
frequency               bigint                  频次
dt                      string
```

ad.dmp_user_info_purchase_data_raw

```
time                    bigint                  unix时间戳，精确到秒
user_id                 string                  用户标识 pin或deviceid
user_id_type            int                     用户标识类型 1-pin 2-pc user id 4-mobile user id 8-imei 16-openudid 等
device_id               string                  用户设备号
device_type             int                     用户设备类型，原始流量渠道，定义与user_id_type一致
sku_num                 int                     sku购买数量
sku_id                  bigint                  skuid
cid1                    bigint                  一级类目id
cid2                    bigint                  二级类目id
cid3                    bigint                  三级类目id
brand_id                string                  品牌id
vender_id               string                  店铺id
dt                      string
```

ad.dmp_user_info_follow_sku_data_raw

```
time                    bigint                  unix时间戳，精确到秒
user_id                 string                  用户标识 pin或deviceid
user_id_type            int                     用户标识类型 1-pin 2-pc user id 4-mobile user id 8-imei 16-openudid 等
device_id               string                  用户设备号
device_type             int                     用户设备类型，原始流量渠道，定义与user_id_type一致
sku_id                  bigint                  skuid
cid1                    bigint                  一级类目id
cid2                    bigint                  二级类目id
cid3                    bigint                  三级类目id
brand_id                string                  品牌id
vender_id               string                  店铺id
dt                      string
```

ad.dmp_user_info_cart_data_raw

```
time                    bigint                  unix时间戳，精确到秒
user_id                 string                  用户标识 pin或deviceid
user_id_type            int                     用户标识类型 1-pin 2-pc user id 4-mobile user id 8-imei 16-openudid 等
device_id               string                  用户设备号
device_type             int                     用户设备类型，原始流量渠道，定义与user_id_type一致
sku_num                 int                     sku加购物车数量
sku_id                  bigint                  skuid
cid1                    bigint                  一级类目id
cid2                    bigint                  二级类目id
cid3                    bigint                  三级类目id
brand_id                string                  品牌id
vender_id               string                  店铺id
dt                      string
```

ad.ad_base_click

```
sid                     string                  used on pvalue
pos_id                  string                  COMMON字段 广告位ID
material_id             bigint                  素材id
ad_group_id             bigint                  单元id
ad_plan_id              bigint                  COMMON字段 计划id
sku_id                  string                  COMMON字段 商品id
ad_billing_type         int                     计费类型
act_price               double                  打折后计费价格
cpc_price               double                  打折前计费价格
ad_spread_type          int                     站内外类型
advertise_pin           string                  COMMON字段 广告主pin
vendor_id               int                     广告主的vendor_id
provider_code           string                  广告主的供应商简码
user_pin                string                  游客pin
user_ip                 string                  COMMON字段 游客ip
user_id                 string                  游客id
day                     bigint                  请求时间 秒数
click_time              string                  COMMON字段 点击时间
f_value                 double                  广告主账户中的假钱比例
ref_cate_id3            int                     页面三级分类id
click_id                string                  COMMON字段 标示唯一点击的id
adu                     string
ip_area                 string                  游客地区编码
ad_type                 int                     广告类型（商品、图片）
ad_sku_type             int                     1:广告,2:自然结果,3:推荐
ad_traffic_type         int                     广告流量提供方区分0:其他 1：百度贴吧 2：拍拍运营流量 3：京东运营流量 4： 百度sem流量 5：百度ADX流量 6：CPS流量 7：百度阿拉丁流量 8：广点通流量。
log_version             int                     日志版本号
match_type              int                     广告重定向标识
re_uid                  bigint                  中间页unionid, CPC联盟一跳点击unionid
flow_tag                string
uuid                    string                  临时标示用户，一小时有效, x.jd.com的cookie, aduuid
is_bill                 int                     0表示计费，其余表示不计费
is_reach                int                     若到达主站则为1，否则为0
retrieval_type          int                     必填  1展示广告，2搜索广告,  搜索广告必填
keyword                 string                  搜索广告用户检索关键字
hit_key                 string                  搜索广告实际命中关键字
page_uuid               string                  中间页 page_uuid,中间页的同一个pv
cvr_value               double                  ROI model预估的cvr值.  反作弊jzf
cvr_level               int                     cvr值对应的转化level值. 反作弊jzf
roi_value               double                  ROI model预估的roi值. 反作弊jzf
roi_level               int                     roi值对应的roi level值. 反作弊jzf
last_pos_id             int                     上次点击广告位id,默认为0表示没有上一广告位id
last_click_id           string                  上次点击广告位点击id
retest                  string                  中间页实验tag
anti_info               struct<hit_policy:array<string>,refund:int,experiment_group_id:int,gmv:double,anti_time:bigint,refund_time:bigint,more_filter:int,is_spam:int>  反作弊相关信息
trigger_type            int                     触发该广告的类型，是一个位图
mobile_type             int                     0. pc 1. phone
origin_type             int                     ccc.x.jd.com
advertiser_id           bigint                  COMMON字段 广告主id
sku_cid3                int                     sku category 3
sku_brand_id            int                     sku brand id, default 0
utm_source              string                  点击refer 域名（通过http协议中Referer中获取的域设置
utm_medium              string                  根据流量来源标记的tag
utm_campaign            string                  1为联盟号，2为联盟子号(进一步细分)
utm_term                string                  点击id(去掉"-"符号的clickid,同样用来标记一次点击)
jda_time                bigint                  cookie中jda字段保存的时间
click_ip                string                  点击ip
advertiser_type         int                     广告主类型,1:自营 2:pop商家 3:供应商 4:东联 5:拍拍 7:代理
targeting_type          int                     广告主买词投放类型 同索引定义一致 0:default, 1:精确匹配, 2:智能匹配, 4:短语匹配
overlap_exp             string                  分层实验记录，格式ra,ta,pa
bid_price               double                  用户出价（cpc,cpm）
sku_cid1                int                     sku category 1
sku_cid2                int                     sku category 2
device_id               string                  app 搜索广告设备ID
targeting_retr_type     int                     targeting_type的买词对应的匹配类型，如短语购买的匹配类型可以是精确
worldwide               int                     全球购 2表示是，1表示不是， 0表示未知
site_id                 bigint                  COMMON字段 联盟网站id
position_id             string                  COMMON字段 推广位id, 联盟站长管理的推广位, 注意: 区别于播放用字段2广告位id
device_type             int                     对应device_id的设备类型,具体定义参考AdSearchLogInfo
cpc_union               int                     CPC联盟点击分成(分), 分成计算时填写
union_id                int                     CPC联盟点击,联盟id
refer                   string                  浏览器refer
ua                      string                  用户的user_agent
model_type              int                     请求模型类型,取值见请求日志
cached_by_client        int                     客户端是否会缓存广告，用于信息流广告反作弊，1表示客户端缓存，0或者空表示不缓存
dsp_posid               string                  adx的广告位ID
promo_type              int                     店铺活动类型，1:普通 2:高质量
ssp_user_seg_type       int                     是否利用媒体方标签数据， 1表示媒体方提供标签并且利用标签；2表示媒体方提供标签但是没有利用标签；3表示媒体方没有提供用户标签
ad_traffic_group        int                     广告流量类型：1:站内, 2:展示包断, 3:SEM, 4:ADX, 5:CPS联盟, 6:CPC联盟, 7:APP联盟, 8:轻微店联盟, 9:广点通, 10:SEO
x                       double                  广告点击横坐标
y                       double                  广告点击纵坐标
click_source            int                     点击日志来源, 1表示商务舱系统, 2表示JDX系统
behaviour               string                  鼠标行为，用于反作弊
anti_flag               int                     click server为反作弊生成的预判标记
to_url                  string                  落地页url
last_match_type         int                     （中间页）站外一跳是否是重定向 1.重定向 2.非重定向 0.其他
pre_nc                  int                     两次跳转前端反作弊验证标记，1:验证通过 其他:验证不通过
sku_id_str              string                  商品id, string, 临时字段
pos_group_id            int                     广告位组id
marketing_data          struct<program_id:int,ad_tag_id:int>    展示市场系统数据
dmp_id                  bigint                  广告展示所使用的dmp_id
landing_page_type       int                     落地页类型，0：默认，1：京东618引流页面
business_type           int                     0.其他 1.品牌聚效 2. 京东快车 3. 定价CPM 4. 信息流直投
campaign_type           int                     1:普通聚效计划,2:普通快车计划, 3:快车的海投计划, 4:聚效的APP消息推送计划
bid_request_id          string                  Unique ID of the bid request, provided by the exchange.
charge_data             struct<real_price:bigint,fake_price:bigint,total_price:bigint,fake_perf_price:bigint,fake_unperf_price:bigint,budget_price:bigint,aggre_log_price:bigint,aggre_budget_price:bigint,real_
aggre_price:bigint,fake_aggre_price:bigint,aggre_deduct_id:string,charge_time:bigint,win_notice_price:int,commission_price:bigint,commission_aggre_price:bigint,charge_pre_price:bigint,details:array<struct<id:
string,amount:bigint>>> 实际计费信息
cps_clicktime           string                  点击时间
cps_st                  string                  点击来源
cps_tu                  string                  目标url
cps_unionid             bigint                  联盟id
cps_cuid                string                  子联盟
cps_euid                string                  扩展字段
cps_webtype             string                  网络类型
cps_siteid              bigint                  网站id
cps_uuid                string                  点击用户uuid
cps_playid              string                  jd播放时的播放id
cps_caseid              bigint                  js播放的橱窗id
cps_matid               bigint                  js播放的素材id
cps_protype             bigint                  推广类型
cps_ua                  string                  浏览器类型（含spider）
cps_refer               string                  浏览器refer
cps_sku_id              bigint                  商品SKUID
cps_ext_adowner         string                  参照CPS扩展列中定义
cps_ext_nc              string                  参照CPS扩展列中定义
cps_ext_uuid            string                  参照CPS扩展列中定义
cps_ext_csid            string                  参照CPS扩展列中定义
cps_ext_converttype     string                  参照CPS扩展列中定义
cps_ext_unpl            string                  参照CPS扩展列中定义
cps_ext_adtraffictype   string                  参照CPS扩展列中定义
cps_ext_pid             string                  参照CPS扩展列中定义
cps_ext_semrefer        string                  参照CPS扩展列中定义
cps_ext_gettime         string                  参照CPS扩展列中定义
cps_ext_qwdpin          string                  参照CPS扩展列中定义
cps_ext_actid           string                  参照CPS扩展列中定义
cps_ext_checkjda        string                  参照CPS扩展列中定义
cps_ext_utm_term        string                  参照CPS扩展列中定义
cps_ext_adspreadtype    string                  参照CPS扩展列中定义
cps_ext_utm_source      string                  参照CPS扩展列中定义
cps_ext_exp             string                  参照CPS扩展列中定义
cps_ext_dlink           string                  参照CPS扩展列中定义
cps_ext_ext             string                  参照CPS扩展列中定义
gdt_fhour               bigint                  小时
gdt_trace_id            string                  点击跟踪ID
gdt_showtime            bigint                  曝光时间
gdt_bidprice            bigint                  广告出价
gdt_expectcost          bigint                  应扣金额
gdt_realcost            bigint                  实扣金额
gdt_clicktype           bigint                  恶意点击命中类型 0：有效点击
gdt_accounttype         bigint                  帐户类型 1:通用-现金 3:通用-赠送 4:通用-分成 8:内部领用（仅用于京东） 9:红海（红海广告位专账） 10:蓝海（蓝海广告位专账） 11:京东无线补贴现金账 12:京东无线补贴领
用金账 13:拍拍无线补贴帐 22:赞助-现金 23:赞助-赠送 24:赞助-分成
gdt_openid              string                  OPENID
gdt_ad_id               bigint                  广告ID
gdt_adtype              bigint                  广告类型：0=未知/所有 1=搜索广告 2=置顶广告(废弃) 3=空间直投广告 4=腾讯直投广告(废弃) 5=空间直投CPM广告 6=无线广告
gdt_ad_uin              bigint                  广告主UIN
gdt_profile_id          bigint                  用户群ID
gdt_materialtype        bigint                  素材类型=0=所有/未知 1=商品 2=店铺 3: 自定义链接
gdt_customurl           string                  自定义链接
gdt_adspec_id           bigint                  广告规格ID
gdt_gdtaid              bigint                  广点通广告ID
gdt_site_name           string                  站点集合名称
gdt_adpos_name          string                  广告位名称
gdt_autoflag            int                     自动化投放标识
gdt_platform            int                     2-6是京东直投 99是运营投放工具
material_id_str         string                  素材id, string类型
q_value                 double                  小黑珑专用，根据媒体二价计算cpc二价
cps_ext_platform        string                  平台类型
user_target             int
gdt_mediatype           int
search_promote_rank     int
device_fp               string                  设备指纹
pos_group_type          int                     广告位所属分组属性
app_info                string
p13n_material_id        bigint
dmp_crowd_ids           array<bigint>
activity_id             bigint
search_click_info       struct<browser_uniq_id:string>
automated_bidding_type  bigint
tcpa_phase              int
tech_fee                double
data_fee                double
deal_id                 bigint
event_type              int
index                   int
real_mobile_type        bigint
data_billing_type       bigint
drawing_type            bigint
adv_id                  string                  广告层级主键，目前无线通使用
new_arrival_type        int                     海投新品推广-新品sku标识，0：无效值，1：非新品，2：新品sku
mobile_device_info      struct<make:string,model:string,osversion:string,connectiontype:int,ppi:int,flashver:string,language:string,macidmd5:string,wifi:string,radius:double,screenwidth:int,screenheight:int,l
at:double,lon:double>
merchant_id             bigint
media_verification_code string                  点击校验码 头条使用
act_price_ratio         double                  小黑龙，每个sku出价在广告位总出价中所占的比率
activity_type           int                     联合广告计划的活动类型
position_offset         int
project_id              string
floor_id                int
target_bid_price        double                  智能出价时，广告主设置的目标（例如TCPC）出价,具体出价类型automated_bidding_type
cps_ext_pro_type        int                     映射后推广方式（自定义，非自定义，js）
cps_ext_pro_cont        int                     映射后推广内容
delivery_system_type    int                     流量货币化标识字段
pt                      string
dt                      string
hour                    string
```

ad.ad_base_impression

```
sid                     string                  请求ID
pos_id                  int                     广告位id
material_id             bigint                  素材id
ad_group_id             bigint                  单元id
ad_plan_id              bigint                  计划id
sku_id                  bigint                  商品id
ad_billing_type         int                     计费类型
act_price               double                  打折后计费价格
cpc_price               double                  打折前计费价格
ad_spread_type          int                     站内外类型
advertise_pin           string                  广告主pin
vendor_id               int                     广告主的vendor_id
provider_code           string                  广告主的供应商简码
user_pin                string                  游客pin
user_ip                 string                  游客ip
user_id                 string                  游客id
day                     bigint                  请求时间 毫秒数
impress_time            bigint                  展示时间 秒数
f_value                 double                  广告主账户中的假钱比例
ref_cate_id3            int                     当前页面的3级分类
adu                     string
ip_area                 string                  游客地区编码
ad_type                 int                     广告类型（商品、图片）
ad_sku_type             int                     1:广告,2:自然结果,3:推荐
ad_traffic_type         int                     广告流量提供方区分0:其他 1：百度贴吧 2：拍拍运营流量 3：京东运营流量 4： 百度sem流量 5：百度ADX流量 6：CPS流量 7：百度阿拉丁流量 8：广点通流量。
log_version             int                     日志版本号
match_type              int                     广告重定向标识
re_uid                  bigint                  中间页 unionid
flow_tag                string
uuid                    string                  临时标示用户，一小时有效, x.jd.com的cookie
is_bill                 int                     0表示计费，其余表示不计费
retrieval_type          int                     1展示广告，2搜索广告
keyword                 string                  搜索广告用户检索关键字
hit_key                 string                  搜索广告实际命中关键字
page_uuid               string                  中间页page_uuid,中间页的同一个pv
last_pos_id             int                     上次点击广告位id,默认为0表示没有上一广告位id
last_click_id           string                  上次点击广告位点击id
retest                  string                  中间页实验tag
cookie_pin              string
trigger_type            int                     触发该广告的类型，是一个位图
mobile_type             int                     0. pc 1. phone
advertiser_id           bigint                  广告主id
advertiser_type         int                     广告主类型,1:自营 2:pop商家 3:供应商 4:东联 5:拍拍 7:代理
targeting_type          int
overlap_exp             string                  分层实验记录
bid_price               double                  用户出价（cpc,cpm）
sku_cid1                int                     sku category 1
sku_cid2                int                     sku category 2
sku_cid3                int                     sku category 3
anti_info               struct<hit_policy:array<string>,refund:int,experiment_group_id:int,gmv:double,anti_time:bigint,refund_time:bigint,more_filter:int,is_spam:int>  反作弊相关信息
user_agent              string                  user agent
referer                 string                  referer
jda_time                bigint                  jda生成时间
targeting_retr_type     int
worldwide               int                     全球购 2表示是，1表示不是， 0表示未知
device_id               string                  移动设备对应device_id
device_type             int                     对应device_id的设备类型
site_id                 int                     联盟网站id
position_id             int                     推广位id, 联盟站长管理的推广位, 注意: 区别于播放用字段2广告位id
union_id                int                     CPC联盟点击,联盟id
impression_ip           string                  曝光ip
referer_domain          string                  refer域名
model_type              int                     请求模型类型,取值见请求日志
cached_by_client        int                     客户端是否会缓存广告，用于信息流广告反作弊，1表示客户端缓存，0或者空表示不缓存
dsp_posid               string                  adx的广告位ID
promo_type              int                     店铺活动类型，1:普通 2:高质量
ssp_user_seg_type       int                     是否利用媒体方标签数据， 1表示媒体方提供标签并且利用标签；2表示媒体方提供标签但是没有利用标签；3表示媒体方没有提供用户标签
ad_traffic_group        int                     广告流量类型：1:站内, 2:展示包断, 3:SEM, 4:ADX, 5:CPS联盟, 6:CPC联盟, 7:APP联盟, 8:轻微店联盟, 9:广点通, 10:SEO
source                  int                     曝光对象的来源，0:来自推荐，1:来自广告(mixer server使用)
last_match_type         int                     （中间页）站外一跳是否是重定向 1.重定向 2.非重定向 0.其他
pos_group_id            int                     广告位组id
marketing_data          struct<program_id:int,ad_tag_id:int>    展示市场系统数据
dmp_id                  bigint                  广告展示所使用的dmp_id
imp_id                  string                  标示唯一曝光的id
landing_page_type       int                     落地页类型 0：默认，1：京东618引流页面
business_type           int                     0.其他 1.品牌聚效 2. 京东快车 3. 定价CPM 4. 信息流直投
sku_brand_id            int                     sku brand id, default 0
campaign_type           int                     1:普通聚效计划,2:普通快车计划, 3:快车的海投计划, 4:聚效的APP消息推送计划
charge_data             struct<real_price:bigint,fake_price:bigint,total_price:bigint,fake_perf_price:bigint,fake_unperf_price:bigint,budget_price:bigint,aggre_log_price:bigint,aggre_budget_price:bigint,real_
aggre_price:bigint,fake_aggre_price:bigint,aggre_deduct_id:string,charge_time:bigint,win_notice_price:int,commission_price:bigint,commission_aggre_price:bigint,charge_pre_price:bigint,details:array<struct<id:
string,amount:bigint>>> 实际计费信息
gdt_fhour               bigint                  小时
gdt_site_id             bigint                  广告曝光时站点ID
gdt_adpos_id            string                  广告位ID
gdt_ad_id               string                  广告ID
gdt_channel_id          bigint                  外部渠道
gdt_channel_desc        string                  外部广告位
gdt_abtest              bigint                  ABTest
gdt_put_position_id     string                  外部推广位ID
gdt_user_id             string                  外部推广者ID
gdt_ochannel_id         string                  渠道ID
gdt_creative_id         string                  创意ID
gdt_tip_id              string                  推广曝光广告ID
gdt_etgs_flag           bigint                  etg标
gdt_data_source         bigint                  数据源
gdt_keyword             string                  关键词
gdt_search_kw           string                  搜索关键词
gdt_adtype              bigint                  广告类型：0=未知/所有 1=搜索广告 2=置顶广告(废弃) 3=空间直投广告 4=腾讯直投广告(废弃) 5=空间直投CPM广告 6=无线广告
gdt_ad_uin              bigint                  广告主UIN
gdt_scomm_id            string                  广告对应的商品ID十六进制
gdt_ncomm_id            bigint                  广告对应的商品ID十进制
gdt_recmdtitle1         string                  商品推广标题1
gdt_recmdtitle2         string                  商品推广标题2
gdt_plan_id             string                  推广计划ID
gdt_profile_id          bigint                  用户群ID
gdt_materialtype        bigint                  素材类型=0=所有/未知 1=商品 2=店铺 3: 自定义链接
gdt_customurl           string                  自定义链接
gdt_platform            bigint                  平台类型 0=拍拍 1=网购 2=京东Pop 3=京东供应商
gdt_adspec_id           bigint                  广告规格ID
gdt_exp_data_src        bigint                  数据来源
gdt_hittype             bigint                  广告命中类型
gdt_ads_hit_type        string                  广告命中类型 关键词/类目
gdt_opr_flag            bigint                  运营标 0：投放工具 9：广告主
gdt_expose_nums         bigint                  曝光量
gdt_expose_bid          bigint                  曝光出价
gdt_site_name           string                  站点集合名称
gdt_adpos_name          string                  广告位名称
material_id_str         string                  素材ID,string类型
play_duration           int                     视频广告播放时长百分比值
user_target             int
search_promote_rank     int
device_fp               string                  设备指纹
media_qvalue            double                  媒体预估的ctr
pos_group_type          int                     广告位所属分组属性
p13n_material_id        bigint
dmp_crowd_ids           array<bigint>
activity_id             bigint
search_pv_info          struct<browser_uniq_id:string,sku_ids:array<string>,page_num:string>
automated_bidding_type  bigint
tcpa_phase              int
tech_fee                double
data_fee                double
deal_id                 bigint
index                   int
title_id                string
win_price               bigint
real_mobile_type        bigint
data_billing_type       bigint
drawing_type            bigint
adv_id                  string                  广告层级主键，目前无线通使用
new_arrival_type        int                     海投新品推广-新品sku标识，0：无效值，1：非新品，2：新品sku
mobile_device_info      struct<make:string,model:string,osversion:string,connectiontype:int,ppi:int,flashver:string,language:string,macidmd5:string,wifi:string,radius:double,screenwidth:int,screenheight:int,l
at:double,lon:double>
merchant_id             bigint
act_price_ratio         double                  小黑龙，每个sku出价在广告位总出价中所占的比率
activity_type           int                     联合广告计划的活动类型
position_offset         int
project_id              string
floor_id                int
target_bid_price        double                  智能出价时，广告主设置的目标（例如TCPC）出价,具体出价类型automated_bidding_type
ad_form                 int                     广告形态, 用于明确标识不同的广告类型
delivery_system_type    int                     流量货币化标识字段
pt                      string
dt                      string
hour                    string
```

ad.dmp_user_info_searchkeyword_data_raw

```
time                    bigint                  unix时间戳，精确到秒
user_id                 string                  用户标识 pin或deviceid
user_id_type            int                     用户标识类型 1-pin 2-pc user id 4-mobile user id 8-imei 16-openudid 等
device_id               string                  用户设备号
device_type             int                     用户设备类型，原始流量渠道，定义与user_id_type一致
kwd                     string                  搜索词
dt                      string
```

fdm.fdm_search_log

```
rectype                 int
uuid                    string
uid                     string
wid                     bigint
cid                     int
timestamps              string
key_word                string
i_sort                  int
page                    int
refer                   string
url                     string
pos                     int
type                    int
ev                      int
word                    string
abtesttype              int
ja_uuid                 string
session_id              string
seq_id                  int
back_time               int
front_time              int
ip                      string
validate                string
validate_ip             string
validate_session_id     string
validate_time_stamp     string
validate_random         bigint
related_cid2            string                  高相关二级分类
related_cid3            string                  高相关三级分类
logid                   string                  日志编号
expand_field            map<string,string>      扩展字段，用来标识当前日志的扩展信息
mtest                   string                  记录测试信息
dt                      string
server                  string
```

fdm.fdm_plus_n_plus_pins_chain

```
start_date              string
change_code             string
source_ip               string                  source_ip
source_db               string                  source_db
source_tb               string                  source_tb
id                      bigint                  ID
pin                     string                  用户pin
stage                   int                     阶段:0不属于任何阶段,2000体验期,3000试用期,4000正式期
stage_status            int                     付费会员阶段状态:0不属于任何阶段状态，1000黑名单,2000-3000体验期区间（2010体验期-未体验,2020体验期-体验中,2030体验期-已过期）3000-4000试用期区间,4000-5000正式期
区间
close_status            int                     0没有任何关闭状态:1未关闭,2已关闭
level                   int                     用户级别
tried                   int                     是否试用过
pinfile_id              bigint                  pinfile_id（外键）
open_date               string                  试用期或正式期第一次开通时间
begin_date              string                  当前阶段有效期开始时间
v_end_date              string                  当前阶段有效期结束时间
un_paid_order           int                     是否有未付款的订单(0：没有，1：有)
creater                 string                  创建人
created                 string                  创建时间
modifier                string                  修改人
modified                string                  修改时间
context                 string                  扩展字段json格式
dp                      string
dt                      string
end_date                string
```

gdm.gdm_m01_userinfo_enterprise_da

```
user_id                 bigint                  用户ID
user_log_acct           string                  用户账号
dj_state                int                     账号冻结状态
status                  int                     账号有效性
created                 string                  账号创建时间
modified                string                  账号修改时间
create_src              int                     账号创建来源
user_lv_cd              string                  账号级别
user_reg_tm             string                  账号注册时间
reg_ip                  string                  账号注册IP
user_nickname           string                  账号昵称
upgrade_time            string                  上次升级时间
parent_user_id          bigint                  父用户ID
corp_name_reg           string                  企业名称（注册填写）
corp_name               string                  企业名称（系统存储）
user_type               int                     企业类型
marketing_state         int                     营销状态
credit                  int                     信用级别
weight                  int                     内部重要度
industry                int                     企业行业
emplyee_number          int                     企业人数
enterprize_cate         int                     企业性质
user_hierarchy          int                     用户层级
buyer_type              string                  购买用途
province_id             int                     所属省
province_name           string                  省份名称
city_id                 int                     所属市
city_name               string                  城市名称
county_id               int                     所属县镇
county_name             string                  所属县镇名称
zip                     string                  邮编
recommended             string                  推荐人
valid_flag              int                     企业信息有效性
allocate_saler          int                     是否分配销售
saler_erp               string                  销售人员ERP
region_id               int                     所属区域ID
business_line           int                     所属业务线
contacted               int                     是否联系过
ent_created             string                  企业创建时间
ent_modified            string                  企业修改时间
distribute_time         string                  企业分配时间
acct_id                 bigint                  账号id
geo_region_id           bigint                  物理区域
register_tm             string                  账号录入时间
last_order_tm           string                  账号上次成单时间
org_code                string                  账号机构查询码
business_line_name      string                  所属业务线名称
region_name             string                  所属区域名称
business_flag           string                  业务标示
project_id              bigint                  项目ID
saler_name              string                  销售员姓名
dt                      string
```

gdm.gdm_m01_userinfo_enterprise_da

```
user_id                 bigint                  用户ID                                                                                                                                                   [8/860]
user_log_acct           string                  用户账号
dj_state                int                     账号冻结状态
status                  int                     账号有效性
created                 string                  账号创建时间
modified                string                  账号修改时间
create_src              int                     账号创建来源
user_lv_cd              string                  账号级别
user_reg_tm             string                  账号注册时间
reg_ip                  string                  账号注册IP
user_nickname           string                  账号昵称
upgrade_time            string                  上次升级时间
parent_user_id          bigint                  父用户ID
corp_name_reg           string                  企业名称（注册填写）
corp_name               string                  企业名称（系统存储）
user_type               int                     企业类型
marketing_state         int                     营销状态
credit                  int                     信用级别
weight                  int                     内部重要度
industry                int                     企业行业
emplyee_number          int                     企业人数
enterprize_cate         int                     企业性质
user_hierarchy          int                     用户层级
buyer_type              string                  购买用途
province_id             int                     所属省
province_name           string                  省份名称
city_id                 int                     所属市
city_name               string                  城市名称
county_id               int                     所属县镇
county_name             string                  所属县镇名称
zip                     string                  邮编
recommended             string                  推荐人
valid_flag              int                     企业信息有效性
allocate_saler          int                     是否分配销售
saler_erp               string                  销售人员ERP
region_id               int                     所属区域ID
business_line           int                     所属业务线
contacted               int                     是否联系过
ent_created             string                  企业创建时间
ent_modified            string                  企业修改时间
distribute_time         string                  企业分配时间
acct_id                 bigint                  账号id
geo_region_id           bigint                  物理区域
register_tm             string                  账号录入时间
last_order_tm           string                  账号上次成单时间
org_code                string                  账号机构查询码
business_line_name      string                  所属业务线名称
region_name             string                  所属区域名称
business_flag           string                  业务标示
project_id              bigint                  项目ID
saler_name              string                  销售员姓名
dt                      string
```

app.index_resource_sku_data

```
sku_id                  bigint                  商品ID
cid                     bigint                  商品三级分类ID
vid                     bigint                  商家ID
brand_id                bigint                  品牌ID
state                   int                     商品上下架状态
comments                bigint                  评论总数
good_comments           bigint                  好评数
sku_name                string                  商品名
image_url               string                  商品图片地址
send_type               bigint                  是否为京东配送；值：0-不是、1-是
commprice               bigint                  商品价格,单位0.01元
provider_code           string                  供应商编码
update_time             bigint                  本商品最后更新时间
pid                     bigint                  主商品id
shop_id                 bigint                  pop商家店铺编号
color                   string                  商品颜色
size                    string                  商品尺码
worldwide               bigint                  是否全球购；值：0-未知 1-否 2-是
cid2                    bigint                  商品二级分类ID
sku_readme              string                  商品的广告语（指的是商品详情页下方的说明）
cid1                    bigint                  商品一级分类ID
wechat_price            bigint                  微信价格
pop_pid                 bigint                  POP商品编号
model                   string                  型号
mobile_price            bigint                  手机端价格
ad_content              string                  材质、类型等说明字段
hot_key                 string                  热门搜索词
vender_name             string                  店铺名
state_bitmap            bigint                  状态位图
data_type               bigint                  商品数据类型；值：1-自营、2-POP、999-其他（应用、电子书、团购等）
stock_bitmap            bigint                  商品库存信息
shop_name               string                  店铺名称
brand_name              string                  品牌名称
product_name            string                  商品词
is_oversea_purchase     int                     海外直采；(0/NULL；否；1：全球购POP店自营商品；2：全球购POP商品；3：标识全球购自营商品；4：全球购跨境代购；)
pre_sell_type           bigint                  是否预售商品 (1：预约商品 2：预定金商品 0/NULL：否，不是预约、预售类型)
image_tag_id            bigint                  图片标签id
primary_name            string                  主商品名称
first_department_id     bigint                  一级部门id
sku_image_tag_list_json string                  商品的图片标签
sku_tag_list_json       string                  商品筛选属性,例如尺寸,颜色等
dt                      string
```

## 访问方式

通过 ads_model 堡垒机上的 Hive 可访问