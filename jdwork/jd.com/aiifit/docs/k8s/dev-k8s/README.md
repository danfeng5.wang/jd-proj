# dev-k8s (开发实验集群，廊坊润泽机房)

## 集群信息

- **Kubernetes Version :** 1.11.0
- **Cluster Name :** dev.k8s.ovs.jd.local
- **API Server :** https://172.18.161.49:6443

### 基本配置

COUNT | CPU | GPU | MEM | DISK
----- | --- | --- | ---- | ----
4 | E5-2640 * 40 | P40 * 4 | 256G | 4T SSD * 2 (RAID 1) + 4T SSD * 2 (RAID 0)

### IP 列表
IP |
--
172.18.161.49 |
172.18.161.53 |
172.18.161.55 |
172.18.161.57 |

### 角色分配

- **k8s-master :** 172.18.161.49
- **heketi-server :** 172.18.161.49
- **etcd-cluster-status :** 172.18.161.49
- **etcd-event :** 172.18.161.53
- **Ingress :** 172.18.161.49

## 集群管理

### 节点账号

用户名 | 密码
----- | ----
root | DtkdVZn83IG3CglEVbfPKBpeFNpil
admin | 0okm(IJN