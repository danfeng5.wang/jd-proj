# etcd data backup solution [draft version]

[官方建议](https://coreos.com/etcd/docs/latest/op-guide/performance.html)：etcd 集群都是至少 3 台机器，官方也说明了集群容错为 (N-1)/2，所以备份数据一般都是用不到，但是以防万一。

## Overview

v2 与 v3 API 使用了不同的存储引擎，所以客户端命令也完全不同，我们首先观察版本号：

`[wangdanfeng5@A02-R12-I161-49-7292425 ~]$ etcdctl --version`

`etcdctl version: 3.2.18`

`API version: 2`


## [etcd V2和V3版本之间的数据备份恢复问题](https://github.com/coreos/etcd/issues/7002)

##对于 API 2 备份与恢复方法

[官方 v2 doc guide](https://github.com/coreos/etcd/blob/master/Documentation/v2/admin_guide.md#disaster-recovery) 参照

### 实验集群操作

`$ etcdctl backup --data-dir /export/etcd-data  --backup-dir /export/etcd_backup`

`$ etcd -data-dir=/export/etcd_backup  -force-new-cluster`

etcd的数据默认会存放在我们的命令工作目录中，我们发现数据所在的目录，会被分为两个文件夹中：
snap: 存放快照数据,etcd防止WAL文件过多而设置的快照，存储etcd数据状态。

wal: 存放预写式日志,最大的作用是记录了整个数据变化的全部历程。在etcd中，所有数据的修改在提交前，都要先写入到WAL中。

## API 3 备份与恢复方法

[官方 v3 doc guide](https://github.com/coreos/etcd/blob/master/Documentation/op-guide/recovery.md) 指令参照

$ ETCDCTL_API=3 etcdctl --endpoints $ENDPOINT snapshot save snapshot.db

$ ETCDCTL_API=3 etcdctl snapshot restore snapshot.db \
  --name m1 \
  --initial-cluster m1=http://host1:2380,m2=http://host2:2380,m3=http://host3:2380 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-advertise-peer-urls http://host1:2380

$ ETCDCTL_API=3 etcdctl snapshot restore snapshot.db \
  --name m2 \
  --initial-cluster m1=http://host1:2380,m2=http://host2:2380,m3=http://host3:2380 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-advertise-peer-urls http://host2:2380

$ ETCDCTL_API=3 etcdctl snapshot restore snapshot.db \
  --name m3 \
  --initial-cluster m1=http://host1:2380,m2=http://host2:2380,m3=http://host3:2380 \
  --initial-cluster-token etcd-cluster-1 \
  --initial-advertise-peer-urls http://host3:2380

Next, start etcd with the new data directories:

$ etcd \
  --name m1 \
  --listen-client-urls http://host1:2379 \
  --advertise-client-urls http://host1:2379 \
  --listen-peer-urls http://host1:2380 &

$ etcd \
  --name m2 \
  --listen-client-urls http://host2:2379 \
  --advertise-client-urls http://host2:2379 \
  --listen-peer-urls http://host2:2380 &

$ etcd \
  --name m3 \
  --listen-client-urls http://host3:2379 \
  --advertise-client-urls http://host3:2379 \
  --listen-peer-urls http://host3:2380 &

### Restoring a cluster from membership mis-reconfiguration with wrong URLs




###实验集群操作

在命令行设置：

`# export ETCDCTL_API=3`
备份数据：

`# etcdctl --endpoints localhost:2379 snapshot save snapshot.db`
恢复：

`# etcdctl snapshot restore snapshot.db --name m3 --data-dir=/home/etcd_data`


恢复后的文件需要修改权限为 etcd:etcd
--name:重新指定一个数据目录，可以不指定，默认为 default.etcd
--data-dir：指定数据目录
建议使用时不指定 name 但指定 data-dir，并将 data-dir 对应于 etcd 服务中配置的 data-dir

