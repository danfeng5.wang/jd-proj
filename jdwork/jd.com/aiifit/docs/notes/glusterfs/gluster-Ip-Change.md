# gluster cluster IP Change solution
## Overview

- 应用场景
- 解决方案
- 试验展示
- 约束

## 应用场景

如果现网环境gluster集群环境直接使用ip地址，考虑到机房的迁移的情况，我们应该考虑两方面： 一个是数据的完整性问题，需要对原有数据进行备份以防万一；二是对ip地址改变后对gluster各个节点的配置修改


## 解决方案
我们在尽量不做volume操作，在改变gluster配置的情况下，找回数据
一般来说：/var/lib/glusterd下保存着gluster的配置信息，包括volume和bricks相关的。
- step 1
在gluster各个节点执行：

```bash
systemctl stop glusterd
```

- step 2
```bash
 grep -rl glusterfs-1 /var/lib/glusterd | xargs sed -i 's/glusterfs-1/glusterfs-1-bk/g'
 grep -rl glusterfs-2 /var/lib/glusterd | xargs sed -i 's/glusterfs-2/glusterfs-2-bk/g'
 grep -rl glusterfs-3 /var/lib/glusterd | xargs sed -i 's/glusterfs-3/glusterfs-3-bk/g'
```
以上述几行command为例说明：我们的domain name（或者ip地址）更换成了glusterfs-3-bk，我们需要修改配置目录下的所有文件

- step 3

/var/lib/glusterd/vols/ 在此目录下是volume和bricks相关的信息
执行以下操作：

```bash
rename glusterfs-1 glusterfs-1-bk *
rename glusterfs-1 glusterfs-1-bk bricks/*
rename glusterfs-2 glusterfs-2-bk *
rename glusterfs-2 glusterfs-2-bk bricks/*
rename glusterfs-3 glusterfs-3-bk *
rename glusterfs-3 glusterfs-3-bk bricks/*
```
将目录下的文件名进行批量修改

- step 4
重新启动所有节点的glusterd服务

```bash
systemctl start glusterd
```
## 试验展示

1 挂载volume

mount -t glusterfs localhost:/test-volume test

```bash
[root@glusterfs-1 test]# ll
total 1
-rw-r--r--. 1 root root 27 Aug 22 08:32 1.txt
-rw-r--r--. 1 root root  0 Aug 22 08:31 2.tx2
-rw-r--r--. 1 root root  0 Aug 22 08:31 3.txt
```
我们看到该volume下有3个文件

2 修改domain name

修改各个节点的hosts文件
```bash
[root@glusterfs-1 test]# cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

192.168.1.9    glusterfs-1-bk
192.168.1.10   glusterfs-2-bk
192.168.1.13   glusterfs-3-bk
192.168.1.4    glusterfs-4
192.168.1.5    glusterfs-5
192.168.1.7    glusterfs-6
```
3 挂载volume发现失败

未截图。。。

4 依据[解决方案](##解决方案)进行数据恢复

5 重新挂载volume

```bash
[root@glusterfs-1 test]# ll
total 1
-rw-r--r--. 1 root root 27 Aug 22 08:32 1.txt
-rw-r--r--. 1 root root  0 Aug 22 08:31 2.tx2
-rw-r--r--. 1 root root  0 Aug 22 08:31 3.txt
```


## 约束
我们实际环境是kubernetes通过heketi的api来动态创建volume的。
这个方案解决了gluster端的数据恢复问题。但是heketi端的配置是否需要改动需要进一步调研。

因此，gluster集群上的volume恢复后，kubernetes也不一定可用。需要进一步了解heketi及heketi如何跟k8s对接。




