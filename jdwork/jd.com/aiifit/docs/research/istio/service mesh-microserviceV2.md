# micro service & service mesh

# Istio 分享

## overview
* [Istio是什么 ?](#Istio是什么-?)
* [微服务演化到Istio经历了哪些历程]([#微服务演化到Istio经历了哪些历程)
* [Istio的基本组件及功能](#Istio的基本组件及功能)
* [我们选择Istio的好处及优势](#我们选择Istio的好处及优势)
* [开发团队](#开发团队)

## Istio是什么 ？

Istio是Google/IBM/Lyft联合开发的开源项目，2017年5月发布第一个release 0.1.0， 官方定义为: Istio：一个连接，管理和保护微服务的开放平台。

Istio是service mesh一种解决方案，在解释什么是Istio之前我们先了解什么是service mesh， 了解了service mesh之后，Istio就有了一个基本的了解：
- 什么是service mesh ？

### 1. service mesh是一个十分新的概念和词汇。

这个词最早使用由开发Linkerd的Buoyant公司提出，并在内部使用。2016年9月29日第一次公开使用这个术语。2017年的时候随着Linkerd的传入，Service Mesh进入国内技术社区的视野。最早翻译为“服务啮合层”，这个词比较拗口。用了几个月之后改成了服务网格。



### 2. 一个简单的模型

![service-mesh](images/p2-service-mesh.png)
描述：Service Mesh的部署模型，先看单个的，对于一个简单请求，作为请求发起者的客户端应用实例，会首先用简单方式将请求发送到本地的Service Mesh实例。这是两个独立进程，他们之间是远程调用。
总之，service Mesh会完成完整的服务间调用流程，如服务发现负载均衡，最后将请求发送给目标服务

### 3. 简单模型的拓展

![service-mesh](images/p3-service-mesh.png)
描述：多个服务调用的情况，在这个图上我们可以看到Service Mesh在所有的服务的下面，这一层被称之为服务间通讯专用基础设施层。Service Mesh会接管整个网络，把所有的请求在服务之间做转发。在这种情况下，我们会看到上面的服务不再负责传递请求的具体逻辑，只负责完成业务处理。服务间通讯的环节就从应用里面剥离出来，呈现出一个抽象层

### 4. 再进一步拓展成网络

![service-mesh](images/p4-service-mesh.png)

### 5. 总结什么是service mesh

![service-mesh](images/p5-service-mesh.png)

## 微服务演化到Istio经历了哪些历程？

[微服务历史PPT](istio演化历史.pptx)



## Istio的基本组件及功能
![service-mesh](images/p13-service-mesh.png)

Istio 首先是一个服务网络，但是Istio又不仅仅是服务网格: 在 Linkerd， Envoy 这样的典型服务网格之上，Istio提供了一个完整的解决方案，为整个服务网格提供行为洞察和操作控制，以满足微服务应用程序的多样化需求。
### 1. Istio服务网格逻辑上分为数据面板和控制面板
- 数据面板由一组智能代理（Envoy）组成，代理部署为边车，调解和控制微服务之间所有的网络通信。

- 控制面板负责管理和配置代理来路由流量，以及在运行时执行策略。
### 2. Istio在服务网络中统一提供了许多关键功能(以下内容来自官方文档)：

- 流量管理：控制服务之间的流量和API调用的流向，使得调用更可靠，并使网络在恶劣情况下更加健壮。

- 可观察性：了解服务之间的依赖关系，以及它们之间流量的本质和流向，从而提供快速识别问题的能力。

- 策略执行：将组织策略应用于服务之间的互动，确保访问策略得以执行，资源在消费者之间良好分配。策略的更改是通过配置网格而不是修改应用程序代码。

- 服务身份和安全：为网格中的服务提供可验证身份，并提供保护服务流量的能力，使其可以在不同可信度的网络上流转。
## 我们选择Istio的好处及优势

- 实际应用部署一起，但对应用透明。对应用服务无侵入
Istio 超越 spring cloud和dubbo 等传统开发框架之处， 就在于不仅仅带来了远超这些框架所能提供的功能， 而且也不需要应用程序为此做大量的改动， 开发人员也不必为上面的功能实现进行大量的知识储备。
- Istio 大幅降低微服务架构下应用程序的开发难度，势必极大的推动微服务的普及。
它可以解放业务开发人员，业务更加专注。Istio将大量的运维策略控制及监控等非业务功能代码抽象出来
- 还有很多。。。


## 开发团队

[istio work group](https://github.com/istio/community/blob/master/WORKING-GROUPS.md)




