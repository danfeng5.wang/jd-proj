package main

import (
	"bufio"
	"context"
	"fmt"
	"github.com/olivere/elastic"
	"os"
	"strings"
)

type CidSearchHistory struct {
	CidType  string   `json:"cid_type"`
	PINs     []string `json:"pins"`
	pins_map map[string]struct{}
}

func main() {
	file, err := os.Open("/tmp/searchcid.txt")
	scanner := bufio.NewScanner(file)
	items := make(map[string]*CidSearchHistory)
	for scanner.Scan() {
		line := scanner.Text()
		strs := strings.Fields(line)
		pin := strs[0]

		if len(strs) < 3 || "0" == pin {
			continue
		}

		cid2s := strings.Split(strs[1], ",")
		cid3s := strings.Split(strs[2], ",")

		for _, cid := range cid2s {
			key := fmt.Sprintf("cid2-%s", cid)
			if _, found := items[key]; !found {
				items[key] = &CidSearchHistory{
					CidType:  "cid2",
					PINs:     make([]string, 0),
					pins_map: make(map[string]struct{}),
				}
			}
			if _, found := items[key].pins_map[pin]; found {
				continue
			}
			items[key].PINs = append(items[key].PINs, pin)
			items[key].pins_map[pin] = struct{}{}
		}

		for _, cid := range cid3s {
			key := fmt.Sprintf("cid3-%s", cid)
			if _, found := items[key]; !found {
				items[key] = &CidSearchHistory{
					CidType:  "cid3",
					PINs:     make([]string, 0),
					pins_map: make(map[string]struct{}),
				}
			}
			if _, found := items[key].pins_map[pin]; found {
				continue
			}
			items[key].PINs = append(items[key].PINs, pin)
			items[key].pins_map[pin] = struct{}{}
		}
	}

	ctx := context.Background()
	esCli, err := elastic.NewClient(
		elastic.SetURL("http://ads-es1.jd.local:9200"),
		elastic.SetBasicAuth("ads_udmp", "YTliYmUxMTQwOTk4N2Y3OWM1ZTJjNTYz"))
	if nil != err {
		fmt.Println(err)
		os.Exit(1)
	}

	p, err := esCli.BulkProcessor().
		Name("worker-1").
		Workers(16).
		BulkActions(1000).
		Do(ctx)
	if nil != err {
		fmt.Println("Failed to init bulk processor:", err)
		os.Exit(1)
	}
	for id, context := range items {
		r := elastic.NewBulkIndexRequest().
			Index("ads_udmp_test").
			Type("2018-08-22").
			Id(id).
			Doc(context)
		p.Add(r)
	}

	err = p.Close()
	if nil != err {
		fmt.Println("Failed to close bulk processor:", err)
		os.Exit(1)
	}

	os.Exit(0)
}
