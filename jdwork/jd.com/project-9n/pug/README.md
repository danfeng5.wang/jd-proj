# Pug

RESTful API for 9N's job launching

## Configuration

```
-kubeconfig
-listen
```

## APIs

### Create a job

PUT /job

#### Request Body

```
{
    "jobs": [
        {
            "job_name": "job1",
            "image": "mirror.jd.com/job_images/job_image_1:v1.0",
            "entrypoint": "sleep infinity",
            "storage": "40Gi",
            "resources": {
                "request": {
                    "cpu": 4,
                    "mem": "16Gi"
                },
                "limit": {
                    "cpu": 8,
                    "mem": "32Gi"
                }
            }
        },
        {
            "job_name": "job2",
            "image": "mirror.jd.com/job_images/job_image_1:v1.0",
            "entrypoint": "sleep infinity",
            "storage": "40Gi",
            "resources": {
                "request": {
                    "cpu": 4,
                    "mem": "16Gi"
                },
                "limit": {
                    "cpu": 8,
                    "mem": "32Gi"
                }
            }
        }
    ]
}
```

#### Response Body

```
{
    "results": [
        {
            "job": "job1",
            "status": "successful",
            "message": "ok"
        },
        {
            "job": "job2",
            "status": "failed",
            "message": "something unexpected happen"
        }
    ],
    "successful_count": 1,
    "failed_count": 1
}
```

### Check created job

GET /job

#### Request Body

```
{
    "jobs": [
        "job1",
        "job3"
    ]
}
```

#### Response Body

```
{
    "jobs_found": [
        {
            "job_name": "job1",
            "image": "mirror.jd.com/job_images/job_image_1:v1.0",
            "create_time": "1536225647",
            "start_time": "1536225649",
            "job_status": "running",
            "message": "it's a running job",
            "storage": "40Gi",
            "resources": {
                "request": {
                    "cpu": 4,
                    "mem": "16Gi"
                },
                "limit": {
                    "cpu": 8,
                    "mem": "32Gi"
                }
            }
        }
    ],
    "jobs_not_found": [
        "job3"
    ]
}
```

### Delete a job

DELETE /job

#### Request Body

```
{
    "jobs": [
        "job1"
    ]
}
```

#### Response Body

```
{
    "results": [
        {
            "job_name": "job1",
            "status": "successful",
            "message": "all right"
        },
        {
            "job_name": "job1",
            "status": "failed",
            "message": "something unexpected happend"
        },
    ],
    "successful_count": 1,
    "failed_count": 1
}
```
