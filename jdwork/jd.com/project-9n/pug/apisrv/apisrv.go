package apisrv

import (
	"github.com/gin-gonic/gin"
	apiv1 "jd.com/project-9n/pug/apisrv/v1"
	"jd.com/project-9n/pug/kubecli"
)

type ApiSrv struct {
	lsnAddr string
	kubeCli *kubecli.KubeCli
}

var apiSrvIns *ApiSrv

func GetInstance() *ApiSrv {
	if nil == apiSrvIns {
		apiSrvIns = &ApiSrv{
			lsnAddr: "0.0.0.0:8080",
			kubeCli: nil,
		}
	}
	return apiSrvIns
}

func (a *ApiSrv) SetListenAddress(addr string) *ApiSrv {
	a.lsnAddr = addr
	return a
}

func (a *ApiSrv) SetKubeClient(kubeClient *kubecli.KubeCli) *ApiSrv {
	a.kubeCli = kubeClient
	return a
}

func (a *ApiSrv) Start() {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		v1.GET("biz/:biz/group/:group/job", func(c *gin.Context) {
			apiv1.GetJob(c, a.kubeCli)
		})
		v1.PUT("biz/:biz/group/:group/job", func(c *gin.Context) {
			apiv1.CreateJob(c, a.kubeCli)
		})
		v1.DELETE("biz/:biz/group/:group/job", func(c *gin.Context) {
			apiv1.DeleteJob(c, a.kubeCli)
		})
	}
	r.Run(a.lsnAddr)
}
