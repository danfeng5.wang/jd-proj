package v1

import (
	"github.com/gin-gonic/gin"
	// "github.com/golang/glog"
	"jd.com/project-9n/pug/kubecli"
)

type CreateJobBody struct {
	Jobs []JobDetail `json:"jobs"`
}

type JobDetail struct {
	JobName    string            `json:"job_name" binding:"required"`
	Image      string            `json:"image" binding:"required"`
	Entrypoint string            `json:"entrypoint" binding:"required"`
	Resources  JobResourceDetail `json:"resources" binding:"required"`
}

type JobResourceDetail struct {
	Request ResourceDetail `json:"request" binding:"required"`
	Limit   ResourceDetail `json:"limit" binding:"required"`
}

type ResourceDetail struct {
	Cpu string `json:"cpu"`
	Mem string `json:"mem"`
}

type CreateJobResponseBody struct {
	Results         []CreateJobResponseSection `json:"results" binding:"required"`
	SuccessfulCount int                        `json:"successful_count" binding:"required"`
	FailedCount     int                        `json:"failed_count" binding:"required"`
}

type CreateJobResponseSection struct {
	Job     string `json:"job" binding:"required"`
	Status  string `json:"status" binding:"required"`
	Message string `json:"message" binding:"required"`
}

type CheckJobBody struct {
	Jobs []string `json:"jobs" binding:"required"`
}

type CheckJobResponseBody struct {
	JobsFound    []CheckJobFoundSection `json:"jobs_found" binding:"required"`
	JobsNotFound []string               `json:"jobs_not_found" binding:"required"`
}

type CheckJobFoundSection struct {
	JobName    string            `json:"job_name" binding:"required"`
	Namespace  string            `json:"namespace", binding:"required"`
	Entrypoint string            `json:"entrypoint" binding:"required"`
	Image      string            `json:"image" binding:"required"`
	CreateTime string            `json:"create_time" binding:"required"`
	StartTime  string            `json:"start_time" binding:"required"`
	JobStatus  string            `json:"job_status" binding:"required"`
	Message    string            `json:"message" binding:"required"`
	Resources  JobResourceDetail `json:"resources" binding:"required"`
}

type DeleteJobBody struct {
	Jobs []string `json:"jobs" binding:"required"`
}

type DeleteJobResponseBody struct {
	Results         []DeleteJobResponseSection `json:"results" binding:"required"`
	SuccessfulCount int                        `json:"successful_count" binding:"required"`
	FailedCount     int                        `json:"failed_count" binding:"required"`
}

type JobMeta struct {
	Biz   string
	Group string
}

type DeleteJobResponseSection struct {
	JobName string `json:"job_name" binding:"required"`
	Status  string `json:"status" binding:"required"`
	Message string `json:"message" binding:"required"`
}

func GetJob(c *gin.Context, k *kubecli.KubeCli) {
	body := CheckJobBody{}
	err := c.ShouldBindJSON(&body)
	if nil != err {
		c.JSON(400, err)
		return
	}

	jobMeta := getJobMetaByRequestContext(c)
	kubeNamespace := getKubeNamespaceByJobMeta(jobMeta)

	result := checkJobsInBatch(kubeNamespace, body.Jobs, k)

	c.JSON(200, result)
}

func CreateJob(c *gin.Context, k *kubecli.KubeCli) {
	body := CreateJobBody{}
	err := c.ShouldBindJSON(&body)
	if nil != err {
		c.JSON(400, err)
		return
	}

	jobMeta := getJobMetaByRequestContext(c)
	kubeNamespace := getKubeNamespaceByJobMeta(jobMeta)
	kubeJobInfos := getKubeJobInfosByCreateRequestBody(kubeNamespace, body)

	result := createJobsInBatch(kubeJobInfos, k)

	c.JSON(200, result)
}

func DeleteJob(c *gin.Context, k *kubecli.KubeCli) {
	body := DeleteJobBody{}
	err := c.ShouldBindJSON(&body)
	if nil != err {
		c.JSON(400, err)
		return
	}

	jobMeta := getJobMetaByRequestContext(c)
	kubeNamespace := getKubeNamespaceByJobMeta(jobMeta)

	result := deleteJobsInBatch(kubeNamespace, body.Jobs, k)

	c.JSON(200, result)
}

func getJobMetaByRequestContext(c *gin.Context) JobMeta {
	return JobMeta{
		Biz:   c.Param("biz"),
		Group: c.Param("group"),
	}
}

func getKubeNamespaceByJobMeta(jobMeta JobMeta) string {
	return jobMeta.Biz
}

func getKubeJobInfosByCreateRequestBody(ns string, body CreateJobBody) []kubecli.KubeJobInfo {
	jobs := make([]kubecli.KubeJobInfo, 0)
	for _, jobDetail := range body.Jobs {
		jobs = append(jobs, kubecli.KubeJobInfo{
			JobName:    jobDetail.JobName,
			Namespace:  ns,
			Image:      jobDetail.Image,
			Entrypoint: jobDetail.Entrypoint,
			CpuRequest: jobDetail.Resources.Request.Cpu,
			CpuLimit:   jobDetail.Resources.Limit.Cpu,
			MemRequest: jobDetail.Resources.Request.Mem,
			MemLimit:   jobDetail.Resources.Limit.Mem,
		})
	}
	return jobs
}

func createJobsInBatch(jobs []kubecli.KubeJobInfo, k *kubecli.KubeCli) CreateJobResponseBody {
	response := CreateJobResponseBody{
		Results:         make([]CreateJobResponseSection, 0),
		SuccessfulCount: 0,
		FailedCount:     0,
	}

	for _, job := range jobs {
		var (
			status  string
			message string
		)
		err := k.CreateJob(job)
		if nil != err {
			status = "failed"
			message = err.Error()
			response.FailedCount++
		} else {
			status = "successful"
			message = ""
			response.SuccessfulCount++
		}
		response.Results = append(response.Results, CreateJobResponseSection{
			Job:     job.JobName,
			Status:  status,
			Message: message,
		})
	}

	return response
}

func checkJobsInBatch(ns string, jobs []string, k *kubecli.KubeCli) CheckJobResponseBody {
	response := CheckJobResponseBody{
		JobsFound:    make([]CheckJobFoundSection, 0),
		JobsNotFound: make([]string, 0),
	}

	for _, job := range jobs {
		found, kubeJobStatus := k.CheckJob(ns, job)
		if !found {
			response.JobsNotFound = append(response.JobsNotFound, job)
			continue
		}
		response.JobsFound = append(response.JobsFound, CheckJobFoundSection{
			JobName:    kubeJobStatus.JobInfo.JobName,
			Namespace:  kubeJobStatus.JobInfo.Namespace,
			Image:      kubeJobStatus.JobInfo.Image,
			Entrypoint: kubeJobStatus.JobInfo.Entrypoint,
			CreateTime: kubeJobStatus.CreateTime,
			StartTime:  kubeJobStatus.StartTime,
			JobStatus:  kubeJobStatus.Status,
			Message:    kubeJobStatus.Message,
			Resources: JobResourceDetail{
				Request: ResourceDetail{
					Cpu: kubeJobStatus.JobInfo.CpuRequest,
					Mem: kubeJobStatus.JobInfo.MemRequest,
				},
				Limit: ResourceDetail{
					Cpu: kubeJobStatus.JobInfo.CpuLimit,
					Mem: kubeJobStatus.JobInfo.MemLimit,
				},
			},
		})
	}

	return response
}

func deleteJobsInBatch(ns string, jobs []string, k *kubecli.KubeCli) DeleteJobResponseBody {
	response := DeleteJobResponseBody{
		Results:         make([]DeleteJobResponseSection, 0),
		SuccessfulCount: 0,
		FailedCount:     0,
	}

	for _, job := range jobs {
		var (
			status  string
			message string
		)
		err := k.DeleteJob(ns, job)
		if nil != err {
			status = "failed"
			message = err.Error()
			response.FailedCount++
		} else {
			status = "successful"
			message = ""
			response.SuccessfulCount++
		}
		response.Results = append(response.Results, DeleteJobResponseSection{
			JobName: job,
			Status:  status,
			Message: message,
		})
	}

	return response
}
