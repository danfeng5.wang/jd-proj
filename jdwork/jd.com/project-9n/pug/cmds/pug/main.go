package main

import (
	"flag"
	"fmt"
	"github.com/golang/glog"
	"jd.com/project-9n/pug/apisrv"
	"jd.com/project-9n/pug/kubecli"
	"os"
)

const (
	LISTEN_ADDRESS = "0.0.0.0:8448"
)

var (
	lsnAddr    = flag.String("l", LISTEN_ADDRESS, "listenning address")
	kubeConfig = flag.String("k", "", "absolute path to the kubeconfig file")
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: \n")
	fmt.Fprintf(os.Stderr, "Logs: \n")
	flag.PrintDefaults()
	os.Exit(2)
}

func init() {
	flag.Usage = usage
	flag.Parse()
}

func main() {
	kubeCli := kubecli.NewKubeCli(*kubeConfig)
	glog.Info(kubeCli)
	apiSrv := apisrv.GetInstance().
		SetListenAddress(*lsnAddr).
		SetKubeClient(kubeCli)
	apiSrv.Start()

	select {}

	glog.Flush()

	os.Exit(0)
}
