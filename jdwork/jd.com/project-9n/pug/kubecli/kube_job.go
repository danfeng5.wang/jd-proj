package kubecli

import (
	// "errors"
	// "github.com/golang/glog"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strings"
)

type KubeJobStatus struct {
	JobInfo    KubeJobInfo
	CreateTime string
	StartTime  string
	Status     string
	Message    string
}

type KubeJobInfo struct {
	JobName    string
	Namespace  string
	Image      string
	Entrypoint string
	CpuRequest string
	CpuLimit   string
	MemRequest string
	MemLimit   string
}

func (k *KubeCli) CheckJob(ns string, jobName string) (bool, KubeJobStatus) {
	return true, KubeJobStatus{
		JobInfo: KubeJobInfo{
			JobName:    "asd",
			Namespace:  "9n",
			Image:      "xx",
			Entrypoint: "xc",
			CpuRequest: "3",
			CpuLimit:   "10",
			MemRequest: "8Gi",
			MemLimit:   "16Gi",
		},
		CreateTime: "1111",
		StartTime:  "2222",
		Status:     "Running",
		Message:    "alright",
	}
}

func (k *KubeCli) CreateJob(job KubeJobInfo) error {
	jobCli := k.cliSet.BatchV1().Jobs(job.Namespace)
	kubeJob := generateNewKubeJob(job)
	_, err := jobCli.Create(kubeJob)
	return err
}

func (k *KubeCli) DeleteJob(ns string, jobName string) error {
	return nil
}

func generateNewKubeJob(job KubeJobInfo) *batchv1.Job {
	kubeJob := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name: job.JobName,
		},
		Spec: getKubeJobSpec(job),
	}
	return kubeJob
}

func getKubeJobSpec(job KubeJobInfo) batchv1.JobSpec {
	jobSpec := batchv1.JobSpec{
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name: job.JobName,
			},
			Spec: getKubePodSpec(job),
		},
	}
	return jobSpec
}

func getKubePodSpec(job KubeJobInfo) corev1.PodSpec {
	podSpec := corev1.PodSpec{
		DNSPolicy:     corev1.DNSClusterFirst,
		RestartPolicy: corev1.RestartPolicyNever,
		Containers:    getKubeJobContainers(job),
	}
	return podSpec
}

func getKubeJobContainers(job KubeJobInfo) []corev1.Container {
	jobContainers := make([]corev1.Container, 0)
	containerPrivileged := false
	jobContainer := corev1.Container{
		Name:    job.JobName,
		Image:   job.Image,
		Command: strings.Split(job.Entrypoint, " "),
		SecurityContext: &corev1.SecurityContext{
			Privileged: &containerPrivileged,
		},
		Resources: getKubeResourceRequirements(job),
	}
	jobContainers = append(jobContainers, jobContainer)
	return jobContainers
}

func getKubeResourceRequirements(job KubeJobInfo) corev1.ResourceRequirements {
	cpuLimit, _ := resource.ParseQuantity(job.CpuLimit)
	cpuRequest, _ := resource.ParseQuantity(job.CpuRequest)
	memLimit, _ := resource.ParseQuantity(job.MemLimit)
	memRequest, _ := resource.ParseQuantity(job.MemRequest)

	resourceLimit := corev1.ResourceList{
		"cpu":    cpuLimit,
		"memory": memLimit,
	}
	resourceRequest := corev1.ResourceList{
		"cpu":    cpuRequest,
		"memory": memRequest,
	}
	resourceRequirements := corev1.ResourceRequirements{
		Limits:   resourceLimit,
		Requests: resourceRequest,
	}
	return resourceRequirements
}
