package kubecli

import (
	"github.com/golang/glog"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"path/filepath"
)

type KubeCli struct {
	cliSet *kubernetes.Clientset
}

func NewKubeCli(kc string) *KubeCli {
	var kubeConfigStr string

	if "" == kc {
		if homeDir := homedir.HomeDir(); "" == homeDir {
			return nil
		} else {
			kubeConfigStr = filepath.Join(homeDir, ".kube", "config")
		}
	} else {
		kubeConfigStr = kc
	}
	glog.Info(kubeConfigStr)
	kubeConfig, err := clientcmd.BuildConfigFromFlags("", kubeConfigStr)
	if nil != err {
		return nil
	}

	clientSet, err := kubernetes.NewForConfig(kubeConfig)
	if nil != err {
		return nil
	}

	return &KubeCli{
		cliSet: clientSet,
	}
}
