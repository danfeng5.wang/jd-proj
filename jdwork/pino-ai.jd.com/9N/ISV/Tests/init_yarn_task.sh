#!/bin/bash
baseDir=/mnt/mfsb/pino/online/UnboundedDMP
TOP_PATH=$(cd ${baseDir} && pwd)

source ${TOP_PATH}/init-env.sh

yarn_tasks=$(yarn application -list -appStates ALL | awk -v OFS="|" '{print $1,$4,$6,$7,$8}' | grep test_9n | xargs)

for task in ${yarn_tasks[@]}
do
    python insert_db.py $task
done