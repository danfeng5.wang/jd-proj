#!/usr/bin/env python3
# Time: 2018/6/6 21:48

__author__ = 'hejingjian@jd.com'

import pymysql
import sys

task = sys.argv[1]

db_config = dict(
    dbhost='10.196.39.195',
    dbuser='isvpreuser',
    dbpassword='Wo8bF3Sq0ZtPd9iFswT',
    dbport=3306,
    dbname='isv_pre',
    dbcharset='utf8')

class ConnectionDB(object):
    def __init__(self, **kwargs):
        self.dbuser = kwargs['dbuser']
        self.dbpassword = kwargs['dbpassword']
        self.dbhost = kwargs['dbhost']
        self.dbname = kwargs['dbname']
        self.dbport = kwargs['dbport']
        self.dbcharset = kwargs['dbcharset']

    def get_conn(self):
        conn = pymysql.connect(user=self.dbuser, passwd=self.dbpassword, host=self.dbhost, db=self.dbname,
                               charset=self.dbcharset)
        return conn

    def close_conn(self, cursor, conn):
        if not cursor:
            cursor.close()
        if not conn:
            conn.close()


db_object = ConnectionDB(**db_config)
conn = db_object.get_conn()
cursor = conn.cursor()

tasklist = task.split('|')
try:
    cursor.execute('update yarn_tasks set state = %s, final_state = %s, progress = %s where id = %s', [tasklist[2], tasklist[3], tasklist[4], tasklist[0]])
    status = conn.commit()
    print('status:', status)
    # sql = cursor.execute(
    #     "insert into yarn_tasks(application_id, username, state, final_state, progress) VALUE (%s,%s,%s,%s,%s)", [tasklist[0], tasklist[1], tasklist[2], tasklist[3], tasklist[4]])
    #print("sql:", sql)
    #conn.commit()
except Exception:
    conn.rollback()
finally:
    db_object.close_conn(cursor, conn)
