#!/usr/bin/env python3
# Time: 2018/3/30 11:27

__author__ = 'wanggangshan@jd.com'



# Usage:  python test_all..py sql_add
# will exec func:  test_sql_add()


import requests

gateway_isv = 'http://apis.jd.com/v1/isv'
# gateway_isv = 'http://10.181.55.51:6006'
# gateway_isv = 'http://10.181.54.64:6022'


def http_request(url, params=None, data=None, request_type='post',
                 login_auth=None, timeout=None, files=None):
    print(url)
    print(data)
    print('-----------------------------------------')
    url = gateway_isv + url
    session = requests.Session()
    headers = {
        'Content-Type': 'application/json', 'Accept': 'application/json'}

    # try:
    if files:
        response = session.request(
            request_type, url, params=params, data=data,
            files=files)
    else:
        response = session.request(
            request_type, url, params=params, json=data, headers=headers)
    return response



# 添加一个sql模板
def test_sql_add():
    path = '/v1/sql/manager/add'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "job": {
            "api_name": "querydemo",
            "sql": "<script>select kol_type,kol_type_en,platform,platform_en,kolname from tbl_seg_kol_combine where `segmentation`=#{seg} and month=#{month} and noofkol = 1 order by value DESC;</script>",
            "note": "querydemo"
        }
    }
    res = http_request(path, data=data)
    print(res.text)


# 删除sql模板
def test_sql_delete():
    path = '/v1/sql/manager/delete'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "api_name": "querydemo",
    }
    res = http_request(path, data=data)
    print(res.text)


# 查询当前生效的所有sql模板
def test_sql_query_all():
    path = '/v1/sql/manager/query/all'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
    }
    res = http_request(path, data=data)
    print(res.text)




# 执行查询sql
def test_sql_exec():
    path = '/v1/sql/manager/exec'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "api_name": "querydemo",
        "args": {"seg":"1_1","month":200101}
    }
    res = http_request(path, data=data)
    print(res.text)



if __name__ == '__main__':
    import sys
    will_do = sys.argv[1]
    eval('test_'+will_do)()
