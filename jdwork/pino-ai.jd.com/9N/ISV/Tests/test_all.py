#!/usr/bin/env python3
# Time: 2018/3/30 11:27

__author__ = 'wanggangshan@jd.com'



# Usage:  python test_all.py sql_add
# will exec func:  test_sql_add()


import requests

## from online(inner network) Gateway to isv server
#gateway_isv = 'http://apis-internal.jd.com/v1/isv'

## from online(out network) Gateway to isv server
gateway_isv = 'http://apis-preview.jd.com/v1/tableau'

## redict offline to isv server
#gateway_isv = 'http://10.181.55.51:6006'
# gateway_isv = 'http://10.181.54.64:6022'


def http_request(url, params=None, data=None, request_type='post',
                 login_auth=None, timeout=None, files=None):
    print(gateway_isv + url)
    print(data)
    print('-----------------------------------------')
    url = gateway_isv + url
    session = requests.Session()
    headers = {
        'Content-Type': 'application/json', 'Accept': 'application/json'}

    # try:
    if files:
        response = session.request(
            request_type, url, params=params, data=data,
            files=files)
    else:
        response = session.request(
            request_type, url, params=params, json=data, headers=headers)
    return response



# 添加一个sql模板
def test_sql_add():
    path = '/v1/sql/manager/add'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "job": {
            "api_name": "querySingerInfo",
            "sql": "<script>select DISTINCT name from singer_info where age=#{age} and phone_number=#{phone_number};</script>",
            "note": "test....just"
        }
    }
    res = http_request(path, data=data)
    print(res.text)


# 删除sql模板
def test_sql_delete():
    path = '/v1/sql/manager/delete'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "api_name": "querySingerInfo",
    }
    res = http_request(path, data=data)
    print(res.text)


# 查询当前生效的所有sql模板
def test_sql_query_all():
    path = '/v1/sql/manager/query/all'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
    }
    res = http_request(path, data=data)
    print(res.text)




# 执行查询sql
def test_sql_exec():
    path = '/v1/sql/manager/exec'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "api_name": "querySingerInfo",
        "args": {
            'age': 20,
            'phone_number': 15010000000
        }
    }
    res = http_request(path, data=data)
    print(res.text)


# 批量删除mysql表中指定月份的数据
def test_sql_data_delete():
    path = '/v1/mysql/data/delete'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "month": 201801,
        "tables": ["tbl_user_lable_1m"]
    }
    res = http_request(path, data=data)
    print(res.text)


# 假接口,虚拟返回结果
# ftp通知获取
def test_transfer_files():
    path='/v1/job/transfer/files'
    data={
        "token": "88d84d442cfb11e8949cfa163ed5cc75",  # str, 统一接口验证token 32位uuid,定值
        "ip": "10.12.128.207",  # str, 服务器端ip地址
        "port": 22,  # int, 端口号
        "username": "denglei",  # str,  用户名
        "password": "111111",  # str, 密码，需要协定加密和解密策略
        "remote_path": "/a/b",  # str,  服务器端需要下载文件所在目录
        "callback_url": "/v1/job/test/transfer",  # str, 异步回调的url，任务完成后异步通知结果
        "callback_param": {"": ""},
    }
    res = http_request(path,data=data)
    print(res.text)

# 回调，假接口,虚拟返回结果
def test_transfer_result():
    path='/v1/callback/transfer/result'
    data={
        "status": True,									#bool, 下载是否成功
        "message": "success",
    }
    res = http_request(path,data=data)
    print(res.text)


# 假接口,虚拟返回结果
# 创建mr任务
def test_create_mr_task():
    path = '/v1/create/mr/task'
    data = {
        "token": "88d84d442cfb11e8949cfa163ed5cc75",
        "callback_url": "",
        "callback_param": {},
        "jar_package": "",
        "main_class": "",
        "task_id": "",
        "paramter": "",
}
    res = http_request(path, data=data)
    print(res.text)



if __name__ == '__main__':
    import sys
    will_do = sys.argv[1]
    eval('test_'+will_do)()
