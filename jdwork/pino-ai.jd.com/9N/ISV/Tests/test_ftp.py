#!/usr/bin/env python3
# Time: 2018/5/9 18:21

__author__ = 'hejingjian@jd.com'

# FTP操作
import ftplib
import threading

host = 'jvftp.meritco-group.com'
port = 2121
username = 'jdpanel'
password = 'JDpanel!@WSXxpo'
file = '/Meritco_Data/test-01.zip'
# file = '/Meritco_Data/201708.zip.ok'

# f = ftplib.FTP(host)  # 实例化FTP对象
ftp = ftplib.FTP_TLS()
ftp.connect(host=host, port=port, timeout=10)

ftp.login(username, password)  # 登录
ftp.prot_p()

# 获取当前路径
pwd_path = ftp.pwd()
print("FTP当前路径:", pwd_path)



def ftp_download():
    '''以二进制形式下载文件'''
    file_remote = file
    file_local = 'test-01.zip'
    bufsize = 81920  # 设置缓冲器大小
    fp = open(file_local, 'wb')
    print(ftp.retrbinary('RETR %s' % file_remote, fp.write, bufsize))
    fp.close()

    # def background():
    #     f = open(file_local, 'wb')
    #     with ftp.transfercmd('RETR ' + file_remote) as sock:
    #         while True:
    #             block = sock.recv(1024 * 1024)
    #             if not block:
    #                 break
    #             f.write(block)
    #     sock.close()
    # t = threading.Thread(target=background)
    # t.start()
    # while t.is_alive():
    #     t.join(60)
    #     ftp.voidcmd('NOOP')


def ftp_upload():
    '''以二进制形式上传文件'''
    file_remote = 'ftp_upload.txt'
    file_local = 'D:\\test_data\\ftp_upload.txt'
    bufsize = 1024  # 设置缓冲器大小
    fp = open(file_local, 'rb')
    ftp.storbinary('STOR ' + file_remote, fp, bufsize)
    fp.close()

def sock_download():
    sock = ftp.transfercmd('RETR ' + file)
    file_local = 'sock_download.zip'
    fp = open(file_local, 'wb')
    while True:
        block = sock.recv(1024 * 1024)
        if not block:
            break
        fp.write(block)
    sock.close()

ftp_download()
# ftp_upload()
print('is ok!')
#ftp.quit()