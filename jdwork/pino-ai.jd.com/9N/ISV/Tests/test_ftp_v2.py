
import gzip
import tarfile
import os
import sys
sys.path.append('%s/libs' % os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, '../libs')
import zipfile
import ftplib
import filetype
import climate
from utils.config_helper import config

climate.enable_default_logging()
logging = climate.get_logger(__name__)

class Ftps(object):
    PATH_TYPE_FILE=0
    PATH_TYPE_DIR=1
    PATH_TYPE_UNKONWN=2
    def __init__(self,**kwargs):
        # username=kwargs["username"]
        # password=kwargs["password"]
        # ip=kwargs["ip"]
        # port=kwargs["port"]
        # remote_path=kwargs["remote_path"]
        self.username = "jdpanel"
        self.password = "JDpanel!@WSXxpo"
        self.ip = "jvftp.meritco-group.com"
        self.port = 2121
        self.remote_path="/Meritco_Data"
        self.local_path=""
        self.hdfs_path=""
        #链接FTPS服务器
        self.ftps = ftplib.FTP_TLS()
        self.ftps.connect(host=self.ip, port=self.port)
        self.ftps.set_pasv(True)
        self.ftps.login(self.username, self.password)
        self.ftps.prot_p()

    #获取文件的属性等相关信息
    def dir(self, *args):
        '''
        by defualt, ftplib.FTP.dir() does not return any value.
        Instead, it prints the dir info to the stdout.
        So we re-implement it in FtpDownloader, which is able to return the dir info.
        '''
        info = []
        cmd = 'LIST'
        for arg in args:
            if arg:
                cmd = cmd + (' ' + arg)
        self.ftps.retrlines(cmd, lambda x: info.append(x.strip().split()))
        return info

    #获取文件目录树结构和文件及相应类型
    def tree(self, remote_dir=None, init=True):
        '''
        recursively get the tree structure of a directory on FTP Server.
        args:
            remote_dir - remote direcotry path of the FTP Server.
            init - flag showing whether in a recursion.
        '''
        if init and remote_dir in ('.', None):
            remote_dir = self.ftps.pwd()
        # tree = []
        # tree.append((remote_dir, self.PATH_TYPE_DIR))

        dir_info = self.dir(remote_dir)
        type=1
        if len(dir_info)==1:
            temp=dir_info[0]
            attr=temp[0]
            name=temp[-1]
            if name==remote_dir:
                if attr.startswith("-"):
                    type=self.PATH_TYPE_FILE
                elif attr.startswith("d"):
                    type=self.PATH_TYPE_DIR
                else:
                    type=self.PATH_TYPE_UNKNOWN
        tree = []
        tree.append((remote_dir, type))
        print("dir_info:")
        print(dir_info)
        print("_________________")
        for info in dir_info:
            print(info)
            attr = info[0]  # attribute
            name = info[-1]
            #path = os.path.join(remote_dir, name)
            if remote_dir!=name:
                path=remote_dir+"/"+name
                if attr.startswith('-'):
                    tree.append((path, self.PATH_TYPE_FILE))
                elif attr.startswith('d'):
                    if (name == '.' or name == '..'):  # skip . and ..
                        continue
                    tree.extend(self.tree(remote_dir=path,init=False))  # recurse
                else:
                    tree.append(path, self.PATH_TYPE_UNKNOWN)
        return tree

    #从FTPS服务器获取文件
    def transfer_files_from_ftps(self,remote_dir,local_dir):
        file_list=self.tree(remote_dir=remote_dir,init=True)
        result=[]
        for file,file_type in file_list:
            flag=len(file.split("/"))==len(remote_dir.split("/"))
            print(file,remote_dir)
            path = "\\" + "\\".join(file.split("/")[len(remote_dir.split("/")):])
            if file_type==self.PATH_TYPE_FILE and flag:
                path="\\"+file.split("/")[-1]
            if file_type==self.PATH_TYPE_DIR and not os.path.exists(local_dir+path):
                os.makedirs(local_dir+path)
            if file_type==self.PATH_TYPE_FILE and file.split(".")[-1]=="ok":
                path=(local_dir+".".join(path.split(".")[0:-1])).split("-")[0]+".zip"
                with open(path,'ab') as f:
                    self.ftps.retrbinary('RETR ' + file, f.write)
                #self.ftps.rename(file,".".join(file.split(".")[0:-1])+".yes")
                f.close()
                logging.info("download--------------"+file+"-----------------success")
                result.append(path)
        return result

    #判断文件类型
    def file_type(self,local_path):
        if os.path.isdir(local_path):
            print("File {} is a dir!".format(local_path))
            return ""
        elif os.path.isfile(local_path):
            kind = filetype.guess(local_path)
            if kind is None:
                print('Cannot guess file {} type!'.format(local_path))
                return ""
            print('File %s extension: %s' % (local_path , kind.extension))
            return kind.extension

    #解压缩.tar.gz和.tgz格式的文件,参数type是区分.tar.gz和.tgz
    def unzip_tgz(self,local_path,type):
        path_split = local_path.split("\\")
        file_name = path_split[-1]
        f_name = file_name.replace("."+type, "")
        path = "\\".join(path_split[0:-1]) + "\\" + f_name
        g_file = gzip.GzipFile(local_path)
        with open(path, 'wb') as f:
            f.write(g_file.read())
        g_file.close()
        #os.remove(local_path)
        is_tar = tarfile.is_tarfile(path)
        if is_tar:
            tar = tarfile.open(path)
            # names不仅包含tar包中的文件名，还包含本身的名字
            names = tar.getnames()
            #去掉后缀名
            if type=="gz":
                tar_path = path.replace(".tar", "")
            #建立同名文件
            if os.path.isdir(tar_path):
                pass
            else:
                os.mkdir(tar_path)
            store_unzip_files="\\".join(tar_path.split("\\")[0:-1])
            for name in names:
                tar.extract(name,store_unzip_files)
            tar.close()
            #os.remove(path)
            return tar_path
        return path

    #解压zip格式文件
    def unzip_zip(self,local_path):
        zip_file = zipfile.ZipFile(local_path)
        path=local_path.replace(".zip","")
        if os.path.isdir(path):
            pass
        else:
            os.mkdir(path)
        store_unzip_files="\\".join(path.split("\\")[0:-1])
        for names in zip_file.namelist():
            zip_file.extract(names, store_unzip_files)
        zip_file.close()
        #os.remove(local_path)
        return path

    #删除压缩文件
    def delete_file(self,path):
        os.remove(path)

    #解压缩文件
    def unzip(self,local_path):
        type=self.file_type(local_path)
        if type=="gz":
            path=self.unzip_tgz(local_path,type)
        elif type=="tgz":
            path=self.unzip_tgz(local_path,type)
        elif type=="zip":
            path=self.unzip_zip(local_path)
        else:
            path=""
            print("Unsupported file format!")
        return path

    #把文件传到hdfs上
    def put_files_to_hdfs(self, local_dir, hdfs_dir):
        print("local_dir:%s" % local_dir)
        print("hdfs_dir:%s" % hdfs_dir)
        shell = 'sh ../put_to_hdfs.sh ' + config.get('user_name') + ' ' + local_dir + ' ' + hdfs_dir
        status = os.system(shell)#0--success，1--fail
        print("status:%s" % status)
        return status

    #传输文件
    def transfer_files_from_ftps_to_hdfs(self):
        remote_path=self.remote_path
        local_path=self.local_path
        path_list=self.transfer_files_from_ftps(remote_dir=remote_path,local_dir=local_path)
        for path in path_list:
            after_unzip_path=self.unzip(path)
            print(after_unzip_path)
            local_dir=after_unzip_path
            hdfs_dir=self.hdfs_path+after_unzip_path.replace(self.local_path,"")
            # status=self.put_files_to_hdfs(local_dir=local_dir,hdfs_dir=hdfs_dir)
            # if status==0:
            #     if remote_path.endswith(".ok"):
            #         self.ftps.rename(remote_path,remote_path.replace(".ok",".yes"))
            #     else:
            #         old=remote_path+path.replace(self.local_path,"")+".ok"
            #         new=old.replace(".ok",".yes")
            #         self.ftps.rename(old,new)
            if remote_path.endswith(".ok"):
                self.ftps.rename(remote_path, remote_path.replace(".ok", ".yes"))
            else:
                old = remote_path +"/"+"/".join((path.replace(self.local_path, "") + ".ok").split("\\"))
                new = old.replace(".ok", ".yes")
                self.ftps.rename(old, new)

if __name__=='__main__':
    ftps=Ftps()
    ftps.transfer_files_from_ftps_to_hdfs()