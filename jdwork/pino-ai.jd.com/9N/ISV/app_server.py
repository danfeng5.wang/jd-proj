# codeing=utf-8
import os
import sys

import time

sys.path.append('%s/libs' % os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, '../libs')
from flask import Flask, request, render_template

from optparse import OptionParser

import climate

climate.enable_default_logging()
logging = climate.get_logger(__name__)

from utils.config_helper import load_all_config, config

load_all_config()

from db.db import db_session_manager as db_manager

app = Flask(__name__)
from controller import route_config

route_config.route_init(app)
from common import url_verify

db_cfg = config.get('mysql_db')
db_manager.register_db(db_cfg, 'default')

url_verify.urlCheck(app)

# from src.api.check_label import CheckLabelApi
# from utils.user_label_init import user_labels
# _check_label_api = CheckLabelApi()
# month = time.strftime('%Y%m', time.localtime(time.time()))

# user_labels_out, user_labels_in = _check_label_api.query_user_labels(**{'month': '201711'})
# count = user_labels_out.count() + user_labels_in.count()
# print('user_labels_info:', count)
#
#
# def init_user_labels(crowd_type, user_labels_info):
#     label_dict = {}
#     for uli in user_labels_info.all():
#         if label_dict.get(uli.month, {}):
#             label_dict[uli.month][uli.user] = uli.alltag
#         else:
#             label_dict[uli.month] = {}
#             label_dict[uli.month][uli.user] = uli.alltag
#
#     user_labels.set_user_dict(crowd_type, label_dict)
#
# init_user_labels(1, user_labels_in)
#
# init_user_labels(2, user_labels_out)




if __name__ == '__main__':
    usage = 'usage: python %prog [options] arg1 arg2'
    parser = OptionParser(usage=usage)

    parser.add_option(
        '-g',
        '--debug',
        dest='debug_flag',
        type='int',
        default=99,
        action='store',
        metavar='DEBUG',
        help='debug flag')
    parser.add_option(
        '-i',
        '--ip',
        dest='server_ip',
        type='string',
        default='0.0.0.0',
        action='store',
        metavar='IP',
        help='pas server ip')
    parser.add_option(
        '-p',
        '--port',
        dest='server_port',
        type='string',
        default='5002',
        action='store',
        metavar='PORT',
        help='pas server port')

    (options, args) = parser.parse_args()

    # set listen '0.0.0.0'

    ip = '0.0.0.0'
    port = int(options.server_port)

    app.run(host=ip, port=port, debug=False)
