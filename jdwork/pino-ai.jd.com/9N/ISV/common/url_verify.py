# encoding=utf-8
from flask import request, session, jsonify, redirect, url_for

from utils.config_helper import config

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)


def urlCheck(app):
    @app.before_request
    def before_request():
        if request.path == '/health':
            return jsonify('ok')

        if request.files:
            req_json = request.form
        else:
            req_json = request.get_json(force=True)

        ip = request.remote_addr
        logging.info('>>%s>>request.path: %s' % (ip, request.path))
        logging.info('>>%s>>request.json: %s' % (ip, req_json))

        token = req_json.get('token', '')
        if not token:
            return jsonify(
                {'status': False, 'error_code': 403, 'result': {},
                 'message': 'without token'})

        if token != config.get('api_token'):
            return jsonify(
                {'status': False, 'error_code': 403, 'result': {},
                 'message': 'token not correct'})
