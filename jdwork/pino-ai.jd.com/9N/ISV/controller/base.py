#!/usr/bin/env python3
# Time: 2017/10/16 15:24
from collections import defaultdict

from utils.config_helper import config
from utils.util import http_request
import requests

import climate

logging = climate.get_logger(__name__)
__author__ = 'wanggangshan@jd.com'


from flask.views import MethodView


class Base(MethodView):
    def __init__(self, db_session=None):

        pass

    @property
    def status(self):
        return self._status

    @property
    def result(self):
        return self._result

    @status.setter
    def status(self, values):
        self._status = values

    @result.setter
    def result(self, values):
        self._result = values

    def set_response(self, kwargs={}):
        response = defaultdict(list)
        response['status'] = kwargs.get('status', True)
        response['result'] = kwargs.get('result', {})
        response['message'] = kwargs.get('message', '')
        response['error_code'] = kwargs.get('error_code', 0)
        return response

    def report_result(self, data, url_rule):
        report_url = url_rule
        try:
            logging.info('Request to <%s>: %s', report_url, data)
            response = http_request(
                report_url,
                data=data,
                request_type='post')
            if response.status_code != requests.codes.ok:
                logging.error('Error: response of code is not 200:\n%s' % str(response))
                return {"success": False, 'errorMsg': 'Error: response of code is not 200: %s' % str(response), 'resultCode': -1}
            else:
                logging.info('Response: %s', response)
                # result = json.loads(response.content.decode())
                return {"success": True, 'errorMsg': '', 'resultCode': -1}
        except Exception as e:
            logging.error('Error: error_msg: %s' % str(e))
            return {"success": False, 'errorMsg': str(e), 'resultCode': -1}