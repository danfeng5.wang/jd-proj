#!/usr/bin/env python3
# Time: 2018/4/25 14:44
import time

import gc
from multiprocessing import Process
from flask import request, jsonify

from controller.base import Base
from db.db import redis_manager
from src.api.check_label import CheckLabelApi
from utils.user_label_init import user_labels
from utils.config_helper import load_all_config, config
import threading
import climate

climate.enable_default_logging()
logging = climate.get_logger(__name__)

__author__ = 'hejingjian@jd.com'


class InitUserLabel(Base):

    def __init__(self):
        super(InitUserLabel, self).__init__()
        self._check_label_api = CheckLabelApi()

    def get(self):
        pass

    def post(self):
        user_labels_out, user_labels_in = self._check_label_api.query_user_labels()

        out_count = user_labels_out.count()
        in_count = user_labels_in.count()
        logging.info("out_count_1m: %s, in_count_baby: %s" % (out_count, in_count))
        count = out_count + in_count
        logging.info('user_labels_info: %s', count)
        
        def init_user_label(table_name):
            redis_config = config.get('jmdb_redis_config')
            redis_manager.register_db(redis_config)
            redis = redis_manager.get_client()
            user_labels_data = {}
            alldict = {}
            page_size = 500000
            if table_name == "tbl_user_lable_1m":
                user_labels_data = self._check_label_api.query_uesr_labels_1m()
            elif table_name == "tbl_user_lable_baby":
                user_labels_data = self._check_label_api.query_user_labels_baby()
            if user_labels_data.count != 0:
                count = user_labels_data.count()
                for page_index in range(count // page_size + 1):
                    user_labels = user_labels_data.slice(page_index * page_size, (page_index + 1) * page_size)
                    for uli in user_labels:
                        if str(uli.month) not in alldict:
                            alldict[str(uli.month)] = {}
                        alldict[str(uli.month)][uli.user] = uli.alltag
                    for month in alldict:
                        redis.hmset(table_name + '_' + str(month), alldict[month])
                        logging.info("talename: %s  month: %s  page_index: %s  " % (table_name, month, page_index))
                    del alldict
                    gc.collect()
                    alldict = {}
                    del user_labels
                    gc.collect()
                logging.info("----------- InitUserLabel END -----------")
            else:
                logging.info(" No data in %s  " % table_name)

        t1 = Process(target=init_user_label, args=('tbl_user_lable_1m',))
        t1.start()
        t2 = Process(target=init_user_label, args=('tbl_user_lable_baby',))
        t2.start()

        result = self.set_response({'result': {'count': count}})
        return jsonify(result)


# 人群创建校验接口
class CheckLabel(Base):

    def __init__(self):
        super(CheckLabel, self).__init__()
        self._check_label_api = CheckLabelApi()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        label_json = args["label_json"]
        crowd_type = int(args.get("crowd_type", 1))
        month = args["month"]

        redis_config = config.get('jmdb_redis_config')
        # redis_config['db'] = 2
        redis_manager.register_db(redis_config)
        redis = redis_manager.get_client()
        if crowd_type == 1:
            table = 'tbl_user_lable_baby_'
        elif crowd_type == 2:
            table = 'tbl_user_lable_1m_'
        key_name = table + str(month)
        user_dict = redis.hgetall(key_name)


        print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
        if user_dict:
            count = self._check_label_api.checklable(user_dict, label_json)
            result = self.set_response({'result': {'count': count}})
        else:
            result = self.set_response({'status': False, 'message': "ERROR: User_Dict Is None"})
        print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))

        return jsonify(result)
