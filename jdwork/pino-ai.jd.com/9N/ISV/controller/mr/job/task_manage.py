import os

import climate
from flask import request, jsonify
import subprocess
import threading
from src.api.mr_task import MrTaskApi
from controller.mr.base import Base
from db.columntype import TaskStatus
from utils.config_helper import config
from db.db import redis_manager
from src.api.check_label import CheckLabelApi
from src.api.sql_manager import SqlManagerApi
import gc

import time

climate.enable_default_logging()
logging = climate.get_logger(__name__)


class DeleteDataMonth(Base):
    def __init__(self):
        super(DeleteDataMonth, self).__init__()
        self._check_label_api = CheckLabelApi()
        self.__sql_manager_api = SqlManagerApi()

    def get(self):
        pass

    def post(self):
        kwargs = request.get_json(force=True)
        month = kwargs.get('month', 0)
        tables = kwargs.get('tables', [])

        if (not month) or (not tables):
            response = self.set_response(
                {'status': False, 'message': 'error input kwargs： %s' % kwargs})
            return jsonify(response)

        redis_config = config.get('jmdb_redis_config')
        redis_manager.register_db(redis_config)
        redis = redis_manager.get_client()

        def delete_data_mysql(**kwargs):
            status, info = self.__sql_manager_api.delete_month_data(**kwargs)
            if status:
                logging.info("Delete mysql success: ", {'status': status, 'result': {'data': info}})
            else:
                logging.info("Delete mysql fail: ", {'status': status, 'message': info})

        t_mysql = threading.Thread(target=delete_data_mysql, args=kwargs)
        t_mysql.start()

        def delete_data_redis(tables, kwargs):
            for table in tables:
                redis.hdel(table + '_' + str(month))
            logging.info("The Redis has been delete: %s", kwargs)

        t_redis = threading.Thread(target=delete_data_redis, args=(tables, kwargs,))
        t_redis.start()

        response = self.set_response(
            {'status': True, 'message': kwargs})
        return jsonify(response)


# 创建MR任务，调用yarn-mapreduce.sh
class CreateMrTask(Base):
    def __init__(self):
        super(CreateMrTask, self).__init__()
        self._mr_task_api = MrTaskApi()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        task_id = args["task_id"]
        jar_package = config.get("mr_task.jar_path") + args["jar_package"]
        main_class = args["main_class"]
        paramter = args["paramter"]
        info = dict(
            task_id=task_id,
            jar_package=jar_package,
            main_class=main_class,
            paramter=paramter
        )
        mr_create_result = self._mr_task_api.add_mr_task(**info)
        shell_command = "sh " + config.get('mr_task.shell_path') + "yarn-mapreduce.sh %s %s %s %s \"%s\"" % (
            config.get('user_name'),
            jar_package,
            main_class,
            task_id,
            paramter
        )
        logging.info("shell_command:%s" % shell_command)
        status, output = subprocess.getstatusoutput(shell_command)
        logging.info("\nstatus: %s\noutput: %s\n" % (status, output))
        res_status = True
        if status != 0:
            res_status=False
        result = self.set_response({'status': res_status, 'message': output})
        # if not mr_create_result:
        #     result = self.set_response({'status': False, 'message': output})
        # sh shell.sh create task
        # os.system('')
        return jsonify(result)


# 停止MR任务，调用kill_application.sh
class StopMrTask(Base):
    def __init__(self):
        super(StopMrTask, self).__init__()
        self._mr_task_api = MrTaskApi()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        task_id = args["task_id"]
        shell_command = 'sh ' + config.get('mr_task.shell_path') + 'kill_application.sh ' + config.get('user_name') + ' ' + task_id
        logging.info(shell_command)
        status, output = subprocess.getstatusoutput(shell_command)
        res_status = True
        if status == 1:
            res_status = False
        elif status == 0:
            data = dict(task_id=task_id, status=TaskStatus.Stopped)
            self._mr_task_api.update_mr_task(**data)
            # 更改数据库中任务的状态
        else:
            res_status = False
        response = dict(status=res_status, error_code=status, message=output)
        result = self.set_response(response)
        return jsonify(result)


# 查询MR任务，调用get_application_state.sh
class QueryMrTask(Base):
    def __init__(self):
        super(QueryMrTask, self).__init__()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        task_id = args["task_id"]
        shell_command = 'sh  ' + config.get('mr_task.shell_path') + 'get_application_state.sh ' + config.get('user_name') + ' ' + task_id
        logging.info(shell_command)
        status, output = subprocess.getstatusoutput(shell_command)
        # status得到的值是0或1,0---成功，1---失败，但是返回值需要True或False，res_status为True或False
        res_status = True
        # 返回值需要一个progress表示进度
        progress = 0
        # output得到的是一个字符串，如果成功是一个Start-Time : 1522740810372 Finish-Time : 1522740834617 Progress : 100% State : FINISHED
        # Final-State : SUCCEEDED 如果失败是错误提示信息，所以希望成功后把字符串转换成字典，得到Progress
        output_dict = {}
        if status != 0:
            res_status = False
            if status == 2:
                res_status = True
        else:
            output_list = output.split("\n")
            for out in output_list:
                key_value = out.split(":")
                key = key_value[0].strip()
                value = key_value[-1].strip()
                output_dict = dict(output_dict, **{key: value})
            progress = int(float(output_dict["Progress"][0:-1]))
        response = dict(status=res_status, error_code=status, message=output, result={'progress': progress})
        result = self.set_response(response)
        return jsonify(result)


# 落数据库，调用load_to_mysql.sh
class DataToDBTask(Base):
    def __init__(self):
        super(DataToDBTask, self).__init__()
        self._check_label_api = CheckLabelApi()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        source_path = args["source_path"]
        target_table = args["target_table"]
        sn_field = args["sn_field"]
        res_status = True
        shell_command = 'sh ' + config.get('mr_task.shell_path') + 'load_to_mysql.sh %s %s %s %s' % (
            config.get('user_name'), source_path, target_table, sn_field)
        logging.info(shell_command)
        status, output = subprocess.getstatusoutput(shell_command)
        if status != 0:
            res_status = False
        response = dict(status=res_status, error_code=status, message=output)
        result = self.set_response(response)

        def init_user_label(table_name):
            redis_config = config.get('jmdb_redis_config')
            redis_manager.register_db(redis_config)
            redis = redis_manager.get_client()
            user_labels_data = {}
            alldict = {}
            page_size = 500000
            if table_name == "tbl_user_lable_1m":
                user_labels_data = self._check_label_api.query_uesr_labels_1m()
            elif table_name == "tbl_user_lable_baby":
                user_labels_data = self._check_label_api.query_user_labels_baby()
            if user_labels_data.count != 0:
                count = user_labels_data.count()
                for page_index in range(count // page_size + 1):
                    user_labels = user_labels_data.slice(page_index * page_size, (page_index + 1) * page_size)
                    for uli in user_labels:
                        if str(uli.month) not in alldict:
                            alldict[str(uli.month)] = {}
                        alldict[str(uli.month)][uli.user] = uli.alltag
                    for month in alldict:
                        redis.hmset(table_name + '_' + str(month), alldict[month])
                        logging.info("talename: %s  month: %s  page_index: %s  " % (table_name, month, page_index))
                    del alldict
                    gc.collect()
                    alldict = {}
                    del user_labels
                    gc.collect()
                logging.info("----------- DataToDBTask END -----------")
            else:
                logging.info(" No data in %s  " % table_name)

        if target_table == "tbl_user_lable_1m" or target_table == "tbl_user_lable_baby":
            t = threading.Thread(target=init_user_label, args=(target_table,))
            t.start()

        return jsonify(result)


# 从Hdfs删除文件，调用delete_from_hdfs.sh
class DeleteFromHdfs(Base):
    def __init__(self):
        super(DeleteFromHdfs, self).__init__()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        delete_path = args["delete_path"]
        shell_command = 'sh ' + config.get('mr_task.shell_path') + 'delete_from_hdfs.sh ' + config.get('user_name') + ' ' + delete_path
        logging.info(shell_command)
        status, output = subprocess.getstatusoutput(shell_command)
        res_status = True
        if status != 0:
            res_status = False
        response = dict(status=res_status, error_code=status, message=output)
        result = self.set_response(response)
        return jsonify(result)
