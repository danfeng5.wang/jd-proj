from flask import request, jsonify

from controller.mr.base import Base


class CreateMrTaskResult(Base):
    def __init__(self):
        super(CreateMrTaskResult, self).__init__()

    def get(self):
        pass

    def post(self):
        args=request.get_json(True)
        response=dict(**args)
        data = self.set_response({'result': response})
        result = self.report_result(data, self.report_url_dict['create_job'])
        return jsonify(result)


class QueryMrTaskResult(Base):
    def __init__(self):
        super(QueryMrTaskResult, self).__init__()

    def get(self):
        pass

    def post(self):
        args=request.get_json(True)
        response=dict(**args)
        data = self.set_response({'result': response})
        result = self.report_result(data, self.report_url_dict['query_job'])
        return jsonify(result)


class DataToDBTaskResult(Base):
    def __init__(self):
        super(DataToDBTaskResult, self).__init__()

    def get(self):
        pass

    def post(self):
        args=request.get_json(True)
        response=dict(**args)
        data = self.set_response({'result': response})
        result = self.report_result(data, self.report_url_dict['data_to_db'])
        return jsonify(result)