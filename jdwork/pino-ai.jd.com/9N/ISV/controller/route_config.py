from controller.checklabel.checklabel import CheckLabel
from controller.checklabel.checklabel import InitUserLabel
from controller.mr.job.task_result import CreateMrTaskResult, QueryMrTaskResult, DataToDBTaskResult

__author__ = 'wanggangshan@jd.com'
from controller.mr.job.task_manage import CreateMrTask, QueryMrTask, StopMrTask, DataToDBTask, DeleteFromHdfs, DeleteDataMonth
from controller.transfer.ftp.ftps import TransferWithFtps,CallBackTransferResult
from controller.web.sql_manager import (
    SqlManagerAdd,
    SqlManagerQueryAll,
    SqlManagerQueryOne,
    SqlManagerDelete,
    SqlManagerUpdate,
    SqlManagerExec,
    MsqlDataDelete,
)



def route_init(app):

    app.add_url_rule('/v1/sql/manager/add', view_func=SqlManagerAdd.as_view("sql_manager_add"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/sql/manager/query/all',
                     view_func=SqlManagerQueryAll.as_view("sql_manager_query_all"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/sql/manager/query/one',
                     view_func=SqlManagerQueryOne.as_view("sql_manager_query_one"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/sql/manager/delete',
                     view_func=SqlManagerDelete.as_view("sql_manager_delete"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/sql/manager/update',
                     view_func=SqlManagerUpdate.as_view("sql_manager_update"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/sql/manager/exec',
                     view_func=SqlManagerExec.as_view("sql_manager_exec"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/mysql/data/delete',
                     view_func=MsqlDataDelete.as_view("nysql_data_delete"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/create/mr/task',
                     view_func=CreateMrTask.as_view("create_mr_task"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/stop/mr/task',
                     view_func=StopMrTask.as_view("stop_mr_task"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/query/mr/task',
                     view_func=QueryMrTask.as_view("query_mr_task"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/data/to/db',
                     view_func=DataToDBTask.as_view("data_to_db"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/create/mr/task/result',
                     view_func=CreateMrTaskResult.as_view("create_mr_task_result"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/query/mr/task/result',
                     view_func=QueryMrTaskResult.as_view("query_mr_task_result"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/data/to/db/result',
                     view_func=DataToDBTaskResult.as_view("data_to_db_task_result"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/delete/data/month',
                     view_func=DeleteDataMonth.as_view("delete_data_month"),
                     methods=['GET', 'POST'])


    app.add_url_rule('/v1/job/transfer/files',
                     view_func=TransferWithFtps.as_view("isv_transfer_file_task"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/callback/transfer/result',
                     view_func=CallBackTransferResult.as_view("callback_transfer_result"),
                     methods=['GET', 'POST'])


    app.add_url_rule('/v1/hdfs/files/delete',
                     view_func=DeleteFromHdfs.as_view("delete_from_hdfs"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/job/check/label',
                     view_func=CheckLabel.as_view("check_label"),
                     methods=['GET', 'POST'])

    app.add_url_rule('/v1/job/init/userlabel',
                     view_func=InitUserLabel.as_view("init_user_lable"),
                     methods=['GET', 'POST'])

