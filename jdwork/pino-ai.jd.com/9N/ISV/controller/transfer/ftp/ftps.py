from flask import request, jsonify

from controller.transfer.base import Base
from src.api.transfer_files import TransferApi
import uuid
import climate
from pyDes import des, CBC, PAD_PKCS5
import binascii

logging = climate.get_logger(__name__)


class TransferWithFtps(Base):
    def __init__(self):
        super(TransferWithFtps, self).__init__()
        self.__transfer_api = TransferApi()
        self.key = 'nhizuniv'  # 密码加密的key

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        kwargs = dict(args)
        task_id = str(uuid.uuid1())
        kwargs["task_id"] = task_id
        kwargs["status"] = 0
        kwargs["try_num"] = 0
        kwargs["callback_param"] = str(kwargs["callback_param"])
        en_password=kwargs['password']
        # 解密
        obj = des(self.key, CBC, self.key, pad=None, padmode=PAD_PKCS5)
        de_password = obj.decrypt(binascii.a2b_hex(en_password), padmode=PAD_PKCS5)
        kwargs['password'] = de_password
        del kwargs["token"]
        result = self.__transfer_api.add_transfer_task(**kwargs)
        logging.info('Created Task Success. ID: %s' % result.id)
        response = {
            "status": True,  # bool,  成功/失败 ，本次请求访问结果
            "result": '',  # 格式不定,具体参考指定接口说明，用于返回数据内容
            "message": "",  # str,   失败时可从该字段读取错误信息
            "error_code": 0,  # int,   【保留字段,忽略】 后续可用于错误类型分类
        }
        response = self.set_response(response)
        return jsonify(response)


class CallBackTransferResult(Base):
    def __init__(self):
        super(CallBackTransferResult, self).__init__()

    def get(self):
        pass

    def post(self):
        args = request.get_json(True)
        return jsonify(args)
