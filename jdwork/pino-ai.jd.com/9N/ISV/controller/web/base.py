#!/usr/bin/env python3
# Time: 2017/10/16 15:24
from collections import defaultdict

__author__ = 'wanggangshan@jd.com'


from flask.views import MethodView


class Base(MethodView):
    def __init__(self, db_session=None):

        pass

    @property
    def status(self):
        return self._status

    @property
    def result(self):
        return self._result

    @status.setter
    def status(self, values):
        self._status = values

    @result.setter
    def result(self, values):
        self._result = values

    def set_response(self, kwargs={}):
        response = defaultdict(list)
        response['status'] = kwargs.get('status', True)
        response['result'] = kwargs.get('result', {})
        response['message'] = kwargs.get('message', '')
        response['error_code'] = kwargs.get('error_code', 0)
        return response