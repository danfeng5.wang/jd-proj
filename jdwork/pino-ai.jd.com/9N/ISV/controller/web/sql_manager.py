#!/usr/bin/env python3
# Time: 2018/3/22 16:19

__author__ = 'wanggangshan@jd.com'

from flask import render_template, jsonify, request

from controller.web.base import Base
from src.api.sql_manager import SqlManagerApi

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)

import json


class SqlManagerAdd(Base):
    def __init__(self):
        super(SqlManagerAdd, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):

        job = request.get_json(force=True).get('job', {})
        api_name = job.get('api_name', '')
        sql = job.get('sql', '')
        note=job.get('note')

        if not (job and api_name and sql):
            response = self.set_response({'status': False, 'message': 'not useable job info'})
            return jsonify(response)
        if self.__sql_manager_api.query_sql_record_by_name(api_name):
            response = self.set_response(
                {'status': False, 'message': 'api name has exiest, please delete it first'})
            return jsonify(response)

        job['sql'] = json.dumps(job['sql'])
        status, msg = self.__sql_manager_api.add_sql_record(**job)
        response = self.set_response( {'status': status, 'message': msg})
        return jsonify(response)


class SqlManagerQueryAll(Base):
    def __init__(self):
        super(SqlManagerQueryAll, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):
        records = self.__sql_manager_api.query_all_records()
        response = self.set_response( {'status': True, 'result': {'data': records, 'count': len(records)}})
        return jsonify(response)


class SqlManagerQueryOne(Base):
    def __init__(self):
        super(SqlManagerQueryOne, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):
        api_name = request.get_json(force=True).get('api_name', '')
        if not api_name:
            response = self.set_response({'status': False, 'message': 'no api name for query'})
            return jsonify(response)
        record = self.__sql_manager_api.query_sql_record_by_name(api_name)
        response = self.set_response( {'status': True, 'result': {'data': [record], 'count': len(record)}})
        return jsonify(response)


class SqlManagerDelete(Base):
    def __init__(self):
        super(SqlManagerDelete, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):

        api_name = request.get_json(force=True).get('api_name', '')
        if not api_name:
            response = self.set_response(
                {'status': False, 'message': 'no api name for query'})
            return jsonify(response)
        status = self.__sql_manager_api.delete_sql_record_by_name(api_name)
        response = self.set_response( {'status': status})
        return jsonify(response)


class SqlManagerUpdate(Base):
    def __init__(self):
        super(SqlManagerUpdate, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):

        job = request.get_json(force=True).get('job', {})
        api_name = job.get('api_name', '')
        sql = job.get('sql', '')

        if not (job and api_name and sql):
            response = self.set_response(
                {'status': False, 'message': 'not useable job info'})
            return jsonify(response)
        if not self.__sql_manager_api.query_sql_record_by_name(api_name):
            response = self.set_response(
                {'status': False,
                 'message': 'api name has not exiest'})
            return jsonify(response)

        job['sql'] = json.dumps(job['sql'])
        status, msg = self.__sql_manager_api.update_sql_record(**job)
        response = self.set_response({'status': status, 'message': msg})
        return jsonify(response)


class SqlManagerExec(Base):
    def __init__(self):
        super(SqlManagerExec, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):

        api_name = request.get_json(force=True).get('api_name', '')
        args = request.get_json(force=True).get('args', {})
        if not api_name:
            response = self.set_response(
                {'status': False, 'message': 'not api name'})
            return jsonify(response)

        sql_info = self.__sql_manager_api.query_sql_record_by_name(api_name)
        if not sql_info:
            response = self.set_response(
                {'status': False,
                 'message': 'api name has not exiest'})
            return jsonify(response)

        exec_dict = {
            'api_name': api_name,
            'args': args,
            'sql': sql_info['sql'],
        }

        status, records = self.__sql_manager_api.exec_sql(**exec_dict)
        if status:
            response = self.set_response({'status': status,
                                          'result':
                                              {
                                                'data': records
                                              }
                                          })

            return jsonify(response)
        else:
            response = self.set_response(
                {'status': False,
                 'message': records})
            return jsonify(response)


class MsqlDataDelete(Base):
    def __init__(self):
        super(MsqlDataDelete, self).__init__()
        self.__sql_manager_api = SqlManagerApi()

    def post(self):

        kwargs = request.get_json(force=True)
        month = kwargs.get('month', 0)
        tables = kwargs.get('tables', [])

        if (not month) or (not tables):
            response = self.set_response(
                {'status': False, 'message': 'error input kwargs： %s' % kwargs})
            return jsonify(response)

        status, info = self.__sql_manager_api.delete_month_data(**kwargs)
        if status:
            response = self.set_response({'status': status, 'result': {'data': info}})
        else:
            response = self.set_response({'status': status, 'message': info})
        return jsonify(response)
