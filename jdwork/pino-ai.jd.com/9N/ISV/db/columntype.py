#!/usr/bin/env python3
# Time: 2018/1/4 16:25

__author__ = 'hejingjian@jd.com'


class TaskTopicType(object):
    cli = 1
    web = 2
    crm = 3

    @classmethod
    def trans_to_int(cls, topic_str):
        if topic_str.lower() == 'cli':
            return cls.cli
        if topic_str.lower() == 'web':
            return cls.web
        if topic_str.lower() == 'crm':
            return cls.crm

class TaskType(object):
    train = 1
    eval = 2
    predict = 3


class TaskStatus(object):
    Waiting = 0
    Fail = -1
    FinalFail = -2
    Success = 1
    Stopped=2

    @classmethod
    def Fails(cls):
        return [cls.Fail, cls.FinalFail]


class TaskPeriod(object):
    Waiting = 0
    Queued = 1
    Submitted = 2
    Running = 3
    Complete = 4


class UserSourceType(object):
    System = -1
    Web = 0
    WebErp = 1
    CliErp = 2
    WebPin = 3

    @classmethod
    def from_erp(cls):
        return [cls.WebErp, cls.CliErp]

    @classmethod
    def from_three(cls):
        return [cls.WebErp, cls.CliErp, cls.WebPin]

    @classmethod
    def from_web_three(cls):
        return [cls.WebErp, cls.WebPin]

    @classmethod
    def trans_type(cls, type):
        if type == 'sys':
            return cls.System
        if type == 'web':
            return cls.System
        if type == 'webErp':
            return cls.WebErp
        if type == 'cliErp':
            return cls.CliErp
        if type == 'WebPin':
            return cls.WebPin


class ActionType(object):
    download = 1
    upload = 2
    update = 3
    delete = 4


class EngineType(object):
    tensorflow = {
        '1.3.1': 'tensorflow-1.3.1',
        '1.4.0-rc1': 'tensorflow-1.4.0-rc1'
    }
    xgboost = {
        '0.6': 'xgboost-0.6'
    }
    pinoai = {
        '2.0': 'pinoai-2.0'
    }

    @classmethod
    def all(cls):
        return cls.tensflow.values() + cls.xgboost.values() + cls.pinoai.values()

    @classmethod
    def is_pino(cls, engine_name):
        return True if engine_name in cls.pinoai.values() else False



# class WebRunType(object):
#     k8s = 0
#     ip = 1
