# -*- coding: utf-8 -*-
# wanggangshan@jd.com

import os, sys

from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import TINYINT, INTEGER, BIGINT, FLOAT
from sqlalchemy import (
    VARCHAR, CHAR, SMALLINT, DATE, TEXT, TIMESTAMP,
    DATETIME, TIME, BOOLEAN, DECIMAL
)
from sqlalchemy.schema import Sequence
from sqlalchemy import func
from db.db import ModelBase

from utils.config_helper import config
from db.db import db_session_manager as db_manager

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)


class BaseModel(ModelBase):
    __abstract__ = True
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }


def to_dict(self):
    return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


# model表字段comment注释 需sqlalchemy1.2才支持，暂不使用。



class SqlManagerRecord(BaseModel):
    """
        sql管理表
    """
    __tablename__ = config.get('db_table.sql_manager')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 是否超级管理员: 0-不是, 1-是
    api_name = Column(VARCHAR(100), nullable=False, default='')
    sql = Column(VARCHAR(200), nullable=False, default='')
    note = Column(VARCHAR(200), nullable=False, default='')
    is_delete = Column(TINYINT(1), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False,
                        server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text(
        'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))

class TransferInfo(BaseModel):
    """
        id,
        username,
        password,
        ip,
        port,
        remote_path,
        callback_url,
        callback_params
    """
    __tablename__ = config.get('db_table.transfer_info')

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    task_id=Column(VARCHAR(200),nullable=False,default='')
    status=Column(INTEGER,nullable=False,default=0)
    try_num=Column(INTEGER,nullable=False,default=0)
    ip = Column(VARCHAR(50),nullable=False,default='')
    port = Column(BIGINT,nullable=False,default=0)
    username = Column(VARCHAR(100), nullable=False, default='')
    password = Column(VARCHAR(200), nullable=False, default='')
    remote_path = Column(VARCHAR(200), nullable=False, default='')
    callback_url = Column(VARCHAR(200), nullable=False, default='')
    callback_param = Column(VARCHAR(200), nullable=False, default='')

class MrTaskInfo(BaseModel):
    """
        id,
        task_id,
        try_num,
        jar_package,
        main_class,
        paramter,
        create_at
        update_at
    """
    __tablename__ = config.get('db_table.mr_task_info')

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    task_id = Column(VARCHAR(200), nullable=False, default='')
    status = Column(INTEGER, nullable=False, default=1)
    try_num = Column(INTEGER, nullable=False, default=1)
    jar_package = Column(VARCHAR(300), nullable=False, default='')
    main_class = Column(VARCHAR(300), nullable=False, default='')
    paramter = Column(VARCHAR(10240), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class TblUserLable1m(BaseModel):
    """
        id,
        user,
        alltag,
        month
        站外用户标签表
    """
    __tablename__ = config.get('db_table.tbl_user_lable_1m')

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user = Column(VARCHAR(255), nullable=False, default='')
    alltag = Column(VARCHAR(255), nullable=False, default='')
    month = Column(INTEGER, nullable=False, default=0)


class TblUserLableBaby(BaseModel):
    """
        id,
        user,
        alltag,
        month
        站内用户标签表
    """
    __tablename__ = config.get('db_table.tbl_user_lable_baby')

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user = Column(VARCHAR(255), nullable=False, default='')
    alltag = Column(VARCHAR(255), nullable=False, default='')
    month = Column(INTEGER, nullable=False, default=0)


#  not a real table
class DateDeleteUse(BaseModel):
    """
        just used to delete date when need delete somes tables' data by month
        
    """
    __tablename__ = 'test'

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    month = Column(INTEGER, nullable=False, default=0)
