#/bin/sh

echo "Usage: sh deploy/auto_deploy.sh isv pre/online"

delpoy_dir="$( cd "$( dirname "$0"  )" && pwd  )"
root_dir="$( dirname "${delpoy_dir}" )"

project_name=$1

init_conf () {
    sh $1/$2
}

run_branch=${delpoy_dir}/$1/$2
init_sh="init.sh"

echo ">>>>>>> run env init >>>>>>>: $1 $init_sh "
init_conf ${run_branch} ${init_sh}


python_cmd="/export/App/training_platform/anaconda3/bin/python"
gunconf="${delpoy_dir}/gunicorn_conf.py"


# ip.list format: ip:port:servers(delimited by commas)
ips=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $1;}'`)
ports=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $2;}'`)
mul_srvs=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $3;}'`)


deploy () {
    ip=${1}
    port=${2}
    srvs=(${3//,/ })
    remote_dir="/export/9n-servers/${project_name}/${port}"

    echo "deploy ${ip}:${port} begin"
    sed -i 's/server_name = .*/server_name = "'${srvs[0]}'"/g' $gunconf
    sed -i 's/ip = .*/ip = "'${ip}'"/g' $gunconf
    sed -i 's/port = .*/port = "'${port}'"/g' $gunconf

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${remote_dir}
            cd ${remote_dir} &&
            ls | grep -v logs | rm -rf &&
            mkdir -p logs"

    sshpass -p '0okm(IJN' scp -r ${root_dir}/*  admin@${ip}:${remote_dir}

    cur_time=`date +%Y_%m_%d_%H_%M_%S`
    log_file="${remote_dir}/logs/${ip}_${port}_${cur_time}.log"
    sshpass -p '0okm(IJN' ssh  admin@${ip} "/usr/sbin/lsof -i:${port}|grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {};
                cd ${remote_dir};
                nohup gunicorn -w 4 -t 600 -b ${ip}:${port} app_server:app > ${log_file} 2>&1 &"
    if [ $? = 0 ]; then
        echo "logfile: ${log_file}"
        echo "deploy successfully ^-^"
    else
        echo "deploy failed -_-"
    fi
}

for ((i=0;i<${#ips[@]};i++))
do
    deploy ${ips[i]} ${ports[i]} ${mul_srvs[i]}
done

