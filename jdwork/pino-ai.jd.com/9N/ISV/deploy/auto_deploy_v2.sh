#!/bin/sh

init_conf () {
    sh ./$1/$2/$3
    echo ">>>>>>> run env >>>>>>>: $1 $2"
}

delpoy_dir="$( cd "$( dirname "$0"  )" && pwd  )"
gunconf="${delpoy_dir}/gunicorn_conf.py"


run_project=$1
run_branch=$2

init_sh="init.sh"

init_conf $1 $2 $init_sh

# ip.list format: ip:port:servers(delimited by commas)
ips=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $1;}'`)
ports=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $2;}'`)
mul_srvs=(`grep -v -E "^#.*" ${delpoy_dir}/ip.list|awk -F ':' '{print $3;}'`)

# include deploy'configure with baseconf and ipconf.
# . ./base.ip.conf

stop_server () {
    ip=${1}
    port=${2}
    project_dir="/export/App/PinoAI_Application_Service/${port}"
    log_dir="logs"

    echo "deploy ${ip}:${port} stop"
    sshpass -p '0okm(IJN' ssh  admin@${ip} "/usr/sbin/lsof -i:${port}|grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}"

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${project_dir}
        cd ${project_dir}
        ls |grep -v ${log_dir} |xargs /bin/rm -rf"

    if [ $? = 0 ]
    then
        echo "stop server successed!! ^-^"
    else
        echo "stop server failed!! ^-^"
    fi
}

deploy () {
    ip=${1}
    port=${2}
    srvs=(${3//,/ })

    echo "deploy ${ip}:${port} begin"
    cur_time=$(date +%Y_%m_%d_%H_%M_%S)
    project_dir="/export/App/PinoAI_Application_Service/${port}"
    log_file="${project_dir}/logs/${ip}_${port}__${cur_time}.log"
    run_args="-i ${ip} -p ${port}"

    sed -i 's/server_name = .*/server_name = "'${srvs[0]}'"/g' $gunconf
    sed -i 's/ip = .*/ip = "'${ip}'"/g' $gunconf
    sed -i 's/port = .*/port = "'${port}'"/g' $gunconf

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${project_dir}
        cd ${project_dir}
        ls |grep -v logs |xargs /bin/rm -rf
        mkdir -p logs"

    sshpass -p '0okm(IJN' scp -r ../*  admin@${ip}:${project_dir}

    sshpass -p '0okm(IJN' ssh  admin@${ip} "cd ${project_dir}
        nohup gunicorn -w 4 -t 600 -b ${ip}:${port} app_server:app > ${log_file} 2>&1 &"

    if [ $? = 0 ]
    then
        echo "deploy successed!! ^-^"
    else
        echo "deploy failed!! ^-^"
    echo "logfile: ${log_file}"
    fi
}

get_servers=$(python redis_ips.py --func=get_queue --type=server)
echo 'get_servers' $get_servers
for ip_port_str in ${get_servers[@]}
do
    ip_port=(${ip_port_str//:/ })
    ip=${ip_port[0]}
    port_str=${ip_port[1]}
    port_list=(${port_str//,/ })
for port in ${port_list[@]}
do
    stop_server ${ip} ${port}
done
done

del_servers=$(python redis_ips.py --func=del_queue --type=server)
del_topic=$(python redis_ips.py --func=del_topic)


for ((i=0;i<${#ips[@]};i++))
do
    deploy ${ips[i]} ${ports[i]} ${mul_srvs[i]}
    ip_list=${ips[i]}':'${ports[i]}'|'${ip_list}
done


ip_list=${ip_list%?}
set_servers=$(python redis_ips.py --func=set_queue --type=server --args=$ip_list)

set_servers=$(python redis_ips.py --func=set_topic)

