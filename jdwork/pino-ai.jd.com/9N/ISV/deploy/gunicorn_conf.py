import os
import gevent.monkey
gevent.monkey.patch_all()

import multiprocessing

import os, sys

deploy_dir = os.path.dirname(os.path.abspath(__file__))
root_dir = os.path.dirname(deploy_dir)


debug = True
loglevel = 'debug'
bind = '0.0.0.0:8001'
pidfile = '%s/logs/gunicorn.pid' % root_dir
logfile = '%s/logs/debug.log' % root_dir

#启动的进程数
workers = multiprocessing.cpu_count() if multiprocessing.cpu_count() < 16 else 16
worker_class = 'gunicorn.workers.ggevent.GeventWorker'

x_forwarded_for_header = 'X-FORWARDED-FOR'
server_name = "isv"
ip = "10.181.59.19"
port = "6006"
