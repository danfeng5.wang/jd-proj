#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
DEFAULT_YML="$SCRIPT_DIR/default.yml"
IP_CONF="$SCRIPT_DIR/ip.list"
RUNNER_IP_CONF="$SCRIPT_DIR/runner.ip.conf"

cmd1="cp -f $DEFAULT_YML $SCRIPT_DIR/../../../etc/default.yml"
cmd2="cp -f $IP_CONF $SCRIPT_DIR/../../"
cmd3="cp -f $RUNNER_IP_CONF $SCRIPT_DIR/../../"

echo $cmd1
$cmd1
echo $cmd2
$cmd2
echo $cmd3
$cmd3
