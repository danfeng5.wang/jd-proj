#!/usr/bin/env python3
# Time: 2018/4/27 17:09
import argparse

import sys
sys.path.insert(0, '../')
import redis

from utils.config_helper import load_all_config, config

__author__ = 'hejingjian@jd.com'


load_all_config()
topic_set = config.get('topic_set')
servers_topic = config.get('servers_topic')
servers_set = servers_topic + config.get('servers_set_tag')
runners_set = servers_topic + config.get('runners_set_tag')

class _RedisManager(object):
    def __init__(self):
        self.__pool = {}

    def register_db(self, db_config, server_name='default'):
        if server_name in self.__pool:
            return True
        self.__pool[server_name] = redis.ConnectionPool(**db_config)

    def get_client(self, name='default'):
        pool = self.__pool[name]
        return redis.Redis(connection_pool=pool)


class _RedisOption(object):
    def __init__(self):
        redis_manager = _RedisManager()
        redis_config = config.get('redis_config')
        redis_manager.register_db(redis_config)
        self.redis_client = redis_manager.get_client()

    def get_queue(self, **kwargs):
        # length = self.redis_client.llen(kwargs['queue'])
        ips = self.redis_client.smembers(kwargs['set_key'])
        if ips:
            ips_list = []
            for ip in ips:
                ips_list.append(bytes.decode(ip))
            ips_str = " ".join(ips_list)
            return ips_str
        else:
            return 0

    def set_queue(self, **kwargs):
        print(kwargs)
        for ip in kwargs['args']:
            self.redis_client.sadd(kwargs['set_key'], ip)

    def del_queue(self, **kwargs):
        self.redis_client.delete(kwargs['set_key'])

    def set_topic(self, **kwargs):
        self.redis_client.sadd(topic_set, servers_topic)

    def del_topic(self, **kwargs):
        self.redis_client.srem(topic_set, servers_topic)


parser = argparse.ArgumentParser()

parser.add_argument(
    '--func',
    help='func')

parser.add_argument(
    '--type',
    default='',
    help='runner/server')

parser.add_argument(
    '--args',
    type=str,
    default='',
    help='args')

FLAGS = parser.parse_args()

_redis_option = _RedisOption()
if FLAGS.type == 'server':
    set_key = servers_set
else:
    set_key = runners_set

args = dict(func=FLAGS.func,
            set_key=set_key,
            args=FLAGS.args.split('|'),
            )

result = getattr(_redis_option, args['func'])(**args)
print(result)
