#!/bin/sh

init_conf () {
    sh ./$1/$2/$3
    echo ">>>>>>> run env >>>>>>>: $1 $2"
}

run_project=$1
run_branch=$2
init_sh="init.sh"

init_conf $1 $2 $init_sh

# include deploy'configure with baseconf and ipconf.
. ./runner.ip.conf


stop_runner () {
    ip=${1}
    topic=${2}
    project_dir="/export/App/PinoAI_Application_Service/${topic}-runner"

    echo "ps -ef |grep 'start_runner.py --topic='${2} | grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}"
    sshpass -p '0okm(IJN' ssh  admin@${ip} "ps -ef |grep 'start_runner.py --topic='${2} | grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}"

    if [ $? = 0 ]
    then
        echo "stop server successed!! ^-^"
    else
        echo "stop server failed!! ^-^"
    fi
}

deploy () {
    ip=${1}
    topic=${2}
    p_num=${3}
    c_num=${4}
    project_dir="/export/App/PinoAI_Application_Service"

    echo "deploy ${ip} begin"

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${project_dir}/${topic}-runner"
    sshpass -p '0okm(IJN' scp -r ../*  admin@${ip}:${project_dir}/${topic}-runner/

    echo "ps -ef |grep 'start_runner.py --topic='${2} | grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}"
    sshpass -p '0okm(IJN' ssh  admin@${ip} "ps -ef |grep 'start_runner.py --topic='${2} | grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}
        cd ${project_dir}/${topic}-runner
        mkdir -p ${log_dir}
        cd scripts && sh start_runner.sh ${topic} ${p_num} ${c_num}"

    if [ $? = 0 ]
    then
        echo "deploy successed!! ^-^"
    else
        echo "deploy failed!! ^-^"
    fi
}

get_runners=$(python redis_ips.py --func=get_queue --type=runner)
echo 'get_runners' get_runners
for ip_runner_str in ${get_runners[@]}
do
    ip_runner=(${ip_runner_str//:/ })
    ip=${ip_runner[0]}
    runner_topic_str=${ip_runner[1]}
    runner_topic=(${runner_topic_str//,/ })
    stop_runner ${ip} ${runner_topic[0]}
done

del_servers=$(python redis_ips.py --func=del_queue --type=runner)


for ip_args_str in ${ips[@]}
do
    ip_port=(${ip_args_str//:/ })
    ip=${ip_port[0]}
    args_str=${ip_port[1]}
    args=(${args_str//,/ })
    echo "deploy ${ip} ${args[0]} ${args[1]} ${args[2]}"
    deploy ${ip} ${args[0]} ${args[1]} ${args[2]}
    ip_runners=${ip}':'${args[0]}','${args[1]}','${args[2]}'|'${ip_runners}
done

ip_runners=${ip_runners%?}
set_runners=$(python redis_ips.py --func=set_queue --type=runner --args=$ip_runners)
