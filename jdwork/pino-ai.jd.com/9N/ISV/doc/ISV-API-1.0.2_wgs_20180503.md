# ISV WEB 接口 1.0 

> **authors:**   

>   wanggangshan@jd.com, hejingjian@jd.com, zhangxiaojiong@jd.com, denglei13@jd.com

##common部分

##### DOMAIN: http://apis.jd.com/
```
1、 久谦侧所有接口body需带有授权token信息
	{
		"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
		...
		...
	}

2、京东服务接口侧统一返回格式（json最外层）：

	{
	    "status": true,                                 # bool,  成功/失败 ，本次请求访问结果
		"result": int/list/dict/...,				    # 格式不定,具体参考指定接口说明，用于返回数据内容                           
	    "message": "",                                  # str,   失败时可从该字段读取错误信息
	    "error_code": 0,                                # int,   【保留字段,忽略】 后续可用于错误类型分类
	}

3、 path说明： /v1/isv/v1/...

    第一个v1为服务端网关版本，isv为本业务服务代号，第二个v1为本业务当前版本，后续升级延续 v2,v3...

4、 凡是供给久谦侧调用的接口，后面均有“【久谦侧调用】” 标识，回调久谦侧的均有【回调久谦】,其余为服务内部调用。

5、 回调相关： 返回由京东回调久谦的接口，均会采用 POST 方式， 参数均会放在body里以json方式调用。
```
&nbsp;
&nbsp;

### 第一部分: 【sql管理及Query数据】
```
业务说明：
	1、由京东平台产品侧定义一些接口， 也可由久谦定义，产品侧审核通过（接口含义：根据...条件 查询...数据）, 产品侧定义好接口名和对	应的mybatis语句.
    2、最后由久谦侧调用相应接口进行查询时，将需要的条件值组成数组传递过来。 由server端做最后的mybatis转义并拼接执行。

etc:
	功能描述：  查询 年龄，住址满足条件的人群的手机号, 去重，并以id倒序排列后去第100-199条。
	    假如最终完整sql:
		  select distinc phone_number from tablename where age=10 and phone_number=15010308562;

	1、京东产品/久谦， 录入接口:
		接口名： queryXXX
		mybatis样本:
			"<script>select DISTINCT name from singer_info where age=#{age} and phone_number=#{phone_number};</script>"

		ps:  注意， baitis语句中表名或字段名 不要用sql里的那种反引号‘·’括起来。

	2、久谦侧调用queryXXX 传递myvatis样本'{}'中的keys及其值，：
		json args:  {"age": 10, "phone_number": 15010308562，"": "", "":""}


建议：
	制作mybatis样本时请充分结合表结构，业务数据类型特征，采用性能较优的语法，如where条件的先后顺序等
```

&nbsp;
### 1、【sql管理】【增加】业务服务sql接口

#### 接口说明:  新增查询接口名和对应的sql语句,
#### path:  /v1/isv/v1/sql/manager/add
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"job": {
		"api_name": "",						     # str, 后续用于查询相关sql的key, e.g： "query_five_days_sales_data"
		"sql": "",      						    # str, mybatis,  e.g:  "<script>select DISTINCT name from singer_info where age=#{age} and phone_number=#{phone_number};</script>"
		"note": ""								  # str, 接口的备注信息
	}
}

产品侧sql录入参考样例：

```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": {}, 								  # dict, 【忽略】,在其他接口中用于返回数据内容
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```

&nbsp;

### 2、【sql管理】【查询全部】业务服务sql接口列表

#### 接口说明:  查询当前所有生效的接口信息
#### path:  /v1/isv/v1/sql/manager/query/all
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
}
```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": { 								    # dict, 查询到的所有可用sql记录
		"data": [								   # list, 数据数组
			{
				"api_name": "",					 # str， 接口sql名称
				"sql": "",	   				   # str， sql内容
				"note": "" 				   	  # str, 接口的备注信息
			},
			{}
		]
		"count": 99,								# int, 总条数
	}
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```


### 3、【sql管理】【查询单条】业务服务sql接口内容

#### 接口说明:  通过接口名查询单条接口详情
#### path:  /v1/isv/v1/sql/manager/query/one
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"api_name": "", 						        # str, 后续用于查询相关sql的key, e.g： "query_five_days_sales_data"
}
```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": { 								    # dict, 查询到的所有可用sql记录
		"data": [								   # list, 数据数组
			{
				"api_name": "",					 # str， 接口sql名称
				"sql": "",					      # str， sql内容
				"note": ""					      # str, 接口的备注信息
			},
			{}
		]
		"count": 1,								 # int, 条数, 0/1
	}
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```


&nbsp;

### 4、【sql管理】【删除】业务服务sql接口

#### 接口说明:  通过接口名删除(假删)接口记录
#### path:  /v1/isv/v1/sql/manager/delete
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"api_name": ""  						        # str, 要删除sql的job name, e.g： "query_five_days_sales_data"
}
```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": {}, 								  # dict, 【忽略】,在其他接口中用于返回数据内容
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```


&nbsp;
### 5、【sql管理】【修改】业务服务sql接口

#### 接口说明:  修改已存在接口名的sql语句,但不能修改接口名
#### path:  v1/isv/v1/sql/manager/update
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"job": {
		"api_name": "",  						   # str, 要修改的接口名, e.g： "query_five_days_sales_data"
		"sql": ""       						    # str, 合法mybatis语句,  e.g:  "<script>select DISTINCT name from singer_info where age=#{age} and phone_number=#{phone_number};</script>"
	}
}
```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": {}, 								  # dict, 【忽略】,在其他接口中用于返回数据内容
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```



&nbsp;
### 6、【sql管理】【执行】业务服务sql接口 【久谦侧调用】

#### 接口说明:  久谦执行接口
#### path:  /v1/isv/v1/sql/manager/exec
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"api_name": "",  						       # str, 要执行接口名, e.g： "query_five_days_sales_data"
    "args": {"k1": "v1", "k2": "v2"}	   	     # list, 要传递的填空mybatis模板的值，模板里{x}里的x均要在json里提供

}

```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": {									 # dict,  查询结果
		"data": [[], []],					       # list/str/int,  查询结果，根据sql查询内容原类型返回
	},
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```

&nbsp;
### 7、【sql管理】【删除mysql数据】删除已入库指定月份mysql表数据 【久谦侧调用】

#### 接口说明:  久谦执行接口
#### path:  /v1/isv/v1/mysql/data/delete
#### method:  POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75",    # str, 统一接口验证token 32位uuid,定值
	"month": 201805,  						      # int, 要删除的数据的指定月份
    "tables": ["table1", "table2"]	   	       # list, 要删除的月份数据所设计的表名列表

}

```

#### 出参

```
{
	"status": true,                    			 # bool,  成功/失败
	"result": {}，								  # dict, 结果，默认为空{}
	"message": "",								  # str,   失败时可从该字段读取错误信息
	"error_code": 0,							    # int,   【保留字段,忽略】 后续可用于错误类型分类
}
```
&nbsp;

## 第二部分： 【FTPS文件传输】 
	业务说明：
		1、久谦平台部分调用接口传递可以传输文件的通知，京东侧同步返回一个收到通知回应，然后开始异步从FTPS服务器扫描并下载文件，重命名文件为done，并最终提交hdfs存储。
		2、京东侧最终将本次文件下载、上传hdfs的流程结果，通过调用久谦的在1中提供的回传结果，返回相应的结果信息，通知久谦下载结果。
	etc:
		功能介绍：
			完成从FTPS服务器上传输文件到本地，然后将文件上传到HDFS上面。

		密码部分说明：
			采用Python中Crypto.Cipher.DES加密解密方法，公共秘钥规定为'nhizuniv',密码长度必须为8的整数倍。
		
		python代码样例:

			from binascii import b2a_hex, a2b_hex
			from Crypto.Cipher import DES
			key = 'nhizuniv' #长度必须是8位的
			password = '1234567812345678' #长度必须是8的倍数
			# 实例化
			obj = DES.new(key)
			# 加密
			cryp = obj.encrypt(password)
			pass_hex = b2a_hex(cryp)
			print(pass_hex)
			print('=' * 20)
			# 解密
			get_cryp = a2b_hex(pass_hex)
			after_password = obj.decrypt(get_cryp)
			print(after_password)
	
		

### 1、【下载】基于FTPS下载文件

#### 接口说明：基于FTPS协议从服务器下载文件,久谦上传文件完毕后通知结果,
#### path：/v1/isv/v1/job/transfer/files
#### method: POST

#### 文件可下载通知

#### 入参
	{
		"token": "88d84d442cfb11e8949cfa163ed5cc75",     		 # str, 统一接口验证token 32位uuid,定值
		"ip": "10.12.128.207",                                    #str, 服务器端ip地址
		"port":22,												#int, 端口号
		"username": "denglei",									#str,  用户名
		"password": "11111111",									 #str, 密码，需要协定加密和解密策略
		"remote_path": "/a/b",									#str,  服务器端需要下载文件所在目录
		"callback_url":""										 #str, 异步回调的url，任务完成后异步通知结果
		"callback_param": {"":""},								#json,非必填,回调时传回的参数
	}

#### 出参

	{
	    "status": true,                                 	 # bool,  成功/失败 ，本次请求访问结果
	    "result": '',								        # 格式不定,具体参考指定接口说明，用于返回数据内容                           
	    "message": "",                                  	 # str,   失败时可从该字段读取错误信息
	    "error_code": 0,                                	 # int,   【保留字段,忽略】 后续可用于错误类型分类
	}


### 2、【回调】下载完毕后回调接口(京东请求，久谦响应)

#### 接口说明：完成从FTPS服务器上下载文件后回调久谦提供的callback_url
#### callback_url:从上面下载的请求中获取
#### method：POST

#### 下载任务完成回调


#### 入参

	{
		"status": true/false,									#bool,   下载是否成功
		"message": "",										   #str,		描述信息
	}


#### 出参
	{
		"status": true/false								     # 响应结果 
	}




&nbsp;
&nbsp;
## 第三部分： 【MR任务/数据处理/入库】
```
业务说明：
	1、由京东平台产品侧定义一些接口，也可由久谦定义，执行MR任务，停止MR任务，查询MR任务进度，将数据导入Mysql数据库，删除hdfs上的过期数据。

etc:
	执行MR任务，每次只能提交一个job，且job的id为32位UUID，以后的停止和查询都要依据此job id，
	此job id需要由久谦的java程序接收，并设置任务ID。

	数据导入Mysql数据库，数据文件的序号是从1开始计数，可以有些列不导入，列的序号可以乱序，不要求必须从大到小。

	删除hdfs上的过期数据，如果删除的是目录，路径后面需要加"/"。
```

### 1、【创建】创建mr任务【久谦侧调用】

#### 接口说明：创建mr任务
#### path：/v1/isv/v1/create/mr/task
#### method: POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"                # str, 统一接口验证token 32位uuid,定值
	"callback_url": "",										# str,非必填,回调地址
	"callback_param": {"":""},								 # json,非必填,回调时传回的参数
	"jar_package": "",										 # str,必填,java的jar包文件名
	"main_class": "",										  # str,必填,执行的主类名称
	"task_id": "",											 # str,必填,任务id，确认任务唯一性标识(32位UUID)
	"paramter": "",											# str,必填,任务中程序需要的参数，整个参数列表为空格分隔的字符串。
}

```

#### 例如
```
注: 每一个MR任务只能包含一个job和一个任务ID，此参数设置job name，不能重复
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"
	"callback_url": "",
	"callback_param": {"":""},
	"jar_package": "statperson.jar",
	"main_class": "BuyOnline",
	"task_id": "pcf3b6efea994b418e5c1c851410d476",
	"paramter": "pcf3b6efea994b418e5c1c851410d476 5 online /tmp/input /tmp/output"
}
```

#### 出参

```
{
	"status": ture/false,										 #bool,Ture/False
	"error_code": 1,											  #int,异常码
	"message": "",												#str,异常描述
	"result": '',								    			 # 格式不定,具体参考指定接口说明，用于返回数据内容  
}
```


&nbsp;
### 2、【停止】停止mr任务【久谦侧调用】

#### 接口说明：停止mr任务
#### path：/v1/isv/v1/stop/mr/task
#### method: POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"     			# str, 统一接口验证token 32位uuid,定值
	"callback_url": "",										 # str,非必填,回调地址
	"callback_param": {"":""},								  # json,非必填,回调时传回的参数
	"task_id": "",											  # str,必填,任务id.uuid,确认任务唯一性标识
}

```

#### 出参

```
{
	"status": ture/false,										# bool,Ture/False
	"error_code": 1,											 # int,异常码
	"message": "",											   # str,异常描述
	"result": '',								   			 # 格式不定,具体参考指定接口说明，用于返回数据内容  
}
```


&nbsp;
### 3、【查询】查询mr任务进度【久谦侧调用】

#### 接口说明：查询mr任务进度
#### path: /v1/isv/v1/query/mr/task
#### method: POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"    			 # str, 统一接口验证token 32位uuid,定值
	"callback_url": "",										 # str,非必填,回调地址
	"callback_param": {"":""},								  # json,非必填,回调时传回的参数
	"task_id": "",											  # str,必填,任务id.uuid,确认任务唯一性标识
}

```

#### 出参

```
{
	"status": ture/false,										# bool,Ture/False
	"error_code": 1,											 # int,异常码
	"message": "",											   # str,异常描述
	"result": {												  # 格式不定,具体参考指定接口说明，用于返回数据内容  
		"progress": 60,									  	# int,数值,例60代表60%
	}，								    
	
}
```


&nbsp;
### 4、【查询】mr任务结果汇报 【回调久谦】

#### 接口说明：mr任务结果汇报
#### callback_url:从上面下载的请求中获取 
#### method: POST


#### 入参
```
{
	"status": ture/false,									 # bool, mr任务执行结果
	"task_id": "",											# str,必填,任务id.uuid,确认任务唯一性标识
}

```

#### 出参

```
{
	"status": ture/false,									 # bool,Ture/False
}
```


&nbsp;
### 5、【增加】结果数据入库 【久谦侧调用】

#### 接口说明：结果数据入库
#### path: /v1/isv/v1/data/to/db
#### method: POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"                # str, 统一接口验证token 32位uuid,定值
	"callback_url": "",										# str, 非必填,回调地址
	"callback_param": {"":""},								 # json, 非必填,回调时传回的参数
	"source_path": "",										 # str, 数据源目录
	"target_table": "",										# str, 数据库表名
	"sn_feild": "",											# str, 文件列号与数据库字段名对应列表，字段列从1开始排序，文件列号与数据库字段用冒号分隔，多个之间用逗号分隔，如 3:feild1,5:feild2,1:feild3
}

```

#### 例如:
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"
	"callback_url": "",
	"callback_param": {"":""},
	"source_path": "/user/username/data/output/20180330",
	"target_table": "DB_TableName",
	"sn_feild": "3:feild1,5:feild2,1:feild3"
}
```


#### 出参

```
{
	"status": ture/false,										# bool, Ture/False
	"error_code": 1,											 # int, 异常码
	"message": "",											   # str, 异常描述
	"result": '',								   			 # 格式不定,具体参考指定接口说明，用于返回数据内容  

}
```


### 6、【删除】删除hdfs上的文件或目录【久谦侧调用】

#### 接口说明：删除hdfs上的文件或目录
#### path: /v1/isv/v1/hdfs/files/delete
#### method: POST

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"           # str, 统一接口验证token 32位uuid,定值
	"callback_url": "",								   #str,非必填,回调地址
	"callback_param": {"":""},							#json,非必填,回调时传回的参数
	"delete_path": "",									#str,要删除的数据目录或文件名，如果为目录必须以 "/" 结尾
}

```

#### 例如:
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"
	"callback_url": "",
	"callback_param": {"":""},
	"delete_path": "/user/username/data/output/20180330/"
}
```

#### 出参

```
{
	"status": ture/false,										 # bool,Ture/False
	"error_code": 1,											  # int,异常码
	"message": "",												# str,异常描述
	"result": ''，								   			 # 格式不定,具体参考指定接口说明，用于返回数据内容  
}
```

&nbsp;
### 7、【查询】人群创建检查接口【久谦侧调用】

#### 接口说明：创建mr任务前，通过人群标签获取满足标签的人群数量
#### path：/v1/isv/v1/job/check/label
#### method: POST
#### 备注：label_json参数定义，每一个大括号代表一种大标签类型，大标签类型之间是交集的关系，大括号内通过标识字段（and和or）判断，and-代表交集，or-代表并集

#### 入参
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"     			   #str, 统一接口验证token 32位uuid,定值
	"callback_url": "",											#str,非必填,回调地址
	"callback_param": {"":""},									 #json,非必填,回调时传回的参数
	"crowd_type": "",									          #int,人群标识，1-站内人群 1m，2-站外人群 baby
	"month": "",									          	 #str,月份 
	"label_json": {
					"": {"": []},
					"": {"": []}, ...	
					},											#json,必填,标签参数
}

```
#### 例如:
```
{
	"token": "88d84d442cfb11e8949cfa163ed5cc75"
	"callback_url": "",
	"callback_param": {"":""},
	"label_json":  {
					"3": {"or": [5, 4]}, 
					"4": {"and": [2]}, 
					"6": {"and": [1]},
				}
}
```

#### 出参

```
{
	"status": ture/false,									   #bool,Ture/False
	"error_code": 1,											#int,异常码
	"message": "",											  #str,异常描述
	"result": {
	    "count": 1000
	    },											  #int,必填,满足标签的人群数量
}
```