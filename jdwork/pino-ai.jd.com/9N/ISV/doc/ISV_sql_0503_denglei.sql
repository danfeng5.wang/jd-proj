

CREATE DATABASE IF NOT EXISTS isv_pre DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE isv_pre;

DROP TABLE IF EXISTS `sql_manager`;
CREATE TABLE `sql_manager` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `api_name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'sql接口名', 
  `sql` VARCHAR(200) NOT NULL DEFAULT '' COMMENT 'sql接口内容',
  `note` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '备注',
  `is_delete` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1：已删除, 0：未删除',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='sql管理表';

DROP TABLE IF EXISTS `transfer_info`;
CREATE TABLE `transfer_info`(
   `id` BIGINT UNSIGNED AUTO_INCREMENT,
   `task_id` VARCHAR(200) NOT NULL,
   `status` INTEGER,
   `try_num` INTEGER,
   `username` VARCHAR(100) NOT NULL,
   `password` VARCHAR(200) NOT NULL,
   `ip` VARCHAR(50) NOT NULL,
   `port` BIGINT,
   `remote_path` VARCHAR(200) NOT NULL,
   `callback_url` VARCHAR(200) NOT NULL,
   `callback_param` VARCHAR(200) NOT NULL,
   PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='启动ftps任务和相关信息表';

DROP TABLE IF EXISTS `mr_task_info`;
CREATE TABLE `mr_task_info`(
   `id` BIGINT UNSIGNED AUTO_INCREMENT,
   `task_id` VARCHAR(200) NOT NULL,
   `status` INTEGER,
   `try_num` INTEGER,
   `jar_package` VARCHAR(300) NOT NULL,
   `main_class` VARCHAR(300) NOT NULL,
   `paramter` VARCHAR(500) NOT NULL,
   `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
   PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='创建mr任务相关信息表';


---demo table

DROP TABLE IF EXISTS `singer_info`;
CREATE TABLE `singer_info` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '姓名', 
  `city` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '城市', 
  `phone_number` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '手机号',
  `age` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='歌手表';


insert into singer_info (name, city, phone_number, age) values('aaa', 'beijing', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('bbb', 'beijing', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('ddd', 'beijing', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('34', 'beijing', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('aa342a', 'asdas', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('wgs', 'shanghai', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('wgs', 'shanghai', '15010000000', 33);
insert into singer_info (name, city, phone_number, age) values('wgs', 'shanghai', '15010000000', 444);
insert into singer_info (name, city, phone_number, age) values('asdasd', 'shanghai', '15010000000', 55);
insert into singer_info (name, city, phone_number, age) values('dsfs', 'shanghai', '15010000000', 22);
insert into singer_info (name, city, phone_number, age) values('fff', 'shanghai', '15010000000', 55);
insert into singer_info (name, city, phone_number, age) values('gg', 'shanghai', '15010000000', 20);
insert into singer_info (name, city, phone_number, age) values('ggg', 'shanghai', '15010000000', 20);
