#!/usr/bin/env python3
# Time: 2018/3/23 22:17
import traceback
from db.db import redis_manager
#from lookalike.lookalike_auto_9n import lookalike_extend_by_times
from src.api.mr_task import MrTaskApi
import subprocess
import time
import climate

climate.enable_default_logging()
logging = climate.get_logger(__name__)

from runners.base import RunnerBase
from utils.config_helper import config


class MrTaskRunner(RunnerBase):
    def __init__(self, **kwargs):
        super(MrTaskRunner, self).__init__(**kwargs)
        self.__mr_task_api = MrTaskApi()

    def excute_create_mr_task_shell(self,params):
        """
        例如: sh yarn-mapreduce.sh test_9n loaddata.jar doLoad XXXXXXXXXX "parm1 parm2 parm3 parm4"
          sh yarn-mapreduce.sh test_9n /mnt/mfsb/pino/test/UnboundedDMP/tools/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar pi 553651f44dc74d0eba0ae7e79ea09087 "6 10"
        :param params:
        :return:
        """
        task_id=params["task_id"]
        jar_package=config.get("mr_task.jar_path")+params["jar_package"]
        main_class=params["main_class"]
        paramter=params["paramter"]
        callback_url=params.get("callback_url", None)
        shell_command="sh " + config.get('mr_task.shell_path') + "yarn-mapreduce.sh %s %s %s %s \"%s\"" % (
            config.get('user_name'),
            jar_package,
            main_class,
            task_id,
            paramter
        )
        print("shell_command:%s"%shell_command)
        status,output=subprocess.getstatusoutput(shell_command)
        print("\nstatus: %s\noutput: %s\n"%(status,output))
        res_status=True
        if status==1:
            res_status=False
        response=dict(status=res_status,error_code=status,message=output,result='')
        if callback_url is None:
            logging.info('Callback url is None!')
        else:
            self.report_result(response,callback_url)

    def _execute(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        run_params = {}
        run_params['task_id'] = self._task_info.task_id
        run_params['jar_package'] = self._task_info.jar_package
        run_params['main_class'] = self._task_info.main_class
        run_params['paramter'] = self._task_info.paramter
        # run_params['callback_url']=self._task_info.callback_url
        logging.info('[Mr Task] started... run_params: %s', run_params)
        self.excute_create_mr_task_shell(run_params)
        #lookalike_extend_by_times(run_params)
        # except Exception as e:
        #     logging.info('[Consumer Error] Generalize Task Data:%s. Error Message: %s.', self._task_info, e)

    def __get_redis_client(self):
        redis_config = config.get('jmdb_redis_config')
        redis_manager.register_db(redis_config, 'MrTask')
        redis_client = redis_manager.get_client('MrTask')
        return redis_client

    def setup(self, **kwargs):
        self._redis = self.__get_redis_client()
        self._max_length = config.get('redis_mr_task.max_length')
        self._queue = config.get('redis_mr_task.name')
        try:
            self._redis = self.__get_redis_client()
        except Exception as e:
            logging.error('Error: failed to connect Redis: %s，Exception: %s',
                          config.get('redis'), traceback.format_exc())
            return False
        return True

    def _feed_many(self, **kwargs):
        feed_num = self._max_length - self._cur_queue_len()
        if feed_num <= 0:
            return [], 0
        try_limit = config.get('try_limit.runner.mr_task')
        tasks = self.__mr_task_api.feed_many_waiting_tasks(try_limit, feed_num)
        queue_tasks = []
        new_num = 0
        for task in tasks:
            try:
                self._redis.lpush(self._queue, task.task_id)
                queue_tasks.append(task)
                logging.info('New task %s are pushed to queue.', task.task_id)
                new_num += 1
            except Exception as e:
                logging.error('Error: redis task queue add failed: %s. ErrorInfo: %s',
                              task.task_id, traceback.format_exc())
                self.__mr_task_api.recover_queued_to_waitting(task.task_id)
                continue
        return queue_tasks, new_num

    def _feed(self, **kwargs):
        logging.name = __name__ + '.Producer-%d' % self.index
        while True:
            if self._cur_queue_len() >= self._max_length:
                # 当前redis队列已满
                time.sleep(60)
            else:
                tasks, feed_num = self._feed_many(**kwargs)
                if not tasks:
                    # 没有任务新加入队列
                    logging.info('[MrTaskRunner-Producer] no tasks to queue')
                    time.sleep(10)
                    continue

    def _cur_queue_len(self):
        try:
            length = self._redis.llen(self._queue)
            return length
        except Exception as e:
            logging.error('Error: query redis queue length failed: %s.',
                          traceback.format_exc())
            return self._max_length + 1

    def _fetch_one(self, **kwargs):
        logging.info('[Fetch One]')
        task_id = None
        try:
            while True:
                task_id = self._redis.rpop(self._queue)
                if not task_id:
                    logging.warning('No task fetched.')
                    return None
                else:
                    result = self.__mr_task_api.query_task_by_task_id(task_id)
                    if not result:
                        self._redis.lpush(self._queue, task_id)
                        logging.error('Redis has task but mysql not exiests!'
                                      ' Task_id: %s, Poped; db result: %s', task_id, result)
                        continue
                    logging.info('Pop task %s', task_id)
                    break
            return task_id
        except Exception as e:
            logging.error('Error: %s.', traceback.format_exc())
            return None

    def _fetch(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        while True:
            time.sleep(2)
            if not self._cur_queue_len():
                time.sleep(10)
                continue
            task_id = self._fetch_one(**kwargs)
            if not task_id:
                time.sleep(5)
                logging.info('[MrTaskRunner-Consumer] no tasks to queue')
                continue
            logging.info('[MrTaskRunner-Consumer] new tasks to queue: %s', task_id)

            self._task_info = self.__mr_task_api.query_task_by_task_id(task_id)
            self._pipeline(**kwargs)