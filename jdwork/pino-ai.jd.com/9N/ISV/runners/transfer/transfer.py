#!/usr/bin/env python3
# Time: 2018/3/23 22:17
import traceback
from db.db import redis_manager
from transfer.ftps import Ftps
from src.api.transfer_files import TransferApi

__author__ = 'hejingjian@jd.com'

import time
import climate

climate.enable_default_logging()
logging = climate.get_logger(__name__)

from runners.base import RunnerBase
from utils.config_helper import config


class TransferRunner(RunnerBase):
    def __init__(self, **kwargs):
        super(TransferRunner, self).__init__(**kwargs)
        self.__transfer_api = TransferApi()

    def _execute(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        run_params = {}
        run_params['username'] = self._task_info.username
        run_params['password'] = self._task_info.password
        run_params['ip'] = self._task_info.ip
        run_params['port'] = self._task_info.port
        run_params['remote_path'] = self._task_info.remote_path
        run_params['callback_url'] = self._task_info.callback_url
        run_params['callback_param'] = self._task_info.callback_param
        run_params['task_id'] = self._task_info.task_id
        logging.info('[Transfer Task] started... run_params: %s', run_params)
        ftps = Ftps(**run_params)
        status, output = ftps.transfer_files_from_ftps_to_hdfs()
        res_status = True
        if status == 1:
            res_status = False
        data = dict(status=res_status, message=output)
        try:
            self.report_result(data=data, url_rule=run_params['callback_url'])
        except KeyError as e:
            logging.error(e)

    def __get_redis_client(self):
        redis_config = config.get('jmdb_redis_config')
        redis_manager.register_db(redis_config, 'Transfer')
        redis_client = redis_manager.get_client('Transfer')
        return redis_client

    def setup(self, **kwargs):
        self._redis = self.__get_redis_client()
        self._max_length = config.get('redis_transfer.max_length')
        self._queue = config.get('redis_transfer.name')
        try:
            self._redis = self.__get_redis_client()
        except Exception as e:
            logging.error('Error: failed to connect Redis: %s，Exception: %s',
                          config.get('redis'), traceback.format_exc())
            return False
        return True

    def _feed_many(self, **kwargs):
        feed_num = self._max_length - self._cur_queue_len()
        if feed_num <= 0:
            return [], 0
        try_limit = config.get('try_limit.runner.transfer')
        tasks = self.__transfer_api.feed_many_waiting_tasks(try_limit, feed_num)
        queue_tasks = []
        new_num = 0
        for task in tasks:
            try:
                self._redis.lpush(self._queue, task.task_id)
                queue_tasks.append(task)
                logging.info('New task %s are pushed to queue.', task.task_id)
                new_num += 1
            except Exception as e:
                logging.error('Error: redis task queue add failed: %s. ErrorInfo: %s',
                              task.task_id, traceback.format_exc())
                self.__transfer_api.recover_queued_to_waitting(task.task_id)
                continue
        return queue_tasks, new_num

    def _feed(self, **kwargs):
        logging.name = __name__ + '.Producer-%d' % self.index
        while True:
            if self._cur_queue_len() >= self._max_length:
                # 当前redis队列已满
                time.sleep(60)
            else:
                tasks, feed_num = self._feed_many(**kwargs)
                if not tasks:
                    # 没有任务新加入队列
                    logging.info('[TransferRunner-Producer] no tasks to queue')
                    time.sleep(10)
                    continue

    def _cur_queue_len(self):
        try:
            length = self._redis.llen(self._queue)
            return length
        except Exception as e:
            logging.error('Error: query redis queue length failed: %s.',
                          traceback.format_exc())
            return self._max_length + 1

    def _fetch_one(self, **kwargs):
        logging.info('[Fetch One]')
        task_id = None
        try:
            while True:
                task_id = self._redis.rpop(self._queue)
                if not task_id:
                    logging.warning('No task fetched.')
                    return None
                else:
                    result = self.__transfer_api.query_task_by_task_id(task_id)
                    if not result:
                        self._redis.lpush(self._queue, task_id)
                        logging.error('Redis has task but mysql not exiests!'
                                      ' Task_id: %s, Poped; db result: %s', task_id, result)
                        continue
                    logging.info('Pop task %s', task_id)
                    break
            return task_id
        except Exception as e:
            logging.error('Error: %s.', traceback.format_exc())
            return None

    def _fetch(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        while True:
            time.sleep(2)
            if not self._cur_queue_len():
                time.sleep(10)
                continue
            task_id = self._fetch_one(**kwargs)
            if not task_id:
                time.sleep(60)
                logging.info('[TransferRunner-Consumer] no tasks to queue')
                continue
            logging.info('[TransferRunner-Consumer] new tasks to queue: %s', task_id)

            self._task_info = self.__transfer_api.query_task_by_task_id(task_id)
            self._pipeline(**kwargs)
