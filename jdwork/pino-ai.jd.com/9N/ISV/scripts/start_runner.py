# -*- coding: utf-8 -*-

__author__ = 'penghao5@jd.com'

import os
import sys
import argparse
import pymysql

ex_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.insert(0, ex_path)
sys.path.insert(0, '..')
sys.path.insert(0, '../libs')
os.environ['HADOOP_CONF_DIR'] = '/export/hadoop-2.7.1-admin/hadoop_conf'
os.environ['HIVE_CONF_DIR'] = '/export/servers/software/jdhive-2.0.0-HADOOP-2.7.1/hive_conf'
from utils.config_helper import load_all_config, config
from db.db import db_session_manager as db_manager
load_all_config()
from runners.base import RunnerBase
# import exocoetidae as ex
db_cfg = config.get('mysql_db')
db_manager.register_db(db_cfg, 'default')

parser =argparse.ArgumentParser()

parser.add_argument(
    '--topic',
    default='Test',
    help='Runner topic, [Test/CRM]')

parser.add_argument(
    '--role',
    help='Producer number, [p/c]')

parser.add_argument(
    '--index',
    type=int,
    help='Consumer number, [0/1/2/...]')

FLAGS = parser.parse_args()

if __name__ == '__main__':
    runner = RunnerBase.build(
        FLAGS.topic + 'Runner',
        **dict(role=FLAGS.role,
        index=FLAGS.index))
    runner.setup()
    runner.start()
