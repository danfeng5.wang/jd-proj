
topic=${1}
p_num=${2}
c_num=${3}
python_cmd="/export/App/training_platform/anaconda3/bin/python"

for((i=0;i<${p_num};i++))
do
    echo "Start Runner-Producer-${i}"
    nohup ${python_cmd} start_runner.py --topic=${topic} --role=p --index=${i} >> ../logs/producer-${i}.log 2>&1 &
done

for((i=0;i<${c_num};i++))
do
    echo "Start Runner-Consumer-${i}"
    nohup ${python_cmd} start_runner.py --topic=${topic} --role=c --index=${i} >> ../logs/consumer-${i}.log 2>&1 &
done
