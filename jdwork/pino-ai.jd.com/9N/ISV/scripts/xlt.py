import time
import pymysql
import redis

table = 'tbl_user_lable_1m'

class CheckLabelApi(object):

    def checkindex(self, sub_user_info, label_json):

        flag = False
        for x, y in label_json.items():
            if x == "and":
                for local in y:
                    if int(sub_user_info) & 2 ** local != 2 ** local:
                        return False
                    else:
                        flag = True
            if x == "or":
                for local in y:
                    if int(sub_user_info) & 2 ** local == 2 ** local:
                        return True
                    else:
                        flag = False

        return flag

    def checklable(self, user_dict, args):

        count = 0
        for user_id, user_info in user_dict.items():
            flag = True
            index = str(user_info).split('_')
            for x, y in args.items():
                if index[int(x)] == '0':
                    flag = False
                    break
                flag = flag and self.checkindex(index[int(x)], y)
            if flag == True:
                count += 1
            else:
                continue

        return count


class _RedisManager(object):
    def __init__(self):
        self.__pool = {}

    def register_db(self, db_config, server_name='default'):
        if server_name in self.__pool:
            return True
        self.__pool[server_name] = redis.ConnectionPool(**db_config)

    def get_client(self, name='default'):
        pool = self.__pool[name]
        return redis.Redis(connection_pool=pool)


class _RedisOption(object):
    def __init__(self):
        redis_manager = _RedisManager()
        redis_config = {'host': "10.181.54.64",
                        'port': 6379,
                        'db': 1,
                        'password': "d41d8cd98f00b204e9800998ecf8427e"}
        redis_manager.register_db(redis_config)
        self.redis_client = redis_manager.get_client()

    def set_value(self, queue_key, value):
        self.redis_client.rpush(queue_key, value)

    def get_value(self, queue_key):
        length = self.redis_client.llen(queue_key)
        self.redis_client.lrange(queue_key, 0, length)

    def del_key(self, queue_key):
        self.redis_client.delete(queue_key)

# redis = _RedisOption()
# redis.del_key('users')
# redis.del_key('alltags')
# redis.del_key('months')
# print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
# for d in data:
#     redis.set_value('users', d[0])
#     redis.set_value('alltags', d[1])
#     redis.set_value('months', d[2])
# print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


redis_manager = _RedisManager()
redis_config = {'host': "10.181.54.64",
                'port': 6379,
                'db': 2,
                'password': "d41d8cd98f00b204e9800998ecf8427e"}
redis_manager.register_db(redis_config)
redis = redis_manager.get_client()

def mysql_to_redis():
    db = pymysql.connect(host='10.196.39.195', port=3306, user='isvpreuser', passwd='Wo8bF3Sq0ZtPd9iFswT', db='isv_pre',
                         charset='utf8')
    cursor = db.cursor()
    # cursor = db.cursor(cursor=pymysql.cursors.DictCursor)
    cursor.execute("SELECT user, alltag, month from " + table)
    data = cursor.fetchall()

    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
    for d in data:
        redis.hmset(table + '_' + str(d[2]), {d[0]: d[1]})
    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


def checklabel():
    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
    label_json = {"3": {"or": [2]}}
    month = '201712'
    key_name = table + '_' + month
    user_dict = redis.hgetall(key_name)
    count = CheckLabelApi().checklable(user_dict, label_json)
    print(count)
    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


mysql_to_redis()

