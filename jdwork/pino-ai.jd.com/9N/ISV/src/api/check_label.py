#!/usr/bin/env python3
# Time: 2018/4/25 14:51

from db.db import db_session_manager as session_manager
from src.services.user_label import UserLabelService

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)
__author__ = 'xiaoluoting@jd.com'


class CheckLabelApi(object):

    def query_user_labels(self):
        with session_manager.with_session() as session:
            user_label_service = UserLabelService(session)
            return user_label_service.query_user_labels()

    def query_user_labels_baby(self):
        with session_manager.with_session() as session:
            user_label_service = UserLabelService(session)
            return user_label_service.query_user_labels_baby()

    def query_uesr_labels_1m(self):
        with session_manager.with_session() as session:
            user_label_service = UserLabelService(session)
            return user_label_service.query_user_labels_1m()

    def checkindex(self, sub_user_info, label_json):

        flag = False
        for x, y in label_json.items():
            if x == "and":
                for local in y:
                    if int(sub_user_info) & 2 ** local != 2 ** local:
                        return False
                    else:
                        flag = True
            if x == "or":
                for local in y:
                    if int(sub_user_info) & 2 ** local == 2 ** local:
                        return True
                    else:
                        flag = False
        return flag

    def checklistindex(self, taglist, label_json):
        flag = False
        for x, y in label_json.items():
            if x == "and":
                for local in y:
                    if str(local) in taglist:
                        flag = True
                    else:
                        return False
            if x == "or":
                for local in y:
                    if str(local) in taglist:
                        return True
                    else:
                        flag = False
        return flag


    def checklable(self, user_dict, args):

        count = 0
        num = 0
        for user_id, user_info in user_dict.items():

            flag = True
            index = str(user_info, encoding='utf-8').split('_')
            for x, y in args.items():
                try:
                    tag = index[int(x)]
                except Exception as e:
                    print(e)
                    flag = False
                    break
                if tag == '0':
                    flag = False
                    break
                if int(x) in [7, 8]:
                    tag = tag.split(',')
                    flag = flag and self.checklistindex(tag, y)
                else:
                    flag = flag and self.checkindex(tag, y)
            if flag:
                count += 1
            else:
                continue

        return count
