#!/usr/bin/env python3
# Time: 2018/3/22 17:12
from db.db import db_session_manager as session_manager
from src.services.mr_task import MrTaskService


class MrTaskApi(object):

    def add_mr_task(self, **kwargs):
        with session_manager.with_session() as session:
            mr_task_service = MrTaskService(session)
            return mr_task_service.add_mr_task(**kwargs)

    def update_mr_task(self,**kwargs):
        with session_manager.with_session() as session:
            mr_task_service = MrTaskService(session)
            return mr_task_service.update_mr_task(**kwargs)

    def query_task_by_task_id(self, task_id):
        with session_manager.with_session() as session:
            mr_task_service = MrTaskService(session)
            return mr_task_service.query_task_by_task_id(task_id)

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        with session_manager.with_session() as session:
            mr_task_service = MrTaskService(session)
            return mr_task_service.feed_many_waiting_tasks(try_limit, feed_num)

    def recover_queued_to_waitting(self, task_id):
        with session_manager.with_session() as session:
            mr_task_service = MrTaskService(session)
            return mr_task_service.recover_queued_to_waitting(task_id)