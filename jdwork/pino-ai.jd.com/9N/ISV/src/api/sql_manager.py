#!/usr/bin/env python3
# Time: 2018/3/22 16:31

__author__ = 'wanggangshan@jd.com'


import os
import json
import collections
import traceback
from db.db import db_session_manager as session_manager

from src.logic.sql_manager import SqlManager, to_dict

from utils.util import exec_cmd

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)



class SqlManagerApi(object):


    def add_sql_record(self, **kwargs):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            status, msg = sql_manager.add_sql_record(**kwargs)
            return status, msg

    def query_all_records(self):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            records = sql_manager.query_all_records()
            return [to_dict(record) for record in records]

    def query_sql_record_by_name(self, name):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            record = sql_manager.query_sql_record_by_name(name)
            return to_dict(record) if record else None

    def delete_sql_record_by_name(self, api_name):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            status = sql_manager.delete_sql_record_by_name(api_name)
            return status

    def update_sql_record(self, **kwargs):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            status, msg = sql_manager.update_sql_record(**kwargs)
            return status, msg

    def exec_sql(self, **kwargs):

        sql_batis = kwargs['sql'].replace('#', '\$')
        sql_batis = sql_batis.replace('`', '\`')

        json_args = kwargs['args']
        for key in json_args:
            if isinstance(json_args[key], str):
                json_args[key] = '"%s"' % json_args[key]
            if isinstance(json_args[key], list) or isinstance(json_args[key], tuple):
                v_list = []
                for unit in json_args[key]:
                    if isinstance(unit, str):
                        unit = '"%s"' % unit
                    v_list.append(unit)
                json_args[key] = v_list


        root_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        batis_java_sh_dir = os.path.join(root_path, 'trans_mybatis', 'tmp')
        cmd = 'cd %s && java -cp fastjson-1.2.29.jar:mybatis-3.4.0.jar:mybatis-parser-1.0-SNAPSHOT.jar com.jd.ads.union.App %s %s' % (batis_java_sh_dir, sql_batis, json.dumps(json.dumps(json_args)))
        logging.info('[trans batis cmd]:\n %s', cmd)
        status, final_sql = exec_cmd(cmd, mode='subprocess')
        if not status:
            errmsg = '[trans sql exec fail]. cmd:\n %s' % cmd
            logging.error(errmsg)
            return False, errmsg

        with session_manager.with_connection() as conn:
            try:
                res = conn.execute(final_sql)
                if final_sql.split(' ')[0] == 'select':
                    data = res.fetchall()
                    data = [line.values() for line in data]
                else:
                    data =[]

                logging.info('[exec sql]:\n %s, Return num: %s', final_sql, len(data))
                return True, data

            except Exception as e:
                logging.error('[Faild exec sql]:\n %s , err info: %s',final_sql, traceback.format_exc())
                return False, 'Faild exec sql: %s' % traceback.format_exc()

    def delete_month_data(self, **kwargs):
        with session_manager.with_session() as session:
            sql_manager = SqlManager(session)
            status, msg = sql_manager.delete_month_data(**kwargs)
            return status, msg