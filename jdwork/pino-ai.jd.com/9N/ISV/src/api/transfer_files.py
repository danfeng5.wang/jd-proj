
from db.db import db_session_manager as session_manager
from src.services.transfer import TransferService

class TransferApi(object):

    def add_transfer_task(self, **kwargs):
        with session_manager.with_session() as session:
            transfer_service = TransferService(session)
            return transfer_service.add_transfer_task(**kwargs)

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        with session_manager.with_session() as session:
            transfer_service = TransferService(session)
            return transfer_service.feed_many_waiting_tasks(try_limit, feed_num)

    def recover_queued_to_waitting(self, task_id):
        with session_manager.with_session() as session:
            transfer_service = TransferService(session)
            return transfer_service.recover_queued_to_waitting(task_id)

    def query_task_by_task_id(self, task_id):
        with session_manager.with_session() as session:
            transfer_service = TransferService(session)
            return transfer_service.query_task_by_task_id(task_id)