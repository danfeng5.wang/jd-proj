#!/usr/bin/env python3
# Time: 2018/3/22 17:15
from db.models import MrTaskInfo
from src.logic.base import BaseLogic


import climate

logging = climate.get_logger(__name__)


class MrTaskManager(BaseLogic):

    def __init__(self, db_session):
        super(MrTaskManager, self).__init__(db_session)

    def add_mr_task(self, **kwargs):

        mr_task = MrTaskInfo(**kwargs)
        self.db.add(mr_task)
        self.db.commit()
        return mr_task

    def update_mr_task(self,**kwargs):
        extract=MrTaskInfo(**kwargs)
        result=self.db.query(MrTaskInfo).filter(MrTaskInfo.task_id==kwargs["task_id"]).update(kwargs)
        self.db.commit()
        return result

    def query_task_by_task_id(self, task_id):
        infos = self.db.query(MrTaskInfo)\
            .filter_by(task_id=task_id)\
            .first()
        return infos

    def task_status_waiting_to_doing(self, **kwargs):
        result = []
        try:
            result = self.db.query(MrTaskInfo).with_for_update(read=False) \
                .filter_by(status=kwargs['status']
                           )\
                .filter(MrTaskInfo.try_num <= kwargs['try_limit'],)\
                .order_by(MrTaskInfo.created_at.desc())\
                .limit(kwargs['limit']).all()
        except:
            self.db.rollback()
        for task in result:
            try_num = task.try_num + 1
            self.db.query(MrTaskInfo).filter_by(id=task.id).update({'status': kwargs['to_status'],
                                                                        'try_num': try_num})
        self.db.commit()
        return result

    def update(self, **kwargs):
        result = []
        try:
            result = self.db.query(MrTaskInfo).with_for_update(read=False) \
                .filter_by(**kwargs['filters']).all()
        except:
            self.db.rollback()
        for r in result:
            new_error_msg = kwargs['updates'].get('error_msg', '')
            if r.error_msg and new_error_msg:
                kwargs['updates']['error_msg'] = r.error_msg + '|' + new_error_msg
            self.db.query(MrTaskInfo) \
                .filter_by(id=r.id) \
                .update(kwargs['updates'])
        self.db.commit()
        return True