#!/usr/bin/env python3
# Time: 2018/3/22 16:31

__author__ = 'wanggangshan@jd.com'

import traceback
import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)

import copy
from src.logic.base import BaseLogic
from db.models import SqlManagerRecord, DateDeleteUse, to_dict



class SqlManager(BaseLogic):

    def __init__(self, db_session):
        super(SqlManager, self).__init__(db_session)

    def add_sql_record(self, **kwargs):

        record_dict = {
            'api_name': kwargs.get('api_name'),
            'sql': kwargs.get('sql')
        }
        record = SqlManagerRecord(**record_dict)
        try:
            self.db.add(record)
            self.db.commit()
        except Exception as e:
            logging.error('[Error] on add sql: %s', traceback.format_exc())
            return False, 'add failed'
        return True, ''

    def query_all_records(self):
        records = self.db.query(SqlManagerRecord)\
            .filter_by(is_delete=0).all()
        return records


    def query_sql_record_by_name(self, name):
        record = self.db.query(SqlManagerRecord)\
            .filter_by(api_name=name, is_delete=0).first()
        return record


    def delete_sql_record_by_name(self, name):
        self.db.query(SqlManagerRecord) \
            .filter_by(api_name=name)\
            .update({'is_delete': 1})
        self.db.commit()
        return True

    def update_sql_record(self, **kwargs):
        api_name = kwargs.get('api_name'),
        record_dict = {
            'api_name': kwargs.get('api_name'),
            'sql': kwargs.get('sql')
        }
        try:
            self.db.query(SqlManagerRecord) \
                .filter_by(api_name=api_name) \
                .update(record_dict)
            self.db.commit()
        except Exception as e:
            logging.error('[Error] on add sql: %s', traceback.format_exc())
            return False, 'update failed'
        return True, ''

    def delete_month_data(self, **kwargs):
        month = kwargs.get('month')
        tables = kwargs.get('tables')

        try:
            data = {}
            for table_name in tables:
                TableModel = copy.deepcopy(DateDeleteUse)
                TableModel.__table__.name = table_name
                lines = self.db.query(TableModel) \
                            .filter_by(month=month) \
                            .delete()
                data[table_name] = lines
            self.db.commit()
        except Exception as e:
            self.db.rollback()
            err_msg = '[Error ON Delete Data (has rollback)]: \n%s', traceback.format_exc()
            logging.error(err_msg)
            return False, err_msg
        return True, data
