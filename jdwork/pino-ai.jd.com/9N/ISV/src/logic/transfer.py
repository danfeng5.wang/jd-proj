#!/usr/bin/env python3
# Time: 2018/3/22 17:15
from db.models import TransferInfo
from src.logic.base import BaseLogic


import climate

logging = climate.get_logger(__name__)


class TransferManager(BaseLogic):

    def __init__(self, db_session):
        super(TransferManager, self).__init__(db_session)

    def add_transfer_task(self, **kwargs):
        """
            添加传输文件任务
        """
        transfer = TransferInfo(**kwargs)
        self.db.add(transfer)
        self.db.commit()
        return transfer

    def query_task_by_task_id(self, task_id):
        info = self.db.query(TransferInfo)\
            .filter_by(task_id=task_id)\
            .first()
        return info

    def task_status_waiting_to_doing(self, **kwargs):
        """
            通过task_id更新transfer_info表
        """
        result = []
        try:
            result = self.db.query(TransferInfo).with_for_update(read=False) \
                .filter_by(status=kwargs['status'])\
                .filter(TransferInfo.try_num <= kwargs['try_limit'],)\
                .limit(kwargs['limit']).all()
        except:
            self.db.rollback()
        for task in result:
            try_num = task.try_num + 1
            self.db.query(TransferInfo).filter_by(id=task.id).update({'status': kwargs['to_status'],
                                                                        'try_num': try_num})
        self.db.commit()
        return result

    def update(self, **kwargs):
        result = []
        try:
            result = self.db.query(TransferInfo).with_for_update(read=False) \
                .filter_by(**kwargs['filters']).all()
        except:
            self.db.rollback()
        for r in result:
            new_error_msg = kwargs['updates'].get('error_msg', '')
            if r.error_msg and new_error_msg:
                kwargs['updates']['error_msg'] = r.error_msg + '|' + new_error_msg
            self.db.query(TransferInfo) \
                .filter_by(id=r.id) \
                .update(kwargs['updates'])
        self.db.commit()
        return True