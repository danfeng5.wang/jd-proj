#!/usr/bin/env python3
# Time: 2018/3/22 17:15
from db.models import TransferInfo, TblUserLable1m, TblUserLableBaby
from src.logic.base import BaseLogic


import climate

logging = climate.get_logger(__name__)


class UserLabelManager(BaseLogic):

    def __init__(self, db_session):
        super(UserLabelManager, self).__init__(db_session)

    def query_user_labels(self):
        info_out = self.db.query(TblUserLable1m) \
            .filter_by()
        info_in = self.db.query(TblUserLableBaby)\
            .filter_by()
        return info_out, info_in

    def query_user_labels_baby(self):
        info_in = self.db.query(TblUserLableBaby)\
            .filter_by()
        return info_in

    def query_user_labels_1m(self):
        info_out = self.db.query(TblUserLable1m) \
            .filter_by()
        return info_out
