#!/usr/bin/env python3
# Time: 2017/11/24 11:17

__author__ = 'wanggangshan@jd.com'

import copy

class BaseService(object):
    """服务统一接口的基类
    """
    def __init__(self, db_session=None):
        #
        # self.db = session

        self.response = Response()

    def set_response(self, **kwargs):
        response = copy.deepcopy(self.response)
        response.status = kwargs.get('status', response.status)
        response.result = kwargs.get('result', response.result)
        response.message = kwargs.get('message', response.message)
        response.error_code = kwargs.get('error_code', response.error_code)
        return response


class Response(object):
    """
        services层对外统一格式返回结果
    """
    status = False
    result = {}
    message = ''
    error_code = 0
