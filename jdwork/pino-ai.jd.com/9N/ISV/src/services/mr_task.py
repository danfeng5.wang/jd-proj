#!/usr/bin/env python3
# Time: 2018/3/22 17:14
from src.logic.mr_task import MrTaskManager

from db.columntype import TaskStatus
from src.services.base import BaseService



import climate

logging = climate.get_logger(__name__)


class MrTaskService(BaseService):

    def __init__(self, db_session):
        super(MrTaskService, self).__init__(db_session)
        self.__mr_task_manager = MrTaskManager(db_session)

    def add_mr_task(self, **kwargs):
        mr_task_info = self.__mr_task_manager.add_mr_task(**kwargs)
        return mr_task_info

    def update_mr_task(self,**kwargs):
        mr_task_info=self.__mr_task_manager.update_mr_task(**kwargs)
        return mr_task_info

    def query_task_by_task_id(self, task_id):
        mr_task_info = self.__mr_task_manager.query_task_by_task_id(task_id)
        return mr_task_info

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        kwargs = {
                  'status': TaskStatus.Waiting,
                  'to_status': TaskStatus.Success,
                  'try_limit': try_limit,
                  'limit': feed_num,
                  }
        result = self.__mr_task_manager.task_status_waiting_to_doing(**kwargs)
        return result

    def recover_queued_to_waitting(self, task_id):
        kwargs = {'filters': {'task_id': task_id},
                  'updates': {'status': TaskStatus.Waiting},
                  }
        result = self.__mr_task_manager.update(**kwargs)
        return result