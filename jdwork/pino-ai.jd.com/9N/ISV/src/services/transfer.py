from db.columntype import TaskStatus
from src.logic.transfer import TransferManager
from src.services.base import BaseService

__author__ = 'hejingjian@jd.com'


import climate

logging = climate.get_logger(__name__)


class TransferService(BaseService):

    def __init__(self, db_session):
        super(TransferService, self).__init__(db_session)
        self.__transfer_manager = TransferManager(db_session)

    def add_transfer_task(self, **kwargs):
        transfer_info = self.__transfer_manager.add_transfer_task(**kwargs)
        return transfer_info

    def query_task_by_task_id(self, task_id):
        transfer_info = self.__transfer_manager.query_task_by_task_id(task_id)
        return transfer_info

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        kwargs = {
                  'status': TaskStatus.Waiting,
                  'to_status': TaskStatus.Success,
                  'try_limit': try_limit,
                  'limit': feed_num,
                  }
        result = self.__transfer_manager.task_status_waiting_to_doing(**kwargs)
        return result

    def recover_queued_to_waitting(self, task_id):
        kwargs = {'filters': {'task_id': task_id},
                  'updates': {'status': TaskStatus.Waiting},
                  }
        result = self.__transfer_manager.update(**kwargs)
        return result