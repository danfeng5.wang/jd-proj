from db.columntype import TaskStatus
from src.logic.transfer import TransferManager
from src.logic.user_label import UserLabelManager
from src.services.base import BaseService

__author__ = 'hejingjian@jd.com'


import climate

logging = climate.get_logger(__name__)


class UserLabelService(BaseService):

    def __init__(self, db_session):
        super(UserLabelService, self).__init__(db_session)
        self.__user_label_manager = UserLabelManager(db_session)

    def query_user_labels(self):
        return self.__user_label_manager.query_user_labels()

    def query_user_labels_baby(self):
        return self.__user_label_manager.query_user_labels_baby()

    def query_user_labels_1m(self):
        return self.__user_label_manager.query_user_labels_1m()
