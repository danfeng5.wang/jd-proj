package com.jd.ads.union;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.builder.xml.XMLMapperEntityResolver;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.scripting.xmltags.XMLScriptBuilder;
import org.apache.ibatis.session.Configuration;

/**
 * Hello world!
 */
public class App {

    /**
     * ����������sql���� <br>
     *
     * @param args <script>select singer,monthly_unique_visitor as 'muv',monthly_page_view as 'mpv',ta_coverage_rate as 'cRate' ,ta_penetration_rate as 'pRate', if(ta_penetration_rate='>1',99999999999,ta_penetration_rate) AS 'pRateC' from tbl_seg_music_singer where segmentation=${seg} and month=${month} <choose><when test="index=='muv'"><if test="sort==1">order by CAST(monthly_unique_visitor as SIGNED) ASC</if><if test="sort==0">order by CAST(monthly_unique_visitor as SIGNED) DESC</if></when><when test="index=='mpv'"><if test="sort==1">order by CAST(monthly_page_view as SIGNED) ASC</if><if test="sort==0">order by CAST(monthly_page_view as SIGNED) DESC</if></when><when test="index=='cRate'"><if test="sort==1">order by cRate ASC</if><if test="sort==0">order by cRate DESC</if></when><when test="index=='pRate'"><if test="sort==1">order by CAST(pRateC as DECIMAL) ASC</if><if test="sort==0">order by CAST(pRateC as DECIMAL) DESC</if></when></choose> order by CAST(monthly_unique_visitor as SIGNED) ASC <if test="start != null " >LIMIT ${start},${lines}</if>;</script>
     *              {"index":"muv","sort":1,"start":1,"lines":20}
     * @since 2018/4/24
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.print("usage: dynamicSql paramsJson");
            return;
        }

        "{\"index\":\"muv\",\"sort\":1,\"start\":1,\"lines\":20}"
        String script = args[0];
        JSONObject params = JSON.parseObject(args[1]);
        Configuration configuration = new Configuration();

        XPathParser parser = new XPathParser(script, false, null, new XMLMapperEntityResolver());
        SqlSource sqlSource = createSqlSource(configuration, parser.evalNode("/script"), null);

        BoundSql boundSql = sqlSource.getBoundSql(params);
        System.out.println(boundSql.getSql());
    }

    public static SqlSource createSqlSource(Configuration configuration, XNode script, Class<?> parameterType) {
        XMLScriptBuilder builder = new XMLScriptBuilder(configuration, script, parameterType);
        return builder.parseScriptNode();
    }
}
