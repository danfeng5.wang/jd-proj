# -*- coding: utf-8 -*-

import gzip
import tarfile
import sys
import os
import zipfile
import ftplib
import subprocess
import climate
from utils.config_helper import config
# sys.path.append('%s/libs' % os.path.dirname(os.path.abspath(__file__)))
# sys.path.insert(0, '../libs')
import filetype

climate.enable_default_logging()
logging = climate.get_logger(__name__)


class Ftps(object):
    PATH_TYPE_FILE = 0
    PATH_TYPE_DIR = 1
    PATH_TYPE_UNKONWN = 2

    def __init__(self, **kwargs):
        self.username = kwargs["username"]
        self.password = kwargs["password"]
        self.ip = kwargs["ip"]
        self.port = kwargs["port"]
        self.remote_path = kwargs["remote_path"]
        self.local_path = config.get("transfer_task.local_path")
        self.hdfs_path = config.get("transfer_task.hdfs_path")
        self.task_id = kwargs['task_id']
        # 链接FTPS服务器
        self.ftps = ftplib.FTP_TLS()
        self.ftps.connect(host=self.ip, port=self.port)
        self.ftps.login(self.username, self.password)
        self.ftps.prot_p()

    # 获取文件的属性等相关信息
    def dir(self, *args):
        '''
        by defualt, ftplib.FTP.dir() does not return any value.
        Instead, it prints the dir info to the stdout.
        So we re-implement it in FtpDownloader, which is able to return the dir info.
        '''
        info = []
        cmd = 'LIST'
        for arg in args:
            if arg:
                cmd = cmd + (' ' + arg)
        self.ftps.retrlines(cmd, lambda x: info.append(x.strip().split()))
        return info

    # 获取文件目录树结构和文件及相应类型
    def tree(self, remote_dir=None, init=True):
        '''
        recursively get the tree structure of a directory on FTP Server.
        args:
            remote_dir - remote direcotry path of the FTP Server.
            init - flag showing whether in a recursion.
        '''
        if init and remote_dir in ('.', None):
            remote_dir = self.ftps.pwd()
        # tree = []
        # tree.append((remote_dir, self.PATH_TYPE_DIR))

        dir_info = self.dir(remote_dir)
        type = self.PATH_TYPE_DIR
        if len(dir_info) == 1:
            temp = dir_info[0]
            attr = temp[0]
            name = temp[-1]
            if name == remote_dir:
                if attr.startswith("-"):
                    type = self.PATH_TYPE_FILE
                elif attr.startswith("d"):
                    type = self.PATH_TYPE_DIR
                else:
                    type = self.PATH_TYPE_UNKNOWN
        tree = []
        tree.append((remote_dir, type))
        for info in dir_info:
            attr = info[0]  # attribute
            name = info[-1]
            # path = os.path.join(remote_dir, name)
            if remote_dir != name:
                path = remote_dir + "/" + name
                if attr.startswith('-'):
                    tree.append((path, self.PATH_TYPE_FILE))
                elif attr.startswith('d'):
                    if (name == '.' or name == '..'):  # skip . and ..
                        continue
                    tree.extend(self.tree(remote_dir=path, init=False))  # recurse
                else:
                    tree.append(path, self.PATH_TYPE_UNKNOWN)
        return tree

    # 从FTPS服务器获取文件
    def transfer_files_from_ftps(self, remote_dir, local_dir):
        file_list = self.tree(remote_dir=remote_dir, init=True)
        logging.info("File will be downloaded:%s", file_list)
        local_dir = local_dir + "/" + remote_dir.split("/")[-1]
        if os.path.isdir(local_dir):
            pass
        else:
            os.mkdir(local_dir)
        result = []
        for file, file_type in file_list:
            flag = len(file.split("/")) == len(remote_dir.split("/"))
            path = "/" + "/".join(file.split("/")[len(remote_dir.split("/")):])
            if file_type == self.PATH_TYPE_FILE and flag:
                path = "/" + file.split("/")[-1]
            if file_type == self.PATH_TYPE_DIR and not os.path.exists(local_dir + path):
                os.makedirs(local_dir + path)
            if file_type == self.PATH_TYPE_FILE and file.split(".")[-1] == "ok":
                path = local_dir + ".".join(path.split(".")[0:-1])
                f=open(path, 'wb')
                self.ftps.voidcmd('TYPE I')
                sock = self.ftps.transfercmd('RETR ' + file)
                while True:
                    block = sock.recv(1024 * 1024)
                    if not block:
                        break
                    f.write(block)
                #f.close()
                # with open(path, 'wb') as f:
                #    self.ftps.retrbinary('RETR ' + file, f.write,blocksize=4096)
                # self.ftps.rename(file,".".join(file.split(".")[0:-1])+".yes")
                logging.info("download--------------" + file + "-----------------success")
                result.append(path)
        logging.info("local zip:%s", result)
        return result

    # 判断文件类型
    def file_type(self, local_path):
        if os.path.isdir(local_path):
            logging.info("File {} is a dir!".format(local_path))
            return ""
        elif os.path.isfile(local_path):
            kind = filetype.guess(local_path)
            if kind is None:
                logging.info('Cannot guess file {} type!'.format(local_path))
                return ""
            logging.info('File %s extension: %s' % (local_path, kind.extension))
            return kind.extension

    # 解压缩.tar.gz和.tgz格式的文件,参数type是区分.tar.gz和.tgz
    def unzip_tgz(self, local_path, type):
        path_split = local_path.split("/")
        file_name = path_split[-1]
        f_name = file_name.replace("." + type, "")
        path = "/".join(path_split[0:-1]) + "/" + f_name
        g_file = gzip.GzipFile(local_path)
        with open(path, 'wb') as f:
            f.write(g_file.read())
        g_file.close()
        os.remove(local_path)
        is_tar = tarfile.is_tarfile(path)
        if is_tar:
            tar = tarfile.open(path)
            # names不仅包含tar包中的文件名，还包含本身的名字
            names = tar.getnames()
            # 去掉后缀名
            if type == "gz":
                tar_path = path.replace(".tar", "")
            # 建立同名文件
            if os.path.isdir(tar_path):
                pass
            else:
                os.mkdir(tar_path)
            store_unzip_files = "/".join(tar_path.split("/")[0:-1])
            for name in names:
                tar.extract(name, store_unzip_files)
            tar.close()
            os.remove(path)
            return tar_path
        return path

    # 解压zip格式文件
    def unzip_zip(self, local_path):
        zip_file = zipfile.ZipFile(local_path)
        path = "/".join(local_path.split("/")[0:-1])
        # if os.path.isdir(path):
        #     pass
        # else:
        #     os.mkdir(path)
        # store_unzip_files="/".join(path.split("/")[0:-1])
        temp=""
        for names in zip_file.namelist():
            zip_file.extract(names, path)
            if len(names.strip("/").split("/"))==1:
               temp=names
        zip_file.close()
        os.remove(local_path)  # 删掉压缩包
        return path+"/"+temp

    # 删除压缩文件
    def delete_file(self, path):
        os.remove(path)

    # 解压缩文件
    def unzip(self, local_path):
        type = self.file_type(local_path)
        if type == "gz":
            path = self.unzip_tgz(local_path, type)
        elif type == "tgz":
            path = self.unzip_tgz(local_path, type)
        elif type == "zip":
            path = self.unzip_zip(local_path)
        else:
            path = ""
            logging.info("Unsupported file format!")
        return path

    # 把文件传到hdfs上
    def put_files_to_hdfs(self, local_dir, hdfs_dir):
        logging.info("local_dir:%s" % local_dir)
        logging.info("hdfs_dir:%s" % hdfs_dir)
        shell_command = 'sh ' + config.get('mr_task.shell_path') + 'put_to_hdfs.sh ' + config.get('user_name') + ' ' + local_dir + ' ' + hdfs_dir
        logging.info("shell command:%s", shell_command)
        status, output = subprocess.getstatusoutput(shell_command)  # 0--success，1--fail
        logging.info("status:%s" % status)
        return status, output

    # 传输文件
    def transfer_files_from_ftps_to_hdfs(self):
        remote_path = self.remote_path
        local_path = self.local_path + '/' + self.task_id
        if os.path.isdir(local_path):
            pass
        else:
            os.mkdir(local_path)
        path_list = self.transfer_files_from_ftps(remote_dir=remote_path, local_dir=local_path)
        status, output = 0, "Success to transfer files to HDFS!"
        logging.info("path list:%s" % path_list)
        for path in path_list:
            logging.info("path:%s", path)
            after_unzip_path = self.unzip(path)#本地压缩包解压后的文件路径
            logging.info("after_unzip_path:%s", after_unzip_path)
            #local_dir和hdfs_dir都要保证结尾不是以"/"结尾
            local_dir = after_unzip_path
            if local_dir.endswith("/"):
                local_dir = local_dir[0:-1]
            #拼接处hdfs的文件路径,eg : self.hdfs_path=/tmp/testpy     self.loca_path=/home/admin/denglei/isv_files
            #after_unzip_path=/home/admin/denglei/isv_files/PanelData/201712    hdfs_dir=/tmp/testpy /PanelData/201712
            hdfs_dir = self.hdfs_path + after_unzip_path.replace(local_path, "")
            if hdfs_dir.endswith("/"):
                hdfs_dir = hdfs_dir[0:-1]

            # 拷贝Meritco_Data
            shell_cmd = '\cp -rf %s/* %s/' % (local_path, self.local_path)
            logging.info('[Copy Meritco_Data] %s' % shell_cmd)
            status, output = subprocess.getstatusoutput(shell_cmd)
            logging.info('[Copy Meritco_Data RESULT]: %s, %s' % (status, output))

            logging.info("Begin to put files to HDFS : local_dir----%s , hdfs_dir----%s" , local_dir , hdfs_dir)
            status, output = self.put_files_to_hdfs(local_dir=local_dir, hdfs_dir=hdfs_dir)

            if status == 0:
                if remote_path.endswith(".ok"):
                    self.ftps.connect(host=self.ip, port=self.port)
                    self.ftps.login(self.username, self.password)
                    self.ftps.prot_p()
                    self.ftps.rename(remote_path, remote_path.replace(".ok", ".yes"))
                else:
                    old = "/".join(remote_path.split("/")[0:-1]) + "/".join((path.replace(local_path, "") + ".ok").split("/"))
                    new = old.replace(".ok", ".yes")
                    logging.info("old remote filename:%s" % old)
                    logging.info("new remote filename:%s" % new)
                    self.ftps.connect(host=self.ip, port=self.port)
                    self.ftps.login(self.username, self.password)
                    self.ftps.prot_p()
                    self.ftps.rename(old, new)
            else:
                self.ftps.quit()
                return status, output
        self.ftps.quit()
        return status, output


if __name__ == '__main__':
    ftps = Ftps()
    ftps.transfer_files_from_ftps_to_hdfs()
