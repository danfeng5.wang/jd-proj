#!/usr/bin/env python3
# Time: 2017/12/22 18:12

__author__ = 'wanggangshan@jd.com'



import arrow
import requests
import climate
import socket
climate.enable_default_logging()
logging = climate.get_logger(__name__)

# from utils.config_helper import config


def get_my_ip():
    """
    Returns the actual ip of the local machine.
    This code figures out what source address would be used if some traffic
    were to be sent out to some well known address on the Internet. In this
    case, a Google DNS server is used, but the specific address does not
    matter much.  No traffic is actually sent.
    """
    try:
        csock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        csock.connect(('8.8.8.8', 80))
        (addr, port) = csock.getsockname()
        csock.close()
        return addr
    except socket.error:
        return "127.0.0.1"


alarm_server_cfg = {
    "url": "http://alarm.pino-ai.jd.com/as/1.1/report",
    "api_version": "1.1",
    "task": "report",
    "sign": "e4c2e8edac362acab7123654b9e73432",
    "ump_key": "ump.PinoAI.Framework.Data.FailureExceedMaxValue",
    "hostname": get_my_ip(),
    "type": "0",
    "value": "0",
}


def info_format(str=''):

    tag = '<9n-alarm>: '
    return tag + str


def http_alarm(alarm_info, now_env='', now_name=''):
    if now_env != 'online':
        return True
    logging.info(alarm_info)
    info = info_format('[server] %s, [Message] %s' % (now_name + ' ' + now_env, alarm_info))
    config = alarm_server_cfg
    now = arrow.now('+08:00').format('YYYYMMDDHHmmssSSS')
    json = {
        "api_version": config.get("api_version"),
        "task": config.get("task"),
        "sign": config.get("sign"),
        "alarm_message": {
            "time": now,
            "key": config.get("ump_key"),
            "hostname": config.get("hostname"),
            "type": config.get("type"),
            "value": config.get("value"),
            "detail": info
         }
    }

    url = config.get("url")
    logging.info('<alarm to UMP><request> data: %s', json)
    res = requests.post(url, json=json)
    result = res.json()
    logging.info('<alarm to UMP><response> return: %s', result)
    return result
