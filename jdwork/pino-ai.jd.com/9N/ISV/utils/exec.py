#!/usr/bin/env python3
# Time: 2018/2/5 19:58

__author__ = 'wanggangshan@jd.com'


import os
import climate
import subprocess
import traceback
climate.enable_default_logging()
logging = climate.get_logger(__name__)


def exec_cmd(cmd, mode='system'):
    if mode == 'system':
        logging.info('Cmd: %s', cmd)
        return os.system(cmd)
    elif mode == 'subprocess':
        logging.info('Cmd: %s', ' '.join(cmd))
        return subprocess.call(cmd)