#!/usr/bin/env python3
# Time: 2018/1/30 20:48

__author__ = 'wanggangshan@jd.com'

import paramiko

import traceback
import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)


def ssh_connect(_host, _username, _password):
    try:
        _ssh_fd = paramiko.SSHClient()
        _ssh_fd.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        _ssh_fd.connect(_host, username=_username, password=_password)
    except Exception as e:
        logging.info('ssh %s@%s: %s' % _username, _host, traceback.format_exc())
        return False
    return _ssh_fd


def ssh_exec_cmd(_ssh_fd, _cmd):
    return _ssh_fd.exec_command(_cmd)


def ssh_close(_ssh_fd):
    _ssh_fd.close()


def remote_exec_cmd(hostname , username , password, cmd):
    sshd = ssh_connect(hostname, username, password)
    if not sshd:
        return False
    stdin, stdout, stderr = ssh_exec_cmd(sshd, cmd)
    err_list = stderr.readlines()
    if len(err_list) > 0:
        logging.info('SSH %s Error : %s', hostname, err_list[0])
        return False
    res = stdout.read()
    ssh_close(sshd)
    return res