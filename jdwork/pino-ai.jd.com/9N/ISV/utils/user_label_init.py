#!/usr/bin/env python3
# Time: 2018/4/26 10:48

__author__ = 'hejingjian@jd.com'


class UserLabelManege(object):
    user_dict = dict()

    def set_user_dict(self, crowd_type, args):
        self.user_dict[crowd_type] = args

    def get_user_dict(self, crowd_type, month):
        return self.user_dict.get(crowd_type, {}).get(month, {})


user_labels = UserLabelManege()
