#!/usr/bin/env python3
# Time: 2017/11/30 15:34

__author__ = 'wanggangshan@jd.com'



import sys
import os

path = os.path.dirname(os.path.dirname((os.path.abspath(__file__))))
sys.path.insert(0, path)

from db.db import db_session_manager as db_manager

from utils.config_helper import config
config_path = os.path.join(path, 'etc/default.yml')
config.load_config_file(config_path)

db_cfg = config.get('mysql_db')
db_manager.register_db(db_cfg, 'default')

from db.db import db_session_manager
from db.models import WhiteListUser, UserSource
session = db_session_manager.get_session()

args = sys.argv
names = args[1:]
fail_not_exist = []
fail_has_add = []
success = []
for name in names:
    user= session.query(UserSource).filter_by(source_id=name, source_type=1).first()
    if not user:
        fail_not_exist.append(name)
        print('-----该用户系统不存在，请先登录: ', name)
        print('-----继续添加其他用户...')
        continue
    user_id = user.uid
    info = session.query(WhiteListUser).filter_by(uid=user_id).first()
    if info:
        fail_has_add.append(name)
        print('-----该用户已开通白名单', name)
        print('-----继续添加其他用户...')
        continue
    white = WhiteListUser(uid=user_id)
    session.add(white)
    session.commit()
    success.append(name)
print('操作完成, 结果如下:\n')

if fail_not_exist:
    print('系统不存在得用户，需先登录: ', fail_not_exist)
if fail_has_add:
    print('之前已开通过的用户: ', fail_has_add)
if success:
    print('本次新开通的用户: ', success)
