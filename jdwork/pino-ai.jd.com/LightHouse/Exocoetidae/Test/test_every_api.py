#!/usr/bin/env python3
# Time: 2017/12/3 16:51

__author__ = 'wanggangshan@jd.com'


import sys
import os

path = os.path.dirname(os.path.dirname((os.path.abspath(__file__))))
sys.path.insert(0, path)
pc_path = os.path.join(path, 'PinoCore')

sys.path.insert(0, pc_path)
sys.path.append('%s/libs' % path)
import pctx

args = sys.argv

moudle_str = args[1]
moudle_list = moudle_str.split('.')

if moudle_list[0] == 'src':
    from db.db import db_session_manager as db_manager

    from utils.config_helper import config, load_all_config

    load_all_config()

    db_cfg = config.get('mysql_db')
    db_manager.register_db(db_cfg, 'default')


import json
all_arg = args[2:]
def check_args(all_arg):
    ok_args = []
    special_arg = {}
    for i in all_arg:
        if ('[' in i) or ('{' in i):
            ok_args.append(json.loads(i))
            continue
        if '=' in i:
            key, value = i.split('=')
            try:
                value = int(value)
            except:
                pass
            special_arg[key] = value
        else:
            if i:
                try:
                    i = int(i)
                except:
                    pass
                ok_args.append(i)

    return ok_args, special_arg

base_module = __import__(moudle_list[0])
for m_str in (moudle_list[1:-1]):
    base_module = getattr(base_module, m_str)
last_module = getattr(base_module, moudle_list[-1])

print('测试模块函数:', last_module)

if isinstance(base_module, type):

    if len(args) > 2:
        if args[2][0:2] == '/[':
            class_args = args[2]
            print('命令行输入的类初始化参数: ', class_args)
            class_ok_args, class_special_arg = check_args(args[2][2:-2].split('-'))
            func_args = args[3:]
            print('命令行输入的函数参数: ', func_args)
            func_ok_args, func_special_arg = check_args(func_args)

            res = last_module(base_module(*class_ok_args, **class_special_arg), *func_ok_args, **func_special_arg)
        else:
            print('命令行输入的类初始化参数: 无', )
            func_args = args[2:]
            print('命令行输入的函数参数: ', func_args)
            func_ok_args, func_special_arg = check_args(func_args)
            res = last_module(base_module(),
                              *func_ok_args, **func_special_arg)
    else:
        res = last_module(base_module())
else:
    if len(args) > 2:
        func_args = args[2:]
        print('命令行输入的函数参数: ', func_args)
        func_ok_args, func_special_arg = check_args(func_args)
        res = last_module(*func_ok_args, **func_special_arg)
    else:
        res = last_module()
print('运行结果: ', res)