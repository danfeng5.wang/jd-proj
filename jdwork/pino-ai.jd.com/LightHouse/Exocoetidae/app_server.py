# codeing=utf-8
import os
import sys
import uuid
import multiprocessing
from threading import Lock
from optparse import OptionParser

import climate

import pymysql

sys.path.insert(0, './PinoCore')
sys.path.append('%s/libs' % os.path.dirname(__file__))

climate.enable_default_logging()
logging = climate.get_logger(__name__)

# from controller.route_config import roure_init
from common.session_object import user_session_manager


# from utils.config_helper import config
# config.load_config_file(os.path.join(os.path.dirname(__file__), 'etc/default.yml'))
from utils.config_helper import load_all_config, config
load_all_config()

import pctx
from settings import getPinoConfig
pinoCfg = getPinoConfig()
pinoCfg.initializeLogger('')

from common import url_verify
from common import init_all

os.path.dirname(os.path.abspath(__file__))
from flask import Flask, request, render_template
from flask import session as flaskSession
from libs.flask_socketio import SocketIO, emit
from controller import route_config
from PinoCore.pino_socketio import PinoSocketIO
from wstty import PinoWebMessage
import server
import flask
import services
from db.db import db_session_manager as db_manager
from db.models import init_db, drop_db

app = Flask(__name__)
app.secret_key = 'Pohoi07t8oFiGo87tUglkj(*&6&%e'

socketio = SocketIO(app, async_mode='gevent')

thread = None
thread_lock = Lock()

route_config.route_init(app)

@app.route('/')
@app.route('/index')
def index():
    return render_template('api_doc.html')

# @app.route('/logout')
# def logout():
#     return userIdentification.logout()

@app.route('/test')
def test():
    return render_template('test.html')

def background_thread():
    print("""Example of how to send server generated events to clients.""")
    count = 0
    while True:
        socketio.sleep(2)
        count += 1
        data = 'Server generated event count %d' % count
        socketio.emit(PinoSocketIO.EVENT,
                    PinoWebMessage.outputMessage(data).serialize(),
                    #   {'data': 'Server generated event', 'count': count},
                    namespace='/output')

@socketio.on('web_connect', namespace='/output')
def output_connect(userinfo):
    # print('>>>>>>>>> websocket userinfo', userinfo)
    session = server.PinoSession(uuid.uuid1().hex)
    # user = server.PinoUser.loadInstance(pinoCfg, userinfo['username'])
    # session.update(user=user)
    # tty size
    cols = userinfo.get('cols', 0)
    rows = userinfo.get('rows', 0)
    session.update(cols=cols, rows=rows)

    socketio = flask.current_app.extensions['socketio']
    session.setWebSocket(PinoSocketIO(socketio, '/output', flask.request.sid))
    # print('first:', id(socketio))
    user_session_manager.set_user_session(session.uniName, session)
    user_session_manager.set_socket_session(session.uniName, socketio)
    # global thread
    # with thread_lock:
    #     if thread is None:
    #         thread = socketio.start_background_task(target=background_thread)
    
    emit('my_response', session.uniName, namespace='/output', room=flask.request.sid)

    # msg = PinoWebMessage.outputMessage('hello world!')
    # emit('pino-ws', msg.serialize())
    # msg = PinoWebMessage.outputMessage('hello world socket!')
    # print(type(msg.serialize()))
    # socketio.emit('pino-ws', msg.serialize(), namespace='/output')
    
    flaskSession['pino_session'] = session
    ##

@socketio.on('tty-input', namespace='/output')
def output_tty_input(pinoWebMessageData):
    
    session = flaskSession.get('pino_session', None)
    if not session:
        return

    message = PinoWebMessage.parseFrom(pinoWebMessageData)
    services.onWebMessage(session, message)

if __name__ == '__main__':
    db_cfg = config.get('mysql_db')
    db_manager.register_db(db_cfg, 'default')
    # init_db()
    # drop_db()


    usage = 'usage: python %prog [options] arg1 arg2'
    parser = OptionParser(usage=usage)

    parser.add_option(
        '-g',
        '--debug',
        dest='debug_flag',
        type='int',
        default=99,
        action='store',
        metavar='DEBUG',
        help='debug flag')
    parser.add_option(
        '-i',
        '--ip',
        dest='server_ip',
        type='string',
        default='0.0.0.0',
        action='store',
        metavar='IP',
        help='pas server ip')
    parser.add_option(
        '-p',
        '--port',
        dest='server_port',
        type='string',
        default='5002',
        action='store',
        metavar='PORT',
        help='pas server port')

    (options, args) = parser.parse_args()

    url_verify.urlCheck(app)

    # init topic: user,group,quota,runner...
    init_all.topic_init()

    # set listen '0.0.0.0'
    ip = '0.0.0.0'
    port = int(options.server_port)
    processes = multiprocessing.cpu_count() if multiprocessing.cpu_count() < 16 else 16
    socketio.run(app, host=ip, port=port)
