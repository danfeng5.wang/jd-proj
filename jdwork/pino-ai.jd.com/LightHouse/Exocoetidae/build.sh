#!/bin/bash

PACKAGE_DIR=".build"
WORKING_DIR="PinoAI"
CLIENT_RELEASE_DIR="release-client"
OUT="RUN.zip"
PROJECT_DIR=`pwd`
ANACONDA_PATH="/export/App/training_platform/anaconda3"
PYINSTALLER_BIN=${ANACONDA_PATH}/bin/pyinstaller

build_mode='client'
if [ -z ${1} ];then
  build_mode="client"
else
  build_mode=${1}
fi

if [ "${build_mode}"x = "client"x ]; then
    if [ ! -d ${PACKAGE_DIR} ]; then
        mkdir ${PACKAGE_DIR}
    fi
    echo -n "[I] Compile & Building ."
    echo -n .
    \rm -rf ${PROJECT_DIR}/${PACKAGE_DIR}/*.tar.bz2* ${PROJECT_DIR}/${PACKAGE_DIR}/*.zip*
    echo -n .
    \rm -rf ${PROJECT_DIR}/${PACKAGE_DIR}/${WORKING_DIR}
    echo -n .
    \rm -rf ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}
    echo -n .
    PYTHON_URL="http://storage.jd.local/anaconda/python-pino-client.tar.bz2"
    echo -n .
    # Get Python and Packup
    cd ${PROJECT_DIR}/${PACKAGE_DIR} && wget -q ${PYTHON_URL} && tar xf *.bz2
    echo -n .
    cd ${PROJECT_DIR}/${PACKAGE_DIR} && mkdir ${WORKING_DIR} && mv python ${WORKING_DIR}/
    echo -n .
    echo `git log -n 1` > ${PROJECT_DIR}/pino_client/_version
    echo -n .
    cp -rf ${PROJECT_DIR}/pino_client/* ${PROJECT_DIR}/${PACKAGE_DIR}/${WORKING_DIR}
    echo -n .
    (cd ${PROJECT_DIR}/${PACKAGE_DIR}/${WORKING_DIR} && find . -type f | sort | zip -qDX@ "${PROJECT_DIR}/${PACKAGE_DIR}/${OUT}")
    echo -n .
    mkdir -p ${PROJECT_DIR}/${CLIENT_RELEASE_DIR} && cp ${PROJECT_DIR}/dist-tools/pino.sh ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}/pino
    echo -n "Finish!"
    echo -n .
    (cp ${PROJECT_DIR}/dist-tools/install.sh ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}/install.sh)
    echo -n .
    cat ${PROJECT_DIR}/${PACKAGE_DIR}/${OUT} >> ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}/pino
    echo -n .
    echo "Packed!"
    sleep 1
    release_info=`ls -alh ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}/pino`
    release_info_md5=$(md5sum `ls -a ${PROJECT_DIR}/${CLIENT_RELEASE_DIR}/pino`)
    echo "INFO:"
    echo "     ${release_info}"
    echo "     md5sum : ${release_info_md5}"
elif [ "${build_mode}"x = "runner"x ]; then
    GEN_RUNNER_BIN=ex_runner
    PY_PATH=${PROJECT_DIR}
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/libs
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore/uniKubePino/pysrc
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore/uniKubePino/pylibs
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore/uniKubePino/pylibs3
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore/uniKubePino/pysrc/apis
    PY_PATH=${PY_PATH}:${PROJECT_DIR}/PinoCore/saltwater
    cd ${PROJECT_DIR} && ${PYINSTALLER_BIN} -F -n ${GEN_RUNNER_BIN} \
    -p ${PY_PATH} \
    --add-data ${PROJECT_DIR}/etc/default.yml:etc/ \
    --add-data ${PROJECT_DIR}/etc/crm.yml:etc/ \
    start_runner.py && \
    mv dist/${GEN_RUNNER_BIN} . && \
    /bin/rm -rf dist build && cd -
    echo "Bulid runner complete. Binary at ${PROJECT_DIR}/scripts/${GEN_RUNNER_BIN}"
else
    echo "Unrecognized build mode, exit."
fi



