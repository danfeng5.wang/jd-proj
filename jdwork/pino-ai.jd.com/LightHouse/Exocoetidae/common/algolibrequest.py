


class AlgoLibResponse(object):

    def __init__(self, **kwargs):
        for (key, value) in kwargs.items():
            setattr(self, key, value)


class AlgoLibRequest(object):

    def __init__(self, **kwargs):
        self.author = kwargs.get('author', None)
        self.algo_name = kwargs.get('algo_name', None)
        self.library = kwargs.get('library', None)
        self.token = kwargs.get('token', None)
        self.output_format = kwargs.get('output_format', None)
        self.local_dir = kwargs.get('local_dir', None)
        self.keywords = kwargs.get('keywords', None)
        self.configure_path = kwargs.get('configure_path', None)
        self.workspace_dir = kwargs.get('workspace_dir', None)

    def valdiate(self, **kwargs):

        assert author is None or isinstance(author, str)
        if library not in ['public', 'private', None]:
            raise TypeError
        if output_format not in ['dict', 'json', 'pb', None]:
            raise TypeError

    @classmethod
    def check_required_args(self, *required_arg_list):

        def func_wrapper(func):

            def arg_wrapper(*args, **kwargs):
                if len(args) != 1:
                    return AlgoLibResponse(
                        success=False, error_message='Input argument error.')
                algo_lib_request = args[0]
                if not isinstance(algo_lib_request, AlgoLibRequest):
                    return AlgoLibResponse(
                        success=False,
                        error_message=
                        'Input argument is not a instance of AlgoLibRequest.')
                for arg in required_arg_list:
                    if getattr(algo_lib_request, arg) is None:
                        return AlgoLibResponse(
                            success=False,
                            error_message='AlgoLibRequest miss argument %s' %
                            arg)
                return func(*args, **kwargs)

            return arg_wrapper

        return func_wrapper
