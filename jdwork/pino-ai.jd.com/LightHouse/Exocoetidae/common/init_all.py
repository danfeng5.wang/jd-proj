#!/usr/bin/env python3
# Time: 2017/12/19 15:16

__author__ = 'wanggangshan@jd.com'


from utils.config_helper import config


def topic_init():

    topic = config.get('topic')

    if topic.get('cli'):
        pass

    if topic.get('web'):
        pass

    if topic.get('crm'):
        from src.init.crm import CrmInit
        CrmInit().init()

    return True


