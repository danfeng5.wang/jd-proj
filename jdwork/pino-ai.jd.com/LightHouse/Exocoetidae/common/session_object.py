import time

import sys


class _SessionObject(object):
    def __init__(self):
        self.__session_user_dict = {}

    def set_user_session(self, session_uuid, session_object):
        self.__session_user_dict[session_uuid] = session_object

    def get_user_session(self, session_uuid):
        return self.__session_user_dict[session_uuid]

    def set_socket_session(self, session_uuid, session_object):
        self.__session_user_dict[session_uuid+'socket'] = session_object

    def get_socket_session(self, session_uuid):
        return self.__session_user_dict[session_uuid+'socket']

    def remove_socket_session(self, session_uuid):
        self.__session_user_dict.pop(session_uuid+'socket')

user_session_manager = _SessionObject()


#
# class PinoSession(object):
#     def __init__(self, uniName):
#         self.createTime = time.time()
#         self.uniName = uniName
#         self.user = None
#         self.groups = None
#         # tty size
#         self.ttyCols = 250
#         self.ttyRows = 80
#         # websocket associated
#         self._stream = None
#
#     def update(self, **pwargs):
#         self.ttyCols = pwargs.get('cols', self.ttyCols)
#         self.ttyRows = pwargs.get('rows', self.ttyRows)
#         self.user = pwargs['user'] if 'user' in pwargs else self.user
#         self._stream = pwargs['stream'] if 'stream' in pwargs else self._stream
#
#     def setWebSocket(self, ws):
#         self.update(stream=ws)
#
#     @property
#     def stream(self):
#         if not self._stream:
#             return sys.stdout
#         return self._stream
#
#
# class PinoUser(object):
#     def __init__(self, **pwargs):
#         self.name = pwargs['username'] if 'username' in pwargs else None
#         self.email = pwargs['email'] if 'email' in pwargs else None
#         self.token = pwargs['token'] if 'token' in pwargs else None
#         self.groups = pwargs['groups'] if 'groups' in pwargs else []
#         self.is_superuser = pwargs['is_superuser'] if 'is_superuser' in pwargs else False
