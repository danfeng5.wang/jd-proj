# encoding=utf-8
from flask import request, session, jsonify, redirect, url_for
from utils.config_helper import config
from src.api.user import UserApi
from src.api.white_list import WhiteListApi

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)

def logout():
    session.pop('user', None)
    print("logouted!")
    return jsonify({'status': 'ok', 'result': 'logout!'})


def urlCheck(app):
    @app.before_request
    def before_request():

        def check_user(req_json):
            user_id = req_json.get('user_id', 0)
            token = req_json.get('token', '')
            if (not user_id) or (not token):
                return False
            if not UserApi().check_user_token(user_id):
                return False
            return True

        def check_white_list(user_id):
            user = WhiteListApi().get_white_user(user_id)
            if not user:
                return False
            return True

        req_json = {}

        if request.files:
            req_json = request.form
        else:
            req_json = request.get_json(force=True)
        logging.info('>>request.path: %s', request.path)
        logging.info('>>request.json: %s', req_json)


        if request.path.lower().startswith("/web/"):
            if request.path != '/web/login' and request.path != '/web/register':
                if not check_user(req_json):
                    reponse = {'status': False, 'error_code': 401,'result': {},
                               'message': '登录信息有误，请先登录'}
                    return jsonify(reponse)

        if request.path.lower().startswith("/cli/"):
            if request.path != '/cli/login':
                if not check_user(req_json):
                    reponse = {'status': False, 'error_code': 401,'result': {},
                               'message': '登录信息有误，请先登录'}
                    return jsonify(reponse)

        elif request.path.lower().startswith("/crm/"):
            pass

        user_id = req_json.get('user_id', 0)
        if user_id:
            if not check_white_list(user_id):
                errmsg = '请先申请加入白名单, 发送申请邮件给zhangzehua@jd.com后等待开通即可'
                return jsonify(
                    {'status': False, 'error_code': 403, 'result': {}, 'message': errmsg})

