from collections import defaultdict

import services
from flask import request, jsonify

from controller.cli.base import Base
from src.services.algo import AlgoService


class VerifyDeleteAuth(Base):
    def __init__(self):
        super(VerifyDeleteAuth, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()

    def get(self):
        pass

    def post(self):
        uid = request.headers.get('uid', None)
        algo_name = request.get_json(force=True).get('algo', None)
        req_kwargs = defaultdict(list)
        req_kwargs['uid'] = uid
        req_kwargs['algo_name'] = algo_name
        response = self.__algo_service.delete_algo(**req_kwargs)
        if response.status:
            return jsonify({'status': 'success', 'result': {}, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})


class DeleteAlgo(Base):
    def __init__(self):
        super(DeleteAlgo, self).__init__()
        self.result = {}

    def get(self):
        pass

    def post(self):
        uid = request.get_json(force=True).get('user_id', None)
        algo_name = request.get_json(force=True).get('algo', None)
        req_kwargs = defaultdict(list)
        req_kwargs['uid'] = uid
        req_kwargs['algo_name'] = algo_name
        response = self.__algo_service.delete_algo(**req_kwargs)
        if response.status:
            return jsonify({'status': 'success', 'result': {}, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})