from collections import defaultdict

import services
from flask import request, jsonify, make_response, send_file

from controller.cli.base import Base
from src.services.algo import AlgoService
from utils.util import make_zip, ProjectPathManager


class VerifyDownloadAuth(Base):
    def __init__(self):
        super(VerifyDownloadAuth, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        # author = request.get_json(force=True).get('author', None)
        algo_name = request.get_json(force=True).get('algo', None)
        request_info = services.AlgoLibRequest(
            author=username,
            algo_name=algo_name)
        response = services.verify_download_auth(request_info)
        if response.success:
            return jsonify({'status': 'success', 'result': {}, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})


class DownloadAlgo(Base):
    def __init__(self):
        super(DownloadAlgo, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()

    def get(self):
        pass

    def post(self):

        uid = request.get_json(force=True).get('user_id', None)
        algo_name = request.get_json(force=True).get('algo', None)
        download_base_dir = ProjectPathManager().generate_work_path(uid, 'algo_download')
        req_kwargs = defaultdict(list)
        req_kwargs['uid'] = uid
        req_kwargs['algo_name'] = algo_name
        req_kwargs['local_dir'] = download_base_dir
        response = self.__algo_service.download_algo(**req_kwargs)
        if response.status:
            return response.result
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})
