from collections import defaultdict

from flask import request, jsonify

from controller.cli.base import Base
import services

from src.services.algo import AlgoService


class QueryAlgoInfo(Base):
    def __init__(self):
        super(QueryAlgoInfo, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()


    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        uid = request.headers.get('uid', None)
        algo_name = request.get_json(force=True).get('algo', None)
        library = request.get_json(force=True).get('library', None)
        output_format = request.get_json(force=True).get('output_format', None)
        token = request.get_json(force=True).get('token', None)
        all = request.get_json(force=True).get('all', False)
        # admin function has not yet been realized

        req_kwargs = defaultdict(list)
        req_kwargs['uid'] = uid
        req_kwargs['algo_name'] = algo_name
        response = self.__algo_service.get_algo_info(req_kwargs)
        if response.status:
            return jsonify({'status': 'success', 'result': response.result, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})

        # response = services.query_algo_info_with_author(request_info)
        # if response.success:
        #     return jsonify({'status': 'success', 'result': response.info, 'message': ''})
        # else:
        #     return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})

