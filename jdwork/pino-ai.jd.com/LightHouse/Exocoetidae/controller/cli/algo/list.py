from collections import defaultdict, OrderedDict

from flask import request, jsonify

from controller.cli.base import Base
import services

from src.services.algo import AlgoService


class QueryAllAlgos(Base):
    def __init__(self):
        super(QueryAllAlgos, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()

    def get(self):
        pass

    def post(self):
        all = request.get_json(force=True).get('all', False)
        user_id = request.get_json(force=True).get('user_id', None)

        algo_list_reponse = self.__algo_service.get_algo_list(user_id, all)
        return jsonify(algo_list_reponse.__dict__)

