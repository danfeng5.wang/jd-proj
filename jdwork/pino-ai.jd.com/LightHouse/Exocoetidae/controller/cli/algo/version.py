from flask import render_template, jsonify, request
from controller.cli.base import Base
from src.services.algo import AlgoService


class AlgoVersion(Base):
    def __init__(self):
        super(AlgoVersion, self).__init__()

    def get(self):
        pass

    def post(self):
        algo_service = AlgoService()
        response = algo_service.get_algo_version()

        return jsonify(response.__dict__)
