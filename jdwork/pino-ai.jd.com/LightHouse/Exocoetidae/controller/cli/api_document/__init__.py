from .view import ApiDocument


def init(app):
    app.add_url_rule('/cli/api_document', view_func=ApiDocument.as_view("cli_api_document"), methods=['GET', 'POST'])
