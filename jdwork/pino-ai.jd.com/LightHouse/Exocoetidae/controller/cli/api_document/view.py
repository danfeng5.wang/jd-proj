from flask import render_template, jsonify
from controller.cli.base import Base


class ApiDocument(Base):
    def __init__(self):
        super(ApiDocument, self).__init__()

    def get(self):

        return render_template('/api_doc.html')

    def post(self):
        pass
