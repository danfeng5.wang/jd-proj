from .nodes import CloudNodes
from .version import CloudVersion
from .quota import CloudQuota


def init(app):
    app.add_url_rule('/cli/cloud/version', view_func=CloudVersion.as_view("cli_cloud_version"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/cloud/nodes', view_func=CloudNodes.as_view("cli_cloud_nodes"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/cloud/quota', view_func=CloudQuota.as_view("cli_cloud_quota"), methods=['GET', 'POST'])
