from flask import render_template, jsonify, request
from controller.cli.base import Base
import PinoCore.services as PinoCoreApi
from src.services.cloud import CloudService
from src.services.task import TaskService
from src.services.user import UserService


class CloudNodes(Base):
    def __init__(self):
        super(CloudNodes, self).__init__()
        self.__user_service = UserService()
        self.__cloud_service = CloudService()
        self.__task_service = TaskService()

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        uid = request.get_json(force=True).get('user_id', None)
        task_id = request.get_json(force=True).get('task_id', None)
        # response = self.__cloud_service.query_task_nodes(task_id)
        user_obj = self.__user_service.get_user_above_info(uid)
        if not user_obj.status:
            return jsonify(user_obj)
        userinfo = user_obj.result
        # response = PinoCoreApi.service_listTaskNode(userinfo['username'], task_id)
        response = self.__cloud_service.cloud_nodes(userinfo, task_id)
        response_dict = response.__dict__
        return jsonify(response_dict)
