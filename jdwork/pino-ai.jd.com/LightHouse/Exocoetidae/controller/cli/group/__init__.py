#!/usr/bin/env python3
# Time: 2017/12/4 17:38

__author__ = 'wanggangshan@jd.com'


from .group_info import GroupInfo
from .list import GroupList


def init(app):
    app.add_url_rule('/cli/group/list', view_func=GroupList.as_view("cli_group_list"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/info/group', view_func=GroupInfo.as_view("cli_info_group"),
                     methods=['GET', 'POST'])