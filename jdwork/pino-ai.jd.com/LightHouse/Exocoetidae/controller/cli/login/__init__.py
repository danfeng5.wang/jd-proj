from .login import Login


def init(app):
    app.add_url_rule('/cli/login', view_func=Login.as_view("cli_login"), methods=['GET', 'POST'])
