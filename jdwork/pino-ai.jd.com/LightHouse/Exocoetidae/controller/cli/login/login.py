import os

import sys

from flask import render_template, jsonify, request, session

from utils.config_helper import config
from controller.cli.base import Base
from utils.util import http_request
from src.services.user import UserService
from db.models import UserSourceType

import climate
logging = climate.get_logger(__name__)


class Login(Base):

    def __init__(self):
        super(Login, self).__init__()
        self.__user_api = UserService()

    def get(self):
        retmsg = {'username': 'hejingjian',
                  'password': '12345678'}
        cls = "1132233"

        return render_template('/login.html', result=retmsg, cls=cls)
        # return render_template('/user/userinfo.html', result=retmsg, cls=cls)

    def post(self):
        kwargs = request.get_json(force=True)
        logging.info('<receive post http><login> username: %s' % kwargs.get('username'))
        username = kwargs.get('username', '')
        password = kwargs.get('password', '')
        data = {
            "sourceType":UserSourceType.CliErp,
            "sourceId":username,
            "password":password
        }
        erp_sso_url = config.get('erp_sso_url')
        response = http_request(erp_sso_url, data=data, request_type='post')
        logging.info('<send post http><login-erp-sso> source_id: %s' % username)
        try:
            res_json = response.json()
        except:
            logging.error('<error><login-erp-sso>kwargs: %s' % kwargs)
            return jsonify({'status': False, 'result': '', 'message':
                '统一验证系统返回数据格式错误'})

        status = res_json.get('status')
        if not status:
            return jsonify(
                {'status': False, 'result': '', 'message': res_json.get('message')})
        else:
            user_source = self.__user_api\
                .get_source_by_username(username, UserSourceType.CliErp)
            if not user_source:

                user_dict = {
                    'username': username,
                    'source_id': username,
                    'source_type': UserSourceType.CliErp,
                    'mobile': res_json.get('mobile', ''),
                    'email': res_json.get('email', ''),
                    'source_describe': '命令行客户端注册用户',
                }
                user_info = self.__user_api.register_user(**user_dict)
                return jsonify(user_info.__dict__)

            user_info = self.__user_api.get_user_above_info(user_source.uid)

            return jsonify(user_info.__dict__)
