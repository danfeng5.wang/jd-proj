from .logs import Logs
from .stop import Stop
from .list import List
from .start import Start, StartResult
from .view import View
from .logs import Logs
from .tty import TTY
from .version import Version
from .dump import Dump
from .release import Release
from .publish import Publish


def init(app):
    app.add_url_rule('/cli/task/list', view_func=List.as_view("cli_task_list"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/stop', view_func=Stop.as_view("cli_task_stop"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/view', view_func=View.as_view("cli_task_view"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/start', view_func=Start.as_view("cli_task_start"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/logs', view_func=Logs.as_view("cli_task_logs"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/tty', view_func=TTY.as_view("cli_task_tty"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/version', view_func=Version.as_view("cli_task_version"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/dump', view_func=Dump.as_view("cli_task_dump"), methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/release', view_func=Release.as_view("cli_task_release"),
                     methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/publish', view_func=Publish.as_view("cli_task_publish"),
                     methods=['GET', 'POST'])
    app.add_url_rule('/cli/task/start/result', view_func=StartResult.as_view("cli_task_start_result"),
                     methods=['GET', 'POST'])