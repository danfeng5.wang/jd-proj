import PinoCore.services as PinoCoreApi
from flask import request, jsonify

from PinoCore.saltwater.saltwater.framework.output_formatter import format_list
from controller.cli.base import Base
from collections import defaultdict, OrderedDict

from src.services.task import TaskService
from src.services.user import UserService


class List(Base):
    def __init__(self):
        super(List, self).__init__()
        self.result = {}
        self.__task_service = TaskService()
        self.__user_service = UserService()

    def get(self):
        pass

    def post(self):
        uid = request.get_json(force=True).get('user_id', None)
        gid = request.get_json(force=True).get('group_id', None)
        all = request.get_json(force=True).get('all', False)
        response = self.__task_service.get_task_list_by_uid(uid)
        if gid:
            pass
        # if not gid:
        #     userinfo = self.__user_service.get_user_above_info(uid)
        #     gids = [group['group_id'] for group in userinfo.result['groups']]
        #     if len(gids) > 1:
        #         return jsonify({'status': False, 'result': {}, 'message': '你属于多个组，请选择当前组'})
        #     if len(gids) < 1:
        #         return jsonify({'status': False, 'result': {}, 'message': '你还未属于任何组，请先加入用户组'})
        #     gid = gids[0]
        #     response = self.__task_service.get_task_list_by_gid(gid)
        return jsonify(response.__dict__)
