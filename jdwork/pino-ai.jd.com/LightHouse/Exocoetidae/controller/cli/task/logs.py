import services
from controller.cli.base import Base
from collections import defaultdict
from common.session_object import user_session_manager
from flask import request
from flask import jsonify

from src.services.task import TaskService


class Logs(Base):
    def __init__(self):
        super(Logs, self).__init__()
        self.result = {}
        self.__task_service = TaskService()

    def get(self):
        pass

    def post(self):
        #print('request:', request.headers)

        username = request.headers.get('Username', None)
        sessionId = request.headers.get('Session', None)

        uid = request.get_json(force=True).get('user_id', None)
        user_obj = self.__user_service.get_user_above_info(uid)
        if not user_obj.status:
            return jsonify(user_obj)
        user_info = user_obj.result

        task_id = request.get_json(force=True).get('task_id', None)
        node_id = request.get_json(force=True).get('node_id', None)
        logfile = request.get_json(force=True).get('logfile', '/export/App/PinoAI/logs/pino_start.log')
        if not logfile:
            logfile = '/export/App/PinoAI/logs/pino_start.log'
        mode = request.get_json(force=True).get('mode', None)

        session = user_session_manager.get_user_session(sessionId)

        if not username or \
           not sessionId or \
           not task_id or \
           not node_id or \
           not logfile or \
           not mode:
            return jsonify({
                'status': 'fail',
                'result': {},
                'message': 'username or session or task_id or node_id or logfile or verb not specified',
            })
        logs_kwargs = defaultdict(list)
        logs_kwargs['user_info'] = user_info
        logs_kwargs['task_id'] = task_id
        logs_kwargs['node_id'] = node_id
        logs_kwargs['logfile'] = logfile
        logs_kwargs['mode'] = mode
        logs_kwargs['session'] = session
        response = self.__task_service.task_logs(**logs_kwargs)
        if not response.status:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})
        return jsonify({'status': 'success', 'result': {}, 'message': ''})


