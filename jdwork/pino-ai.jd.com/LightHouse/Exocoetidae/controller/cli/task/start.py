import uuid
import os
import sys
import arrow
import shutil

from common.session_object import user_session_manager
from controller.cli.base import Base
import server
import ctx
import services
import projects
from flask import request, jsonify

from src.services.group import GroupService
from src.services.task import TaskService
from src.services.user import UserService
from utils.util import unzip, ProjectPathManager
from werkzeug.utils import secure_filename
from threading import Lock

import climate
logging = climate.get_logger(__name__)

thread = None
thread_lock = Lock()


# def sendclientmsg(socket_session, request_info):
#     response = services.service_startTrain(socket_session, request_info)
#     print('>>>>>>>>Response>>>>>>>>>: ',response)
#     if not response.success:
#         print('startTrain fail,', response.errmsg)

class TaskStart(object):
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def start(self):
        print('train kwargs: ', self.kwargs)

        session_uuid = self.kwargs.get('session_uuid')
        project = self.kwargs.get('project_dir')

        # 写死 待定
        resource_path = None

        # project_type 由algo值决定 待定。
        algoType = self.kwargs.get('algo')
        # project_type = projects.PROJECT_PINO_V2

        user_session = user_session_manager.get_user_session(session_uuid)
        config_path = self.kwargs.get('config_path')
        library = self.kwargs.get('library', None)
        token = self.kwargs.get('token', None)
        other_args_dict = self.kwargs.get('other_args_dict')

        print('session: ', user_session.user)

        # Pino_V2
        test = False
        if test:
            request_info = services.StartTrainRequest(
                algoType=projects.PROJECT_PINO_V2,
                project='/export/wanggangshan/project/pino_client/Exocoetidae/demos/mnist.SR',
                resource='/export/wanggangshan/project/pino_client/Exocoetidae/PinoCore/uniKubePino/conf/example/task.json',
                extra={},
            )
            response = services.service_startTrain(user_session, request_info)
            if not response.success:
                return jsonify({'status': 'fail', 'result': {response.errmsg},
                                'message': 'startTrain fail'})

            return jsonify({'status': 'success', 'result': {}, 'message': ''})

        # [PINO_V2 Request Example]
        print('========start args========')
        print('algoType:', algoType)
        print('project:', project)
        print('configure_path:', config_path)
        print('library:', library)
        print('token:', token)
        print('keywords:', other_args_dict)
        print('==========================')
        request_info = services.StartTrainRequest(
            algoType=algoType,
            project=project,
            extra={
                'configure_path': config_path,
                'library': library,
                'keywords': other_args_dict,
                'token': token
            },
        )
        response = services.service_startTrainNew(user_session, request_info)
        if not response.success:
            return jsonify({'status': 'fail', 'result': {}, 'message': 'startTrain fail,' + response.errmsg})
        # print('delete file:', os.path.dirname(project))
        # shutil.rmtree(os.path.dirname(project))
        return jsonify({'status': 'success', 'result': {}, 'message': ''})


class Start(Base):
    def __init__(self):
        super(Start, self).__init__()
        self.__group_service = GroupService()
        self.__user_service = UserService()
        self.__task_service = TaskService()

    def post(self):
        if request.files:
            post_data = request.form
        else:
            post_data = request.get_json(force=True)
        logging.info('post_data:' + str(post_data))
        # 获取用户信息
        user_id = post_data.get('user_id', None)
        userinfo = self.__user_service.get_user_above_info(user_id)
        group_id = post_data.get('gid', None)
        if not group_id:
            gids = [group for group in userinfo.result['groups']]
            if len(gids) > 1:
                return jsonify({'status': 'fail', 'result': {}, 'message': '你属于多个组，请选择当前组'})
            group_id = gids[0]['group_id']
            group_name = gids[0]['group_name']
        logging.info('Userinfo: ' + str(userinfo.result))

        algo = post_data.get('algo', '')
        socket_uuid = post_data.get('session_uuid', '')
        project_manager = ProjectPathManager()
        project_path = post_data.get('project_path', None)
        config_path = post_data.get('config_path', None)

        if not algo:
            return jsonify({'status': 'fail', 'result': {}, 'message': 'algo参数必填'})
        if not socket_uuid:
            return jsonify({'status': 'fail', 'result': {}, 'message': 'websocket未开启'})

        train_base_dir = project_manager.generate_work_path(user_id)
        real_project_dir = train_base_dir
        if project_path:
            project_zip = request.files['project_file']
            p_real_name = secure_filename(project_zip.filename)
            to_file_path = os.path.join(train_base_dir, p_real_name)
            project_zip.save(to_file_path)
            unzip(to_file_path, train_base_dir)
            os.remove(to_file_path)
            real_project_dir = project_manager.check_real_project_dir(train_base_dir)

        config_file_path = None
        if config_path:
            config_file = request.files['config_file']
            c_real_name = secure_filename(config_file.filename)
            config_file_path = os.path.join(train_base_dir, c_real_name)
            config_file.save(config_file_path)

        other_args_dict = post_data.get('other_args_dict', {})
        train_kwargs = {
            'algo': algo,
            'socket_uuid': socket_uuid,
            'project_dir': real_project_dir,
            'config_path': config_file_path,
            'other_args_dict': other_args_dict,
            'group_id': group_id,
            'group_name': group_name,
            'user_info': userinfo.result
        }

        result = self.__task_service.task_start(**train_kwargs)
        return jsonify(result.__dict__)

class StartResult(Base):
    def __init__(self):
        super(StartResult, self).__init__()
        self.__task_service = TaskService()

    def post(self):
        req_kwargs = request.get_json(force=True)
        result = self.__task_service.task_start_result(**req_kwargs)
        return jsonify(result.__dict__)