import services
from controller.cli.base import Base
from collections import defaultdict
from common.session_object import user_session_manager
from flask import request
from flask import jsonify

from src.services.task import TaskService


class TTY(Base):
    def __init__(self):
        super(TTY, self).__init__()
        self.result = {}
        self.__task_service = TaskService()

    def get(self):
        pass

    def post(self):
        # print('>>>> TTY')

        username = request.headers.get('Username', None)
        sessionId = request.headers.get('Session', None)

        task_id = request.get_json(force=True).get('task_id', None)
        node_id = request.get_json(force=True).get('node_id', None)

        session = user_session_manager.get_user_session(sessionId)

        if not username or \
                not sessionId or \
                not task_id or \
                not node_id or \
                not session:
            return jsonify({
                'status': 'fail',
                'result': {},
                'message': 'username or session or task_id or node_id not specified',
            })
        ttu_kwargs = defaultdict(list)
        ttu_kwargs['session'] = session
        ttu_kwargs['task_id'] = task_id
        ttu_kwargs['node_id'] = node_id
        response = self.__task_service.task_tty(**ttu_kwargs)
        if not response.status:
            return jsonify({'status': 'fail', 'result': {}, 'message': 'TTY fail,' + response.errmsg})
        return jsonify({'status': 'success', 'result': {}, 'message': ''})

