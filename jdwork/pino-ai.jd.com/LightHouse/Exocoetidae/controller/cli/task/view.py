import os

import sys

from flask import render_template, jsonify, request, session

import services
from controller.cli.base import Base


class View(Base):
    def __init__(self):
        super(View, self).__init__()
        self.result = {}

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        task_id = request.get_json(force=True).get('task_id', None)

        uid = request.get_json(force=True).get('user_id', None)
        user_obj = self.__user_service.get_user_above_info(uid)
        if not user_obj.status:
            return jsonify(user_obj)
        user_info = user_obj.result
        response = self.__task_service.task_view(user_info, task_id)
        if not response.status:
            return jsonify({'status': 'success', 'result': response.result, 'message': response.message})
        task_detail = response['result']
        print(task_detail)
        self.result['Author'] = task_detail.author
        self.result['UniName'] = task_detail.unique_name
        self.result['Type'] = task_detail.taskType
        self.result['Project'] = task_detail.project
        self.result['Status'] = task_detail.statusName
        self.result['CreateTime'] = task_detail.create_time
        self.result['StatusTime'] = task_detail.statusTime
        self.result['Description'] = task_detail.description
        self.result['Extra'] = {k: v for k, v in task_detail.extra.items()}
        print(self.result)
        return jsonify({'status': 'success', 'result': self.result, 'message': ''})
