from .user_info import UserInfo


def init(app):
    app.add_url_rule('/cli/info/user', view_func=UserInfo.as_view("cli_user_info"), methods=['GET', 'POST'])
