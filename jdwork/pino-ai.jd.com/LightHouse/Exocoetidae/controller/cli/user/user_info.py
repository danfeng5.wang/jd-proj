from flask import render_template, jsonify, request
from controller.cli.base import Base
from src.services.user import UserService

class UserInfo(Base):
    
    def __init__(self):
        super(UserInfo, self).__init__()
        self.__user_api = UserService()


    def get(self):
        pass

    def post(self):
        user_id = request.get_json(force=True).get('user_id', 0)
        user_info = self.__user_api.get_user_above_info(user_id)
        return jsonify(user_info.__dict__)
