#!/usr/bin/env python3
# Time: 2017/12/12 21:13
from controller.common.task.view import TaskStartResult

__author__ = 'hejingjian@jd.com'


def init(app):

    app.add_url_rule('/task/start/result', view_func=TaskStartResult.as_view("task_start_result"),
                     methods=['GET', 'POST'])