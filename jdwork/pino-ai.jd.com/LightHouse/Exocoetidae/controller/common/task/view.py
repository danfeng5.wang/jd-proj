#!/usr/bin/env python3
# Time: 2017/12/12 20:49
from flask import request, jsonify

from controller.common.base import Base

from src.api.common_task import CommonTaskApi

__author__ = 'hejingjian@jd.com'


class TaskStartResult(Base):
    def __init__(self):
        super(TaskStartResult, self).__init__()
        self.__common_task_api = CommonTaskApi()

    def post(self):
        req_kwargs = request.get_json(force=True)
        if not req_kwargs.get('task_id'):
            return jsonify({'status': False, 'error_code': 0, 'result': '', 'message': 'task_id not found '})
        result = self.__common_task_api.update_task_start_status(**req_kwargs)
        return jsonify({'status': result, 'error_code': 0, 'result': '', 'message':''})
