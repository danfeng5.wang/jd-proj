#!/usr/bin/env python3
# Time: 2017/12/10 17:07
from controller.crm.log.log import Log

__author__ = 'hejingjian@jd.com'


def init(app):
    app.add_url_rule('/crm/log', view_func=Log.as_view("crm_log"), methods=['GET', 'POST'])