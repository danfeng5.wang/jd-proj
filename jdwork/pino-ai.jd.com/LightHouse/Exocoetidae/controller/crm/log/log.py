#!/usr/bin/env python3
# Time: 2017/12/10 17:07
from flask import request, jsonify

from controller.crm.base import Base
from src.api.crm_log import CRMLogApi

import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)

__author__ = 'hejingjian@jd.com'


class Log(Base):

    def __init__(self):
        super(Log, self).__init__()
        self.__crm_log_api = CRMLogApi()

    def get(self):
        pass

    def post(self):
        try:
            logging.info('[Exocoetidae controller] request path: %s; request body: %s',
                         str(request.path), str(request.get_json(force=True)))
        except:
            pass
        data = request.get_json(force=True).get('term', {})
        if not data:
            return jsonify({'status': False, 'error_code': 0, 'result': '', 'message': 'report task term is not specified'})
        elif not isinstance(data, dict):
            return jsonify({'status': False, 'error_code': 0, 'result': '', 'message': 'report task term is not a dict'})
        response = self.__crm_log_api.add_crm_log_info(**data)
        return jsonify(response.__dict__)