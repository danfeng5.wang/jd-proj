#!/usr/bin/env python3
# Time: 2017/12/6 21:19
from controller.crm.task.start import CRMTaskStart
from controller.crm.task.stop import CRMTaskStop

__author__ = 'hejingjian@jd.com'

def init(app):
    app.add_url_rule('/crm/task/start', view_func=CRMTaskStart.as_view("crm_task_start"), methods=['GET', 'POST'])
    app.add_url_rule('/crm/task/stop', view_func=CRMTaskStop.as_view("crm_task_stop"), methods=['GET', 'POST'])