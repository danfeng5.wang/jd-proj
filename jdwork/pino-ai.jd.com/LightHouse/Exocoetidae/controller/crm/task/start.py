#!/usr/bin/env python3
# Time: 2017/12/6 21:20
import json
import uuid

from controller.crm.base import Base
from flask import request, jsonify

from src.api.crm_log import CRMLogApi
from src.api.crm_task import CRMTaskApi

__author__ = 'hejingjian@jd.com'


class CRMTaskStart(Base):

    def __init__(self):
        super(CRMTaskStart, self).__init__()
        self.__crm_task_api = CRMTaskApi()

    def get(self):
        pass

    def post(self):
        jsondata = request.get_json(force=True)
        response = self.__crm_task_api.add_task_info(**jsondata)
        return jsonify(response.__dict__)

