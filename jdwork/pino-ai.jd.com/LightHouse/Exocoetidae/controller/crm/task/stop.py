from flask import request, jsonify

from controller.cli.base import Base
from src.api.crm_task import CRMTaskApi
from src.api.user import UserApi


class CRMTaskStop(Base):
    def __init__(self):
        super(CRMTaskStop, self).__init__()
        self.__user_api = UserApi()
        self.__crm_task_api = CRMTaskApi()

    def get(self):
        pass

    def post(self):
        task_id = request.get_json(force=True).get('task_id', None)
        uid = request.get_json(force=True).get('user_id', None)
        user_obj = self.__user_api.get_user_above_info(uid)
        if not user_obj.status:
            return jsonify(user_obj)
        user_info = user_obj.result
        response = self.__crm_task_api.task_stop(task_id)
        if not response.status:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})
        return jsonify({'status': 'success', 'result': response.result, 'message': ''})