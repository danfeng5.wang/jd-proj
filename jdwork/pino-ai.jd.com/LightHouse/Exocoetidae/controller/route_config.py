# coding=utf-8
from controller.cli import algo as cli_algo, cloud as cli_cloud, user as cli_user, task as cli_task, group as cli_group, api_document as cli_api_document, login as cli_login
from controller.web import algo as web_algo, cloud as web_cloud, user as web_user, task as web_task, group as web_group, api_document as web_api_document, login as web_login
from controller.crm import task as crm_task, log as crm_log
from controller.common import task


def route_init(app):
    # common
    task.init(app)

    # log
    crm_log.init(app)

    # user
    cli_user.init(app)
    web_user.init(app)

    # group
    cli_group.init(app)
    web_group.init(app)

    # api_document
    cli_api_document.init(app)
    web_api_document.init(app)

    # login get userinfo
    cli_login.init(app)
    web_login.init(app)

    # get list tasks
    cli_task.init(app)
    web_task.init(app)
    crm_task.init(app)

    # get code list tasks
    cli_algo.init(app)
    web_algo.init(app)

    # cloud view
    cli_cloud.init(app)
    web_cloud.init(app)
