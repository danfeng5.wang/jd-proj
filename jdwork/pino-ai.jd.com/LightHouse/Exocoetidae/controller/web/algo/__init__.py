from .delete import VerifyDeleteAuth, DeleteAlgo
from .download import VerifyDownloadAuth, DownloadAlgo
from .info import QueryAlgoInfo
from .list import QueryAllAlgos
from .share import UpdateToken, QueryAlgoToken
from .upload import UploadAlgo, VerifyUploadAuth
from .version import AlgoVersion


def init(app):
    app.add_url_rule('/web/algo/version', view_func=AlgoVersion.as_view("web_algo_version"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/list', view_func=QueryAllAlgos.as_view("web_algo_list"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/info', view_func=QueryAlgoInfo.as_view("web_algo_info"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/upload', view_func=UploadAlgo.as_view("web_algo_upload"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/verify/upload', view_func=VerifyUploadAuth.as_view("web_algo_verify_upload"),
                     methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/verify/download', view_func=VerifyDownloadAuth.as_view("web_algo_verify_download"),
                     methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/download', view_func=DownloadAlgo.as_view("web_algo_download"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/verify/delete', view_func=VerifyDeleteAuth.as_view("web_algo_verify_delete"),
                     methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/delete', view_func=DeleteAlgo.as_view("web_algo_delete"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/share/update', view_func=UpdateToken.as_view("web_algo_share_update"), methods=['GET', 'POST'])
    app.add_url_rule('/web/algo/share/show', view_func=QueryAlgoToken.as_view("web_algo_share_show"), methods=['GET', 'POST'])

