import services

from controller.web.base import Base
from flask import request, jsonify


class UpdateToken(Base):
    def __init__(self):
        super(UpdateToken, self).__init__()
        self.result = {}

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        algo_name = request.get_json(force=True).get('algo', None)
        request_info = services.AlgoLibRequest(
            author=username,
            algo_name=algo_name,
            async=True)
        response = services.query_algo_token(request_info)
        if not response.success:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
        else:
            response = services.update_token(request_info)
            if not response.success:
                return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
            else:
                return jsonify({'status': 'success', 'result': response.desc, 'message': ''})


class QueryAlgoToken(Base):
    def __init__(self):
        super(QueryAlgoToken, self).__init__()
        self.result = {}

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        algo_name = request.get_json(force=True).get('algo', None)
        request_info = services.AlgoLibRequest(
            author=username,
            algo_name=algo_name)
        response = services.query_algo_token(request_info)
        if response.success:
            return jsonify({'status': 'success', 'result': {"token": response.token}, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
