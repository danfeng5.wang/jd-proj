import os
import services

from flask import request, jsonify
from werkzeug.utils import secure_filename

from controller.web.base import Base
from src.services.algo import AlgoService
from utils.util import ProjectPathManager, unzip, dict_processing


class UploadAlgo(Base):
    def __init__(self):
        super(UploadAlgo, self).__init__()
        self.result = {}
        self.__algo_service = AlgoService()

    def get(self):
        pass

    def post(self):
        # upload file to server
        print('request.files:', request.files)
        username = request.headers.get('Username', None)
        project_zip = request.files['project_file']
        algo_base_dir = ProjectPathManager().generate_work_path(username, 'algo')
        file_name = secure_filename(project_zip.filename)
        to_file_path = os.path.join(algo_base_dir, file_name)
        project_zip.save(to_file_path)
        unzip(to_file_path, algo_base_dir)
        local_dir = algo_base_dir + '/' + os.path.splitext(file_name)[0]
        # author = request.form.get('author', None)
        algo_name = request.form.get('algo', None)
        print('username:', username)
        print('algo_name:', algo_name)
        print('local_dir:', local_dir)
        req_dict = {'username': username,
                    'algo_name': algo_name,
                    'local_dir': local_dir,
                    }
        result = self.__algo_service.upload_algo(**req_dict)
        return jsonify(result)

        # request_info = services.AlgoLibRequest(
        #     author=username,
        #     algo_name=algo_name,
        #     local_dir=local_dir,
        #     async=True
        # )
        # response = services.verify_upload_auth(request_info)
        # if not response.success:
        #     return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
        # else:
        #     response = services.upload_algo(request_info)
        #     if not response.success:
        #         return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
        #     else:
        #         AlgoService().add_algo_info(response.desc)
        #         AlgoService().add_algo_job_history(response.desc)
        #         response.desc = dict_processing(response.desc, {'directory': '******'})
        #         return jsonify({'status': 'success', 'result': response.desc, 'message': ''})


class VerifyUploadAuth(Base):
    def __init__(self):
        super(VerifyUploadAuth, self).__init__()
        self.result = {}

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        project_zip = request.files['project_file']
        algo_base_dir = ProjectPathManager().generate_work_path(username, 'algo')
        file_name = secure_filename(project_zip.filename)
        to_file_path = os.path.join(algo_base_dir, file_name)
        project_zip.save(to_file_path)
        unzip(to_file_path, algo_base_dir)
        local_dir = algo_base_dir + '/' + os.listdir(algo_base_dir)[0]
        # author = request.form.get('author', None)
        algo_name = request.form.get('algo', None)
        request_info = services.AlgoLibRequest(
            author=username,
            algo_name=algo_name,
            local_dir=local_dir
        )
        response = services.verify_upload_auth(request_info)
        if response.success:
            return jsonify({'status': 'success', 'result': {}, 'message': ''})
        else:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.error_message})
