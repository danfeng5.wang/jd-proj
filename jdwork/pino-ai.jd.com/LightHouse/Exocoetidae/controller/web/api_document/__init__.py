from .view import ApiDocument


def init(app):
    app.add_url_rule('/web/api_document', view_func=ApiDocument.as_view("web_api_document"), methods=['GET', 'POST'])
