from .nodes import CloudNodes
from .version import CloudVersion
from .quota import CloudQuota


def init(app):
    app.add_url_rule('/web/cloud/version', view_func=CloudVersion.as_view("web_cloud_version"), methods=['GET', 'POST'])
    app.add_url_rule('/web/cloud/nodes', view_func=CloudNodes.as_view("web_cloud_nodes"), methods=['GET', 'POST'])
    app.add_url_rule('/web/cloud/quota', view_func=CloudQuota.as_view("web_cloud_quota"), methods=['GET', 'POST'])
