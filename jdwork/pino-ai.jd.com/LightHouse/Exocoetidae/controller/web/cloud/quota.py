#!/usr/bin/env python3
# Time: 2017/12/4 11:13

__author__ = 'wanggangshan@jd.com'


from flask import render_template, jsonify, request
from controller.web.base import Base
from src.services.group_quota import GroupQuotaService


class CloudQuota(Base):
    def __init__(self):
        super(CloudQuota, self).__init__()
        self.__group_quota_api = GroupQuotaService()

    def get(self):
        pass

    def post(self):
        user_id = request.get_json(force=True).get('user_id', 0)
        group_id = request.get_json(force=True).get('group_id', 0)
        response = self.__group_quota_api.get_group_quota_usage(user_id, group_id)
        return jsonify(response.__dict__)
