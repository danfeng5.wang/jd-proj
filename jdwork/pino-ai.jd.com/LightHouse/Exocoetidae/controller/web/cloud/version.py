#!/usr/bin/env python3
# Time: 2017/11/29 11:13

__author__ = 'wanggangshan@jd.com'


from flask import render_template, jsonify, request
from controller.web.base import Base
from src.services.cloud import CloudService


class CloudVersion(Base):
    def __init__(self):
        super(CloudVersion, self).__init__()
        self.__cloud_api = CloudService()

    def get(self):
        pass

    def post(self):
        response = self.__cloud_api.get_cloud_version()
        return jsonify(response.__dict__)
