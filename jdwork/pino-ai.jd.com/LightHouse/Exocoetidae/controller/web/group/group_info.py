#!/usr/bin/env python3
# Time: 2017/12/4 17:38

__author__ = 'wanggangshan@jd.com'


from flask import render_template, jsonify, request
from controller.web.base import Base
from src.services.group import GroupService


class GroupInfo(Base):
    def __init__(self):
        super(GroupInfo, self).__init__()
        self.__group_api = GroupService()

    def get(self):
        pass

    def post(self):
        user_id = request.get_json(force=True).get('user_id', 0)
        data = self.__group_api.get_user_all_group_infos(user_id)
        return jsonify(data.__dict__)
