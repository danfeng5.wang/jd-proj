from .register import Register
from .login import Login


def init(app):

    app.add_url_rule('/web/register', view_func=Register.as_view("web_register"), methods=['GET', 'POST'])
    app.add_url_rule('/web/login', view_func=Login.as_view("web_login"), methods=['GET', 'POST'])
