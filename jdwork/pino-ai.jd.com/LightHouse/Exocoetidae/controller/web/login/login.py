import os

import sys

from flask import render_template, jsonify, request, session

from utils.config_helper import config
from controller.web.base import Base
from utils.util import http_request, md5_passwd
from src.services.user import UserService
from db.models import UserSourceType


import climate
logging = climate.get_logger(__name__)


class Login(Base):

    def __init__(self):
        super(Login, self).__init__()
        self.__user_api = UserService()

    def get(self):
        retmsg = {'username': 'hejingjian',
                  'password': '12345678'}
        cls = "1132233"

        return render_template('/login.html', result=retmsg, cls=cls)
        # return render_template('/user/userinfo.html', result=retmsg, cls=cls)

    def post(self):
        kwargs = request.get_json(force=True)
        logging.info(
            '<receive post http><login> username: %s, source_id: %s, '
            'source_type: %s' % (kwargs.get('username'),
                                 kwargs.get('source_id'), kwargs.get('source_type')))
        source_id = kwargs.get('source_id', '')
        source_type = kwargs.get('source_type', 0)
        username = kwargs.get('username', '')
        password = kwargs.get('password', '')
        mobile= kwargs.get('mobile', ''),
        email = kwargs.get('email', ''),
        source_describe = kwargs.get('source_describe', ''),
        if source_type in UserSourceType.from_web_three():
            # 第三方登录,用户未存在话创建新用户
            user_source = self.__user_api.get_source_by_sourceId(source_id,
                                                                 source_type)
            if not user_source:
                user_dict = {
                    'source_id': source_id,
                    'source_type': source_type,
                    'username': username,
                    'mobile': mobile,
                    'email': email,
                    'source_describe': source_describe
                }
                self.__user_api.register_user(**user_dict)
            user_source = self.__user_api.get_source_by_sourceId(source_id,
                                                                 source_type)
        else:
            user_source = self.__user_api.get_source_by_username(username, source_type)
            if not user_source:
                response = {'status': False, 'error_code':1, 'result': '', 'message': '用户名不存在'}
                logging.info(
                    '<receive post http><login> Failed: %s' % response)
                return jsonify(response)
            if md5_passwd(password, user_source.password_salt)[0] != user_source.password:
                response = {'status': False, 'error_code': 2, 'result': '',
                            'message': '密码有误，请重新尝试'}
                logging.info(
                    '<receive post http><login> Failed: %s' % response)
                return jsonify(response)

        user_response = self.__user_api.get_user_above_info(user_source.uid)
        logging.info(
            '<receive post http><login> Reponse: %s' % user_response.__dict__)
        return jsonify(user_response.__dict__)
