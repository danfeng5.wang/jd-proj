import os

import sys

from flask import render_template, jsonify, request, session

from utils.config_helper import config
from controller.web.base import Base
from utils.util import http_request, md5_passwd
from src.services.user import UserService
from src.services.white_list import WhiteListService
from db.models import UserSourceType

import climate
logging = climate.get_logger(__name__)


class Register(Base):

    def __init__(self):
        super(Register, self).__init__()
        self.__user_api = UserService()
        self.__white_list_api = WhiteListService()

    def post(self):
        kwargs = request.get_json(force=True)
        logging.info(
            '<receive post http><register> username: %s' % kwargs.get('username'))
        username = kwargs.get('username', '')
        password = kwargs.get('password', '')

        if self.__user_api.get_source_by_username(username, UserSourceType.Web):
            return jsonify(
                {'status': False, 'error_code': 1, 'result': '', 'message': ''})

        is_super = kwargs.get('is_super', 0)
        password, password_salt = md5_passwd(password)
        user_dict = {
            'username': username,
            'password': password,
            'password_salt': password_salt,
            'mobile': kwargs.get('mobile', ''),
            'email': kwargs.get('email', ''),
            'source_id': '',
            'source_type': UserSourceType.Web,
            'source_describe': kwargs.get('source_describe', ''),
        }
        response = self.__user_api.register_user(**user_dict)
        if response.status:
            user_above_dict = response.result
            self.__white_list_api.add_white(user_above_dict.get('user_id', 0))
        return jsonify(response.__dict__)
