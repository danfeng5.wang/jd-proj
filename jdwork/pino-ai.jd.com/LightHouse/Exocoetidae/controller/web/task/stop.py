import services
from flask import request, jsonify

from controller.web.base import Base
from src.services.task import TaskService
from src.services.user import UserService


class Stop(Base):
    def __init__(self):
        super(Stop, self).__init__()
        self.__user_service = UserService()
        self.__task_service = TaskService()

    def get(self):
        pass

    def post(self):
        username = request.headers.get('Username', None)
        task_id = request.get_json(force=True).get('task_id', None)
        print('username', username, 'task_id', task_id)

        uid = request.get_json(force=True).get('user_id', None)
        user_obj = self.__user_service.get_user_above_info(uid)
        if not user_obj.status:
            return jsonify(user_obj)
        user_info = user_obj.result
        response = self.__task_service.task_stop(user_info, task_id)
        # response = services.service_deleteTask(username, uniName)
        if not response.status:
            return jsonify({'status': 'fail', 'result': {}, 'message': response.message})
        return jsonify({'status': 'success', 'result': response.result, 'message': ''})