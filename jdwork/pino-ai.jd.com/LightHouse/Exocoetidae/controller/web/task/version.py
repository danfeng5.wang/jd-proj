import services
from flask import request, jsonify

from controller.web.base import Base


class Version(Base):
    def __init__(self):
        super(Version, self).__init__()

    def get(self):
        pass

    def post(self):
            return jsonify({'status': True, 'result': {"version": '0.1.0'}, 'message': ''})