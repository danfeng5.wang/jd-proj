from .user_info import UserInfo


def init(app):
    app.add_url_rule('/web/info/user', view_func=UserInfo.as_view("web_user_info"), methods=['GET', 'POST'])
