# -*- coding: utf-8 -*-

__author__ = 'penghao5@jd.com'

import os, sys

from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import TINYINT, INTEGER, BIGINT, FLOAT
from sqlalchemy import (
    VARCHAR, CHAR, SMALLINT, DATE, TEXT, TIMESTAMP,
    DATETIME, TIME, BOOLEAN, DECIMAL, INT
)
from sqlalchemy.schema import Sequence
from sqlalchemy import func
from db.db import ModelBase
import climate

from utils.config_helper import crm_config
from db.db import db_session_manager as db_manager
from db.models import BaseModel

climate.enable_default_logging()
logging = climate.get_logger(__name__)


def to_dict(self):
    return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


# model表字段comment注释 需sqlalchemy1.2才支持，暂不使用。
#
# class CRMAppTable(BaseModel):
#
#     __tablename__ = crm_config.get('db_table.app_table')
#
#     # ID: unique identity for application, integer, auto increment
#     ID = Column(BIGINT, primary_key=True, autoincrement=True)
#
#     # model_id: unique identity for machine learning model, string
#     model_id = Column(CHAR(32), nullable=False, default='None')
#     # train_task_id: supersede train_id, integer
#     train_task_id = Column(BIGINT, nullable=False, default=0)
#     # test_task_id: supersede task_id, integer
#     test_task_id = Column(BIGINT, nullable=False, default=0)
#     # predict_task_id: supersede predict_id, integer
#     predict_task_id = Column(BIGINT, nullable=False, default=0)
#
#     # app_name: custom name of an application, string, nullable
#     app_name = Column(CHAR(32), nullable=False, default='None')
#     # app_task: task of an application, string, [tarin, test]
#     app_task = Column(CHAR(8), nullable=False, default='None')
#     # app_type: type of an application, string, [PinoAI, Spark, ...]
#     app_type = Column(CHAR(32), nullable=False, default='None')
#     # app_status: status of an application, string, [waiting, launching, running, finished, over, unknown]
#     app_status = Column(CHAR(32), nullable=False, default='None')
#     # res_result: succeed of failed, string, [successed, failed, upload failed, unknown]
#     app_result = Column(CHAR(32), nullable=False, default='unknown')
#     # app_configure: configure of an application, json string, python dict
#     app_configure = Column(VARCHAR(20000), nullable=False, default='None')
#     # app_producer: the producer of the application, string, [1.1.1.1:80, ...]
#     app_producer = Column(CHAR(32), nullable=False, default='None')
#     # app_consumer: the consumer of the application, string, [1.1.1.1:80, ...]
#     app_consumer = Column(CHAR(32), nullable=False, default='None')
#
#     # create_time: the timestamp of app creation, double
#     create_time = Column(FLOAT(53), nullable=False, default=0)
#     # launch_time: the timestamp of app start execution, double
#     launch_time = Column(FLOAT(53), nullable=False, default=0)
#     # quit_time: the timestamp of app release resource, double
#     quit_time = Column(FLOAT(53), nullable=False, default=0)
#
#     # res_type: app execution type, string, [docker, k8s]
#     res_type = Column(CHAR(32), nullable=False, default='None')
#     # res_demand: hardware resource demands, string, [<type>_<size>_<num>, ...]
#     res_demand = Column(CHAR(32), nullable=False, default='None')
#     # res_assigned: hardware resource ID assigned, one or more, string, [ID1_ID2, ...]
#     res_assigned = Column(CHAR(255), nullable=False, default='None')
#
#     # retry_times: app retry times
#     retry_times = Column(BIGINT, nullable=False, default=0)
#     # training_result:
#     # training_result = Column(VARCHAR(5000), nullable=False, default='None')
#     # testing_result:
#     # testing_result = Column(VARCHAR(5000), nullable=False, default='None')
#     # download_file_uri: file uri for download
#     download_file_uri = Column(CHAR(255), nullable=False, default='None')
#
#
# class CRMResTable(BaseModel):
#
#     __tablename__ = crm_config.get('db_table.res_table')
#
#     # ID: unique identity for resource unit, integer, auto increment
#     ID = Column(BIGINT, primary_key=True, autoincrement=True)
#
#     # res_name: custom name of resource unit, string, nullable
#     res_name = Column(CHAR(32), nullable=False, default='None')
#     # res_type: type of resource unit, string, [docker, k8s]
#     res_type = Column(CHAR(32), nullable=False, default='None')
#     # res_size: size of resource unit, string, [small, medium, large]
#     res_size = Column(CHAR(32), nullable=False, default='None')
#     # res_desc: description of resource unit, json string, python dict
#     res_desc = Column(CHAR(255), nullable=False, default='None')
#     # res_status: status of resource unit, string, [free, in-use, unknown]
#     res_status = Column(CHAR(32), nullable=False, default='None')
#
#     # employ_time: the timestamp of resource employed, double
#     employ_time = Column(FLOAT(53), nullable=False, default=0)
#     # release_time: the timestamp of resource released, double
#     release_time = Column(FLOAT(53), nullable=False, default=0)
#
#     # app_assigned: application ID assigned, one or more, string, [ID1_ID2, ...]
#     app_assigned = Column(CHAR(255), nullable=False, default='None')
#
#     # hw_ip: hardware ip, string, [1.1.1.1, ...]
#     hw_ip = Column(CHAR(32), nullable=False, default='None')
#     # hw_cpu: hardware cpu information, double
#     hw_cpu = Column(FLOAT(53), nullable=False, default=0)
#     # hw_mem: hardware RAM information, double
#     hw_mem = Column(FLOAT(53), nullable=False, default=0)
#     # hw_disk: hardware disk information, double
#     hw_disk = Column(BIGINT, nullable=False, default=0)


class CRMLogTable(BaseModel):

    __tablename__ = crm_config.get('db_table.log_table')

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    model_id = Column(CHAR(20), primary_key=True, nullable=False, default='')
    role = Column(CHAR(20), primary_key=True, nullable=False, default='')
    number = Column(INT, primary_key=True, nullable=False, default=0)

    is_active = Column(INT, nullable=True, default=0)
    task_stage = Column(CHAR(20), nullable=True, default='')
    role_status = Column(CHAR(50), nullable=True, default='')
    epoch = Column(INT, nullable=True, default=0)
    batches = Column(INT, nullable=True, default=0)
    ip = Column(CHAR(20), nullable=True, default='')
    cpu_usage = Column(FLOAT(53), nullable=True, default=0.0)
    mem_usage = Column(FLOAT(53), nullable=True, default=0.0)
    disk_usage = Column(FLOAT(53), nullable=True, default=0.0)
    net_bw = Column(FLOAT(53), nullable=True, default=0.0)
    cur_time = Column(FLOAT(53), nullable=True, default=0.0)
    epoch_time_cost = Column(FLOAT(53), nullable=True, default=0.0)
    aver_time_cost = Column(FLOAT(53), nullable=True, default=0.0)
    loss = Column(FLOAT(53), nullable=True, default=0.0)
    line_number = Column(BIGINT, nullable=True, default=0)
    predict_id = Column(BIGINT, nullable=True, default=0)
    task_id = Column(BIGINT, nullable=True, default=0)
    train_id = Column(BIGINT, nullable=True, default=0)
    model_form = Column(CHAR(32), nullable=True, default='')
    _auc = Column(FLOAT(53), nullable=True, default=0.0)
    _accuracy = Column(FLOAT(53), nullable=True, default=0.0)
    _precision = Column(FLOAT(53), nullable=True, default=0.0)
    _recall = Column(FLOAT(53), nullable=True, default=0.0)
    _positive_ratio = Column(FLOAT(53), nullable=True, default=0.0)
    _f_1 = Column(FLOAT(53), nullable=True, default=0.0)
    _log_loss = Column(FLOAT(53), nullable=True, default=0.0)
    _rmse = Column(FLOAT(53), nullable=True, default=0.0)
    _mean = Column(FLOAT(53), nullable=True, default=0.0)
    _var = Column(FLOAT(53), nullable=True, default=0.0)
    _max = Column(FLOAT(53), nullable=True, default=0.0)
    _min = Column(FLOAT(53), nullable=True, default=0.0)
    _ri = Column(FLOAT(53), nullable=True, default=0.0)
    _dvi = Column(FLOAT(53), nullable=True, default=0.0)
    _raw = Column(VARCHAR(20000), nullable=True, default='')
