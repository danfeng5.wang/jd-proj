CREATE TABLE `user_info` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `is_super` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否超级用户: 0,普通用户; 1,超级用户',
  `mobile` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '手机',
  -- 预期找回密码策略是基于邮箱的验证
  `email` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '邮箱',
  -- 是否删除与业务的相关性未确定
  `token` CHAR(32) NOT NULL DEFAULT '' COMMENT '用户token',
  `is_delete` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1：已删除, 0：未删除',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='用户信息表';


CREATE TABLE `user_source` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户自增id',
  `source_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '-1-系统创建,0-web_账号，1-web_erp, 2-cli_erp, 3-京pin',
  -- 需要考虑来源 id 超长的情况
  `source_id` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '用户在来源处的用户 ID',
  `username` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '用户名',
  -- 密码加密算法 MD5(user_initial_password + password_salt)
  `password` CHAR(32) NOT NULL DEFAULT '' COMMENT '用户密码，加密',
  `password_salt` CHAR(32) NOT NULL DEFAULT '' COMMENT '用户密码盐',
  `source_describe` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '描述',
  -- 授权码用来存贮第三方验证成功后返回的 token
  `source_auth_code` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '来源授权码',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_source_type_id` (`source_type`, `source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户来源表';


CREATE TABLE `menu`(
  `id` INT(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `url` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '菜单url',
  `tile_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '父组id',
  `is_top` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否顶级菜单:1-是，0-不是',
  `is_bottom` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否叶子节点菜单:1-是，0-不是',
  `is_valid` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '是否可用:1-是，0-不是',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='菜单表';


CREATE TABLE `role_menu`(
  `id` INT(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_code` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '角色码',
  `role_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '角色名称',
  `menu_id` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '菜单url',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';


CREATE TABLE `user_role`(
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户自增id',
  `role_code` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '角色码',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户角色表';


CREATE TABLE `group_info` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '组名',
  `owner_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '组长，组拥有者id',
  `owner_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '组长名',
  `email` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '邮箱',
  `is_delete` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否删除:1-是，0-不是',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_group_name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='组表';


CREATE TABLE `user_belong_group` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户自增id',
  `gid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户组自增id',
  `is_delete` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1：已删除, 0：未删除',
  `created_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '创建者的id',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '创建者的id',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_relate_uid` (`uid`),
  KEY `idx_relate_gid` (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='组和用户关系表';


CREATE TABLE `white_list_user` (
  `id` BIGINT(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户自增id',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_user_id` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='组和用户关系表';


CREATE TABLE `ml_task_history` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` CHAR(32) NOT NULL DEFAULT '' COMMENT 'task标识id, uuid',
  `gid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户组自增id',
  `group_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '本次任务使用的组名',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'task类型: 0-训练, 1-评估',
  `describe` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '任务描述',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '状态: 0,running, -1,失败, 1,成功, -2,用户放弃',
  `is_start` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'k8s启动状态: 0-submit, 1-success',
  `duration` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '任务持续时间',
  `owner_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '提交人id',
  `owner_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '本次任务使用的组名',
  `owner_email` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '提交人邮箱',
  `algo_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '运行算法id',
  `algo_engine_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '算法引擎类型： 0-？，1-？  待确认',
  `algo_engine_version` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '算法引擎版本',
  `algo_configure` VARCHAR(10240) NOT NULL DEFAULT '' COMMENT '算法详细属性',
  `algo_run_args` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '算法运行参数',
  `cloud_cpus` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '指定cpus数量',
  `cloud_gpus` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '指定cpus数量',
  `cloud_mems` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '指定内存大小',
  `cloud_disk` INT(10) unsigned NOT NULL DEFAULT 0 COMMENT '指定disk大小',
  `cloud_node_info` VARCHAR(4096) NOT NULL DEFAULT '' COMMENT '运行节点信息',
  `cloud_env_info`    VARCHAR(512) NOT NULL DEFAULT '' COMMENT '运行环境信息',
  `cloud_extra_info` VARCHAR(4096) NOT NULL DEFAULT '' COMMENT '运行资源使用统计',
  `is_release` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否已释放资源: 1,是 0,不是',
  `is_clean` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否clean方式释放: 1,是 0,不是',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_task_id` (`task_id`),
  KEY `inx_user_id` (`owner_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='训练任务记录表';


CREATE TABLE `algo_job_history` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` CHAR(32) NOT NULL DEFAULT '' COMMENT 'job标识id，uuid',
  `author_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '任务提交人id',
  `algo_name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '算法名',
  `action_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '用户操作类型：1-download, 2-upload, 3-update, 4-delete ',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '任务状态：0,doing,1, 完成',
  `directory` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '任务相关路径',
  `description` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT '任务信息',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `created_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '创建者的id',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='算法库操作记录表';


CREATE TABLE `algo_list` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `author_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户自增id',
  `author_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '算法作者账号名',
  `algo_name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '算法名',
  `description` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '详细属性',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='算法库';

CREATE TABLE `algo_auth` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `algo_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '算法实例id',
  `group_or_user` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '算法实例权限针对组还是个人：1，组，0，个人',
  `gid_or_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户uid或者组uid',
  `auth_code` VARCHAR(10) NOT NULL DEFAULT '0000' COMMENT '读/下载/更新/执行的权限: "1100"，1：拥有权限，0：没有权限',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='算法库';

CREATE TABLE `group_quota` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户组id',
  `cpus` FLOAT NOT NULL DEFAULT 0.0 COMMENT '指定cpus数量个',
  `gpus` FLOAT NOT NULL DEFAULT 0.0 COMMENT '指定gpus数量个',
  `mems` FLOAT NOT NULL DEFAULT 0.0 COMMENT '指定内存大小M',
  `disk` FLOAT NOT NULL DEFAULT 0.0 COMMENT '指定disk大小个',
  `over_use_rate` FLOAT NOT NULL DEFAULT 1.0 COMMENT '超额率',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='资源表';



-- ------------------------- CRM 新增 ----------------------------


CREATE TABLE `common_task_list` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` CHAR(32) NOT NULL DEFAULT '' COMMENT 'task标识id, uuid',
  `topic_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '任务来源类型: 1-cli， 2-web， 3-crm, 4-jmq',
  `task_type` CHAR(8) NOT NULL DEFAULT '' COMMENT '任务类型 1-train 2-eval 3-predict',
  `algo_author_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '算法作者id',
  `algo_name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '算法名',
  `engine_type` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '练框架引擎+版本',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '任务结果状态: 0:处理中, -2: 任务终极失败, -1:本次失败(待处理是否重试), 1:成功',
  `period` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '任务处理阶段: 0-waitting, 1-queued, 2-submited, 3-running, 4-complete',
  `mfs_path` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'mfs_path 任务相关文件存储地址',
  `owner_uid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '提交人id',
  `owner_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '提交人用户名',
  `gid` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '用户组自增id',
  `group_name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '本次任务使用的组名',
  `try_num` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '执行次数，用于启动失败后重试次数校验，暂定为5',
  `error_msg` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '每次执行结果描述，多条以|分割追加',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `model_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '模型id',
  `train_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '训练id',
  `test_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '评估id',
  `predict_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '预估id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_task_id` (`task_id`),
  KEY `inx_user_id` (`owner_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='训练任务记录表';


-- - CRM APP Table



CREATE TABLE `crm_log_table` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` CHAR(20) NOT NULL DEFAULT '' COMMENT '模型id',
  `role` CHAR(20) NOT NULL DEFAULT '' COMMENT '机器角色',
  `number` INT(10) NOT NULL DEFAULT 0 COMMENT '编号',
  `is_active` INT(20) NOT NULL DEFAULT 0 COMMENT '是否活跃',
  `task_stage` CHAR(20) NOT NULL DEFAULT '' COMMENT '任务阶段',
  `role_status` CHAR(20) NOT NULL DEFAULT '' COMMENT '角色状态',
  `epoch` INT(20) NOT NULL DEFAULT 0 COMMENT 'epoch',
  `batches` INT(20) NOT NULL DEFAULT 0 COMMENT '批次',
  `ip` CHAR(20) NOT NULL DEFAULT '' COMMENT 'ip',
  `cpu_usage` FLOAT(53) NOT NULL DEFAULT '0' COMMENT 'cpu利用率',
  `mem_usage` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '内存利用率',
  `disk_usage` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '硬盘利用率',
  `net_bw` FLOAT(53) NOT NULL DEFAULT '0' COMMENT 'net_bw',
  `cur_time` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '时间',
  `epoch_time_cost` FLOAT(53) NOT NULL DEFAULT '0' COMMENT 'epoch_time_cost',
  `aver_time_cost` FLOAT(53) NOT NULL DEFAULT '0' COMMENT 'aver_time_cost',
  `loss` FLOAT(53) NOT NULL DEFAULT '0' COMMENT 'loss',
  `line_number` BIGINT(20) NOT NULL DEFAULT 0 COMMENT 'line_number',
  `predict_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '硬盘',
  `task_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '任务id',
  `train_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '训练id',
  `model_form` CHAR(32) NOT NULL DEFAULT '' COMMENT '模型',
  `_auc` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_auc',
  `_accuracy` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_accuracy',
  `_precision` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_precision',
  `_recall` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_recall',
  `_positive_ratio` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_positive_ratio',
  `_f_1` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_f_1',
  `_log_loss` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_log_loss',
  `_rmse` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_rmse',
  `_mean` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_mean',
  `_var` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_var',
  `_max` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_max',
  `_min` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_min',
  `_ri` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_ri',
  `_dvi` FLOAT(53) NOT NULL DEFAULT '0' COMMENT '指标_dvi',
  `_raw` VARCHAR(20000) NOT NULL DEFAULT '' COMMENT '指标_raw',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='crm日志表';

-- ----- typetable
-- drop table `type_enum`;
CREATE TABLE `type_enum` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '类型，algo等',
  `name` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '关键字',
  `enums` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '列举集合，type1|type2|type3...',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='统一类型表';

insert into type_enum (type, name, enums)values('algo', 'algorithm_type', 'LR|GBDT|SVM|CNN|DNN|RNN');
insert into type_enum (type, name, enums)values('algo', 'engine_type', 'TENSORFLOE|XGBOOST|PINOAI');
insert into type_enum (type, name, enums)values('algo', 'tensorflow_version', '1.3.1|1.4.0-rc1');
insert into type_enum (type, name, enums)values('algo', 'xgboost_version', '0.6');
insert into type_enum (type, name, enums)values('algo', 'pinoai_version', '2.0');
insert into type_enum (type, name, enums)values('algo', 'required_args_type', 'FLAOT|DOUBLE|INT|STR|BOOL|LIST|DICT|SET');
insert into type_enum (type, name, enums)values('algo', 'processor_type', 'CPU|GPU');



-- ----------------radar
-- DROP TABLE `radar_task_list`;
CREATE TABLE `radar_task_list` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` CHAR(32) NOT NULL DEFAULT '' COMMENT 'task标识id, uuid',
  `task_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '任务类型 1-generalize 2-dmp 3-push_jt',
     `source_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1-雷达 系统标识区分人群种子包来源渠道',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '任务结果状态: 0:处理中, -2: 任务终极失败, -1:本次失败(待处理是否重试), 1:成功',
  `try_num` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '执行次数，用于启动失败后重试次数校验',
  `error_msg` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '每次执行结果描述，多条以|分割追加',
  `callback_url` VARCHAR(256) NOT NULL DEFAULT '' COMMENT 'JMQ通讯可为空，回调地址，人群包泛化结果通知或投放结果通知',
    `callback_param` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '需要回调时的种子包信息',
    `is_generalize` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0否1是，是否需要泛化处理',
    `is_dmp` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0否1是，是否需要投放',
    `dmp_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1-广告部dmp 默认广告部dmp，投放渠道类型',
    `dmp_url` VARCHAR(256) NOT NULL DEFAULT '' COMMENT '默认广告部dmp地址，投放渠道地址',
    `push_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1-京腾魔方 不需泛化、投放处理时，透传的其他平台类型',
    `push_url` VARCHAR(256) NOT NULL DEFAULT '' COMMENT '不需泛化、投放处理时，透传的其他平台地址',
    `erp` CHAR(64) NOT NULL DEFAULT '' COMMENT '操作人erp',
    `crowd_bag_id` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包id',
    `crowd_bag_name` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包名称',
    `crowd_bag_url` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '人群包地址',
    `crowd_bag_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '人群包类型 1-种子包，2-映射包，3-泛化包',
    `idtype` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1，京东pin；2，手机号，3手机号MD5值，4设备ID包',
    `encryption` CHAR(32) NOT NULL DEFAULT '' COMMENT '加密算法类型 DESede',
    `private_key` CHAR(128) NOT NULL DEFAULT '' COMMENT '私钥，字节码',
    `generalize_id` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '泛化序列号',
    `channel_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '期望泛化的媒体渠道类型 1-京条、2-京度、3-京腾',
    `target_size` INT(32) NOT NULL DEFAULT 0 COMMENT '期望的结果包人群量级 数值',
    `level` CHAR(32) NOT NULL DEFAULT '' COMMENT '扩展优先级',
    `account_list` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '要分配到的京准通商家pin，此参数的最大pin个数为50(pin之间英文逗号分隔)',
    `crowd_id_list` CHAR(20) NOT NULL DEFAULT '' COMMENT ' 有为更新，没有为新建，可选，京腾魔方接口专用',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_task_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='雷达任务表';


-- DROP TABLE `generalize_task_result`;
CREATE TABLE `generalize_task_result` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `generalize_id` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '泛化序列号',
     `bag_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1-映射包，2-扩展包',
    `crowd_bag_id` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包id',
    `crowd_bag_name` CHAR(256) NOT NULL DEFAULT '' COMMENT '人群包名称',
    `crowd_bag_url` VARCHAR(256) NOT NULL DEFAULT '' COMMENT '人群包地址',
    `size` INT(32) NOT NULL DEFAULT 0 COMMENT '条数',
    `idtype` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1，京东pin；2，手机号，3手机号MD5值，4设备ID包',
    `encryption` CHAR(32) NOT NULL DEFAULT '' COMMENT '加密算法类型 DESede',
    `private_key` CHAR(128) NOT NULL DEFAULT '' COMMENT '私钥，字节码',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='雷达泛化任务结果表';


-- DROP TABLE `dmp_task_result`;
CREATE TABLE `dmp_task_result` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` CHAR(32) NOT NULL DEFAULT '' COMMENT '任务id',
    `crowd_bag_id` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包id',
    `pin` CHAR(64) NOT NULL DEFAULT '' COMMENT 'pin,广告主账号',
    `status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '导入人群是否成功：1成功，0失败',
    `error_code` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '失败code枚举：10:人群已分配；20:pin不是京准通用户 ',
    `message` CHAR(128) NOT NULL DEFAULT '' COMMENT '导入失败原因描述',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='雷达投放任务结果表';


-- DROP TABLE `jt_ms_task_result`;
CREATE TABLE `jt_ms_task_result` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `generalize_id` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '泛化序列号',
    `crowd_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '魔方人群类型 1-创建魔方人群，2-扩展魔方人群',
    `status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '状态，1成功，0失败',
    `error_code` CHAR(32) NOT NULL DEFAULT '' COMMENT '错误码',
    `message` CHAR(64) NOT NULL DEFAULT '' COMMENT '异常信息',
    `account` CHAR(64) NOT NULL DEFAULT '' COMMENT '用户账号 请求中的投放账号',
    `crowd_id` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包id',
    `crowd_name` CHAR(64) NOT NULL DEFAULT '' COMMENT '人群包名称',
    `level` INT(32) NOT NULL DEFAULT 0 COMMENT '量级',
    `crowd_status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '人群状态 1-生效中，2-已生效',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='雷达京腾任务结果表';



-- -------------------isv-----------------------
-- DROP TABLE `yarn_tasks`;
CREATE TABLE `yarn_tasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `application_id` varchar(128) DEFAULT NULL COMMENT 'application_id',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `state` varchar(32) DEFAULT NULL COMMENT '状态',
  `final_state` varchar(32) DEFAULT NULL COMMENT '最终状态',
  `progress` varchar(16) DEFAULT NULL COMMENT '进度',
  `created_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `updated_at` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
