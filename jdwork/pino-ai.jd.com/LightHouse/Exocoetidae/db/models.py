# -*- coding: utf-8 -*-
# wanggangshan@jd.com

import os, sys

from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import TINYINT, INTEGER, BIGINT, FLOAT
from sqlalchemy import (
    VARCHAR, CHAR, SMALLINT, DATE, TEXT, TIMESTAMP,
    DATETIME, TIME, BOOLEAN, DECIMAL
)
from sqlalchemy.schema import Sequence
from sqlalchemy import func
from db.db import ModelBase
import climate

from utils.config_helper import config
from db.db import db_session_manager as db_manager

climate.enable_default_logging()
logging = climate.get_logger(__name__)


class BaseModel(ModelBase):
    __abstract__ = True
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }


def to_dict(self):
    return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}


# model表字段comment注释 需sqlalchemy1.2才支持，暂不使用。

class WhiteListUser(BaseModel):
    """
        白名单表
            ps: 控制用户可用pino系统，非erp用户默认添加白名单，erp需单独申请
    """
    __tablename__ = config.get('db_table.white_list_user')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 用户自增id
    uid = Column(BIGINT(20), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class UserInfo(BaseModel):
    """
        用户信息表
    """
    __tablename__ = config.get('db_table.user_info')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 是否超级管理员: 0-不是, 1-是
    is_super = Column(TINYINT(1), nullable=False, default=0)
    mobile = Column(VARCHAR(20), nullable=False, default='')
    email = Column(VARCHAR(128), nullable=False, default='')
    is_delete = Column(TINYINT(1), nullable=False, default=0)
    token = Column(VARCHAR(32), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class UserSourceType(object):
    System = -1
    Web = 0
    WebErp = 1
    CliErp = 2
    WebPin = 3

    @classmethod
    def from_web_three(cls):
        return [cls.WebErp, cls.WebPin]


    @classmethod
    def trans_type(cls, type):
        if type == 'sys':
            return cls.System
        if type == 'web':
            return cls.System
        if type == 'webErp':
            return cls.WebErp
        if type == 'cliErp':
            return cls.CliErp
        if type == 'WebPin':
            return cls.WebPin

class UserSource(BaseModel):
    """
        用户来源信息表
    """
    __tablename__ = config.get('db_table.user_source')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 用户自增id
    uid = Column(BIGINT(20), nullable=False, default=0)
    # 来源id
    source_id = Column(VARCHAR(255), nullable=False, default='')
    # 用户名 要求用户必输，erp登录默认写入erp账号
    username = Column(VARCHAR(64), nullable=False, default='')
    # password加密加盐后
    password = Column(CHAR(32), nullable=False, default='')
    # password加密用的盐
    password_salt = Column(CHAR(32), nullable=False, default='')
    # 用户来源类型: 0-网站注册, 1-web_erp, 2-cli_erp, cli_erp-....
    source_type = Column(TINYINT(4), nullable=False, default=0)
    # 来源描述
    source_describe = Column(VARCHAR(64), nullable=False, default='')
    # 授权码用来存贮第三方验证成功后返回的 token
    source_auth_code = Column(VARCHAR(128), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))


class GroupInfo(BaseModel):
    """
        用户组信息
    """
    __tablename__ = config.get('db_table.group_info')

    # 自增组
    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 组名
    group_name = Column(VARCHAR(64), nullable=False, default='')
    # 组管理员/组长
    owner_uid = Column(VARCHAR(64), nullable=False, default=0)
    # 组长名
    owner_name = Column(VARCHAR(64), nullable=False, default='')
    email = Column(VARCHAR(128), nullable=False, default='')
    is_delete = Column(TINYINT(1), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class UserBelongGroup(BaseModel):
    """
        用户与组所属关系表
    """
    __tablename__ = config.get('db_table.user_belong_group')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 用户自增id
    uid = Column(BIGINT(20), nullable=False, default=0)
    # 用户组自增id
    gid = Column(BIGINT(20), nullable=False, default=0)
    is_delete = Column(TINYINT(1), nullable=False, default=0)
    # 创建该用户的用户id，默认为系统system
    created_uid = Column(BIGINT(20), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_uid = Column(BIGINT(20), nullable=False, default=0)
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class GroupQuota(BaseModel):
    """
        用户组信息
    """
    __tablename__ = config.get('db_table.group_quota')

    # 自增组
    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 组id
    group_id = Column(BIGINT(20), nullable=False, default=0)
    # 运行本次任务需要的cpu数量(个)
    cpus = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的gpu数量(个)
    gpus = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的内存(M)
    mems = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的disk(M)
    disk = Column(INTEGER(unsigned=True), nullable=False, default=0)
    over_use_rate = Column(FLOAT(64), nullable=False, default='1.0')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class Menu(BaseModel):
    """
        菜单栏
    """
    __tablename__ = config.get('db_table.menu')

    id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True)
    # (子)菜单url
    url = Column(VARCHAR(128), nullable=False, default='')
    # 标题名称
    title_name = Column(VARCHAR(20), nullable=False, default='')
    # 父级菜单id
    parent_id =Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 是否顶级菜单
    is_top = Column(TINYINT(1), nullable=False, default=0)
    # 是否叶子节点踩点
    is_bottom = Column(TINYINT(1), nullable=False, default=0)
    # 是否有效子菜单
    is_valid = Column(TINYINT(1), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class RoleMenu(BaseModel):
    """
        角色与菜单关系表
    """
    __tablename__ = config.get('db_table.role_menu')

    id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True)
    # 角色码: et: 100203
    role_code = Column(VARCHAR(32), nullable=False, default='')
    # 角色名称
    role_name = Column(VARCHAR(64), nullable=False, default='')
    # 角色对应的拥有的权限菜单id（默认不包含其子菜单）
    menu_id = Column(INTEGER(unsigned=True), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class UserRole(BaseModel):
    """
        用户角色表
    """
    __tablename__ = config.get('db_table.user_role')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 用户自增id
    uid = Column(BIGINT(20), nullable=False, default=0)
    # 角色码: et: 100203
    role_code = Column(VARCHAR(32), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class AlgoList(BaseModel):
    """
        算法实例表
    """
    __tablename__ = config.get('db_table.algo_list')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 作者用户自增id
    author_uid = Column(BIGINT(20), nullable=False, default=0)
    # 作者账号名
    author_name = Column(VARCHAR(64), nullable=False, default='')
    # 算法实例名称
    algo_name = Column(VARCHAR(128), nullable=False, default='')
    # 算法配置的详细信息json串
    description = Column(VARCHAR(20), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class AlgoJobHistory(BaseModel):
    """
        用户操作算法实例历史记录表
    """
    __tablename__ = config.get('db_table.algo_job_history')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 本次操作job的唯一标识
    job_id = Column(CHAR(32), nullable=False, default='')
    # 该算法实例的作者id
    author_uid = Column(BIGINT(20), nullable=False, default=0)
    # 算法实例名称
    algo_name = Column(VARCHAR(128), nullable=False, default='')
    # 本次操作类型: 1-download, 2-upload, 3-update, 4-delete
    action_type = Column(TINYINT(4), nullable=False, default=0)
    # 本次操作状态: 0-处理中, 1-成功, 2-失败
    status = Column(TINYINT(1), nullable=False, default=0)
    # 服务器临时保存目录
    directory = Column(VARCHAR(1024), nullable=False, default='')
    # 本次操作相关信息
    description = Column(VARCHAR(20), nullable=False, default='')
    # 操作用户的uid
    created_uid = Column(BIGINT(20), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class AlgoAuth(BaseModel):
    """
        算法库权限管理表
    """
    __tablename__ = config.get('db_table.algo_auth')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # 算法实例的id
    algo_id = Column(BIGINT(20), nullable=False, default=0)
    group_or_user = Column(TINYINT(1), nullable=False, default=0)
    # 权限设置针对组或个人的唯一id
    gid_or_uid = Column(BIGINT(64), nullable=False, default=0)
    # 拥有的权限：读(info)/下载/写（更新删除）/执行， et: '1100' (可读和下载，不可写和执行)
    auth_code = Column(VARCHAR(10), nullable=False, default='')
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))

    @property
    def AuthInfo(self):
        auth_info = {
            'read': 0,
            'download': 0,
            'write': 0,
            'exec': 0
        }

        code = self.auth_code
        if int(code[0]):
            auth_info['read'] = 1
        if int(code[1]):
            auth_info['download'] = 1
        if int(code[2]):
            auth_info['write'] = 1
        if int(code[3]):
            auth_info['exec'] = 1
        return auth_info

    def set_auth_code(self, auth_info):
        read = '1' if auth_info['read'] else '0'
        download = '1' if auth_info['download'] else '0'
        write = '1' if auth_info['write'] else '0'
        exec = '1' if auth_info['exec'] else '0'
        code = read + download + write + exec
        self.auth_code = code


class MlTaskHistory(BaseModel):
    """
        machine learning 的任务记录表
    """
    __tablename__ = config.get('db_table.ml_task_history')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # ml的唯一的任务id
    task_id = Column(CHAR(32), nullable=False, default='')
    # ml的唯一的任务id
    gid = Column(BIGINT(20), nullable=False, default=0)
    # 跑本次ml任务指定用的组身份名称，只有一个组不需指定,默认为该组
    group_name = Column(VARCHAR(64), nullable=False, default='')
    # 任务类型: 0-训练， 1-评估， 2-...
    type = Column(TINYINT(4), nullable=False, default=0)
    # 任务描述信息
    describe = Column(VARCHAR(255), nullable=False, default='')
    # 任务状态: 0:处理中, -2:用户放弃,-1:失败, 1:成功
    status = Column(TINYINT(1), nullable=False, default=0)
    # 启动状态: 0-submit, 1-success
    is_start = Column(TINYINT(1), nullable=False, default=0)
    # 任务执行持续时间 秒
    duration = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 提交本次任务的uid
    owner_uid = Column(BIGINT(20), nullable=False, default=0)
    # 提交本次任务的username
    owner_name = Column(VARCHAR(64), nullable=False, default='')
    # 提交本次任务的用户的邮箱
    owner_email = Column(VARCHAR(64), nullable=False, default='')
    # 算法实例名称
    algo_id = Column(BIGINT(20), nullable=False, default=0)
    # 算法引擎类型： 0-？，1-？  待确认
    algo_engine_type = Column(TINYINT(4), nullable=False, default=0)
    # 算法引擎版本
    algo_engine_version = Column(VARCHAR(10), nullable=False, default='')
    # 算法配置的详细信息json串
    algo_configure = Column(VARCHAR(10240), nullable=False, default='')
    # 算法运行时的相关参数串
    algo_run_args = Column(VARCHAR(255), nullable=False, default='')
    # 运行本次任务需要的cpu数量(个)
    cloud_cpus = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的gpu数量(个)
    cloud_gpus = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的内存(M)
    cloud_mems = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # 运行本次任务需要的disk(M)
    cloud_disk = Column(INTEGER(unsigned=True), nullable=False, default=0)
    # k8s运行过程中node节点的信息记录
    cloud_node_info = Column(VARCHAR(4096), nullable=False, default='')
    # k8s运行过程中环境的信息记录
    cloud_env_info = Column(VARCHAR(512), nullable=False, default='')
    # k8s运行过程中资源统计信息
    cloud_extra_info = Column(VARCHAR(4096), nullable=False, default='')
    # 是否已release释放(任务完成后的资源释放): 0-不是，1-是
    is_release = Column(TINYINT(1), nullable=False, default=0)
    # 是否已clean释放(任何时候强制释放): 0-不是，1-是
    is_clean = Column(TINYINT(1), nullable=False, default=0)
    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))



class TaskPeriod(object):
    Waiting = 0
    Queued = 1
    Submitted = 2
    Running = 3
    Complete = 4


class CommonTaskList(BaseModel):
    """
        machine learning 的common任务记录表
    """
    __tablename__ = config.get('db_table.common_task_list')

    id = Column(BIGINT(20), primary_key=True, autoincrement=True)
    # ml的唯一的任务id
    task_id = Column(CHAR(32), nullable=False, default='')
    # 任务来源类型: 1-cli， 2-web， 3-crm, 4-...
    topic_type = Column(TINYINT(4), nullable=False, default=0)
    # *任务类型 train eval predict
    task_type = Column(CHAR(8), nullable=False, default='')
    # *该算法实例的作者id
    algo_author_uid = Column(BIGINT(20), nullable=False, default=0)
    # *算法实例名称
    algo_name = Column(VARCHAR(128), nullable=False, default='')
    # *训练框架引擎+版本 见columntype.py文件EngineType类定义
    engine_type = Column(VARCHAR(64), nullable=False, default='')
    # 任务结果状态: 0:处理中, -2:任务终极失败, -1:当次执行失败（待处理是否重试）, 1:启动成功， 2：任务执行成功
    status = Column(TINYINT(1), nullable=False, default=0)
    # 任务处理阶段: 0-waiting, 1-queued, 2-submitted, 3-running
    period = Column(TINYINT(1), nullable=False, default=0)
    # mfs_path 任务相关文件存储地址
    mfs_path = Column(VARCHAR(128), nullable=False, default='')
    # 提交本次任务的uid
    owner_uid = Column(BIGINT(20), nullable=False, default=0)
    # 提交本次任务的username
    owner_name = Column(VARCHAR(64), nullable=False, default='')
    # 提交本次任务组id
    gid = Column(BIGINT(20), nullable=False, default=0)
    # 跑本次ml任务指定用的组身份名称，只有一个组不需指定,默认为该组
    group_name = Column(VARCHAR(64), nullable=False, default='')
    # 执行次数，用于启动失败后重试次数校验，暂定为5
    try_num = Column(INTEGER(2), nullable=False, default=0)
    # 异常描述
    error_msg = Column(VARCHAR(5000), nullable=False, default='')
    
    # 奇怪的字段
    #need_quota = Column(VARCHAR(1000), nullable=False, default='')
    #run_cmd = Column(VARCHAR(1000), nullable=False, default='')
    #run_type = Column(VARCHAR(20), nullable=False, default='')

    created_at = Column(TIMESTAMP(6), nullable=False, server_default=text('NOW()'))
    updated_at = Column(TIMESTAMP(6), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))

    predict_id = Column(BIGINT(20), nullable=False, default=0)
    test_id= Column(BIGINT(20), nullable=False, default=0)
    model_id = Column(BIGINT(20), nullable=False, default=0)
    train_id = Column(BIGINT(20), nullable=False, default=0)


def init_db():
    engine = db_manager.get_engine()
    BaseModel.metadata.create_all(engine)


def drop_db():
    engine = db_manager.get_engine()
    BaseModel.metadata.drop_all(engine)
