
baseDir=$(realpath $(dirname $0))
envFile=${baseDir}/env.py
lineExpect=$(cat ${envFile} | grep hdfs_username)

[ -z "$(cat ${envFile} | grep hdfs_username | sed '/^[ \t]*#/d')" ] && \
    echo "hdfs_username=''" >> ${envFile}

