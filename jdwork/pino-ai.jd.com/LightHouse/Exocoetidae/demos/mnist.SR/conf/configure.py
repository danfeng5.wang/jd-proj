# -*- coding: utf-8 -*-
# zhangzehua@jd.com
from env import username
from env import password

# yapf : disable
###
model_configure = dict(
    form='PointwiseModel',
    structure=dict(
        blocks=[
            (
                'SequentialBlock',
                'SequentialDemo',  # name
                ['image_input'],  # inputs
                [dict(
                    form='TenseInput',
                    conf=dict(size=784),),],),
        ],
        output_act='softmax',
        output_size=10,
        losses=[dict(
            form='CrossEntropyPosTarget',),],
        export_dir='../dumps',),
    inputs=dict(
        image_input=dict(
            convertor=dict(
                form='ImageConvertor',
                conf=dict(
                    label_sz=10,
                    feature_sz=784,),),),),)
###
train_configure = dict(
    algo='Adam',
    train_dataset=dict(
        filesystem='local_image',
        file_pattern='../data/mnist_dataset_train_*.dat',  ## Here support the regex expression
        #iter_num = 5,
    ),
    valid_dataset=dict(
        filesystem='local_image',
        file_pattern='../data/mnist_dataset_train_*.dat',  ## Here support the regex expression
        #iter_num = 5,
    ),
    eval_dataset=dict(
        filesystem='local_image',
        file_pattern='../data/mnist_dataset_validation_*.dat',  ## Here support the regex expression
        metrics=['accuracy',],),)
###
cluster_configure = dict(
    servers=dict(
        ip=['1.1.1.1',],),
    masters=dict(
        ip=['1.1.1.1',],),
    workers=dict(
        ip=['1.1.1.1',],),
    monitors=dict(
                ip=['10.187.140.202',],
                ports=['6010',],
                mode='standalone',
                version='1.0'),
)
# yapf: enable
