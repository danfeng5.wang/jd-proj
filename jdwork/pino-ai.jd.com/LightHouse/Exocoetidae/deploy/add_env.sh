#!/bin/sh

project_dir=$1
port=$2
kubectl=$3

which kubectl > /dev/null 2>&1

if [ $? -ne 0 ];
then
    echo "export PATH=${project_dir}/${port}/${kubectl}:\$PATH" >> $HOME/.bashrc
    source $HOME/.bashrc
fi