#!/bin/sh

init_conf () {
    sh ./$1/$2/$3
    echo ">>>>>>> run env >>>>>>>: $1 $2"
}

run_project=$1
run_branch=$2
init_sh="init.sh"

init_conf $1 $2 $init_sh

# include deploy'configure with baseconf and ipconf.
. ./base.ip.conf

deploy () {
    ip=${1}
    port=${2}

    echo "deploy ${ip}:${port} begin"
    cur_time=$(date +%Y_%m_%d_%H_%M_%S)
    log_file="${log_dir}/${ip}_${port}__${cur_time}.log"
    run_args="-i ${ip} -p ${port}"

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${project_dir}/${port}
        cd ${project_dir}/${port}
        ls |grep -v ${log_dir} | grep -v 'PinoCore' | xargs /bin/rm -rf"
    sshpass -p '0okm(IJN' scp -r ../*  admin@${ip}:${project_dir}/${port}

    sshpass -p '0okm(IJN' ssh  admin@${ip} "/usr/sbin/lsof -i:${port}|grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}
        cd ${project_dir}/${port}
        mkdir -p ${log_dir}
        cd deploy
        sh add_env.sh ${project_dir} ${port} ${kubectl}
        cd -
        cd PinoCore/saltwater/tools/ssh_key
        sh init_ssh_key.sh
        cd -
        nohup ${python_cmd} app_server.py ${run_args} > ${log_file} 2>&1 &"

    if [ $? = 0 ]
    then
        echo "deploy successed!! ^-^"
    else
        echo "deploy failed!! ^-^"
    echo "logfile: ${log_file}"
    fi
}

for ip_port_str in ${ips[@]}
do
    ip_port=(${ip_port_str//:/ })
    ip=${ip_port[0]}
    port_str=${ip_port[1]}
    ports=(${port_str//,/ })
for port in ${ports[@]}
do
    deploy ${ip} ${port}
done
done

