#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
DEFAULT_YML="$SCRIPT_DIR/complete.yml"
CRM_YML="$SCRIPT_DIR/crm.yml"
IP_CONF="$SCRIPT_DIR/base.ip.conf"
RUNNER_IP_CONF="$SCRIPT_DIR/runner.ip.conf"
CORE_CONF="$SCRIPT_DIR/core_cfg.py"

cmd1="cp -f $DEFAULT_YML $SCRIPT_DIR/../../../etc/default.yml"
cmd2="cp -f $CRM_YML $SCRIPT_DIR/../../../etc/"
cmd3="cp -f $IP_CONF $SCRIPT_DIR/../../"
cmd4="cp -f $RUNNER_IP_CONF $SCRIPT_DIR/../../"
cmd5="cp -f $CORE_CONF $SCRIPT_DIR/../../../PinoCore/etc/"

echo $cmd1
$cmd1
echo $cmd2
$cmd2
echo $cmd3
$cmd3
echo $cmd4
$cmd4
echo $cmd5
$cmd5
