#!/bin/sh

init_conf () {
    sh ./$1/$2/$3
    echo ">>>>>>> run env >>>>>>>: $1 $2"
}

run_project=$1
run_branch=$2
init_sh="init.sh"

init_conf $1 $2 $init_sh

# include deploy'configure with baseconf and ipconf.
. ./runner.ip.conf

deploy () {
    ip=${1}
    topic=${2}
    p_num=${3}
    c_num=${4}

    echo "deploy ${ip} begin"

    sshpass -p '0okm(IJN' ssh -oStrictHostKeyChecking=no admin@${ip} "mkdir -p ${project_dir}/${topic}-runner"
    sshpass -p '0okm(IJN' scp -r ../*  admin@${ip}:${project_dir}/${topic}-runner/

    sshpass -p '0okm(IJN' ssh  admin@${ip} "ps -ef |grep 'start_runner.py --topic='${2} | grep -v 'COMMAND'| awk '{print \$2}'|xargs -i kill -9 {}
        cd ${project_dir}/${topic}-runner
        mkdir -p ${log_dir}
        cd deploy
        sh add_env.sh ${project_dir} ${port} ${kubectl}
        cd -
        cd PinoCore/saltwater/tools/ssh_key
        sh init_ssh_key.sh
        cd -
        cd scripts && sh start_runner.sh ${topic} ${p_num} ${c_num}"

    if [ $? = 0 ]
    then
        echo "deploy successed!! ^-^"
    else
        echo "deploy failed!! ^-^"
    fi
}

for ip_args_str in ${ips[@]}
do
    ip_port=(${ip_args_str//:/ })
    ip=${ip_port[0]}
    args_str=${ip_port[1]}
    args=(${args_str//,/ })
    echo "deploy ${ip} ${args[0]} ${args[1]} ${args[2]}"
    deploy ${ip} ${args[0]} ${args[1]} ${args[2]}
done

