#!/bin/bash
#set -e
#clear
PREFIX="/usr/local"
PROGNAME="$0"

echo "Pino Command Line Tool"
# check if /usr/local/bin writable
bin="${PREFIX}/bin"
base="${PREFIX}/pino_lib/pino"
USR_LOCAL_BIN_WRITABLE=1
ALREADY_INSTALLED=1

# Function Defination >> 
CHECK_writable() {
  local file="$1"
  while [ "$file" != "/" ] && [ -n "${file}" ] && [ ! -e "$file" ]; do
    file="$(dirname "${file}")"
  done
  if [ -w "${file}" ]; then USR_LOCAL_BIN_WRITABLE=0; else USR_LOCAL_BIN_WRITABLE=1; fi
}

CHECK_install() {
  local file="$1"
  while [ "$file" != "/" ] && [ -n "${file}" ] && [ ! -e "$file" ]; do
    file="$(dirname "${file}")"
  done
  local pino_install="${file}/pino"
  local pino_lib_install="${file}/pino_lib"
  if [ -x "${pino_install}" ]; then ALREADY_INSTALLED=1; else ALREADY_INSTALLED=0; fi
  if [ -x "${pino_lib_install}" ]; then ALREADY_INSTALLED=1; else ALREADY_INSTALLED=0; fi
}

install() {
  local file="$1"
  local pino_install="${file}/pino"
  local pino_lib_install="${file}/pino_lib"
  wget -N --no-check-certificate "ftp://mfs.jd.com/mnt/mfs/jks/model/jobs/PinoAI_PinoAlgo_Release_Client_dev-1.0/lastSuccessful/archive/release-client/pino" >/dev/null 2>&1 && chmod a+x pino && ./pino install
}

uninstall() {
  local file="$1"
  local pino_install="${file}/pino"
  local pino_lib_install="${file}/pino_lib"
  pino logout >/dev/null 2>&1
  /bin/rm -rf "${pino_install}" "${pino_lib_install}" >/dev/null 2>&1 
}
# Function Defination <<

# Check for dependencies
# unzip
if ! which unzip >/dev/null; then
  echo "unzip not found, please install the corresponding package." >&2
  exit 1
fi

# Check for write access
CHECK_writable "${bin}"
if [ "${USR_LOCAL_BIN_WRITABLE}" == 1 ]; then bin="$HOME/bin"; base="$HOME/bin/pino_lib"; fi
CHECK_install "${bin}"

# Check for tool itself commands
arg=${@:1:1}
if [ x"install" == x"${arg}" ]; then
    echo -n "Installing ..."
    if [ "${ALREADY_INSTALLED}" == 1 ]; then echo "Already installed! Please uninstall first!"; exit 1; fi
elif [ x"reinstall" == x"${arg}" ]; then
    echo "Reinstalling ..."
    if [ "${ALREADY_INSTALLED}" == 1 ]; then uninstall "${bin}"; install "${bin}"; exit 0; fi
elif [ x"uninstall" == x"${arg}" ]; then
    echo -n "Uninstalling ..."
    if [ "${ALREADY_INSTALLED}" == 1 ]; then uninstall "${bin}"; echo "Done."; exit 0; fi
elif [ x"upgrade" == x"${arg}" ]; then
    echo "Upgrading ..."
    if [ "${ALREADY_INSTALLED}" == 1 ]; then uninstall "${bin}"; install "${bin}"; exit 0; fi
    #set -- "${@:2}"
    #echo $@
    # do upgrade
elif [ x"debug" == x"${arg}" ]; then
    cat "${base}/_version"; exit 0;
else
    if [ "${ALREADY_INSTALLED}" == 0 ]; then echo "Please install first. \$Shell>./pino install"; exit 1; fi;
fi

EXEC_BINARY="${bin}/pino"
EXEC_PYTHON="${base}/python/bin/python3.6"

if [ -x "${EXEC_BINARY}" ]; then ${EXEC_PYTHON} "${base}/pino" "${@}" >&2; fi
if [ ! -x "${EXEC_BINARY}" ]; then
  # Do the actual installation
  echo -n "Uncompressing."
  
  # Cleaning-up, with some guards.
  #rm -f "${bin}/pino"
  if [ -d "${base}" -a -x "${base}/pino" ]; then
    rm -fr "${base}"
  fi
  # prepare directory
  mkdir -p "${bin}" "${base}" "${base}/bin" "${base}/etc"
  echo -n .
  # uncompress
  unzip -q "${BASH_SOURCE[0]}" -d "${base}" >/dev/null 2>&1
  echo -n .
  # chmod
  chmod 0755 "${base}/pino"
  echo -n .
  chmod a+x "${base}/pino"
  echo -n .
  chmod -R og-w "${base}"
  echo -n .
  chmod -R og+rX "${base}"
  echo -n .
  chmod -R u+rwX "${base}"
  echo -n .
  # install execuate program
  cp -rf "${BASH_SOURCE[0]}" "${bin}/pino"
  mv -f "${BASH_SOURCE[0]}" ".pino.installed"
  chmod a+x "${bin}/pino"
  echo -n .
  
  #if [ "${UID}" -ne 0 ]; then
  #  # Uncompress the pino base install for faster startup time
  #  "${bin}/pino" help >/dev/null
  #fi
  echo .
  echo "Pino Commandline tool is now installed! can be use as \$Shell>pino ..."
fi
usage() {
  echo "Usage: ${PROGNAME} [options]" >&2
  echo "Options are:" >&2
  echo "  --prefix=/some/path set the prefix path (default=/usr/local)." >&2
  echo "  --bin= set the binary folder path (default=%prefix%/bin)." >&2
  echo "  --base= set the base install path (default=%prefix%/pino)." >&2
  echo "  --user configure for user install, expands to:" >&2
  echo "  --bin=$HOME/bin --base=$HOME/bin/pino" >&2
  exit 1
}
exit 0
