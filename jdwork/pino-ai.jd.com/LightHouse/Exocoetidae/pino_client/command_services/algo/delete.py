#!/usr/bin/env python3
# Time: 2017/10/11 16:45
import json

__author__ = 'wanggangshan@jd.com'

import sys

import absl.flags as gflags
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base


class Delete(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Delete, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo'],
            'optional': [],
        }
        gflags.DEFINE_string('algo', '', 'algo name')

    def run(self):
        algo= self.Flags.algo
        data = {'algo': algo}
        response = http_request('/cli/algo/delete',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            info = json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('%s算法实例删除任务已提交，请稍后查询算法实例列表查看结果！\n%s' %(algo, info))
        sys.exit(0)



