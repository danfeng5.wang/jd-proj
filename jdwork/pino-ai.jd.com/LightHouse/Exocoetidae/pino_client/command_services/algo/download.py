#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import os
import sys

import requests
import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import (
    http_request,
    normalizeDirName,
    make_zip,
    unzip,
    verify_reponse,
    check_path_dos_file
)

from command_services.base import Base
from collections import defaultdict


class Download(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Download, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo'],
            'optional': ['library', 'directory'],
        }

        gflags.DEFINE_string('algo', '.', 'algo name')
        gflags.DEFINE_string('dir', '', 'directory path')
        gflags.DEFINE_string('directory', '', 'directory path')
        gflags.DEFINE_string('library', 'private', 'library type')


    def run(self):
        post_data = defaultdict(str)
        algo = self.Flags.algo
        project_dir = self.Flags.dir if self.Flags.dir else self.Flags.directory
        if not project_dir:
            project_dir = '.'
        post_data['algo'] = algo
        post_data['directory'] = project_dir

        project_path = normalizeDirName(project_dir)
        if not project_path:
            error_message(type='algo_download', code=100, more=project_dir)

        response = http_request('/cli/algo/download', data=post_data,
                                request_type='post')
        if response.status_code != requests.codes.ok:
            error_message(type='http', code=100, more=response)
        try:
            file_title = response.headers.get('Content-Disposition')
            if not file_title:
                print('未查到该算法实例信息： %s,请确认算法实例名称是否正确' % algo)
                sys.exit(0)
            file_zip_name = file_title.split('filename=')[1][:-1]
            file_zip_path = os.path.join(project_path, file_zip_name)
            if os.path.exists(file_zip_path):
                print('本地路径下已存在同名压缩包: %s，请确认是否删除替换？（可更名后重试）' % file_zip_path)
                print('选择yes，将下载压缩包到该路径，选择no，将放弃本次下载')
                while 1:
                    choice = input('请输入选择(yes/no):')
                    if choice == 'yes':
                        os.remove(file_zip_path)
                        break
                    elif choice == 'no':
                        sys.exit(0)
                    else:
                        print('您的输入有误，请重新输入。')

            with open(file_zip_path, "wb") as f:
                f.write(response.content)

            algo_project_dir = os.path.join(project_path, file_zip_name[:-4])
            if os.path.exists(algo_project_dir):
                print('本地路径下已存在同名文件目录: %s，请确认是否删除？（可更名后重试）' % algo_project_dir)
                print('选择yes，将删除该目录后解压生成新目录，选择no，将放弃解压')
                while 1:
                    choice = input('请输入选择(yes/no):')
                    if choice == 'yes':
                        __import__('shutil').rmtree(algo_project_dir)
                        break
                    elif choice == 'no':
                        sys.exit(0)
                    else:
                        print('您的输入有误，请重新输入。')
            unzip(file_zip_path, project_path)
            os.remove(file_zip_path)

            print('下载成功 生成目录: %s' % algo_project_dir)
        except Exception as e:
            error_message(type='algo_download', code=101, more=e)

        sys.exit(0)

