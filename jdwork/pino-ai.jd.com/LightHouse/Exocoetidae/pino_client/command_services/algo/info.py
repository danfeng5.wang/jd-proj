#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import json
import absl.flags as gflags
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String

from command_services.base import Base


class Info(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Info, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo'],
            'optional': ['token'],
        }

        gflags.DEFINE_string('algo', '', 'algo name')
        gflags.DEFINE_string('token', '', 'token')

    def run(self):

        algo = self.Flags.algo
        token = self.Flags.token
        data = {'algo': algo, 'token': token}
        print('Searching...', end='', flush=True)
        response = http_request('/cli/algo/info',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            print('Done.')
            #info =json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('%s 的算法实例信息: \n%s'% (algo, res_json['result']))
        sys.exit(0)
