#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import json
from collections import defaultdict, OrderedDict
import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String
from utils.output_formatter import format_list

from command_services.base import Base


def req_algo_list(data, is_exit=True):
    if data.get('all'):
        print('Global Searching...', end='', flush=True)
    else:
        print('Searching...', end='', flush=True)
    response = http_request('/cli/algo/list', data=data, request_type='post')
    if not response:
        error_message(type='http', code=100, more=response)

    code, res_json = verify_reponse(response)
    if code:
        print('Done.')
        if res_json['result'].get('total', 0) <= 0:
            print(res_json['message'])
        else:
            show_list = []
            algo_list = res_json['result'].get('data', [])
            header = OrderedDict(
                AlgoId='',
                AuthorId='',
                AuthorName='',
                AlgoName='',
                Description='',
                CreatedAt='',
                UpdatedAt='',
            )

            show_list.append(header)
            for algo_dict in algo_list:
                show_list.append(OrderedDict(
                    AlgoId=str(algo_dict['id']),
                    AuthorId=str(algo_dict['author_uid']),
                    AuthorName=str(algo_dict['author_name']),
                    AlgoName=str(algo_dict['algo_name']),
                    Description=str(algo_dict['description']),
                    CreatedAt=str(algo_dict['created_at']),
                    UpdatedAt=str(algo_dict['updated_at']))
                )
            title = 'Avaliable Algo Table'
            print(format_list(show_list, title))
            total = len(algo_list)
            print('总条数: %s' % total)
            if total == 0:
                sys.exit(0)
    if is_exit:
        sys.exit(0)


class List(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(List, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': ['all'],
        }
        gflags.DEFINE_bool('all', False, 'show all list(must admin user)')


    def run(self):
        data = {'all': self.Flags.all}
        req_algo_list(data)


