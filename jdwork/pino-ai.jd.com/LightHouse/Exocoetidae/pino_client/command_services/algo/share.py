#!/usr/bin/env python3
# Time: 2017/10/11 16:45
import json

__author__ = 'wanggangshan@jd.com'

import sys

import absl.flags as gflags
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base


class Share(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Share, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo'],
            'optional': ['update', 'show'],
        }
        gflags.DEFINE_string('algo', '', 'algo name')
        gflags.DEFINE_bool('update', False, 'update algo\'s token)')
        gflags.DEFINE_bool('show', False, 'show algo\'s token)')

    def run(self):
        algo= self.Flags.algo
        update = self.Flags.update
        show = self.Flags.show
        if (not update) and (not show):
            print('命令缺少参数： --show 或者 --update。')
            usage_help(group=self.group, command=self.command)
        if update and show:
            print('命令参数： --show 与 --update 是或的关系。')
            usage_help(group=self.group, command=self.command)

        data = {'algo': algo}
        if update:
            url = '/cli/algo/share/update'
        else:
            url = '/cli/algo/share/show'
        response = http_request(url, data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            token = res_json['result'].get('token')
            if update:
                if token:
                    notify = '算法实例token更新为: %s' % token
                    print(algo, notify)
                else:
                    notify = '算法实例token更新任务已提交，可稍后查询更新后token\n '
                    print(algo, notify)
            else:
                notify = ' 算法实例token为: '
                print(algo, notify, token)
        sys.exit(0)



