#!/usr/bin/env python3
# Time: 2017/10/11 16:45
import json

__author__ = 'wanggangshan@jd.com'

import os
import sys

import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import (
    http_request,
    normalizeDirName,
    make_zip,
    verify_reponse,
    check_path_dos_file
)

from command_services.base import Base
from collections import defaultdict


class Upload(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Upload, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo', 'directory'],
            'optional': ['library'],
        }

        gflags.DEFINE_string('algo', '', 'algo name')
        gflags.DEFINE_string('dir', '', 'directory path')
        gflags.DEFINE_string('directory', '', 'directory path')
        gflags.DEFINE_string('library', 'private', 'library type')


    def run(self):
        post_data = defaultdict(str)
        upload_files = defaultdict(str)
        algo = self.Flags.algo
        project_path = self.Flags.dir if self.Flags.dir else self.Flags.directory
        post_data['algo'] = algo
        post_data['directory'] = project_path
        if project_path:
            # check and get all_long path
            project_dir_path = normalizeDirName(project_path)
            if not project_dir_path:
                error_message(type='algo_upload', code=100, more=project_path)
            try:
                base_project_name = os.path.basename(project_dir_path)
                zip_name = '%s.zip' % base_project_name
                output_zip_path = os.path.join(
                    os.path.dirname(project_dir_path),
                    zip_name)
                make_zip(project_dir_path, output_zip_path)
                if os.path.getsize(output_zip_path) > 1024 * 1025 * 10:
                    print('上传文件不能大于10MB!')
                    os.remove(output_zip_path)
                    sys.exit(0)

                post_data['project_path'] = project_dir_path
                upload_files['project_file'] = open(output_zip_path, 'rb')
            except Exception as e:
                error_message(type='algo_upload', code=101, more=e)


        response = http_request('/cli/algo/upload', data=post_data,
                                request_type='post', files=upload_files)
        if not response:
            error_message(type='http', code=100, more=response)
        os.remove(output_zip_path)
        code, res_json = verify_reponse(response)
        if code:
            info = json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('算法实例文件上传任务已提交，请稍后查询算法实例列表查看结果！\n%s' % info)
        sys.exit(0)



