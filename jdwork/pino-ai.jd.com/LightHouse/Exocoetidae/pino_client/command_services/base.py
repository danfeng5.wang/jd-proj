#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'


from collections import defaultdict
import absl.flags as gflags
from utils.error_message import error_message


class Base(object):

    def __init__(self):
        self.argv =self.kwargs.get('args')
        self.command_length = self.kwargs.get('command_length')
        Flags = gflags.FLAGS
        flag_args, other_args = self._check_args(self.argv, self.command_length,
                                                 self.define_args)
        try:
            args = Flags(flag_args)
        except Exception as e:
            error_message('', 404, more=e)
        self.Flags = Flags
        self.args = args
        self.other_args_dict = other_args


    def _check_args(self, argv, command_length, define_args):
        flag_args = argv[:command_length]
        other_args = defaultdict(str)
        must_keys = []

        new_args = argv[command_length:]
        for item in new_args:
            key, value = self._is_flags_args(item)
            if key == 'dir':
                key = 'directory'

            if key not in (define_args['require'] + define_args['optional']):
                other_args[key] = value
            else:
                if key in define_args['require']:
                    if not value:
                        error_message('args', 101, more=key,
                                      group=self.group, command=self.command)
                flag_args.append(item)
                if key in define_args['require']:
                    must_keys.append(key)
        for key in define_args['require']:
            if key not in must_keys:
                error_message('args', 101, more=key,
                              group=self.group, command=self.command)
        return flag_args, other_args

    def _is_flags_args(self, word, flag='-'):
        # 支持解析 --x=y / -x=y
        content = None
        if word[0:1] == flag:
            if word[1:2] == flag:
                content = word[2:]
            else:
                content = word[1:]
        else:
            error_message('args', 100, more=word,
                          group=self.group, command=self.command)
        if not content:
            error_message('args', 100, more=word,
                          group=self.group, command=self.command)

        if len(content.split('='))>1:
            key = content.split('=')[0]
            value = content.split('=')[1]
            return key, value
        else:
            return content, None

