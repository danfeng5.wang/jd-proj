#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import json
import absl.flags as gflags
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String
from logic.user import user

from command_services.base import Base


class Nodes(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Nodes, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['task_id'],
            'optional': ['all'],
        }

        gflags.DEFINE_string('task_id', '', 'task id')
        gflags.DEFINE_bool('all', False, 'show all list(must admin user)')

    def run(self):
        task_id = self.Flags.task_id
        all = self.Flags.all
        if all:
            if not user.isSuper:
                error_message(type='cloud_nodes', code=100)
        data = {'task_id': task_id, 'all': all}
        response = http_request('/cli/cloud/nodes',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            info =json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('算法实例列表信息: \n%s' % info)
        sys.exit(0)



