#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import json
import absl.flags as gflags
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String
from logic.user import user

from command_services.base import Base
from table_models.models import TableManager


class Quota(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Quota, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': ['group_id', 'all'],
        }

        gflags.DEFINE_integer('group_id', 0, 'show group quota')
        gflags.DEFINE_bool('all', False, 'show all(must admin user)')

    def run(self):
        if not self.kwargs.get('use_admin'):
            pass
        group_id = self.Flags.group_id
        all = self.Flags.all
        if self.Flags.all:
            if not user.isSuper:
                error_message(type='code_list', code=100)

        if not group_id:

            response = http_request('/group/list', request_type='post')
            if not response:
                error_message(type='http', code=100, more='任务列表查询失败')
            code, res_json = verify_reponse(response)

            if code:

                total = len(res_json['result'])

                if total == 0:
                    print(res_json['message'])
                    sys.exit(0)
                data = []
                for group in res_json['result']:
                    data.append({
                        'GroupId': group.get('group_id'),
                        'GroupName': group.get('group_name'),
                        'GroupOwnerName': group.get('owner_name')
                    })
                table = TableManager('group_list')
                table.show_whole_table(data)

                group_id = input("请输入要查询的组编号(GroupId)>:").strip()
        try:
            group_id = int(group_id)
        except:
            print('抱歉！您的输入有误,请重新输入！')
            sys.exit(0)
        data = {'group_id': group_id, 'all': all}
        response = http_request('/cli/cloud/quota',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            info =json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('组资源信息如下: \n%s' % info)
        sys.exit(0)
