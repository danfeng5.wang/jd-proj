#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import absl.flags as gflags
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String

from command_services.base import Base


class Version(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Version, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': [],
        }

    def run(self):

        response = http_request('/cli/cloud/version', request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            version =res_json['result']['version']
            print('Pino Cloud 当前版本号为: %s' % version)
        sys.exit(0)



