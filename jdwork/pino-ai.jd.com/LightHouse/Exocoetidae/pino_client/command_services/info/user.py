#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys
import json
import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base


class User(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(User, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': [],
        }

    def run(self):
        response = http_request('/cli/info/user',
                                request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            info = res_json['result']
            user.setUserInfo(info)
            show_info = json.dumps(res_json['result'], indent=4, ensure_ascii=False)
            print('当前用户信息: \n%s' % show_info)

        sys.exit(0)



