#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import getpass
import sys

import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_verify, usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base


class Login(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Login, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['username'],
            'optional': ['password'],
        }
        gflags.DEFINE_string('username', '', 'ERP username')
        gflags.DEFINE_string('password', '', 'ERP password')

    def run(self):
        username = self.Flags.username
        if username == '':
            usage_help(group=self.group, command=self.command)

        password = self.Flags.password
        if password == '':
            password = getpass.getpass("ERP Password:")
        data = {"username": username, "password": password}

        response = http_request('/cli/login', data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if not code:
            print('登录失败')
            sys.exit(0)

        info = res_json['result']
        user.setUserInfo(info)
        print('登录成功')

        sys.exit(0)
