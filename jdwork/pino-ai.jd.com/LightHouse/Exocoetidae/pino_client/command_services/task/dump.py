#!/usr/bin/env python3
# Time: 2017/12/1 16:56

__author__ = 'wanggangshan@jd.com'


import os
import sys
import requests
from collections import defaultdict

import absl.flags as gflags
from utils.error_message import error_message
from utils.util import (
    http_request,
    normalizeDirName,
    make_zip,
    unzip,
    verify_reponse,
    check_path_dos_file
)

from command_services.base import Base
from command_services.task.list import req_task_list


class Dump(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Dump, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': ['task_id', 'dir', 'directory', 'log'],
        }

        gflags.DEFINE_string('task_id', '', 'task id')
        # gflags.DEFINE_string('node_id', '', 'node id')
        gflags.DEFINE_string('dir', '', 'directory path')
        gflags.DEFINE_string('directory', '', 'directory path')
        gflags.DEFINE_bool('log', False, '是否下载模型')

    def run(self):
        project_dir = self.Flags.dir if self.Flags.dir else self.Flags.directory
        if not project_dir:
            project_dir = '.'

        project_path = normalizeDirName(project_dir)
        if not project_path:
            error_message(type='algo_download', code=100, more=project_dir)

        task_id= self.Flags.task_id
        if not task_id:
            data = {}
            req_task_list(data, is_exit=False)
            task_id = input("请输入任务编号(TaskId)>:").strip()

        log = self.Flags.log
        data = {'task_id': task_id, 'log': log}
        response = http_request('/cli/task/dump', data=data,
                                request_type='post')
        if response.status_code != requests.codes.ok:
            error_message(type='http', code=100, more=response)
        try:
            file_title = response.headers.get('Content-Disposition')
            if not file_title:
                print('未查到该任务相关日志信息: %s' % task_id)
                sys.exit(0)
            file_zip_name = file_title.split('filename=')[1][:-1]
            file_zip_path = os.path.join(project_path, file_zip_name)
            if os.path.exists(file_zip_path):
                print('本地路径下已存在同名压缩包: %s，请确认是否删除替换？（可更名后重试）' % file_zip_path)
                print('选择yes，将下载压缩包到该路径，选择no，将放弃本次下载')
                while 1:
                    choice = input('请输入选择(yes/no):')
                    if choice == 'yes':
                        os.remove(file_zip_path)
                        break
                    elif choice == 'no':
                        sys.exit(0)
                    else:
                        print('您的输入有误，请重新输入。')

            with open(file_zip_path, "wb") as f:
                f.write(response.content)

            algo_project_dir = os.path.join(project_path, file_zip_name[:-4])
            if os.path.exists(algo_project_dir):
                print('本地路径下已存在同名文件目录: %s，请确认是否删除？（可更名后重试）' % algo_project_dir)
                print('选择yes，将删除该目录后解压生成新目录，选择no，将放弃解压')
                while 1:
                    choice = input('请输入选择(yes/no):')
                    if choice == 'yes':
                        __import__('shutil').rmtree(algo_project_dir)
                        break
                    elif choice == 'no':
                        sys.exit(0)
                    else:
                        print('您的输入有误，请重新输入。')
            unzip(file_zip_path, project_path)
            os.remove(file_zip_path)

            print('下载成功 生成目录: %s' % algo_project_dir)
        except Exception as e:
            error_message(type='algo_download', code=101, more=e)

        sys.exit(0)