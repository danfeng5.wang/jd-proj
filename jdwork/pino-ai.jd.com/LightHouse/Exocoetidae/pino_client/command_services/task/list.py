#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import absl.flags as gflags
from logic.user import user
from utils.usage_message import usage_help
from utils.error_message import error_message
from utils.util import http_request, verify_reponse, String
from table_models.models import TableManager

from command_services.base import Base
from collections import defaultdict, OrderedDict
from utils.output_formatter import format_list


def req_task_list(data, is_exit=True):
    response = http_request('/cli/task/list', data=data, request_type='post')
    if not response:
        error_message(type='http', code=100, more=response)

    code, res_json = verify_reponse(response)
    if code:
        table = TableManager('task_list')
        total = res_json['result']['total']
        data = res_json['result']['data']
        # table.show_whole_table(data)
        status_item = {0: '处理中',
                       -1: '失败',
                       1: '成功',
                       -2: '用户放弃'}
        type_item = {0: '训练',
                     1: '评估'}
        isstart_item = {0: 'submit',
                        1: 'success'}

        show_list = []
        task_list = data
        header = OrderedDict(
            TaskId='',
            AuthorId='',
            AuthorName='',
            Type='',
            RunAs='',
            Status='',
            IsStart='',
            CreatedAt='',
            UpdatedAt='')

        show_list.append(header)

        for task_dict in task_list:
            show_list.append(OrderedDict(
                TaskId=str(task_dict['task_id']),
                AuthorId=str(task_dict['owner_uid']),
                AuthorName=str(task_dict['owner_name']),
                Type=str(type_item[task_dict['type']]),
                RunAs=str(task_dict['group_name']),
                Status=str(status_item[task_dict['status']]),
                IsStart=str(isstart_item[task_dict['is_start']]),
                CreatedAt=str(task_dict['created_at']),
                UpdatedAt=str(task_dict['updated_at']))
            )
        title = 'Avaliable Task Table'
        print(format_list(show_list, title))
        print('总条数: %s' % total)
        if total == 0:
            sys.exit(0)

    if is_exit:
        sys.exit(0)


class NormalList(Base):

    def __init__(self, kwargs):
        self.kwargs = kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(NormalList, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': ['all'],
        }
        gflags.DEFINE_bool('all', False, 'show all list(must admin user)')

    def run(self):

        if self.Flags.all:
            if not user.isSuper:
                error_message(type='task_list', code=100)

        data = {'all': self.Flags.all}
        return data


class AdminList(Base):

    def __init__(self, kwargs):
        self.kwargs = kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(AdminList, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': [],
        }
        # gflags.DEFINE_bool('all', False, 'show all list(must admin user)')

    def run(self):
        return {}


class List(object):

    def __init__(self, kwargs):
        try:
            id_type = kwargs.get('id_type')
            class_name = id_type.capitalize() + self.__class__.__name__
            module_now = sys.modules[__name__]
            self.func_class = getattr(module_now, class_name)
        except Exception as e:
            error_message(type='module', code=100, more=e)
        self.kwargs = kwargs

    def run(self):
        post_data = self.func_class(self.kwargs).run()
        req_task_list(post_data)
