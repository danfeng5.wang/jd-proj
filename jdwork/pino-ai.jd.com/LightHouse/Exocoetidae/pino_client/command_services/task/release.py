#!/usr/bin/env python3
# Time: 2017/12/1 16:57

__author__ = 'wanggangshan@jd.com'


import sys

import absl.flags as gflags
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base
from command_services.task.list import req_task_list


class Release(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Release, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': [],
            'optional': ['task_id'],
        }
        gflags.DEFINE_string('task_id', '', 'task id')

    def run(self):
        task_id= self.Flags.task_id
        if not task_id:
            data = {}
            req_task_list(data, is_exit=False)
            task_id = input("请输入任务编号(TaskId)>:").strip()

        data = {'task_id': task_id}
        response = http_request('/cli/task/release',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            print('任务 %s 已回收' % task_id)
        sys.exit(0)
