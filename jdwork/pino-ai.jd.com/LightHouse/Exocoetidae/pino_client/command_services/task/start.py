#!/usr/bin/env python3
# coding=utf-8
# Time: 2017/10/11 16:45
import threading

__author__ = 'wanggangshan@jd.com'

import os
import sys
import time
from collections import defaultdict

import absl.flags as gflags
from logic.user import user
from utils.config_helper import config
from utils.usage_message import usage_help
from utils.util import (
    http_request,
    normalizeDirName,
    make_zip,
    verify_reponse,
    check_path_dos_file
)

from command_services.base import Base
from utils.error_message import error_message
from libs.wstty import PinoWebMessage
from libs.socketIO_client import SocketIO, LoggingNamespace

_global = dict(
    sid = None,
    closed = False,
)

class Start(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Start, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['algo'],
            'optional': ['config', 'library', 'directory', 'token'],
        }
        gflags.DEFINE_string('algo', '', 'author\'s algo model name')
        gflags.DEFINE_string('config', '', 'custom config for train')
        gflags.DEFINE_string('library', None, 'library')
        gflags.DEFINE_string('dir', '', 'directory path')
        gflags.DEFINE_string('directory', '', 'project dir')
        gflags.DEFINE_string('token', None, 'token')


    def __start_websocket(self, user_info):

        def on_connection_error():
            print('连接服务器失败，请联系管理员')
            os._exit(0)

        # start websocket
        def on_response(*args):
            msg = PinoWebMessage.parseFrom(args[0])
            if msg.type == PinoWebMessage.OUTPUT:
                os.write(sys.stdout.fileno(), msg.data)
                sys.stdout.flush()
            elif msg.type == PinoWebMessage.EOF:
                socketIO.disconnect()
                _global['closed'] = True

        session_uuid = None

        def on_uuid(*args):
            session_uuid = args[0]
            _global['sid'] = session_uuid
            print('session_uuid: ', session_uuid)

        def on_disconnect():
            os._exit(0)

        def socket_wait():
            while not _global['closed']:
                try:
                    socketIO.wait(0.1)
                except Exception as e:
                    print('Warn: socket error')

        domain_ip = config.get('domain_ip')
        domain_port = config.get('domain_port')
        socketIO = SocketIO(domain_ip, domain_port, LoggingNamespace, connection_error=on_connection_error)

        socketIO.on('disconnect', on_disconnect)
        socketIO.on('connection_error', on_connection_error)

        socketIO.on('pino-ws', on_response, path='/output')
        socketIO.on('my_response', on_uuid, path='/output')

        t = threading.Thread(target=socket_wait, name='socket_wait')
        t.start()

        socketIO.emit('web_connect', user_info, path='/output')
        return t

    def __remove_file(self, *args):
        for file in args:
            if os.path.isfile(file):
                os.remove(file)

    def run(self):
        user_info = user.get_user_info()
        post_data = defaultdict(str)
        upload_files = defaultdict(str)

        post_data['algo'] = self.Flags.algo
        post_data['library'] = self.Flags.library
        post_data['token'] = self.Flags.token
        post_data['other_args_dict'] = self.other_args_dict
        config = self.Flags.config
        
        if config:
            try:
                config_path = normalizeDirName(config, type='file')
                if not config_path:
                    error_message(type='task_start', code=100, more=config)
                    
                post_data['config_path'] = config_path
                upload_files['config_file'] = open(config_path, 'rb')
            except Exception as e:
                error_message(type='task_start', code=101, more=e)
        
        project_path = self.Flags.dir if self.Flags.dir else self.Flags.directory
        if project_path: 
            # check and get all_long path
            project_dir_path = normalizeDirName(project_path)
            if not project_dir_path:
                error_message(type='task_start', code=102, more=project_path)
            try:
                base_project_name = os.path.basename(project_dir_path)
                zip_name = '%s.zip' % base_project_name
                output_zip_path = os.path.join(os.path.dirname(project_dir_path),
                                               zip_name)
                make_zip(project_dir_path, output_zip_path)
                if os.path.getsize(output_zip_path) > 1024 * 1025 * 10:
                    print('上传文件不能大于10MB!')
                    os.remove(output_zip_path)
                    sys.exit(0)
                
                post_data['project_path'] = project_dir_path
                upload_files['project_file'] = open(output_zip_path, 'rb')
            except Exception as e:
                error_message(type='task_start', code=103, more=e)
                
            post_data['other_args_dict'] = self.other_args_dict

        try:
            t = self.__start_websocket(user_info)
        except Exception as e:
            # self.__remove_file(post_data['config_path'], post_data['project_path'])
            error_message(type='task_start', code=104, more=e)

        while not _global['sid']:
            time.sleep(0.2)
            
        session_uuid = _global['sid']
        post_data['session_uuid'] = session_uuid


        response = http_request('/cli/task/start', data=post_data,
                                request_type='post', files=upload_files)
        if not response:
            # self.__remove_file(post_data['config_path'], post_data['project_path'])
            error_message(type='http', code=100, more=response)

        # self.__remove_file(post_data['config_path'], post_data['project_path'])
        code, res_json = verify_reponse(response)
        if code:
            print('任务提交成功！')
        sys.exit(0)
