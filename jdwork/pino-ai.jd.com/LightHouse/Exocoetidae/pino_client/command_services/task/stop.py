#!/usr/bin/env python3
# Time: 2017/10/11 16:45

__author__ = 'wanggangshan@jd.com'

import sys

import absl.flags as gflags
from utils.error_message import error_message
from utils.util import http_request, verify_reponse

from command_services.base import Base


class Stop(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Stop, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['task_id'],
            'optional': [],
        }
        gflags.DEFINE_string('task_id', '', 'task id')

    def run(self):
        task_id= self.Flags.task_id
        data = {'task_id': task_id}
        response = http_request('/cli/task/stop',
                                data=data, request_type='post')
        if not response:
            error_message(type='http', code=100, more=response)

        code, res_json = verify_reponse(response)
        if code:
            print('任务 %s 已停止' % task_id)
        sys.exit(0)



