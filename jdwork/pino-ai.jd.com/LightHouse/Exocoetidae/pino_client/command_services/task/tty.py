#!/usr/bin/env python3
# Time: 2017/10/11 16:45
import threading

__author__ = 'wanggangshan@jd.com'

import os
import sys
import time
import atexit
from collections import defaultdict

import absl.flags as gflags
from logic.user import user
from utils.config_helper import config
from utils.usage_message import usage_help
from utils.remote_terminal import RemoteTerminal
from utils.util import (
    http_request,
    normalizeDirName,
    make_zip,
    verify_reponse,
    check_path_dos_file
)

from command_services.base import Base
from utils.error_message import error_message
from libs.wstty import PinoWebMessage
from libs.socketIO_client import SocketIO # LoggingNamespace

_global = dict(
    sid = None,
    closed = False,
    socketIO = None,
    terminal = None,
    thread = None,
)

def _on_exit():
    global _global

    #print('>>>> on_exit ....')
    _global['closed'] = True

    thread = _global.get('thread', None)
    if thread:
        thread.join()
        _global['thread'] = None

    ws = _global.get('socketIO', None)
    if ws:
        ws.disconnect()
        _global['socketIO'] = None

    terminal = _global.get('terminal', None)
    if terminal:
        terminal.close()
        _global['terminal'] = None

    print('')

# register a exit handler
atexit.register(_on_exit)

class Tty(Base):

    def __init__(self, kwargs):
        self.kwargs= kwargs
        self.group = kwargs.get('group')
        self.command = kwargs.get('command')
        self.__define_gflags()
        super(Tty, self).__init__()

    def __define_gflags(self):
        self.define_args = {
            'require': ['task_id', 'node_id'],
            'optional': [ ],
        }

        gflags.DEFINE_string('task_id', '', 'task id')
        gflags.DEFINE_string('node_id', '', 'node id in the task')

    def __start_websocket(self, user_info):

        def on_connection_error():
            print('连接服务器失败，请联系管理员')
            os._exit(0)

        # start websocket
        def on_response(*args):
            msg = PinoWebMessage.parseFrom(args[0])
            terminal = _global.get('terminal', None)
            if terminal:
                terminal.onWebMessage(msg)
            if msg.type == PinoWebMessage.EOF:
                socketIO.disconnect()
                _global['closed'] = True

        session_uuid = None

        def on_uuid(*args):
            session_uuid = args[0]
            _global['sid'] = session_uuid

        def socket_wait():
            while not _global['closed']:
                try:
                    socketIO.wait(1.0)
                except Exception as e:
                    _global['closed'] = True
                    return 

        domain_ip = config.get('domain_ip')
        domain_port = config.get('domain_port')
        
        socketIO = SocketIO(domain_ip, domain_port, async_mode='gevent', connection_error=on_connection_error)
        #socketIO = SocketIO(domain_ip, domain_port, async_mode='gevent')
        
        _global['socketIO'] = socketIO

        socketIO.on('pino-ws', on_response, path='/output')
        socketIO.on('my_response', on_uuid, path='/output')

        thread = threading.Thread(target=socket_wait, name='socket_wait')
        thread.start()
        _global['thread'] = thread

        socketIO.emit('web_connect', user_info, path='/output')
        return thread

    def run(self):
        user_info = user.get_user_info()
        post_data = defaultdict(str)

        ttyCols,ttyRows = RemoteTerminal.getWinSize()
        user_info.update({'cols':ttyCols, 'rows':ttyRows})

        post_data['task_id'] = self.Flags.task_id
        post_data['node_id'] = self.Flags.node_id
        
        thread = None
        try:
            thread = self.__start_websocket(user_info)
        except Exception as e:
            error_message(type='task_tty', code=100, more=e)

        while not _global['sid']:
            time.sleep(0.2)
            
        session_uuid = _global['sid']
        response = http_request('/cli/task/tty', data=post_data, request_type='post', headers={'Session':session_uuid})
        if not response:
            error_message(type='http', code=100, more=response)

        terminal = RemoteTerminal(); _global['terminal'] = terminal
        terminal.open(_global['socketIO'], '/output')

        code, res_json = verify_reponse(response)
        if not code:
            _on_exit()
            sys.exit(0)

        thread.join()
        sys.exit(0)

