# coding=utf-8
import os
import sys
import subprocess
import codecs
import message
import select
import errno
import threading
import websocket
import traceback
import ptyprocess
import signal

class R2IWorker(object):
    def __init__(self, ws):
        print('R2IWorker.__init__')
        self._ws = ws
        self._ps = None # PtyProcess
        self._isChildAlive = False
    
    def __del__(self):
        if self._ps is not None:
            del self._ps
            self._ps = None

    def _doChildIO(self, ps):
        # print('_doChildIO....')
        _stdout = self._ps.fileno()
        read_list = [self._ws.sock, _stdout]
        exc_list  = [self._ws.sock]
        r, _, e = select.select(read_list, [], exc_list, 0.5)
        if self._ws.sock in e:
            raise Exception('socket error')
        if _stdout in r:
            data = self._ps.read()
            print('ps.stdout readable len(data):%d' % len(data))
            if data != '':
                self._sendMessage(message.Message.outputMessage(data))
        if self._ws.sock in r:
            opcode, frame = self._ws.recv_data_frame(True)
            if opcode in (websocket.ABNF.OPCODE_BINARY, websocket.ABNF.OPCODE_TEXT):
                m = message.Message.parseFrom(frame.data); print(m)
                if m.type == message.Message.INPUT:
                    self.on_input(m)
                elif m.type == message.Message.WNDSIZE:
                    self.on_wndSize(m)
                else:
                    raise Exception('unexpected %s' % str(m))
            elif opcode in (websocket.ABNF.OPCODE_CLOSE,):
                raise Exception('socket closed')
            elif opcode in (websocket.ABNF.OPCODE_PING,):
                self._ws.pong(frame.data)
            else:
                raise Exception('unexpected opcode %d' % opcode)

    def _childIO(self, ps):
        print('start child IO thread!')
        while self._isChildAlive:
            try:
                self._doChildIO(ps)
            except (select.error, IOError) as e:
                if e.args and e.args[0] == errno.EINTR:
                    pass
                else:
                    traceback.print_exc()
                    ps.kill()
                    break
            except EOFError as e:
                print('ps stdout EOF, exit')
                break
            except:
                traceback.print_exc()
                ps.kill(signal.SIGKILL)
                break
    
    def execute(self, cmd):
        ps = None; thd = None
        try:
            ps = ptyprocess.PtyProcess.spawn(cmd.split()); self._ps = ps
            print('PtyProcess spawned.')
            thd = threading.Thread(target=self._childIO, args=(ps,))
            self._isChildAlive = True
            thd.start()
            retCode = ps.wait()
            self._isChildAlive = False
            thd.join(); del thd; thd = None
            self._sendMessage(message.Message.retcodeMessage(retCode))
            ps = None; thd = None
        except Exception as e:
            traceback.print_exc()
            self._ws.close()
        finally:
            if ps is not None:
                ps.kill(); del ps
            if thd is not None:
                thd.join(); del thd
    
    def on_wndSize(self, message):
        w,h = message.wndSize
        self._ps.setwinsize(h, w)

    def on_input(self, message):
        self._ps.write(message.data)
    
    def _sendMessage(self, message):
        data = message.serialize()
        self._ws.send_binary(data)
