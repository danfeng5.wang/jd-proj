# coding=utf-8
import os
import sys
import struct
import codecs
import base64

_binary_type = (bytearray, bytes)
if sys.version_info[0] == 2:
    _text_type = (unicode,str)
else:
    _text_type = (str,)

class Message(object):
    INPUT   = 0
    OUTPUT  = 1
    ERROR   = 2
    COMMAND = 3
    ICOMMAND= 4
    WNDSIZE = 5
    EOF     = 6
    RETCODE = 7
    NOP     = 8
    allow_types = (INPUT, OUTPUT, ERROR, COMMAND, ICOMMAND, WNDSIZE, EOF, RETCODE, NOP)

    def __init__(self, type_, data=''):
        self.type = type_
        if isinstance(data, _binary_type):
            self.data = data
        elif isinstance(data, _text_type):
            self.data = codecs.encode(data, 'utf-8')
        else:
            raise Exception('')
        if type_ not in self.allow_types:
            raise Exception('unkonwn message type %s' % str(type_))
    
    @property
    def wndSize(self):
        if self.type != Message.WNDSIZE:
            raise Exception('expect WNDSIZE')
        w,h = struct.unpack('!II', self.data)
        return w, h
    
    @property
    def retCode(self):
        if self.type != Message.RETCODE:
            raise Exception('expect RETCODE')
        return struct.unpack('!I', self.data)[0]

    @staticmethod
    def inputMessage(data=''):
        return Message(Message.INPUT, data)

    @staticmethod
    def outputMessage(data=''):
        return Message(Message.OUTPUT, data)

    @staticmethod
    def errorMessage(data=''):
        return Message(Message.ERROR, data)
    
    @staticmethod
    def commandMessage(data=''):
        return Message(Message.COMMAND, data)

    @staticmethod
    def icommandMessage(data=''):
        return Message(Message.ICOMMAND, data)

    @staticmethod
    def wndSizeMessage(width, height):
        data = struct.pack('!II', width, height)
        return Message(Message.WNDSIZE, data)
    
    @staticmethod
    def eofMessage():
        return Message(Message.EOF, '')

    @staticmethod
    def retcodeMessage(retCode):
        return Message(Message.RETCODE, struct.pack('!I', retCode))

    @staticmethod
    def nopMessage():
        return Message(Message.NOP, b'')

    def __str__(self):
        s = None
        if self.type == Message.INPUT:
            s = 'InputMessage:%s' % self.data.decode('utf-8')
        elif self.type == Message.OUTPUT:
            s = 'OutputMessage:%s' % self.data.decode('utf-8')
        elif self.type == Message.ERROR:
            s = 'ErrorMessage:%s' % self.data.decode('utf-8')
        elif self.type == Message.COMMAND:
            s = 'CommandMessage:%s' % self.data.decode('utf-8')
        elif self.type == Message.ICOMMAND:
            s = 'ICommandMessage:%s' % self.data.decode('utf-8')
        elif self.type == Message.WNDSIZE:
            s = 'WndSizeMessage:%s' % str(self.wndSize)
        elif self.type == Message.EOF:
            s = 'EofMessage'
        elif self.type == Message.RETCODE:
            s = 'RetCodeMessage:%s' % str(self.retCode)
        elif self.type == Message.NOP:
            s = 'NOPMessage'
        else:
            raise Exception('Unkonwn Message type')
        if len(s) > 64:
            s = s[:60] + '....'
        return s

    def serialize(self):
        data = struct.pack('B', self.type) + self.data
        data = base64.b64encode(data)
        return codecs.decode(data, 'utf-8')

    @staticmethod
    def parseFrom(buffer):
        buffer = codecs.encode(buffer, 'utf-8')
        buffer = base64.b64decode(buffer)
        t = struct.unpack('B', buffer[:1])[0]
        data = buffer[1:]
        if t not in Message.allow_types:
            raise Exception('Message.parseFrom, invalid type:%s' % str(t))
        return Message(t, data)
    
if __name__ == '__main__':
    im = Message.inputMessage('hello world!')
    print(im)
    buffer = im.serialize()
    print(Message.parseFrom(buffer))

    m = Message.wndSizeMessage(100, 200)
    print(m)
