# coding=utf-8
from __future__ import print_function
import os
import sys
import codecs
import tty
import termios
import signal
import select
import time
import errno
import threading
import websocket
import commands
import StringIO as StringIO
import fcntl, struct
import traceback
from message import Message as WebMessage

class R2Client(object):
    def __init__(self, url, user, passwd):
        self._ready = False
        self._ws = None
        self._url = url
        self._user = user
        self._passwd = passwd
        self._utf_in = None
        self._utf_out = None
        self._utf_err = None
        self._oldtty = None
        self._old_handler = None
        self._ping_thread = None
        self._open()
        # self._openTerm()

    def close(self):
        self._close()

    def _open(self):
        sock = websocket.WebSocket(enable_multithread=True)
        sock.connect(self._url, timeout=10.0)
        self._ws = sock
        self._startPingThread()
    
    def _startPingThread(self):
        self._stopEvent = threading.Event()
        def pingProc(self):
            while not self._stopEvent.wait(5.0) and self._ready:
                self._ws.ping()
        thd = threading.Thread(target=pingProc, args=(self,))
        thd.start(); self._ping_thread = thd

    def _stopPingThread(self):
        print('stop ping thread')
        self._stopEvent.set()
        self._ping_thread.join()
        self._ping_thread = None

    def _close(self):
        if self._ws is not None:
            self._stopPingThread()
            self._ws.close()
            self._ws = None
            self._ready = False
    
    def _openTerm(self):
        UTF8Reader = codecs.getreader('utf-8')
        UTF8Writer = codecs.getwriter('utf-8')
        self._utf_in = UTF8Reader(sys.stdin)
        self._utf_out = UTF8Writer(sys.stdout)
        self._utf_err = UTF8Writer(sys.stderr)

    def _closeTerm(self):
        #objs =[self._utf_in, self._utf_out, self._utf_err]
        #for obj in objs:
        #    if obj is not None:
        #        del obj
        self._utf_in = None
        self._utf_out = None
        self._utf_err = None
        self._oldtty = None
        self._old_handler = None
        print('')
    
    def _open_iTerm(self):
        self._openTerm()
        self._old_handler = signal.getsignal(signal.SIGWINCH)
        self._oldtty = termios.tcgetattr(self._utf_in)
        def on_term_resize(signum, frame):
            self._sendCurrentWndSize()
        signal.signal(signal.SIGWINCH, on_term_resize)
        tty.setraw(self._utf_in.fileno())
        tty.setcbreak(self._utf_in.fileno())
        self._sendCurrentWndSize()
    
    def _close_iTerm(self):
        if self._utf_in is not None:
            termios.tcsetattr(self._utf_in, termios.TCSADRAIN, self._oldtty)
            signal.signal(signal.SIGWINCH, self._old_handler)
        self._closeTerm()
    
    def _open_fTerm(self):
        self._openTerm()
        self._output = StringIO()
        self._error = StringIO()
        self._utf_out = codecs.getwriter('utf-8')(self._output)
        self._utf_err = codecs.getwriter('utf-8')(self._error)
    
    def _close_fTerm(self):
        self._closeTerm()
        # del self._output; self._output = None
        # del self._error; self._error = None

    def _readSock(self, ws):
        data = ws.recv()
        if len(data) == 0:
            raise websocket.WebSocketConnectionClosedException('socket closed.')
        return WebMessage.parseFrom(data)

    def _readInput(self, fd):
        utf8char = b''
        valid_utf8 = False
        while not valid_utf8:
            utf8char = utf8char + os.read(self._utf_in.fileno(), 1)
            try:
                utf8char.decode('utf-8')
                valid_utf8 = True
            except UnicodeDecodeError:
                pass
        return WebMessage.inputMessage(utf8char)

    def iexecute(self, cmd):
        try:
            self._sendICommand(cmd)
            self._open_iTerm()
            read_list = [self._ws.sock, self._utf_in]
            while True:
                try:
                    messsage = None
                    r,_,_ = select.select(read_list, [], [], 0.5)
                    if self._ws.sock in r:
                        message = self._readSock(self._ws)
                        if message is not None:
                            if message.type == WebMessage.RETCODE:
                                return message.retCode == 0
                            elif message.type == WebMessage.OUTPUT:
                                self._writeOutout(message.data)
                            elif message.type == WebMessage.ERROR:
                                self._writeError(message.data)
                    if self._utf_in in r:
                        message = self._readInput(self._utf_in)
                        if message is not None:
                            self._sendInput(message.data)
                except (select.error, IOError) as e:
                    if e.args and e.args[0] == errno.EINTR:
                        pass
                    else:
                        self._writeOutout('exception: %s' % str(e))
                        return False
                except Exception as e:
                    traceback.print_exc()
                    self._writeOutout('exception: %s' % str(e))
                    return False
            return False
        finally:
            self._close_iTerm()
    
    def _exec(self, cmd, f_touple):
        f_open = f_touple[0]
        f_close = f_touple[1]
        try:
            self._sendCommand(cmd)
            f_open() # self._openTerm()
            read_list = [self._ws.sock]
            while True:
                try:
                    messsage = None
                    r,_,_ = select.select(read_list, [], [], 0.5)
                    if self._ws.sock in r:
                        message = self._readSock(self._ws)
                        if message is not None:
                            print(message)
                            if message.type == WebMessage.RETCODE:
                                return message.retCode == 0
                            elif message.type == WebMessage.OUTPUT:
                                self._writeOutout(message.data)
                            elif message.type == WebMessage.ERROR:
                                self._writeError(message.data)
                except (select.error, IOError) as e:
                    if e.args and e.args[0] == errno.EINTR:
                        pass
                    else:
                        self._writeOutout('exception: %s' % str(e))
                        return False
                except Exception as e:
                    self._writeOutout('exception: %s' % str(e))
                    return False
            return False
        finally:
            f_close() # self._closeTerm()

    def execute(self, cmd):
        f_touple = (self._openTerm, self._closeTerm)
        return self._exec(cmd, f_touple)

    def getstatusoutput(self, cmd):
        try:
            f_touple = (self._open_fTerm, self._close_fTerm)
            success = self._exec(cmd, f_touple)
            if success: return True, self._output.getvalue()
            else: return False, self._error.getvalue()
        finally:
            self._output.close(); self._output = None
            self._error.close(); self._error = None

    def _writeOutout(self, data):
        os.write(sys.stdout.fileno(), data.encode())
        # self._utf_out.write(data)
    
    def _writeError(self, data):
        os.write(sys.stderr.fileno(), data.encode())
        # self._utf_err.write(data)

    def _sendInput(self, idata):
        data = WebMessage.inputMessage(idata).serialize()
        self._ws.send_binary(data)
    
    def _sendOutput(self, odata):
        data = WebMessage.outputMessage(odata).serialize()
        self._ws.send_binary(data)

    def _sendError(self, edata):
        data = WebMessage.errorMessage(edata).serialize()
        self._ws.send_binary(data)

    def _sendICommand(self, cmd):
        data = WebMessage.icommandMessage(cmd).serialize()
        self._ws.send_binary(data)

    def _sendCommand(self, cmd):
        data = WebMessage.commandMessage(cmd).serialize()
        self._ws.send_binary(data)
    
    def _sendWndSize(self, width, height):
        data = WebMessage.wndSizeMessage(width, height).serialize()
        self._ws.send_binary(data)
    
    def _getwinsize(self):
        TIOCGWINSZ = getattr(termios, 'TIOCGWINSZ', 1074295912)
        s = struct.pack('HHHH', 0, 0, 0, 0)
        x = fcntl.ioctl(self._utf_in.fileno(), TIOCGWINSZ, s)
        rows, cols =  struct.unpack('HHHH', x)[0:2]
        return (cols, rows)

    def _sendCurrentWndSize(self):
        self._sendWndSize(*self._getwinsize())

    def _sendRetCode(self, retCode):
        data = WebMessage.retcodeMessage(retCode).serialize()
        self._ws.send_binary(data)

    def _isEofMessage(self, data):
        try:
            message = WebMessage.parseFrom(data)
            return message.type == WebMessage.EOF
        except:
            return False

if __name__ == '__main__':
    client = R2Client('ws://192.168.1.8:8080', None, None)
    #client.execute('ls -l /Users/zhuxiaokun/workspace')
    # client.execute('cat /Users/zhuxiaokun/.bash_profile')
    client.iexecute('vim')
    client.close()

