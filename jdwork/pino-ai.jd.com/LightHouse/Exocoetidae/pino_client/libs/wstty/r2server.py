# coding=utf-8
from __future__ import print_function
import os
import sys
import traceback
from base64 import b64encode
from hashlib import sha1
import re
import message
from websocket import WebSocket
import websocket
import worker, iworker
try:
    from socketserver import TCPServer, ThreadingTCPServer, BaseRequestHandler
except:
    from SocketServer import TCPServer, ThreadingTCPServer, BaseRequestHandler

class HandshakeResponse(object):
    def __init__(self, status=True, headers=[], subprotocol=None):
        self.status = status
        self.headers = headers
        self.subprotocol = subprotocol

class ServerWebSocket(WebSocket):
    def __init__(self, socket):
        WebSocket.__init__(self, enable_multithread=True)
        self.sock = socket
        self.handshake_response = HandshakeResponse()

class MyRequestHandler(BaseRequestHandler):

    def __init__(self, socket, addr, server):
        print('New request', addr)
        self.m_websocket = ServerWebSocket(socket)
        self.server = server
        self.m_handlers = {}
        self.m_handlers[websocket.ABNF.OPCODE_CONT] = self._on_message
        self.m_handlers[websocket.ABNF.OPCODE_TEXT] = self._on_message
        self.m_handlers[websocket.ABNF.OPCODE_BINARY] = self._on_message
        self.m_handlers[websocket.ABNF.OPCODE_CLOSE] = self._on_close
        self.m_handlers[websocket.ABNF.OPCODE_PING] = self._on_ping
        self.m_handlers[websocket.ABNF.OPCODE_PONG] = self._on_pong
        self.clientAddress = addr
        self.keep_alive = True
        self.handshake_done = False
        self.valid_client = False
        BaseRequestHandler.__init__(self, socket, addr, server)

    def setup(self):
        print('MyRequestHandler.setup')
        BaseRequestHandler.setup(self)

    def handle(self):
        opcode = None
        frame = None
        while self.keep_alive:
            if not self.handshake_done:
                self.handshake()
            elif self.valid_client:
                try:
                    opcode,frame = self.m_websocket.recv_data_frame(True)
                except websocket.WebSocketConnectionClosedException as e:
                    break
                self._process(opcode, frame)

    def _process(self, opcode, frame):
        if opcode in self.m_handlers:
            handler = self.m_handlers[opcode]
            handler(frame.data)
        else:
            print('no handler found for opcode %d' % opcode)

    def _on_open(self, message):
        print('on_open:', 'handshake OK')
    
    def _on_close(self, message):
        print('close a client at', self.clientAddress)

    def _on_message(self, data):
        m = None
        try:
            m = message.Message.parseFrom(data)
            print(m)
            if m.type == message.Message.COMMAND:
                self._runCommand(m.data.decode())
            elif m.type == message.Message.ICOMMAND:
                self._runICommand(m.data.decode())
        except:
            print('on_message:', message)

    def _on_ping(self, message):
        print('on_ping:', message)
        self.m_websocket.pong(message)
    
    def _on_pong(self, message):
        print('on_pong:', message)

    def _runCommand(self, cmd ):
        w = None
        try:
            w = worker.R2Worker(self.m_websocket)
            w.execute(cmd)
        except:
            traceback.print_exc()
        finally:
            if w is not None:
                del w
    def _runICommand(self, cmd):
        w = None
        try:
            w = iworker.R2IWorker(self.m_websocket)
            w.execute(cmd)
        except:
            traceback.print_exc()
        finally:
            if w is not None:
                del w

    def calculate_response_key(self, key):
        GUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
        hash = sha1(key.encode() + GUID.encode())
        response_key = b64encode(hash.digest()).strip()
        return response_key.decode('ASCII')

    def getHandshakeHeaders(self, key):
        headers = [
          'HTTP/1.1 101 Switching Protocols',
          'Upgrade: websocket',
          'Connection: Upgrade',
          'Sec-WebSocket-Accept: %s' % self.calculate_response_key(key),
        ]
        return headers

    def make_handshake_response(self, key):
        headers = self.getHandshakeHeaders(key)
        return '\r\n'.join(headers) + '\r\n\r\n'
        
    def handshake(self):
        message = self.request.recv(2048).decode().strip()
        upgrade = re.search('\nupgrade[\s]*:[\s]*websocket', message.lower())
        if not upgrade:
            print('Upgrade not found in handshake.')
            self.keep_alive = False
            return
        key = re.search('\n[sS]ec-[wW]eb[sS]ocket-[kK]ey[\s]*:[\s]*(.*)\r\n', message)
        if key:
            key = key.group(1)
        else:
            print("Key not found in handshake.")
            self.keep_alive = False
            return
        response = self.make_handshake_response(key).encode()
        self.handshake_done = self.request.send(response)
        self.m_websocket.connected = True
        self.valid_client = True
        self._on_open(message)

    def finish(self):
        print('MyRequestHandler.finish of', self.client_address)
        BaseRequestHandler.finish(self)

class MyWebSocketServer(ThreadingTCPServer):
    allow_reuse_address = True

def serverMain():
    HOST,PORT = '0.0.0.0', 8080
    server = MyWebSocketServer((HOST,PORT), MyRequestHandler, True)
    server.serve_forever(0.2)

if __name__ == '__main__':
    serverMain()
