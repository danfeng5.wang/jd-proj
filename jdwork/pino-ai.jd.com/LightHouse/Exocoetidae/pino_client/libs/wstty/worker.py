# coding=utf-8
import os
import sys
import subprocess
import codecs
import message
import select
import errno
import threading
import websocket
import traceback

class R2Worker(object):
    def __init__(self, ws):
        self._ws = ws
        self._ri, self._wi = os.pipe()
        self._ro, self._wo = os.pipe()
        self._re, self._we = os.pipe()
        self._isChildAlive = False
    
    def __del__(self):
        if self._ri:
            os.close(self._ri)
            os.close(self._wi)
            self._ri = None
            self._wi = None
        if self._ro:
            os.close(self._ro)
            os.close(self._wo)
            self._ro = None
            self._wo = None
        if self._re:
            os.close(self._re)
            os.close(self._we)
            self._re = None
            self._we = None

    def _doChildIO(self, ps):
        read_list = [self._ws.sock, self._ro, self._re]
        exc_list  = [self._ws.sock]
        r, w, e = select.select(read_list, [], exc_list, 0.5)
        if self._ws.sock in e:
            raise Exception('socket error')
        if self._ro in r:
            data = os.read(self._ro, 1024)
            print('ps.stdout readable len(data):%d' % len(data))
            if data != '':
                self._sendMessage(message.Message.outputMessage(data))
        if self._re in r:
            data = os.read(self._re, 1024)
            if data != '':
                self._sendMessage(message.Message.errorMessage(data))
        if self._ws.sock in r:
            opcode, frame = self._ws.recv_data_frame(True)
            if opcode in (websocket.ABNF.OPCODE_BINARY, websocket.ABNF.OPCODE_TEXT):
                m = message.Message.parseFrom(frame.data); print(m)
                if m.type == message.Message.INPUT:
                    self.on_input(m)
                elif m.type == message.Message.WNDSIZE:
                    self.on_wndSize(m)
                else:
                    raise Exception('unexpected %s' % str(m))
            elif opcode in (websocket.ABNF.OPCODE_CLOSE,):
                raise Exception('socket closed')
            elif opcode in (websocket.ABNF.OPCODE_PING,):
                self._ws.pong(frame.data)
            else:
                raise Exception('unexpected opcode %d' % opcode)

    def _childIO(self, ps):
        while self._isChildAlive:
            try:
                self._doChildIO(ps)
            except (select.error, IOError) as e:
                if e.args and e.args[0] == errno.EINTR:
                    pass
                else:
                    traceback.print_exc()
                    ps.kill()
                    break
            except:
                traceback.print_exc()
                ps.kill()
                break
    
    def execute(self, cmd):
        ps = None; thd = None
        try:
            ps = subprocess.Popen(cmd.split(),
                    stdin=self._ri,
                    stdout=self._wo,
                    stderr=self._we,
                    close_fds=False,
                    universal_newlines=True)
            self._isChildAlive = True
            thd = threading.Thread(target=self._childIO, args=(ps,)); thd.start()
            retCode = ps.wait(); del ps; ps = None
            self._isChildAlive = False
            thd.join(); del thd; thd = None
            self._sendMessage(message.Message.retcodeMessage(retCode))
        except Exception as e:
            traceback.print_exc()
            self._ws.close()
        finally:
            if ps is not None:
                ps.kill(); del ps
            if thd is not None:
                thd.join(); del thd
    
    def on_wndSize(self, message):
        pass

    def on_input(self, message):
        os.write(self._wi, message.data)
    
    def _sendMessage(self, message):
        data = message.serialize()
        self._ws.send_binary(data)
