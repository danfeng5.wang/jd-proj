# coding=utf-8
from __future__ import print_function
import os
import sys
import traceback
from base64 import b64encode
from hashlib import sha1
import re
import threading
import random
import time
from websocket import WebSocket, WebSocketApp

_exit = False
logicThread = None
def logicProc(socketObj):
    global _exit
    tab = '0123456789'
    r = random.Random(time.time())
    while not _exit:
        time.sleep(3)
        i = r.randrange(0, 9)
        n = r.randrange(10,20)
        s = tab[i] * n
        socketObj.sock.send(s)

def on_open(socketObj):
    global logicThread
    logicThread = threading.Thread(target=logicProc, args=(socketObj,))
    logicThread.setDaemon(True)
    logicThread.start()
    print('on_open')

def on_data(socketObj, message, opcode, fin):
    print('on_data')

def on_message(socketObj, message):
    print('on_message')

def on_error(socketObj, err):
    print('on_error', err)

def on_close(socketObj):
    print('on_close')

def on_ping(socketObj, message):
    print('on_Ping')

def on_pong(socketObj, message):
    print('on_pong')

def on_cont_message(socketObj, message, fin):
    print('on_cont_message')

if __name__ == '__main__':
    # ws = WebSocket()
    # ws.connect("ws://127.0.0.1:8080")
    # ws.send("Hello, Server")
    # ws.recv()
    # ws.close()

    url = 'ws://127.0.0.1:8080'
    app = WebSocketApp(url, on_open=on_open, 
                    on_message=on_message, 
                    on_error=on_error,
                    on_close=on_close, 
                    on_ping=on_ping, 
                    on_pong=on_pong,
                    on_cont_message=on_message,
                    on_data=on_data)
    try:
        app.run_forever(ping_interval=5)
    finally:
        global logicProc
        global _exit
        _exit = True
        logicThread.join()
        app.close()
