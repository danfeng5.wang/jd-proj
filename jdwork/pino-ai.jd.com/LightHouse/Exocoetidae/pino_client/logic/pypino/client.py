#coding=utf-8
import os,os.path,sys
srcdir = os.path.dirname(os.path.abspath(__file__))
srcdir = os.path.abspath(srcdir + os.path.sep + '..')
sys.path.insert(0, srcdir)


import itertools
import time
import json

import requests
import requests.exceptions

import pypino
from .models import Train, Eval, RuntimeMode
from .exceptions import InternalServerError, ForbiddenError, NotFoundError, PinoHttpError, PinoError


class PinoClient(object):

    """Client interface for the Pino REST API."""

    def __init__(self, servers, timeout=10, session=None,
                 auth_token=None, verify=False):
        """Create a PinoClient instance.
        :type servers: str or list
        :param requests.session session: requests.session for reusing the connections
        :param int timeout: Timeout (in seconds) for requests to Pino
        :param str auth_token: Token-based auth token
        :param bool verify: Enable SSL certificate verification
        """
        if session is None:
            self.session = requests.Session()
        else:
            self.session = session
        self.servers = servers if isinstance(servers, list) else [servers]
        self.verify = verify
        self.timeout = timeout

        self.auth_token = auth_token

    def __repr__(self):
        return 'Connection:%s' % self.servers

    @staticmethod
    def _parse_response(response, clazz, is_list=False, resource_name=None):
        """Parse a Pino response into an object or list of objects."""
        target = response.json()[
            resource_name] if resource_name else response.json()
        if is_list:
            return [clazz.from_json(resource) for resource in target]
        else:
            return clazz.from_json(target)

    def _do_request(self, method, path, params=None, data=None):
        """Query Pino server."""
        headers = {
            'Content-Type': 'application/json', 'Accept': 'application/json'}

        if self.auth_token:
            headers['Authorization'] = "Token {}".format(self.auth_token)

        response = None
        servers = list(self.servers)
        while servers and response is None:
            server = servers.pop(0)
            url = ''.join([server.rstrip('/'), path])
            try:
                response = self.session.request(
                    method, url, params=params, data=data, headers=headers,
                    timeout=self.timeout, verify=self.verify)
                pypino.log.info('Got response from %s', server)
            except requests.exceptions.RequestException as e:
                pypino.log.error(
                    'Error while calling %s: %s', url, str(e))

        if response is None:
            pypino.log.error(
                'Error while calling %s', url)
            raise PinoError('No remaining Pino servers to try')

        if response.status_code >= 500:
            pypino.log.error('Got HTTP {code}: {body}'.format(
                code=response.status_code, body=response.text.encode('utf-8')))
            raise InternalServerError(response)
        elif response.status_code >= 400:
            pypino.log.error('Got HTTP {code}: {body}'.format(
                code=response.status_code, body=response.text.encode('utf-8')))
            if response.status_code == 403:
                raise ForbiddenError(response)
            if response.status_code == 404:
                raise NotFoundError(response)
            else:
                raise PinoHttpError(response)
        elif response.status_code >= 300:
            pypino.log.warn('Got HTTP {code}: {body}'.format(
                code=response.status_code, body=response.text.encode('utf-8')))
        else:
            pypino.log.debug('Got HTTP {code}: {body}'.format(
                code=response.status_code, body=response.text.encode('utf-8')))

        return response

    def create_train(self, train):
        """Create an train.
        :param :class:`pypino.models.job.Train` train: the train task to create
        :returns: the created train task (on success)
        :rtype: :class:`pypino.models.job.Train` or False
        """
        data = train.to_json()
        response = self._do_request('POST', '/trains', data=data)
        if response.status_code == 201:
            return self._parse_response(response, Train)
        else:
            return False

    def list_trains(self, owner=None, status=None, project=None,
                  pinoAI=None, pinoTrainID=None, created_before=None,
                  created_after=None, modified_before=None,
                  modified_after=None, **kwargs):
        """List all trains.
        :param str owner: if passed, only show trains with a matching `owner`
        :param kwargs: arbitrary search filters
        :returns: list of trains
        :rtype: list[:class:`pypino.models.job.Train`]
        """
        params = {}
        if owner:
            params['owner'] = owner
        if status:
            params['status'] = status
        if project:
            params['project'] = project
        if pinoAI:
            params['pinoAI'] = project
        if pinoTrainID:
            params['pinoTrainID'] = pinoTrainID
        if created_before:
            params['created_before'] = created_before
        if created_after:
            params['created_after'] = created_after
        if modified_before:
            params['modified_before'] = modified_before
        if modified_after:
            params['modified_after'] = modified_after

        response = self._do_request('GET', '/trains', params=params)
        tasks = self._parse_response(
            response, Train, is_list=True, resource_name='results')
        for k, v in kwargs.items():
            tasks = [o for o in tasks if getattr(o, k) == v]
        return tasks

    def get_train(self, uuid):
        """Get a single train.
        :param str uuid: train UUID
        :returns: train task
        :rtype: :class:`pypino.models.job.Train`
        """
        response = self._do_request(
            'GET', '/trains/{uuid}'.format(uuid=uuid))
        return self._parse_response(response, Train)

    def update_train(self, uuid, train):
        """Update an train.
        :param str uuid: target train UUID
        :param train: train
        :type app: :class:`pypino.models.job.Train`
        :returns: train task
        :rtype: :class:`pypino.models.job.Train`
        """
        data = train.to_json()

        response = self._do_request(
            'PUT', '/trains/{uuid}'.format(uuid=uuid), data=data)
        return response.json()

    def partial_update_train(self, uuid, data):
        """Update an train.
        :param str uuid: target train UUID
        :param data: arbitrary update fields
        :returns: train task
        :rtype: :class:`pypino.models.job.Train`
        """
        data = json.dumps(data)

        response = self._do_request(
            'PATCH', '/trains/{uuid}'.format(uuid=uuid), data=data)
        return response.json()

    def delete_train(self, uuid):
        """Destroy a train task.
        :param str uuid: train UUID
        """
        response = self._do_request(
            'DELETE', '/trains/{uuid}'.format(uuid=uuid))
        return {}

    # eval
    def create_eval(self, evaluation):
        """Create an eval.
        :param :class:`pypino.models.job.Eval` eval: the eval task to create
        :returns: the created eval task (on success)
        :rtype: :class:`pypino.models.job.Eval` or False
        """
        data = evaluation.to_json()
        response = self._do_request('POST', '/evals', data=data)
        if response.status_code == 201:
            return self._parse_response(response, Eval)
        else:
            return False

    def list_evals(self, owner=None, status=None, project=None,
                  pinoAI=None, pinoTrainID=None, created_before=None,
                  created_after=None, modified_before=None,
                  modified_after=None, **kwargs):
        """List all evals.
        :param str owner: if passed, only show trains with a matching `owner`
        :param kwargs: arbitrary search filters
        :returns: list of evals
        :rtype: list[:class:`pypino.models.job.Eval`]
        """
        params = {}
        if owner:
            params['owner'] = owner
        if status:
            params['status'] = status
        if project:
            params['project'] = project
        if pinoAI:
            params['pinoAI'] = project
        if pinoTrainID:
            params['pinoTrainID'] = pinoTrainID
        if created_before:
            params['created_before'] = created_before
        if created_after:
            params['created_after'] = created_after
        if modified_before:
            params['modified_before'] = modified_before
        if modified_after:
            params['modified_after'] = modified_after

        response = self._do_request('GET', '/evals', params=params)
        tasks = self._parse_response(
            response, Eval, is_list=True, resource_name='results')
        for k, v in kwargs.items():
            tasks = [o for o in tasks if getattr(o, k) == v]
        return tasks

    def get_eval(self, uuid):
        """Get a single eval.
        :param str uuid: eval UUID
        :returns: eval task
        :rtype: :class:`pypino.models.job.Eval`
        """
        response = self._do_request(
            'GET', '/evals/{uuid}'.format(uuid=uuid))
        return self._parse_response(response, Eval)

    def update_eval(self, uuid, evaluation):
        """Update an eval.
        :param str uuid: target eval UUID
        :param evaluation: eval task
        :type app: :class:`pypino.models.job.Eval`
        :returns: eval task
        :rtype: :class:`pypino.models.job.Eval`
        """
        data = evaluation.to_json()

        response = self._do_request(
            'PUT', '/evals/{uuid}'.format(uuid=uuid), data=data)
        return response.json()

    def partial_update_eval(self, uuid, data):
        """Update an eval.
        :param str uuid: target eval UUID
        :param data: arbitrary update fields
        :returns: eval task
        :rtype: :class:`pypino.models.job.Eval`
        """
        data = json.dumps(data)

        response = self._do_request(
            'PATCH', '/evals/{uuid}'.format(uuid=uuid), data=data)
        return response.json()

    def delete_eval(self, uuid):
        """Destroy a eval task.
        :param str uuid: eval UUID
        """
        response = self._do_request(
            'DELETE', '/evals/{uuid}'.format(uuid=uuid))
        return {}

    def list_resource_defs(self):
        """Get all runtime mode definitions.
        :rtype: list[runtime mode]
        """
        response = self._do_request(
            'GET', '/resources')
        return self._parse_response(response, RuntimeMode, is_list=True, resource_name='results')

    def get_resource_def(self, mode):
        """Get a runtime mode definition.
        :param str mode: runtime mode
        :returns: runtime mode
        """
        response = self._do_request(
            'GET', '/resources/{mode}'.format(mode=mode))
        return self._parse_response(response, RuntimeMode)

