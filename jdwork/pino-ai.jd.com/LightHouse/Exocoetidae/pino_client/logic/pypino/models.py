# -*- coding: utf-8 -*-
import collections
import datetime
import json

from pino_client.utils.util import String


def is_stringy(obj):
    return isinstance(obj, string_types)


class PinoJsonEncoder(json.JSONEncoder):

    """Custom JSON encoder for Pino object serialization."""

    def default(self, obj):
        if hasattr(obj, 'json_repr'):
            return self.default(obj.json_repr())

        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

        if isinstance(obj, datetime.timedelta):
            return str(obj)

        if isinstance(obj, collections.Iterable) and not is_stringy(obj):
            try:
                return {k: self.default(v) for k, v in obj.items()}
            except AttributeError:
                return [self.default(e) for e in obj]

        return obj


class PinoMinimalJsonEncoder(json.JSONEncoder):

    """Custom JSON encoder for Pino object serialization."""

    def default(self, obj):
        if hasattr(obj, 'json_repr'):
            return self.default(obj.json_repr(minimal=True))

        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

        if isinstance(obj, datetime.timedelta):
            return str(obj)

        if isinstance(obj, collections.Iterable) and not is_stringy(obj):
            try:
                return {k: self.default(v) for k, v in obj.items() if (v or v in (False, 0))}
            except AttributeError:
                return [self.default(e) for e in obj if (e or e in (False, 0))]

        return obj


class PinoObject(object):
    """Base Pino object."""

    def __repr__(self):
        return "{clazz}::{obj}".format(clazz=self.__class__.__name__, obj=self.to_json(minimal=False))

    def __eq__(self, other):
        try:
            return self.__dict__ == other.__dict__
        except:
            return False

    def __hash__(self):
        # Technically this class shouldn't be hashable because it often
        # contains mutable fields, but in practice this class is used more
        # like a record or namedtuple.
        return hash(self.to_json())

    def json_repr(self, minimal=False):
        """Construct a JSON-friendly representation of the object.
        :param bool minimal: Construct a minimal representation of the object (ignore nulls and empty collections)
        :rtype: dict
        """
        if minimal:
            #return {to_camel_case(k): v for k, v in vars(self).items() if (v or v is False or v == 0)}
            return {k: v for k, v in vars(self).items() if (v or v is False or v == 0)}
        else:
            #return {to_camel_case(k): v for k, v in vars(self).items()}
            return {k: v for k, v in vars(self).items()}

    @classmethod
    def from_json(cls, attributes):
        """Construct an object from a parsed response.
        :param dict attributes: object attributes from parsed response
        """
        #return cls(**{to_snake_case(k): v for k, v in attributes.items()})
        return cls(**{k: v for k, v in attributes.items()})

    def to_json(self, minimal=True):
        """Encode an object as a JSON string.
        :param bool minimal: Construct a minimal representation of the object (ignore nulls and empty collections)
        :rtype: str
        """
        if minimal:
            return json.dumps(self.json_repr(minimal=True), cls=PinoMinimalJsonEncoder, sort_keys=True)
        else:
            return json.dumps(self.json_repr(), cls=PinoJsonEncoder, sort_keys=True)



class PinoResource(PinoObject):

    """Base Pino resource."""

    def __repr__(self):
        if 'id' in list(vars(self).keys()):
            return "{clazz}::{id}".format(clazz=self.__class__.__name__, id=self.id)
        else:
            return "{clazz}::{obj}".format(clazz=self.__class__.__name__, obj=self.to_json())

    def __eq__(self, other):
        try:
            return self.__dict__ == other.__dict__
        except:
            return False

    def __hash__(self):
        # Technically this class shouldn't be hashable because it often
        # contains mutable fields, but in practice this class is used more
        # like a record or namedtuple.
        return hash(self.to_json())

    def __str__(self):
        return "{clazz}::".format(clazz=self.__class__.__name__) + str(self.__dict__)


class Train(PinoResource):

    """Pino Train resource.
    """

    DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

    def __init__(self, uuid=None, owner=None, email=None, status=None,
                 created=None, modified=None, project=None, pinoAI=None,
                 pinoTrainID=None, description=None, cpus=None, mems=None,
                 resourceStats=None, trainImage=None, idleTimeDuration=None,
                 force=False, remove=False, waitModel=False, runAs='',
                 extra_info=None):
        self.uuid = uuid
        self.owner = owner
        self.email = email
        self.created = created
        self.modified = modified
        self.status = status
        self.project = project
        self.pinoAI = pinoAI
        self.pinoTrainID = pinoTrainID
        self.cpus = cpus
        self.mems = mems
        self.resourceStats = resourceStats
        self.description = description
        self.trainImage = trainImage
        self.idleTimeDuration = idleTimeDuration
        self.force = force
        self.remove = remove
        self.waitModel = waitModel
        self.runAs = runAs
        self.extra_info = extra_info

    def uuid_format(self):
        self.uuid = self.uuid.replace('-','')
        return self

    @staticmethod
    def shortHeader():
        return String('UUID').rspan(40) + \
               String('Owner').rspan(16) + \
               String('Type').rspan(8) + \
               String('Status').rspan(10) + \
               String('CreateTime').rspan(22) + \
               String('Resource').rspan(10) +\
               String('RunAs').rspan(16) +\
               String('Project').rspan(20)

    @property
    def createTime(self):
        return self.created[0:len('2017-08-27T10:48:41')].replace('T',' ')

    def toShortString(self):
        return String(self.uuid).rspan(40) + \
               String(self.owner).rspan(16) + \
               String('Train').rspan(8) + \
               String(self.status).rspan(10) + \
               String(self.createTime).rspan(22) + \
               String("-/-" if not self.cpus else "%sC/%sGi" % (self.cpus, self.mems)).rspan(10) +\
               String(self.runAs).rspan(16) +\
               String(self.project).rspan(20)

    def resouce_prt(self):
        lst = []
        if len(self.resourceStats) == 0:
            return lst
        v = json.loads(self.resourceStats)
        if len(v) == 0:
            return lst
        if v.has_key('controller'):
            lst += [String('    |-- ' + 'controller').rspan(18), ': ', '\n']
            for k1, v1 in v['controller']['ips'].items():
                lst += [String('    |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('    |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['controller']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('master'):
            lst += [String('   |-- ' + 'master').rspan(18), ': ', '\n']
            for k1, v1 in v['master']['ips'].items():
                lst += [String('     |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['master']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('server'):
            lst += [String('   |-- ' + 'server').rspan(18), ': ', '\n']
            lst += [String('     |-- ' + 'count').rspan(18), ': ', str(v['server']['count']), '\n']
            for item in v['server']['ips']:
                for k1, v1 in item.items():
                    lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['server']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('worker'):
            lst += [String('   |-- ' + 'worker').rspan(18), ': ', '\n']
            lst += [String('     |-- ' + 'count').rspan(18), ': ', str(v['worker']['count']), '\n']
            for item in v['worker']['ips']:
                for k1, v1 in item.items():
                    lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['worker']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
                '''
        lst = [
            String('  |-- master').rspan(18),'\n',
            String('   |-- limits').rspan(18),'\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- controller').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- server').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- worker').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n'
        ]'''
        return ' '.join(lst)

class Eval(PinoResource):

    """Pino eval resource.
    """

    DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

    def __init__(self, uuid=None, owner=None, email=None, status=None,
                 created=None, modified=None, project=None, pinoAI=None,
                 pinoTrainID=None, description=None, cpus=None, mems=None,
                 resourceStats=None, modelUUID=None, modelDir=None, runAs='',
                 extra_info=None):
        self.uuid = uuid
        self.owner = owner
        self.email = email
        self.created = created
        self.modified = modified
        self.status = status
        self.project = project
        self.pinoAI = pinoAI
        self.pinoTrainID = pinoTrainID
        self.cpus = cpus
        self.mems = mems
        self.resourceStats = resourceStats
        self.description = description
        self.modelUUID = modelUUID
        self.modelDir = modelDir
        self.runAs = runAs
        self.extra_info = extra_info

    def uuid_format(self):
        self.uuid = self.uuid.replace('-','')
        return self

    @property
    def createTime(self):
        return self.created[:len('xxxx-xx-xx xx:xx:00')].replace('T',' ')


    @staticmethod
    def shortHeader():
        return String('UUID').rspan(40) + \
               String('Owner').rspan(16) + \
               String('Type').rspan(8) + \
               String('Status').rspan(10) + \
               String('CreateTime').rspan(22) + \
               String('Resource').rspan(10) +\
               String('RunAs').rspan(16) +\
               String('Project').rspan(20)

    def toShortString(self):
        return String(self.uuid).rspan(40) + \
               String(self.owner).rspan(16) + \
               String('Eval').rspan(8) + \
               String(self.status).rspan(10) + \
               String(self.createTime).rspan(22) + \
               String("-/-" if not self.cpus else "%sC/%sGi" % (self.cpus, self.mems)).rspan(10) + \
               String(self.runAs).rspan(16) +\
               String(self.project).rspan(20)

    def resouce_prt(self):
        lst = []
        if len(self.resourceStats) == 0:
            return lst
        v = json.loads(self.resourceStats)
        if v.has_key('controller'):
            lst += [String('    |-- ' + 'controller').rspan(18), ': ', '\n']
            for k1, v1 in v['controller']['ips'].items():
                lst += [String('    |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('    |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['controller']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('master'):
            lst += [String('   |-- ' + 'master').rspan(18), ': ', '\n']
            for k1, v1 in v['master']['ips'].items():
                lst += [String('     |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['master']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('server'):
            lst += [String('   |-- ' + 'server').rspan(18), ': ', '\n']
            lst += [String('     |-- ' + 'count').rspan(18), ': ', str(v['server']['count']), '\n']
            for item in v['server']['ips']:
                for k1, v1 in item.items():
                    lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['server']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
        if v.has_key('worker'):
            lst += [String('   |-- ' + 'worker').rspan(18), ': ', '\n']
            lst += [String('     |-- ' + 'count').rspan(18), ': ', str(v['worker']['count']), '\n']
            for item in v['worker']['ips']:
                for k1, v1 in item.items():
                    lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
            lst += [String('     |-- ' + 'limits').rspan(18), ': ', '\n']
            for k1, v1 in v['worker']['limits'].items():
                lst += [String('       |-- ' + str(k1)).rspan(18), ': ', str(v1), '\n']
                '''
        lst = [
            String('  |-- master').rspan(18),'\n',
            String('   |-- limits').rspan(18),'\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- controller').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- server').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n',
            String('  |-- worker').rspan(18), '\n',
            String('   |-- limits').rspan(18), '\n',
            String('    |-- cpu').rspan(18), ': ', str(res_dict['master']['limits']['cpu']), '\n',
            String('    |-- mem').rspan(18), ': ', str(res_dict['master']['limits']['mem']), '\n'
        ]'''
        return ' '.join(lst)


class RuntimeMode(PinoResource):

    """Pino RuntimeMode resource.
    """

    def __init__(self, mode=None, roles=None, created=None, modified=None, description=None):
        self.mode = mode
        self.roles = roles
        self.created = created
        self.modified = modified
        self.description = description

    @staticmethod
    def shortHeader():
        return String('Mode').rspan(20) + \
               String('Roles').rspan(40) + \
               String('CreateTime').rspan(36) + \
               String('Description').rspan(20)

    def toShortString(self):
        rst = []
        rst += [String(self.mode).rspan(20) + \
                String("").rspan(40) + \
                String(self.created).rspan(36) + \
                String(self.description).rspan(20)]
        for role in self.roles:
            cpus = 0
            mems = 0
            for limit in role["limits"]:
                if limit["resource"] == "cpu":
                    cpus = limit["limit"]
                else:
                    mems = limit["limit"]
            rst += [String("").rspan(20) + \
                    String(role["role"] + " - limits: %sC / %sGi" % (cpus, mems)).rspan(40) + \
                    String("").rspan(36) + \
                    String("").rspan(20)]
        return rst

