# coding=utf-8
import json
import os
import sys
import os.path

import requests

# from pino_client import pinoctl as util

# user info local file
#USER_INFO_FILE = os.path.join(os.path.dirname(__file__), '.user_info')
USER_INFO_FILE = '%s/.pino.user' % os.getenv('HOME')

class User:
    def __init__(self):
        if os.path.isfile(USER_INFO_FILE):
            # read .auth file and initialze
            with open(USER_INFO_FILE, 'r') as fp:
                user_info = json.load(fp)
                self.user_info = user_info
                self.username = user_info.get('username', None)
                self.token = user_info.get('token', None)
                self.email = user_info.get('email', None)
                self.groups = user_info.get('groups', [])
                self.isSuper = user_info.get('is_superuser', False)
                self.verified = True
        else:
            # need login
            self.verified = False
            self.username = 'UNKNOWN'


    @staticmethod
    def setUserInfo(info):
        with open(USER_INFO_FILE, 'w+') as fp:
            json.dump(info, fp)
            fp.write('\n')
            fp.flush()

    def __str__(self):
        return self.username

    @staticmethod
    def hintLogin():
        print('Warn:  you must login at least once, please login first.')
        print('use:   pino login --username=<your ERP account>')


    def get_user_info(self):
        if not self.verified:
            self.hintLogin()
            sys.exit(0)

        return self.user_info

    @staticmethod
    def rm_user_info():
        try:
            os.remove(USER_INFO_FILE)
        except:
            pass
        return

user = User()
