#!/usr/bin/env python3
# Time: 2017/10/31 21:43

__author__ = 'wanggangshan@jd.com'

from utils.util import String


class TableHeader(object):
    ml_list=[
        {'column_name': 'TaskId', 'limit': 38},
        {'column_name': 'Author', 'limit': 16},
        {'column_name': 'Type', 'limit': 10},
        {'column_name': 'RunAs', 'limit': 12},
        {'column_name': 'Status', 'limit': 10},
        {'column_name': 'CreateTime', 'limit': 22},
        # {'column_name': 'Project', 'limit': 40}
    ]

    group_list = [
        {'column_name': 'GroupId', 'limit': 20},
        {'column_name': 'GroupName', 'limit': 20},
        {'column_name': 'GroupOwnerName', 'limit': 20},
    ]

    @classmethod
    def get_total_limit(cls, table_name):
        header = cls.get_header(table_name)
        count = 0
        for column in header:
            count += column['limit']
        return count

    @classmethod
    def get_header(cls, table_name):
        return getattr(cls, table_name)

    @classmethod
    def get_key_limit(cls, table_name, key):
        header = cls.get_header(table_name)
        return header.get(key)


class TableManager(object):
    def __init__(self, table_name):
        self.table_name = table_name

    def show_header(self):
        str = ''
        header = TableHeader.get_header(self.table_name)
        for column in header:
            str += String(column['column_name']).rspan(column['limit'])
        total_limit = TableHeader.get_total_limit(self.table_name)
        print('-' * total_limit)
        print(str)
        print('-' * total_limit)

    def show_whole_table(self, data):
        self.show_header()
        for info in data:
            self.show_single_data(info)

    def show_single_data(self, info):
        str = ''
        header = TableHeader.get_header(self.table_name)
        for column in header:
            column_name = column['column_name']
            limit = column['limit']
            str += String(info[column_name]).rspan(limit)

        print(str,'\n')