# -*- coding: utf-8 -*-
# wanggangshan@jd.com

import sys
import climate
try: climate.enable_default_logging()
except: pass
logging = climate.get_logger(__name__)

error_info = {
    'module': {
        100: '模块代码有误\n'
    },
    'http': {
        100: 'http响应结果有误',
    },
    'args': {
        100: '参数使用格式错误',
        101: '参数为必填项',
    },

    'login':{
        100: '登录请求发生未知错误',

    },
    'admin':{
        100: '非超级用户不能使用该命令',

    },

    'task_start': {
        100: 'Error: config 路径有误',
        101: 'Error: 处理config文件出现错误',
        102: 'Error: directory文件路径有误!',
        103: 'Error: directory文件上传前压缩时出现错误!',
        104: 'Error: websocket启动有误，请重试',
        105: 'Error: 请求开始训练出现错误',

    },
    'task_list': {
        100: 'Error: 请使用管理员用户登录后才能使用--all展示所有任务列表',
    },
    'task_logs': {
        100: 'Error: ......',
    },
    'task_tty': {
        100: 'Error: ......',
    },
    'algo_list': {
        100: 'Error: 请使用管理员用户登录后才能使用--all展示所有任务列表',
    },
    'algo_upload': {
        100: 'Error: 文件上传路径有误!',
        101: 'Error: 文件压缩上传时出现错误!',
    },
    'algo_download': {
        100: 'Error: 文件下载路径有误!',
        101: 'Error: 文件下载解压过程中出现错误!',
    },
    'algo_nodes': {
        100: 'Error: 请使用管理员用户登录后才能使用--all展示所有任务列表',
    },
}


def error_message(type='login', code=100, more='', group='', command='', exit=True):

    try:
        if code == 404:
            print(more)
            sys.exit(0)
        if type == 'args':
            print(more + error_info[type][code])
            print('请使用 pino help %s %s 查看帮助说明' % (group, command))
        else:
            message = error_info[type][code] + ' 错误码(%s):  ' % code
            print(message)
            print(more)
    except Exception as e:
        print('程序提示有误，请等待处理或联系管理员')
    if exit:
        sys.exit(0)
