# -*- coding: utf-8 -*-

__author__ = 'wanggangshan@jd.com'

# -*- coding: utf-8 -*-

import logging

import arrow
import smtplib

from contextlib import contextmanager
from functools import wraps

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders


_smtp = {
    'host': 'mx.jd.local',
    'port': 0,
    'user_name': 'ads_report',
    'password': '2Q@4waZSE$',
    'timeout': 30,
    'from_addr': 'ads_report@jd.com'
}

_receive_list = [
    'wanggangshan@jd.com'
]

class _SmtpManager(object):
    def __init__(self):
        self.__client = None

    @staticmethod
    def __get_mail_client():
        server_host = _smtp['host']
        server_port = _smtp['port']
        user_name = _smtp['user_name']
        password = _smtp['password']
        timeout = _smtp.get('timeout', 30)
        client = smtplib.SMTP(server_host, server_port, timeout=timeout)
        client.starttls()
        client.login(user_name, password)
        return client

    def __is_connected(self):
        try:
            status = self.__client.noop()[0]
        except smtplib.SMTPServerDisconnected:
            status = -1
        return True if status == 250 else False

    def get_mail_client(self):
        if not self.__client or not self.__is_connected():
            self.__client = self.__get_mail_client()
        return self.__client


smtp_manager = _SmtpManager()


class MailSender(object):
    def __init__(self):
        self.__client = None

    def send_email(self, to_addresses, title, content, from_address=None,
                   content_type='html', charset='utf-8', file_name=None):
        """
        发送邮件
        :param to_addresses: 收件人邮箱，以;分隔
        :param title: 邮件主题
        :param content: 邮件正文
        :param content_type:
        :param charset:
        :param from_address
        :param files: 文件名/文件夹名(皆可)列表
        :return:
        """
        self.__client = smtp_manager.get_mail_client()
        if not from_address:
            from_address = _smtp['from_addr']

        if file_name:
            message = MIMEMultipart()
            msg = MIMEBase('application', 'octet-stream')
            with open(file_name, 'r') as f:
                file = f.read()
            msg.set_payload(file)
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment',
                           filename=file_name)
            message.attach(msg)
            message.attach(MIMEText(content, _subtype=content_type,
                                    _charset=charset))
        else:
            message = MIMEText(content, _subtype=content_type, _charset=charset)

        message['Subject'] = title
        message['From'] = from_address
        message['To'] = ';'.join(to_addresses)
        self.__client.sendmail(from_address, to_addresses, message.as_string())