#!/usr/bin/env python3

from __future__ import division

import os
import sys
from collections import OrderedDict
from functools import reduce


_COLORS = dict(
    HIGH='\x1b[1m',
    BLACK='\x1b[30m',
    RED='\x1b[31m',
    GREEN='\x1b[32m',
    PURPLE='\x1b[35m',
    CYAN='\x1b[36m',
    WHITE='\x1b[37m',
    B_BLUE='\x1b[44m',
    B_CYAN='\x1b[46m',
    END='\x1b[00m',)


def format_dict(dt, gap=32, color='CYAN'):
    key_formatter = f"""%-{gap}s%+s\n"""
    key_color = f"""%({color})s"""
    fmt_dt = [
        key_formatter % (key_color + key.upper() + "%(END)s :", value)
        for (key, value) in dt.items()
    ]
    return ''.join(fmt_dt) % _COLORS


def format_bool(flag):
    fmt_bool = ''
    if flag:
        fmt_bool = "%(GREEN)sYES%(END)s"
    else:
        fmt_bool = "%(RED)sNO%(END)s"
    return fmt_bool % _COLORS


def format_table(title='', header=[], lists=list(list())):
    fmt_table = ''
    max_length = 45
    gaps = []
    for j in range(len(lists[0])):
        gap = len(header[j])
        for i in range(len(lists)):
            if len(lists[i][j]) > max_length:
                lists[i][j] = lists[i][j][0:max_length] + '...'
            gap = max(gap, len(lists[i][j]))
        gaps.append(gap + 3)

    total_gap = reduce(lambda x, y: x + y, gaps)
    left_gap = int((total_gap - len(title)) / 2)
    right_gap = total_gap - len(title) - left_gap
    fmt_title = "%(CYAN)s" + \
                " "*left_gap + \
                title + \
                " "*right_gap + \
                "%(END)s\n"
    fmt_table += fmt_title % _COLORS

    fmt_header = [f"""%-{gap}s""" for gap in gaps]
    fmt_header = ''.join(fmt_header) % tuple(header)
    fmt_header = "%(B_CYAN)s%(BLACK)s" + fmt_header + "%(END)s\n"
    fmt_table += fmt_header % _COLORS
    for line in lists:
        fmt_line = [f"""%-{gap}s""" for gap in gaps]
        fmt_line = ''.join(fmt_line) % tuple(line) + '\n'
        fmt_table += fmt_line

    return fmt_table


def is_use_gpu(nodes):
    pass


def format_node(index, node):
    fmt_node = ''

    fmt_node += "%(HIGH)s%(GREEN)s" + "           Role %s           " % index + "%(END)s\n"
    fmt_node = fmt_node % _COLORS

    header = OrderedDict()
    header['name'] = node.get('name', 'Unknown')
    header['entrance'] = node.get('entrance_filename', 'Unknown')
    header['required_args_num'] = len(node.get('required_args', []))
    header['optional_args_num'] = len(node.get('optional_args', []))
    resource = node.get('minimum_resource_config', {})
    header['ram_required'] = str(resource.get('mem', 'Unknown')) + " GB"
    header['disk_required'] = str(resource.get('disk', 'Unknown')) + " GB"
    cpu_num = 0
    gpu_num = 0
    for proc in resource.get('processor', []):
        if proc.get('processor_type', None) == 'CPU':
            cpu_num += int(proc.get('processor_num', 0))
        if proc.get('processor_type', None) == 'GPU':
            gpu_num += int(proc.get('processor_num', 0))
    if not cpu_num:
        cpu_num = 'Unknown'
    if not gpu_num:
        gpu_num = 'Unknown'
    header['cpu_num'] = str(cpu_num) + " Cores"
    header['gpu_num'] = str(gpu_num) + " Cards"
    fmt_node += format_dict(header, 36)

    required_args = node.get('required_args', [])
    if len(required_args):
        header = ['Name', 'Type', 'Default', 'Description']
        lists = [[
            args.get('name', 'Unknown'),
            args.get('type', 'Unknown'),
            args.get('default_value', ''),
            args.get('description', 'None')
        ] for args in required_args]
        fmt_node += format_table(
            title='Required Argument Table', header=header, lists=lists)

    optional_args = node.get('optional_args', [])
    if len(optional_args):
        header = ['Name', 'Type', 'Default', 'Description']
        lists = [[
            args.get('name', 'Unknown'),
            args.get('type', 'Unknown'),
            args.get('default_value', ''),
            args.get('description', 'None')
        ] for args in optional_args]
        fmt_node += format_table(
            title='Optional Argument Table', header=header, lists=lists)

    return fmt_node


def format_collection(dt):
    fmt = ""
    SEP = "%(PURPLE)s============%(END)s\n" % _COLORS

    fmt += SEP

    header = OrderedDict()
    header['name'] = dt.get('name', 'Unknown')
    header['attribute'] = dt.get('attr', 'Unknown').lower()
    header['contributor'] = dt.get('author', 'Unknown')
    header['description'] = ','.join(
        dt.get('algorithm', {}).get('algorithm_type', ['Unknown']))
    fmt += format_dict(header)

    fmt += SEP

    backend = OrderedDict()
    engine = dt.get('engine', {})
    nodes = dt.get('nodes', [])
    backend['backend'] = engine.get('engine_type', 'Unknown')
    backend['version'] = engine.get('version', 'Unknown')
    #backend['use_gpu'] = format_bool(True)
    backend['distributed'] = format_bool(len(nodes) > 1)
    backend['role_type_num'] = len(nodes)
    fmt += format_dict(backend)

    for index, node in enumerate(nodes):
        fmt += SEP
        fmt += format_node(index, node)

    return fmt + SEP


def format_list(lt, title='Avaliable Algorithm Table'):
    fmt = ""

    if len(lt) == 0:
        return fmt

    header = list(lt[0].keys())
    lists = [list(lt[i].values()) for i in range(len(lt))]
    fmt += format_table(title=title, header=header, lists=lists)

    return fmt
