# coding=utf-8
import os
import sys
import os.path
import codecs
import signal
import termios
import struct
import fcntl
import time
import select
import threading
from libs.wstty import PinoWebMessage
import multiprocessing

class _WebSocket(object):
    EVENT = 'tty-input'
    def __init__(self, clientSocketIO, namespace):
        self._socketIO = clientSocketIO
        self._namespace = namespace
    
    @property
    def connected(self):
        return self._socketIO.connected

    def sendWndSize(self, width, height):
        try:
            msg = PinoWebMessage.wndSizeMessage(width, height)
            self._socketIO.emit(self.EVENT, msg.serialize(), path=self._namespace)
            return True
        except Exception as e:
            #print('>>>> Exception:', str(e))
            return False

    def sendInput(self, data):
        try:
            msg = PinoWebMessage.inputMessage(data)
            self._socketIO.emit(self.EVENT, msg.serialize(), path=self._namespace)
            return True
        except Exception as e:
            #print('>>>> Exception:', str(e))
            return False

    def sendMessage(self, message):
        try:
            self._socketIO.emit(self.EVENT, message.serialize(), path=self._namespace)
            return True
        except Exception as e:
            #print('>>>> Exception:', e)
            return False

# 包含一个Thread监控input
class RemoteTerminal(threading.Thread):
    def __init__(self):
        self._exit = False
        self._ws = None
        self._nextNotifyTime = 0.0
        self._nextWinSizeTime = 0.0
        super(RemoteTerminal, self).__init__()
        
    def open(self, socketIO, namespace):
        self._ws = _WebSocket(socketIO, namespace)

        def on_disconnect():
            self.close()
            print('Error: connection closed, exit')
            sys._exit(0)
        socketIO.on('disconnect', on_disconnect, path=namespace)

        UTF8Reader = codecs.getreader('utf-8')
        UTF8Writer = codecs.getwriter('utf-8')

        self._utf_in = UTF8Reader(sys.stdin)
        self._utf_out = UTF8Writer(sys.stdout)
        self._utf_err = UTF8Writer(sys.stderr)
        self._old_handler = signal.getsignal(signal.SIGWINCH)
        self._oldtty = termios.tcgetattr(self._utf_in)

        def on_termResize(signum, frame):
            self._sendCurrentWndSize()

        def on_int(signum, frame):
            self.close()

        signal.signal(signal.SIGWINCH, on_termResize)
        signal.signal(signal.SIGINT, on_int)

        import tty
        tty.setraw(self._utf_in.fileno())
        tty.setcbreak(self._utf_in.fileno())

        def on_connection_error():
            self.close()
            print('\n\rError: connection closed, exit')
            os._exit(0)

        def on_disconnect():
            self.close()
            #print('\n\rError: connection closed, exit')
            os._exit(0)

        socketIO.on('connection_error', on_connection_error)
        socketIO.on('disconnect', on_disconnect)

        self.start()
        return True

    def close(self):
        self._exit = True
        self.join(1.0)

        if not self._ws:
            return

        termios.tcsetattr(self._utf_in, termios.TCSADRAIN, self._oldtty)
        try: 
            signal.signal(signal.SIGWINCH, self._old_handler)
        except:
            pass

        self._utf_in = None
        self._utf_out = None
        self._utf_err = None
        self._oldtty = None
        self._old_handler = None
        self._ws = None

    def onWebMessage(self, msg):
        if msg.type == PinoWebMessage.COMMAND:
            if str(codecs.decode(msg.data, 'utf-8')).lower() == 'ready':
                #print('>>>> send: current window size')
                self._sendCurrentWndSize()
        elif msg.type == PinoWebMessage.OUTPUT:
            os.write(sys.stdout.fileno(), msg.data)
            sys.stdout.flush()
        elif msg.type == PinoWebMessage.EOF:
            self.close()
        
    def run(self):
        while not self._exit:
            try:
                idata = self._readInput()
                if len(idata) > 0:
                    self._ws.sendInput(idata)

                now = time.time()
                if now > self._nextNotifyTime:
                    self._nextNotifyTime = now + 0.03
                    self._sendNop()

                if now > self._nextWinSizeTime:
                    self._nextWinSizeTime = now + 5
                    self._sendCurrentWndSize()
            except:
                pass
    
    def _sendNop(self):
        message = PinoWebMessage.nopMessage()
        self._ws.sendMessage(message) 

    def _readInput(self):        
        utf8char = b''
        fd = self._utf_in.fileno()
        while not self._exit:
            rs,_,_ = select.select([fd,], [], [], 0.02)
            if len(rs) < 1:
                return b''
            utf8char += os.read(self._utf_in.fileno(), 1)
            try:
                utf8char.decode('utf-8')
                return utf8char
            except UnicodeDecodeError:
                pass
    
    def _getwinsize(self):
        return RemoteTerminal.getWinSize()
        # TIOCGWINSZ = getattr(termios, 'TIOCGWINSZ', 1074295912)
        # s = struct.pack('HHHH', 0, 0, 0, 0)
        # x = fcntl.ioctl(self._utf_in.fileno(), TIOCGWINSZ, s)
        # rows, cols =  struct.unpack('HHHH', x)[0:2]
        # return (cols, rows)

    def _sendCurrentWndSize(self):
        self._ws.sendWndSize(*self._getwinsize())

    @staticmethod
    def getWinSize():
        TIOCGWINSZ = getattr(termios, 'TIOCGWINSZ', 1074295912)
        s = struct.pack('HHHH', 0, 0, 0, 0)
        x = fcntl.ioctl(sys.stdin.fileno(), TIOCGWINSZ, s)
        rows, cols =  struct.unpack('HHHH', x)[0:2]
        return (cols, rows)
    
