# -*- coding: utf-8 -*-
# wanggangshan@jd.com

import sys
import climate

try:
    climate.enable_default_logging()
except Exception as e:
    pass
logging = climate.get_logger(__name__)


# group
    # Usage:
    #     pino [group] [command] [options]
    #
    #
    # group       command     usage
    # -------------------------------------------------------------------
    # admin       ----       用admin身份去执行命令（要求当前用户是admin）:
    #                           pino admin [group] [command] [options]
    # 说明:
    #     1、 所有命令可使用 'pino help [group] [command]' 查看命令的具体参数使用说明
    #     2、 以下列表中command前加*号 表示该命令支持admin身份运行。
    #     3、 部分命令暂未开放
    # --------------------------------------------------------------------

class PinoInfoManager(object):

    def __init__(self, id_type='normal'):
        if id_type not in self.id_types:
            print('不支持的用户身份运行方式: ', id_type)
            sys.exit(0)
        self.id_type = id_type
        self.line_len = 86
        self.line_tag = '_' * self.line_len
        self.usage = {
            'normal': {
                'home_title':
"""
-------------------------------------------------------------------
Usage:
     pino  <command_group>  [command]  [--options]
     
""",

                'home_body':
"""
command_group    command        description
-------------------------------------------------------------------
init                            初始化
version                         查看版本号
login                           登录(使用ERP账号)
logout                          登出      
uninstall                       卸载
reinstall                       重装
upgrade                         升级客户端

info      
                  user          显示当前用户信息
                  group         显示当前用户所在组信息
cloud   
                  version       pino云端版本号       
                  quota         用户pino云端的资源配额
                  usage         任务节点占用资源
                  nodes         任务节点列表
algo    
                  version       算法库版本信息
                  list          查询当前用户的算法实例列表
                  info          查询指定算法实例的详细信息
                  upload        上传本地算法实例
                  download      下载指定算法实例到本地 
                  delete        删除远端算法实例
                  share         查询/更新算法实例的共享码
task      
                  version       版本信息
                  list          查询任务列表
                  start         启动任务
                  stop          停止任务
                  release       强制回收任务
                  dump          下载任务结果文件到本地
                  tty           远程登录到任务节点机器
                  log           查看任务节点上的日志
                  attach        将用户任务查看权限赋予某个组
                  detach        剥夺某个组查看当前用户拥有的指定任务的权限
                  publish       发布训练成功后的模型
data    
                  upload        上传数据


可以使用 "pino help [command_group] [command]" 查看命令组及具体命令的详细帮助信息
""",
                'command_info':{
                    'init': {
                        'status': False,
                        'commands': {},
                        'info':
"""
    Usage:
        pino init           初始化环境
""",
                    },

                    'version': {
                        'status': True,
                        'commands': {},
                        'info':
"""
    Usage:
        pino version         查看pino版本信息
""",
                    },

                    'login': {
                        'status': True,
                        'commands': {},
                        'info':
"""
    Usage:
        pino login [options]
    
    options:
        --username=jd               必选， 用户名
        --password=jd123            可选， 密码 
"""
                    },

                    'logout': {
                        'status': True,
                        'commands': {},
                        'info':
"""
    Usage:
        pino logout
"""
                    },
                    'uninstall': {
                        'status': False,
                        'commands': {},
                        'info':
"""
    Usage:
        pino uninstall 
"""
                    },
                    'reinstall': {
                        'status': False,
                        'commands': {},
                        'info':
"""
    Usage:
        pino reinstall 
"""
                    },
                    'upgrade': {
                        'status': False,
                        'commands': {},
                        'info':
"""
    Usage:
        pino upgrade 
"""
                    },

                    'info': {
                        'status': True,
                        'commands': {
                            'user': {
                                'status': True,
                                'info':
"""
    Usage:
        pino info user          显示用户信息    
"""
                            },
                            'group': {
                                'status': True,
                                'info':
"""
    Usage:
        pino info group         显示用户组信息   
"""
                            },
                        },
                        'info':
"""
    info        user        显示当前用户信息
                group       显示当前用户所在组信息           
"""
                    },

                    'cloud': {
                        'status': True,
                        'commands': {
                            'version': {
                                'status': True,
                                'info':
"""
    Usage:
        pino cloud version     pino云端版本号 
"""
                            },
                            'quota': {
                                'status': True,
                                'info':
"""
    Usage:
        pino cloud quota [options]  
    
    options:
        --group_id=xxxx          必选，查询某个组的资源配额                        
"""
                            },
                            'usage': {
                                'status': False,
                                'info':
"""
    Usage:
        pino cloud usage [options]     显示资源使用信息
    
    options:
        --node_id=node_id              必选，节点id
"""
                            },
                            'nodes': {
                                'status': True,
                                'info':
"""
    Usage:
        pino cloud nodes [options]     显示节点任务信息
    
    options:
        --task_id=task_id              必选，任务id
        --all                          admin可选,显示所有 
"""
                            },
                        },
                        'info':
"""
    cloud       version       pino云端版本号       
                quota         用户pino云端的资源配额
                usage         任务节点占用资源
                nodes         任务节点列表
"""
                    },
                    'algo': {
                        'status': True,
                        'commands': {
                            'version': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo version       pino算法库版本号 
"""
                            },
                            'list': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo list [options]    显示用户所有权的算法库
    
    options:
        --all                       显示个人所有可查看的全部算法库
"""
                            },
                            'info': {
                                'status': True,
                                'info':
"""
    sage:
        pino algo info [options]    显示算法实例详细信息

    options:
        --algo=xxx                  必选, 指定algo的name
        --token=xxx                 可选, 算法实例的token
"""
                            },
                            'upload': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo upload [options]      上传算法实例
    
    options:
        --algo=xxx                      必选, 指定algo的name
        --dir|--directory=/path/        必选, 文件目录
        --library=(public|private)      可选, 是否公开（该参数暂未开放）
"""
                            },
                            'download': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo download [options]   下载算法实例
    
    options:
        --algo=xxx                     必选, 指定algo的name
        --dir|--directory=/path        可选, 下载到文件目录(默认为当前目录)
"""
                            },
                            'delete': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo delete        删除算法实例
    
    options:
        --algo=xxx              必选, 指定algo的name
"""
                            },
                            'share': {
                                'status': True,
                                'info':
"""
    Usage:
        pino algo share             显示/更新算法实例token
    
    options:
        --show|--update             必选, 选择显示还是更新token。
"""
                            },
                        },
                        'info':
"""
    algo        version     算法库版本信息
                list        查询当前用户的算法实例列表
                info        查询指定算法实例的详细信息
                upload      上传本地算法实例
                download    下载指定算法实例到本地 
                delete      删除远端算法实例
                share       查询/更新算法实例的共享码
"""
                    },

                    'task': {
                        'status': True,
                        'commands': {
                            'version': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task version       pino task 版本号 
"""
                            },
                            'list': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task list [options]      显示用户所有活跃任务列表
    
    options:
        --all                       admin可选, 显示所有
"""
                            },
                            'start': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task start  [options]    启动训练任务
    
    options:
        --algo=xxx                  必选，算法实例名称
        --token=xxx                 可选, 运行该算法实例的共享码
        --dir|--directory=/path     可选, 上传本地project目录
        --config=/path              可选，运行该算法实例的配置文件
        ...                         可选，以 --x=y 形式指定其他参数              
"""
                            },
                            'stop': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task stop  [options]     停止训练任务
    
    options:
        --task_id=xxx               必选，任务id
        --force                     可选, 强制结束             
"""
                            },
                            'dump': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task dump [options]     下载release后的任务日志和模型
    
    options:
        --task_id=xxx               可选，任务id
        --dir|--directory=/path     可选, 指定下载本地路径，缺省默认当前
        --log                       可选，缺省False，仅Dump日志文件，不Dump模型
"""
                            },
                            'release': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task release [options]     回收任务

    options:
        --task_id=xxx                可选，运行中的任务id
"""
                            },
                            'publish': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task publish [options]     发布模型

    options:
        --task_id=xxx                可选，任务id
"""
                            },
                            'tty': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task tty [options]      远程登录任务节点机器
    
    options:
        --task_id=xxx               必选，任务id
        --node_id=xxx               必选, 节点id 
"""
                            },

                            'log': {
                                'status': True,
                                'info':
"""
    Usage:
        pino task log [options]      查看任务节点日志
    
    options:
        --task_id=xxx               必选，任务id
        --node_id=xxx               必选, 节点id 
        --logfile=xxx               可选，容器中日志文件路径，默认：/export/App/PinoAI/logs/pino_start.log
        --i                         可选，容器中日志文件列表，该参数暂未开放
        --mode=cat (tail/more/cat)  可选, 查看方式，默认cat
"""
                            },
                            'attach': {
                                'status': False,
                                'info':
"""
    Usage:
        pino task attach [options]      赋予任务查看权限给指定组
    
    options:
        --task_id=xxx                 必选，任务id
        --group=xxx                   必选, 组名
"""
                            },

                            'detach': {
                                'status': False,
                                'info':
"""
    Usage:
        pino task attach [options]      剥夺指定组查看用户指定任务的权限
    
    options:
        --task_id=xxx                 必选，任务id
        --group=xxx                   必选, 组名
"""
                            },
                        },
                        'info':
"""
    task    version     查询pino task 版本信息
            list        查询任务列表
            start       启动任务
            stop        停止任务
            release     强制回收任务
            dump        任务回收后，dump日志和模型到本地下
            tty         远程登录到任务节点机器
            log         查看任务节点上的日志
            list        显示任务列表 默认当前活跃或历史任务（默认10条）
            attach      将用户任务查看权限赋予某个组
            detach      剥夺某个组查看当前用户拥有的指定任务的权限
            publish     发布训练成功后的模型
"""
                    },

                    'data': {
                        'status': False,
                        'commands': {
                            'upload ': {
                                'status': False,
                                'info':
"""
    Usage:
        pino data upload [options]      上传数据
    
    options:
        --src=/path                     代码目录
        --dst=/path                     ----
        --list=/path                    ----
"""
                            },
                        },
                        'info':
"""
    data        upload      上传数据
"""
                    },
                }
            }
        }

        if id_type not in self.usage:
            print('暂未开放以%s身份运行方式' % id_type)
            sys.exit(0)

    @property
    def id_types(self):
        return ['normal', 'admin', 'test']

    @property
    def usage_home(self):
        home = self.usage[self.id_type]['home_title'] + self.usage[self.id_type]['home_body']
        return home
    @property
    def usage_commands_info(self):
        try:
            return self.usage[self.id_type]['command_info']
        except Exception as e:
            print('程序出现错误，请联系管理员')
            print(e)
            sys.exit(0)


def usage_help(id_type='normal', group='', command=''):
    infoManager = PinoInfoManager(id_type=id_type)
    home_info = infoManager.usage_home
    usage_info = infoManager.usage_commands_info

    if not group:
        print(home_info)
        sys.exit(0)

    if group not in usage_info:
        print('没有该命令: %s \n' % group)
        print(home_info)
        sys.exit(0)
    if not usage_info[group]['status']:
        print('该命令组暂未开放，敬请期待')
        sys.exit(0)
    if not command:
        print(usage_info[group]['info'], '\n')
        sys.exit(0)
    if command not in usage_info[group]['commands']:
        print(group,' 命令组没有该命令: ', command, '\n')
        print(usage_info[group]['info'], '\n')
        sys.exit(0)
    if not usage_info[group]['commands'][command]['status']:
        print('该命令暂未开放，敬请期待')
        sys.exit(0)
    print(usage_info[group]['commands'][command]['info'])
    sys.exit(0)


def usage_verify(id_type='normal', group='', command=''):
    infoManager = PinoInfoManager(id_type=id_type)
    home_info = infoManager.usage_home
    usage_info = infoManager.usage_commands_info

    def print_usage(info, command=''):
        print('命令使用有误: %s\n请参考:\n %s' % (command, info))


    if not group:
        print(home_info)
        sys.exit(0)

    if group not in usage_info:
        print_usage(home_info, command=group)
        sys.exit(0)

    if not usage_info[group]['status']:
        print('该命令组暂未开放，敬请期待')
        sys.exit(0)

    if not command:
        if usage_info[group]['commands']:
            print_usage(usage_info[group]['info'], command=group)
            sys.exit(0)
        else:
            return True

    if command not in usage_info[group]['commands']:
        print_usage(usage_info[group]['info'], command=command)
        sys.exit(0)

    if not usage_info[group]['commands'][command]['status']:
        print('该命令暂未开放，敬请期待')
        sys.exit(0)

    return True
