# -*- coding: utf-8 -*-
# wanggangshan@jd.com

# Build-in

import hashlib
import json
import os
import sys
import time
import uuid
import zipfile

import climate
import requests

from utils.config_helper import config
from logic.user import user

try:
    climate.enable_default_logging()
except Exception as e:
    pass
logging = climate.get_logger(__name__)


def get_uuid():
    return uuid.uuid1().hex


def check_path_dos_file(path):

    def _dos2unix(file_path):
        with open(file_path, 'r') as f1:
            data = f1.read()
            list = data.split('\r\n')
            if len(list) == 1:
                return
        new_data = '\n'.join(list)

        with open(file_path, 'r') as f2:
            f2.write(new_data)
        return

    for dir_path, dir_names, file_names in os.walk('path'):
        for f_name in file_names:
            if f_name.split('.')[-1] not in ['py', 'sh']:
                continue
            file_path = os.path.join(dir_path, file_names)
            _dos2unix(file_path)


def md5(src_str):
    md5sumer = hashlib.md5()
    if isinstance(src_str, str):
        src = src_str.encode()
    else:
        src = src_str
    md5sumer.update(src)
    return md5sumer.hexdigest()


def store_2_json(data, file_path):
    with open(file_path, 'w') as json_file:
        json_file.write(json.dumps(data))


def load_json(file_path):
    with open(file_path) as json_file:
        data = json.load(json_file)
    return data


def http_request(url, params=None, data={}, request_type='get',
                 timeout=None, files=None, headers={}):
    session = requests.Session()
    domain_url = 'http://' + config.get('domain_ip') + ':' + str(config.get('domain_port'))
    _headers = {
        'Content-Type': 'application/json', 'Accept': 'application/json'}
    user_info = None
    if url != '/cli/login':
        user_dict = user.get_user_info()
        user_info = {
            'user_id': user_dict['user_id'],
            'token': user_dict['token'],

        }

        data.update(user_info)

    # add customized http headers
    if headers:
        _headers.update(headers)

    url = domain_url + url
    try:
        if files:
            response = session.request(
                request_type, url, params=params, data=data,
                files=files)
        else:
            response = session.request(
                request_type, url, params=params, json=data, headers=_headers)
        return response
    except requests.exceptions.ConnectionError:
        print('连接服务器失败，请联系管理员')
        sys.exit(0)
    except Exception as e:
        print('请求异常，异常信息: %s' % e)
        sys.exit(0)


def verify_reponse(response):
    try:
        res_json = response.json()
    except:
        print('服务器端发生错误 错误信息: %s' % response)
        return False, None
    if response.status_code != requests.codes.ok:
        print('code of response is not 200 ok.  response:\n %s' % response)
        return False, None
    if not res_json['status']:
        print(res_json['message'])
        return False, res_json
    return True, res_json


def normalizeDirName(path, type='dir'):
    if not path or len(path) < 1:
        return None
    if not isinstance(path, str):
        return None
    if path[0] == '~':
        path = os.environ['HOME'] + path[1:]
    while path[-1:] == os.path.sep:
        path = path[:-1]
    if type == 'file':
        if not os.path.isfile(path):
            return None
    else:
        if not os.path.isdir(path):
            return None
    return os.path.abspath(path)


#打包目录为zip文件（未压缩）
def make_zip(source_dir, output_filename):
    zipf = zipfile.ZipFile(output_filename, 'w')
    pre_len = len(os.path.dirname(source_dir))
    for parent, dirnames, filenames in os.walk(source_dir):
        for filename in filenames:
            pathfile = os.path.join(parent, filename)
            arcname = pathfile[pre_len:].strip(os.path.sep)
            #相对路径      zipf.write(pathfile, arcname)  zipf.close()
            zipf.write(pathfile, arcname)
    zipf.close()


def unzip(filename, out_dir):
    r = zipfile.is_zipfile(filename)
    if r:
        starttime = time.time()
        fz = zipfile.ZipFile(filename, 'r')
        for file in fz.infolist():
            print(file.filename)  # 打印zip归档中目录
            d = file.date_time
            gettime = "%s/%s/%s %s:%s" % (d[0], d[1], d[2], d[3], d[4])
            fz.extract(file, out_dir)
            filep = os.path.join(out_dir, file.filename)
            # print("恢复文件:%s的最后修改时间" % filep)
            timearry = time.mktime(time.strptime(gettime, '%Y/%m/%d %H:%M'))
            os.utime(filep, (timearry, timearry))
        endtime = time.time()
        times = endtime - starttime
        print('unzip %s times: ' % filename + str(times))
    else:
        print('This file is not zip file')


class String(str):
    def __init__(self, s):
        str.__init__(s)

    def rspan(self, width, ch=' '):
        while len(self) < width:
            self += ch
        return self

    def lspan(self, width, ch=' '):
        while len(self) < width:
            self = ch + self
        return self
