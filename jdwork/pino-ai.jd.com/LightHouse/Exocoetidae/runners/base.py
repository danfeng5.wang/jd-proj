# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = 'penghao5@jd.com'

import os
import climate
import multiprocessing
import traceback
climate.enable_default_logging()
logging = climate.get_logger(__name__)

from utils.alarm import http_alarm


class Registrar(type):

    def __init__(cls, name, bases, dct):
        if not hasattr(cls, '_registry'):
            cls._registry = {}
        else:
            cls._registry[name.lower()] = cls
        super(Registrar, cls).__init__(name, bases, dct)

    def build(cls, key, *args, **kwargs):
        return cls._registry[key.lower()](*args, **kwargs)

    def is_registered(cls, key):
        return key.lower() in cls._registry


class RunnerBase(Registrar(str('Base'), (), {})):
    
    def __init__(self, **kwargs):
        self._topic = 'Base'
        print(kwargs)
        self.role = kwargs.get('role', '')
        self.index = kwargs.get('index', 0)
        self._common_task = None

    def setup(self, **kwargs):
        pass

    #def start(self, **kwargs):
    #    p_num = kwargs.get('p_num', 1)
    #    c_num = kwargs.get('c_num', 1)
    #    logging.info('Start %s, %s Producer, %s Consumer.',
    #        self.__class__.__name__, p_num, c_num)
    #    for i in range(p_num):
    #        process = multiprocessing.Process(target=self._start_producing)
    #        self._producers.append(process)
    #        process.start()
    #    for i in range(c_num):
    #        process = multiprocessing.Process(target=self._start_consuming)
    #        self._consumers.append(process)
    #        process.start()
    def start(self, **kwargs):
        logging.info('Start %s', self.__class__.__name__)
        if self.role.lower() in ['p', 'producer']:
            self._start_producing()
        elif self.role.lower() in ['c', 'consumer']:
            self._start_consuming()
        else:
            logging.error('Unknown role: %s', self.role)

    def _start_producing(self, **kwargs):
        logging.info('%s-Producer-%s started, [PID] %s',
            self.__class__.__name__, self.index, os.getpid())
        self._feed(**kwargs)

    def _start_consuming(self, **kwargs):
        logging.info('%s-Consumer-%s started, [PID] %s',
            self.__class__.__name__, self.index, os.getpid())
        self._fetch(**kwargs)

    def _recall(self, class_name, func_name):
        raise NotImplementedError('Virtual Method')

    def _pipeline(self, **kwargs):
        for func in [self._init, self._execute, self._monitor,
            self._collect, self._report, self._release]:
            ret = False
            try:
                ret = func(**kwargs)
            except NotImplementedError:
                continue
            except Exception as e:
                logging.error('[%s][%s] task_id: %s, Run Exception: %s',
                              self.__class__.__name__, func.__name__,
                              self._common_task.task_id, traceback.format_exc())
                self._recall(self.__class__.__name__, func.__name__)
                # alarm 通过UMP平台报警
                http_alarm('crm runner run error,task_id: %s, exception: %s' %
                           (self._common_task.task_id, traceback.format_exc()))
                return
            if not ret:
                return False
    
    def _feed(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _fetch(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _init(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _execute(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _monitor(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _collect(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _report(self, **kwargs):
        raise NotImplementedError('Virtual Method')

    def _release(self, **kwargs):
        raise NotImplementedError('Virtual Method')


#class RunnerManager(object):
#    
#    '''
#    Start Runner.
#    '''
#
#    def __init__(self, **kwargs):
#        self._runner = RunnerBase.build(kwargs.get('topic') + 'Runner', **kwargs)
#        self._p_num = kwargs.get('producer_num', 0)
#        self._c_num = kwargs.get('consumer_num', 0)
#        max_c_num = multiprocessing.cpu_count() / 4 if multiprocessing.cpu_count() / 4 > 10 else 10 
#        self._c_num = self._c_num if self._c_num < max_c_num else max_c_num
#
#    def setup(self, **kwargs):
#        return self._runner.setup()
#
#    def start(self, **kwargs):
#        self._runner.start(p_num=self._p_num,
#                           c_num=self._c_num)
