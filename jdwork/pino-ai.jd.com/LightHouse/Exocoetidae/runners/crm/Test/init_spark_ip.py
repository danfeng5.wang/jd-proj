#!/usr/bin/env python3
# Time: 2017/12/28 1:21

__author__ = 'wanggangshan@jd.com'
import os
import sys

ex_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.insert(0, ex_path)

from utils.config_helper import config, crm_config, load_all_config
load_all_config()
from db.db import redis_manager

def get_redis_client():
    redis_config = config.get('redis')
    redis_crm_config = crm_config.get('redis_config')
    crm_db = {'db': redis_crm_config.get('db')}
    redis_config.update(crm_db)
    redis_manager.register_db('CRM', redis_config)
    redis_client = redis_manager.get_client('CRM')
    return redis_client


client = get_redis_client()


key = crm_config.get('redis_config.keys.crm_docker_queue.name')
while True:
    ip = client.lpop(key)
    if not ip:
        break

for ip in crm_config.get('spark_ip_list'):
    client.lpush(key, ip)
