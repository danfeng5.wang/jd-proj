# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
from collections import defaultdict

import requests


__author__ = 'penghao5@jd.com'

import os
import sys
import time
import json
import glob
import redis
import shutil
import pprint
import climate
import subprocess
import collections
import traceback
climate.enable_default_logging()
logging = climate.get_logger(__name__)

from runners.base import Registrar, RunnerBase
from utils.config_helper import config, crm_config
from db.db import redis_manager
from db.models import TaskPeriod
from db.crm.models import to_dict

from src.api.crm_task import CRMTaskApi
from src.api.crm_log import CRMLogApi
from src.api.common_task import CommonTaskApi
from utils.util import http_request
from utils.alarm import http_alarm

import services

def exec_cmd(cmd, mode='system'):
    if mode == 'system':
        logging.info('Cmd: %s', cmd)
        return os.system(cmd)
    elif mode == 'subprocess':
        logging.info('Cmd: %s', ' '.join(cmd))
        return subprocess.call(cmd)


class CRMTask(Registrar(str('Base'), (), {})):
    
    def __init__(self, **kwargs):
        self.remote_task_path = kwargs.get('remote_task_path', '')
        self.local_task_path = None
        self.task_type = kwargs.get('task', None)
        self.model_id = kwargs.get('model_id', '')
        self.train_id = kwargs.get('train_id', '')
        self.test_task_id = ''
        self.predict_id = ''
        self.hdfs_username = ''
        self.configure = kwargs.get('configure', None)
        self.is_spark_task = False

        self.sign = kwargs.get('sign', '')
        self.status = ''
        self.err_code = ''
        self.result_context = []


    def check_health(self):
        # NOTE(penghao) `SparkOnlineModel`, not `SparkOnlineModelTrainer`.
        if self.configure.get('model_configure', {}).get('form', '') == 'SparkOnlineModel':
            self.is_spark_task = True
        if not self.task_type in ['train', 'test', 'predict']:
            logging.error('%s Error: invalid task_type %s.', self.__class__.__name__, self.task_type)
            return False
        if self.model_id is None:
            logging.error('%s Error: no model_id.', self.__class__.__name__)
            return False
        if self.train_id is None:
            logging.error('%s Error: no train_id.', self.__class__.__name__)
            return False
        self.hdfs_username = self.configure.get('hdfs_username', None)
        if self.hdfs_username is None:
            logging.error('%s Error: no hdfs_username.', self.__class__.__name__)
            return False
        if not isinstance(self.configure, dict):
            logging.error('%s Error: invalid configure %s.', self.__class__.__name__, self.configure)
            return False
        return True


    def gen_local_task_path(self, runner_workspace):
        # {runner_workspace}/{task_type}/{model_id}_{train_id}
        # e.g. /export/App/Ex-Runner/workspace/train/1_1
        self.local_task_path = os.path.join(
            runner_workspace, self.task_type,
            '%s_%s' % (self.model_id, 
            self.train_id))
        if os.path.exists(self.local_task_path):
            shutil.rmtree(self.local_task_path)
        os.makedirs(self.local_task_path)


    def clean_local_task_path(self):
        filelist = os.listdir(self.local_task_path)
        for f in filelist:
            filepath = os.path.join(self.local_task_path, f)
            if os.path.isfile(filepath):
                os.remove(filepath)
            elif os.path.isdir(filepath):
                shutil.rmtree(filepath, True)


    def prepare_task(self):
        # {local_task_path}/conf
        conf_path = os.path.join(self.local_task_path, 'conf')
        if not os.path.exists(conf_path):
            os.makedirs(conf_path)
        # generate {local_task_path}/conf/configure.py
        configure_py = os.path.join(conf_path, 'configure.py')
        with open(configure_py, 'w') as f:
            f.write('from env import username\n')
            f.write('from env import password\n')
            f.write('from env import hdfs_username\n')
            for k, v in self.configure.items():
                data = pprint.pformat(v)
                f.write('%s = ' % k)
                [f.write(line.rstrip() + '\n') for line in data.splitlines()]
        # generaete {local_task_path}/conf/env.py
        env_py = os.path.join(conf_path, 'env.py')
        with open(env_py, 'w') as f:
            f.write('username = \'\'\n')
            f.write('password = \'\'\n')
            f.write('hdfs_username = \'%s\'\n' % self.hdfs_username)
        return True


    def set_status(self, status, err_code=''):
        self.status = 'successed' if status else 'failed'
        self.err_code = err_code


    def wait_end_log(self, fn):
        return NotImplementedError

    
    def collect_result(self, fn):
        return NotImplementedError


    def gen_result_context(self, **kwargs):
        if self.status == 'failed':
            cxt = dict(status=self.status,
                     sign=self.sign,
                     err_code=self.err_code,
                     model_id=self.model_id,
                     train_id=self.train_id,
                     )
            if self.task_type == 'test':
                cxt['task_id'] = self.test_task_id
            if self.task_type == 'predict':
                cxt['predict_id'] = self.predict_id
            self.result_context = [cxt]


    def pull_file(self,
                  file_path,
                  local_store_path,
                  attemp_times=10,
                  sleep_minutes=1):
        for i in range(attemp_times):
            if os.path.exists(file_path):
                exec_cmd('cp %s %s' % (file_path, local_store_path))
                return True
            time.sleep(60*sleep_minutes)
        return False


    def init_kerberos(self):
        hdfs_krb_dir = crm_config.get('hdfs.krb_dir')
        if self.hdfs_username == '':
            return
        if not os.path.exists('../keytab/getkeytab'):
            exec_cmd('wget -P ../keytab http://172.28.215.119/www/getkeytab;')
        if not os.path.exists('../keytab/%s.keytab' % (self.hdfs_username)):
            exec_cmd('chmod 755 ../keytab/getkeytab; ../keytab/getkeytab %s ../keytab' % (self.hdfs_username))
        exec_cmd('kinit -kt ../keytab/%s.keytab %s' % (self.hdfs_username, self.hdfs_username))


    def upload_file(self, filename):
        remote_file_path = os.path.join(self.remote_task_path, 'logs', filename)
        local_file_path = os.path.join(self.local_task_path, filename)
        if not self.pull_file(remote_file_path, self.local_task_path):
            logging.error('Pulling file failed.')
            return False
        if not os.path.isfile(local_file_path):
            logging.error('Pulling file failed.')
            return False

        hadoop_cmd = crm_config.get('hdfs.hadoop_cmd')
        logging.info('hdfs_username: %s, switch to %s ns.' % (self.hdfs_username, self.hdfs_username))
        prefix = crm_config.get('hdfs.unify_ns') + '/user/%s/predict_results/' % self.hdfs_username

        self.init_kerberos()
        status = exec_cmd([hadoop_cmd, "fs", "-test", "-e", prefix], mode='subprocess')
        if status != 0:
            self.init_kerberos()
            exec_cmd([hadoop_cmd, "fs", "-mkdir", prefix], mode='subprocess')
        self.init_kerberos()
        status = exec_cmd([hadoop_cmd, "fs", "-put", "-f", local_file_path, prefix], mode='subprocess')
        return (not status), os.path.join(prefix, filename)


class CRMTaskTrain(CRMTask):
    
    def __init__(self, **kwargs):
        super(CRMTaskTrain, self).__init__(**kwargs)
        self.title = ''
        self.graph = []
        self.callback_url = crm_config.get('callback_url.TrainResult_URL')

    def check_health(self):
        if not super(CRMTaskTrain, self).check_health():
            return False
        if not self.task_type == 'train':
            logging.error('%s Error: invalid task_type %s.', self.__class__.__name__, self.task_type)
            return False
        return True 


    def wait_end_log(self, fn):
        lastest_log =  fn(**dict(
            model_id=self.model_id,
            train_id=self.train_id))
        if not lastest_log:
            return False, None
        if lastest_log.task_stage != 'after_train_cmd':
            return False, lastest_log
        else:
            return True, None


    def collect_result(self, fn):
        # collect data for epoch-loss graph
        logs = fn(**dict(
            model_id=self.model_id,
            train_id=self.train_id,
            role=['master', 'worker'],
            batches=0,
            task_type='train',
        ))
        return self.gen_graph(logs)


    def gen_graph(self, logs):
        graphs = {}
        if not logs:
            # XXX(penghao) Should this situation return True ?
            self.graph = []
            return True
        for log in logs:
            role_name = log.role + '_' + str(log.number)
            if role_name not in graphs.keys():
                desc = 'train dataset' if log.role == 'worker' else 'validation dataset'
                graphs[role_name] = dict(
                    title='Result on %s' % desc,
                    x_label='epoch',
                    y_label='loss',
                    data=[])
            graphs[role_name]['data'].append(dict(x=log.epoch, y=log.loss))
        for role_name in graphs.keys():
            line = graphs[role_name]['data']
            line = sorted(line, key=lambda x: x['x'])
            for i, p in enumerate(line):
                if i > 0 and p['y'] == 0:
                    p['y'] = line[i - 1]['y']   # p['y'] = round(p['y'], 3)
            # sampling : 100 points
            max_point = 100
            total_point = len(line)
            if total_point > max_point * 2:
                delta = math.floor(total_point / max_point)
                sub_line = [line[p * delta] for p in range(max_point)]
                graphs[role_name]['data'] = sub_line
        self.title = 'This is training result for model_id: %s, train_id: %s' % (self.model_id, self.train_id)
        self.graph = [graph for k, graph in graphs.items()]
        return True


    def gen_result_context(self, **kwargs):
        if self.status == 'failed':
            super(CRMTaskTrain, self).gen_result_context()
            return
        self.result_context.append(dict(
            status=self.status,
            sign=self.sign,
            err_code=self.err_code,
            model_id=self.model_id,
            train_id=self.train_id,
            title=self.title,
            graph=self.graph))


class CRMTaskTest(CRMTask):
    
    def __init__(self, **kwargs):
        super(CRMTaskTest, self).__init__(**kwargs)
        self.test_task_id = kwargs.get('task_id', None)
        self.title = ''
        self.stats = {}
        self.file_uri = ''
        self.callback_url = crm_config.get('callback_url.TestResult_URL')


    def check_health(self):
        if not super(CRMTaskTest, self).check_health():
            return False
        if self.task_type != 'test':
            logging.error('%s Error: invalid task_type %s.', self.__class__.__name__, self.task_type)
            return False
        if self.test_task_id is None:
            logging.error('%s Error: invalid task_id.', self.__class__.__name__)
            return False
        return True 


    def wait_end_log(self, fn):
        lastest_log = fn(**dict(
            model_id=self.model_id,
            train_id=self.train_id,
            task_id=self.test_task_id))
        if not lastest_log:
            return False, None
        if lastest_log.task_stage != 'evaluate_model':
            return False, lastest_log
        else:
            return True, None


    def collect_result(self, fn):
        # Step 1. collect stats
        logs = fn(**dict(
            model_id=self.model_id,
            train_id=self.train_id,
            task_id=self.test_task_id,
            task_stage='evaluate_model',
            task_type='test',
        ))
        self.collect_stats(logs)

        # Step 2. upload file
        status, file_uri = self.upload_file(
            filename='result_%s_%s_%s.log' % (self.model_id, self.train_id, self.test_task_id))
        if not status:
            logging.error('Upload file to hdfs failed.')
            return False
        else:
            self.file_uri = file_uri
            logging.info('Upload file finished, stored at: %s', self.file_uri)
            if crm_config.get('hdfs.unify_ns') in self.file_uri:
                self.file_uri = file_uri.replace(crm_config.get('hdfs.unify_ns'), '')
            return True


    def collect_stats(self, logs):
        stats_check_list = ['auc', 'accuracy', 'precision', 'recall',
                            'positive_ratio', 'f_1', 'log_loss', 'rmse',
                            'mean', 'var', 'max', 'min', 'ri', 'dvi', 'ca',
                            'kld', 'raw', 'mape', 'mae']
        if not logs:
            # XXX(penghao) Should this situation return True ?
            self.stats = {}
            return True
        for log in logs:
            for k in stats_check_list:
                value = getattr(log, '_' + k, None)
                if value is not None:
                    if type(value) == bytes:
                        value = value.decode('utf-8')
                    self.stats[k] = value
        return True


    def gen_result_context(self, **kwargs):
        if self.status == 'failed':
            super(CRMTaskTest, self).gen_result_context()
            return
        self.result_context.append(dict(
            status=self.status,
            sign=self.sign,
            err_code=self.err_code,
            model_id=self.model_id,
            train_id=self.train_id,
            task_id=self.test_task_id,
            title=self.title,
            stats=self.stats))
        self.result_context.append(dict(
            status=self.status,
            sign=self.sign,
            err_code=self.err_code,
            model_id=self.model_id,
            train_id=self.train_id,
            task_id=self.test_task_id,
            file_uri=self.file_uri))


class CRMTaskPredict(CRMTask):
    
    def __init__(self, **kwargs):
        super(CRMTaskPredict, self).__init__(**kwargs)
        self.predict_id = kwargs.get('predict_id', None)
        self.file_uri = ''
        self.callback_url = crm_config.get('callback_url.PredictResult_URL')


    def check_health(self):
        if not super(CRMTaskPredict, self).check_health():
            return False
        if self.task_type != 'predict':
            logging.error('%s Error: invalid task_type %s.', self.__class__.__name__, self.task_type)
            return False
        if self.predict_id is None:
            logging.error('%s Error: invalid predict_id.', self.__class__.__name__)
            return False
        return True 


    def wait_end_log(self, fn):
        lastest_log = fn(**dict(
            model_id=self.model_id,
            train_id=self.train_id,
            predict_id=self.predict_id))
        if not lastest_log:
            return False, None
        if lastest_log.task_stage != 'predict_model':
            return False, lastest_log
        else:
            return True, None


    def collect_result(self, fn):
        # upload file
        status, file_uri = self.upload_file(
            filename='pResult_%s_%s_%s.log' % (self.model_id, self.train_id, self.predict_id))
        if not status:
            logging.error('Upload file to hdfs failed.')
            return False
        else:
            self.file_uri = file_uri
            logging.info('Upload file finished, stored at: %s', self.file_uri)
            if crm_config.get('hdfs.unify_ns') in self.file_uri:
                self.file_uri = file_uri.replace(crm_config.get('hdfs.unify_ns'), '')
            return True


    def gen_result_context(self, **kwargs):
        if self.status == 'failed':
            super(CRMTaskPredict, self).gen_result_context()
            return
        self.result_context.append(dict(
            status=self.status,
            sign=self.sign,
            err_code=self.err_code,
            model_id=self.model_id,
            train_id=self.train_id,
            predict_id=self.predict_id,
            file_uri=self.file_uri))


    def combine_report_url(self, report_url):
        domain_name = config.get('callback_url.domain_name')
        port = config.get('callback_url.port')
        url = 'http://' + domain_name + ':' + str(port) + report_url
        return url


class CRMRunner(RunnerBase):
    
    def __init__(self, **kwargs):
        super(CRMRunner, self).__init__(**kwargs) 
        self._topic = 'CRM'
        self._workspace = os.path.join(os.getcwd(), 'workspace')
        self._crm_task = None
        self._crm_task_api = CRMTaskApi()
        self._common_task_api = CommonTaskApi()
        self._crm_log_api = CRMLogApi()
        self._report_result = None
        self._report_file_result = None
        self.release = True
        self.docker_ip = None

        self._flags = collections.namedtuple('Flags',
            ['execute', 'monitor', 'collect', 'report', 'release']
            )(True, True, True, True, True)


    def __get_redis_client(self):
        redis_config = config.get('redis')
        redis_crm_config = crm_config.get('redis_config')
        crm_db = {'db': redis_crm_config.get('db')}
        redis_config.update(crm_db)
        redis_manager.register_db('CRM', redis_config)
        redis_client = redis_manager.get_client('CRM')
        return redis_client


    def setup(self, **kwargs):
        self._redis = self.__get_redis_client()
        self._max_length = crm_config.get('redis_config.keys.crm_runner_queue.max_length')
        self._queue = crm_config.get('redis_config.keys.crm_runner_queue.name')
        self._docker_queue = crm_config.get('redis_config.keys.crm_docker_queue.name')
        try:
            self._redis = self.__get_redis_client()
        except Exception as e:
            logging.error('Error: failed to connect Redis: %s，Exception: %s',
                          config.get('redis'), traceback.format_exc())
            return False
        return True


    def _recall(self, class_name, func_name):
        logging.info('Run [Class: %s,Func: %s] Exception Error')
        # task_id = self._common_task.task_id
        # update_task = {
        #     'status': -2,
        #     'error_msg': 'Run [Class: %s,Func: %s] Exception Error'
        # }
        # if self._common_task_api.update_task(task_id, **update_task):
        #     logging.info('Write Exception Error Message to DB Task Table ')
        #     return True
        return True


    def _feed(self, **kwargs):
        logging.name = __name__ + '.Producer-%d' % self.index
        while True:
            if self._cur_queue_len() >= self._max_length:
                # 当前redis队列已满
                time.sleep(60)
            else:
                tasks, feed_num = self._feed_many(**kwargs)
                if not tasks:
                    # 没有任务新加入队列
                    time.sleep(10)


    def _fetch(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        while True:
            time.sleep(2)
            if not self._cur_queue_len():
                time.sleep(10)
                continue
            task_id = self._fetch_one(**kwargs)
            if not task_id:
                time.sleep(60)
                continue

            self._common_task = self._crm_task_api.get_by_task_id(task_id)
            self._pipeline(**kwargs)


    def _feed_many(self, **kwargs):
        feed_num = self._max_length - self._cur_queue_len()
        if feed_num <= 0:
            return [], 0
        try_limit = crm_config.get('runner.try_limit')
        tasks = self._crm_task_api.feed_many_waiting_tasks(try_limit, feed_num)
        queue_tasks = []
        new_num = 0
        for task in tasks:
            try:
                self._redis.lpush(self._queue, task.task_id)
                queue_tasks.append(task)
                logging.info('New task %s are pushed to queue.', task.task_id)
                new_num += 1
            except Exception as e:
                logging.error('Error: redis task queue add failed: %s. ErrorInfo: %s',
                              task.task_id, traceback.format_exc())
                self._crm_task_api.recover_queued_to_waitting(task.task_id)
                continue
        return queue_tasks, new_num


    def _cur_queue_len(self):
        try:
            length = self._redis.llen(self._queue)
            return length
        except Exception as e:
            logging.error('Error: query redis queue length failed: %s.',
                          traceback.format_exc())
            return self._max_length + 1

    def get_task_type_by_id(self, task_id):
        common_task = self._crm_task_api.get_by_task_id(task_id)
        return common_task.task_type
       
    def _check_res(self, task_id):
        try:
            task_type = self.get_task_type_by_id(task_id)
            user_id = crm_config.get('user.user_id')
            group_id = crm_config.get('user.group_id')
            algo_name = crm_config.get('algo').get(task_type)
            result = self._crm_task_api\
                .check_quota_usable(user_id, group_id, algo_name)
            return result
        except Exception as e:
            logging.error('Error: Check resource task_id: %s; error_msg: %s', task_id, e)
            return False

    def _fetch_one(self, **kwargs):
        logging.info('[Fetch One]')
        task_id = None
        try:
            while True:
                task_id = self._redis.rpop(self._queue)
                if not task_id:
                    logging.warning('No task fetched.')
                    return None
                else:
                    result = self._crm_task_api.get_by_task_id(task_id)
                    if not result:
                        self._redis.lpush(self._queue, task_id)
                        logging.error('Redis has task but mysql not exiests!'
                                      ' Task_id: %s, Poped; db result: %s', task_id, result)
                        continue
                    logging.info('Pop task %s', task_id)
                if not self._check_res(task_id):
                    time.sleep(10)
                    self._redis.lpush(self._queue, task_id)
                    logging.warning('No resource.')
                    logging.info('Task %s will be pushed back to queue.', task_id)
                    continue
                else:
                    break
            return task_id
        except Exception as e:
            logging.error('Error: %s.', traceback.format_exc())
            return None


    def _set_next_stage(self, status, period, msg=''):
        self._common_task_api.update_task(
                self._common_task.task_id,
                **dict(status=status, period=period, error_msg=msg))


    def _init(self, **kwargs):
        '''
        Generete crm task.
        '''
        logging.info('[Init]')
        self._crm_task = None
        self.docker_ip = None
        self._flags = collections.namedtuple('Flags',
                                             ['execute', 'monitor', 'collect', 'report', 'release']
                                             )(True, True, True, True, True)
        task_path = self._common_task.mfs_path
        task_type = self._common_task.task_type
        request_backup_path = task_path + '/.request_backup'
        if task_type == 'train':            
            file_name = 'train_request.json'
        elif task_type == 'test':
            file_name = 'test_%s_request.json' % (self._common_task.test_id, )
        else:
            file_name = 'predict_%s_request.json' % (self._common_task.predict_id, )
        files = glob.glob(task_path + '/' + file_name)
        if len(files) != 1:
            logging.error('Error: %d json files found at %s.', len(files), task_path)
            return False
        try:
            with open(files[0], 'r') as f:
                task_request = json.load(f)
        except Exception as e:
            logging.error('Error: failed to load %s, %s', files[0],
                          traceback.format_exc())
            return False
        self._crm_task = CRMTask.build(
            'CRMTask' + task_request.get('task', ''),
            remote_task_path=task_path,
            **task_request)
        if not self._crm_task.check_health():
            logging.error('Error: constructing crm task failed: [common_task_id] %s, [task_path] %s.', 
                self._common_task.task_id, task_path)
            return False
        else:
            logging.info('crm task constructed: [common_task_id] %s, [task_path] %s.',
                self._common_task.task_id, self._crm_task.remote_task_path)
            logging.info('crm task parameters: [task_type] %s, [model_id] %s, [train_id] %s, [task_id] %s, [predict_id] %s.',
                self._crm_task.task_type, self._crm_task.model_id,
                self._crm_task.train_id, self._crm_task.test_task_id,
                self._crm_task.predict_id)
            # backup {task_path}/request.json to {task_path}/.{task_type}_request.json
            # e.g. cp /mnt/mfsb/pino/test/crm/1_1/request.json /mnt/mfsb/pino/test/crm/1_1/.train_request.json
            exec_cmd(cmd='cp -f %s %s/%s' % (files[0], request_backup_path, '.' + file_name))
        return True
    

    def _release_docker_ip(self, **kwargs):
        if self._crm_task.is_spark_task:
           logging.info('Release docker ip: %s', self.docker_ip)
           self._redis.lpush(self._docker_queue, self.docker_ip) 
        

    def _submit_spark_job(self, **kwargs):
        # fetch a docker ip.
        start_time = time.time()
        while True:
            docker_ip = self._redis.rpop(self._docker_queue)
            if not docker_ip:
                print('Waiting for idle docker (sec): %d\r' % 
                    int(time.time() - start_time), end='', flush=True)
                time.sleep(30)
                continue
            self.docker_ip = docker_ip
            break
        logging.info('Pop docker ip: %s', self.docker_ip)

        # download files from saltwater.
        req = services.AlgoLibRequest(
            author=str(crm_config.get('user.user_id')),
            algo_name=crm_config.get('algo.spark'),
            local_dir=self._crm_task.local_task_path)
        rsp = services.download_algo_v2(req)
        if not rsp.success:
            self._set_next_stage(0, TaskPeriod.Waiting, 'Failed when download files')
            http_alarm('Failed when download files')
            self._release_docker_ip()
            exit(0)
        algo_dir = rsp.result.get('downloaded_dir')

        # copy files from downloaded folder to workspace folder.
        cmd = 'cp -af %s/* %s/' % (algo_dir, self._crm_task.local_task_path)
        exec_cmd(cmd)

        # obtain commands generated by saltwater.
        logging.info('Local task path: %s', self._crm_task.local_task_path)
        other_args_dict = dict(
            mode=self._crm_task.task_type,
            username=crm_config.get('mirror.username'),
            password="0okm\(IJN",
            crm_model_id=str(self._crm_task.model_id),
            crm_train_id=str(self._crm_task.train_id),
            crm_task_id=str(self._crm_task.test_task_id),
            crm_predict_id=str(self._crm_task.predict_id),
            crm_env_backup_path=crm_config.get('mfs.base_path'),
            crm_monitor_uri=crm_config.get('monitor.report_url'),
            jks_project=crm_config.get('jenkins.project'),
            _worker_num=1,
            ip=self.docker_ip)
        logging.info('Other args dict: %s', other_args_dict)
        req = services.AlgoLibRequest(
            local_dir=self._crm_task.local_task_path,
            keywords=other_args_dict,
            workspace_dir=self._crm_task.local_task_path)
        rsp = services.generate_cmd_from_local_v2(req)
        if not rsp.success:
            self._set_next_stage(0, TaskPeriod.Waiting, 'Failed when generating cmds')
            http_alarm('Failed when generating cmds')
            self._release_docker_ip()
            exit(0)
        run_cmds = rsp.result.get('cmd')
        
        # get command executed on controller.
        logging.info('All cmds: %s', run_cmds)
        controller_cmds = run_cmds.get('controller', {}).get('controller.com', None)
        logging.info('Controller cmds: %s', controller_cmds)
        if (not controller_cmds) or \
            (not isinstance(controller_cmds, list)) or \
            (not len(controller_cmds)):
            self._set_next_stage(0, TaskPeriod.Waiting, 'Failed when parsing cmds')
            http_alarm('Failed when parsing cmds')
            self._release_docker_ip()
            exit(0)
        controller_cmd = controller_cmds[-1]
        logging.info('Controller cmd: %s', controller_cmd)

        # generate and execute real command.
        real_cmd = 'cd %s && mkdir logs && nohup %s > logs/pino_start.log 2>&1 &' % (self._crm_task.local_task_path, controller_cmd) 
        exec_cmd(real_cmd)

        self._set_next_stage(0, TaskPeriod.Running, '')
        return True


    def _execute(self, **kwargs):
        '''
        1. generate configure.py, env.py in local conf folder.
        2. call start train API
            user_name:
            algo_name:
            workpsace:
            keywords:
        3. enqueue -> submitted
        '''
        logging.info('[Execute]')

        if not self._flags.execute:
            logging.info('Execute step is skipped by flag.')
            return True

        # generate local workspace for current crm task.
        logging.info('Generate local task path.')
        self._crm_task.gen_local_task_path(
            self._workspace)

        # prepare task
        logging.info('Prepare task.')
        if not self._crm_task.prepare_task():
            return False

        # if self._crm_task.is_spark_task:
        #     return self._submit_spark_job()

        # submit task
        task_start_args = dict(
            algo=crm_config.get('algo').get(self._crm_task.task_type),
            dir=self._crm_task.local_task_path,
            task_id=self._common_task.task_id,
            other_args_dict=dict(
                mode=self._crm_task.task_type,
                username=crm_config.get('mirror.username'),
                password=crm_config.get('mirror.password'),
                crm_model_id=str(self._crm_task.model_id),
                crm_train_id=str(self._crm_task.train_id),
                crm_task_id=str(self._crm_task.test_task_id),
                crm_predict_id=str(self._crm_task.predict_id),
                crm_env_backup_path=crm_config.get('mfs.base_path'),
                crm_monitor_uri=crm_config.get('monitor.report_url'),
                jks_project=crm_config.get('jenkins.project'),
                _worker_num=1,
            ))
        logging.info('Task start args: %s', task_start_args)
        result = self._crm_task_api.task_start(**task_start_args)
        if not result.status:
            logging.error('Error: failed to submit task to PinoCloud, this runner-consumer will exit.')
            # TODO(penghao) Add UMP here @wanggangshan
            self._set_next_stage(0, TaskPeriod.Waiting, 'Submit failed on cloud')
            http_alarm('Submit failed on cloud')
            exit(0)
        else:
            logging.info('Task has been submitted to PinoCloud, waiting for cloud response. cloud message: %s',
                         result.message)
            self._set_next_stage(0, TaskPeriod.Submitted, '')
            return True


    def _monitor(self, **kwargs):

        '''
        1. wait cloud response: submitted -> failed.
        2. monitor t_crm_log.
        3. success or failed.
        '''
        logging.info('[Monitor]')

        if not self._flags.monitor:
            logging.info('Monitor step is skipped by flag.')
            return True

        resp_time = time.time()
        max_no_resp_time_gap = crm_config.get('runner.log.timeout')
        try_limit = crm_config.get('runner.try_limit')
        is_running = False
        while (time.time() - resp_time) < \
            max_no_resp_time_gap and not is_running:
            task_in_table = self._crm_task_api.\
                get_by_task_id(self._common_task.task_id)
            if task_in_table.status == -2:
                # Launch failed on cloud, don't need retry.
                msg = 'Launch failed on cloud'
                self._crm_task.set_status(False, msg)
                self._flags = self._flags._replace(collect=False)
                self._set_next_stage(-2, TaskPeriod.Complete, msg)
                return True
            if task_in_table.status == -1 and task_in_table.try_num >= try_limit:
                # Lanuch failed on cloud, cannot retry due to out of try times.
                msg = 'Launch failed on cloud to many times'
                self._crm_task.set_status(False, msg)
                self._flags = self._flags._replace(collect=False)
                self._set_next_stage(-2, TaskPeriod.Complete, msg)
                return True
            if task_in_table.status == -1 and task_in_table.try_num < try_limit:
                # Launch failed on cloud, retry it.
                msg = 'Launch failed on cloud, retrying'
                self._flags = self._flags._replace(collect=False, report=False)
                self._set_next_stage(0, TaskPeriod.Waiting, msg)
                return True
            if task_in_table.status == 0 and task_in_table.period == TaskPeriod.Submitted:
                print('Waiting for cloud response (sec): %d\r' % 
                    int(time.time() - resp_time), end='', flush=True)
            if task_in_table.status == 0 and task_in_table.period == TaskPeriod.Running:
                is_running = True
            # XXX(penghao) User other stratergy instead of sleeping 10 secs.
            time.sleep(30)

        # Timeout
        if not is_running:
            logging.error('Error: waiting for cloud response timeout.')
            if task_in_table.try_num >= try_limit:
                # Lanuch timeout on cloud, cannot retry due to out of try times.
                msg = 'Launch timeout on cloud to many times'
                self._crm_task.set_status(False, msg)
                self._flags = self._flags._replace(collect=False)
                self._set_next_stage(-2, TaskPeriod.Complete, msg)
            else:
                # Lanuch timeout on cloud, retry it.
                msg = 'Launch timeout on cloud, retrying'
                self._flags = self._flags._replace(collect=False, report=False)
                self._set_next_stage(-2, TaskPeriod.Waiting, msg)
            return True    

        # Status=0, period=Running, now handle LogTable
        logging.info('Cloud report for launching success.')
        last_log_time = time.time()
        while (time.time() - last_log_time) < \
            max_no_resp_time_gap:
            status, lastest_log = self._crm_task.wait_end_log(
                self._crm_log_api.get_task_last_log)
            if status:
                # End log appeared
                logging.info('Termination log is found.')
                self._crm_task.set_status(True)
                self._set_next_stage(1, TaskPeriod.Complete, 'run success')
                return True
            if not status and not lastest_log:
                print('Waiting for first log (sec): %d\r' % 
                    int(time.time() - last_log_time), end='', flush=True)
            else:
                print('Waiting for termination log, lastest log appeared (sec): %d\r' % 
                    int(time.time() - last_log_time), end='', flush=True)
                last_log_time = lastest_log.cur_time
            # XXX(penghao) User other stratergy instead of sleeping 10 secs.
            time.sleep(60)

        # Timeout 
        logging.error('Error: waiting for termination log timeout.')
        msg = 'waiting for end log timeout'
        self._crm_task.set_status(False, msg)
        self._flags = self._flags._replace(collect=False)
        self._set_next_stage(-2, TaskPeriod.Complete, msg)
        return True


    def _collect(self, **kwargs):
        '''
        1. collect task result
        2. upload file to hdfs
        '''
        logging.info('[Collect]')

        if not self._flags.collect:
            logging.info('Collect step is skipped by flag.')
            return True

        if self._crm_task.is_spark_task:
            logging.info('Collect step is skipped because of spark task.')
            return True

        if not self._crm_task.collect_result(
            self._crm_log_api.get_task_log):
            msg = 'collect result failed'
            self._crm_task.set_status(False, msg)
            self._set_next_stage(-2, TaskPeriod.Complete, msg)
        return True


    def _report(self, **kwargs):
        '''
        1. resultTrainTask/resultTestTask/resultPredictTask
        '''
        logging.info('[Report]')

        if not self._flags.report:
            logging.info('Report step is skipped by flag.')
            return True

        if self._crm_task.is_spark_task:
            logging.info('Report step is skipped because of spark task.')
            return True
 
        domain_name = crm_config.get('callback_url.domain_name')
        port = crm_config.get('callback_url.port')
        report_url = 'http://' + domain_name + ':' + str(port) + self._crm_task.callback_url

        self._crm_task.gen_result_context()
        for ctx in self._crm_task.result_context:
            try:
                logging.info('Report to <%s>: %s', report_url, ctx)
                response = http_request(
                    report_url,
                    data=ctx,
                    request_type='post')
                if response.status_code != requests.codes.ok:
                    logging.error('Error: response of code is not 200:\n%s' % response)
                else:
                    logging.info('Response: %s', response)
            except Exception as e:
                logging.error('Error: error_msg: %s' % str(e))
        if self._crm_task.status != 'successed':
            self._set_next_stage(-2, TaskPeriod.Complete, 'unknown')
        return True

    def _release(self, **kwargs):
        logging.info('[Release]')
        
        logging.info('Clean local task path: %s', self._crm_task.local_task_path)

        # self._release_docker_ip()
            
        if self.release:
            self._crm_task.clean_local_task_path()
            response = self._crm_task_api.task_stop(self._common_task.task_id)
            if not response.status:
                logging.error('[Release] error_msg: %s' % response.message)
            else:
                logging.info('[Release] success task_id: %s' % str(response.result))
            # TODO(penghao) stop task here @hejingjian

        return True
