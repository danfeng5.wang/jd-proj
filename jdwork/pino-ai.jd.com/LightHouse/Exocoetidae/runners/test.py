# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = 'penghao5@jd.com'

import os
import sys
import time
import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)

from runners.base import RunnerBase

class TestRunner(RunnerBase):

    def __init__(self, **kwargs):
        super(TestRunner, self).__init__(**kwargs) 

    def _feed(self, **kwargs):
        logging.name = __name__ + '.Producer-%d' % self.index
        while True:
            time.sleep(5)
            logging.info('This is a feed process.')

    def _fetch(self, **kwargs):
        logging.name = __name__ + '.Consumer-%d' % self.index
        while True:
            time.sleep(5)
            logging.info('This is a fetch process.')
