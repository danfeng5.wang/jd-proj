# -*- coding: utf-8 -*-

__author__ = 'penghao5@jd.com'

import os
import sys
import argparse
import pymysql

ex_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
core_path = os.path.join(ex_path, 'PinoCore')
sys.path.insert(0, ex_path)
import exocoetidae as ex
import services

from utils.config_helper import load_all_config, config
load_all_config()
from db.db import db_session_manager as db_manager
db_cfg = config.get('mysql_db')
db_manager.register_db(db_cfg, 'default')

parser =argparse.ArgumentParser()

parser.add_argument(
    '--topic',
    default='Test',
    help='Runner topic, [Test/CRM]')

parser.add_argument(
    '--role',
    help='Producer number, [p/c]')

parser.add_argument(
    '--index',
    type=int,
    help='Consumer number, [0/1/2/...]')

FLAGS = parser.parse_args()

if __name__ == '__main__':
    #mgr = ex.RunnerManager(
    #    topic=FLAGS.topic,
    #    producer_num=FLAGS.p_num,
    #    consumer_num=FLAGS.c_num)
    #mgr.setup()
    #mgr.start()
    services.init_train_pool(1)
    runner = ex.RunnerBase.build(
        FLAGS.topic + 'Runner',
        **dict(role=FLAGS.role,
        index=FLAGS.index))
    runner.setup()
    runner.start()
