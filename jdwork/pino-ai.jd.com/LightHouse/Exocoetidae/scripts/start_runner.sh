
topic=${1}
p_num=${2}
c_num=${3}

for((i=0;i<${p_num};i++))
do
    echo "Start Runner-Producer-${i}"
    nohup python start_runner.py --topic=${topic} --role=p --index=${i} >> ../logs/producer-${i}.log 2>&1 &
done

for((i=0;i<${c_num};i++))
do
    echo "Start Runner-Consumer-${i}"
    nohup python start_runner.py --topic=${topic} --role=c --index=${i} >> ../logs/consumer-${i}.log 2>&1 &
done
