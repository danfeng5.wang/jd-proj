
import os,sys,os.path
sys.path.append('%s/../../libs' % os.path.dirname(__file__))
sys.path.append('%s/../../PinoCore' % os.path.dirname(__file__))
import pctx

from wstty import PinoWebMessage
from pino_socketio import PinoSocketIO
from socketIO_client import SocketIO, LoggingNamespace

def on_response(*args):
    msg = PinoWebMessage.parseFrom(args[0])
    print('response: ', msg)

socketIO = SocketIO('127.0.0.1', 5000, LoggingNamespace)

# socketIO.on('my_response', on_response, path='/test')
socketIO.on(PinoSocketIO.EVENT, on_response, path='/test')

socketIO.emit('message', "hejingjian string 名称", path='/test')
#socketIO.emit('my_event', {"data": "hejingjian"}, path='/test')
#socketIO.emit('connect', path='/test')
socketIO.wait()

# import os
# import sys
# import json
# import struct
# import base64
# import codecs

# if __name__ == '__main__':
#     data = struct.pack('!III', 1,2,3)
#     d64 = codecs.decode(base64.b64encode(data), 'utf-8')
#     print(len(d64), len(str(data)))
#     s = json.dumps(d64, ensure_ascii=False)
#     s2 = json.loads(s)
#     print(s, s2)

#     d = base64.b64decode(codecs.encode(s2, 'utf-8'))
#     a,b,c = struct.unpack('!III', d)
#     print(a, b, c)
