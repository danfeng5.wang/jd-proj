#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.common.task import CommonTaskService


class CommonTaskApi(object):

    def update_task(self, task_id, **kwargs):
        with session_manager.with_session() as session:
            common_task_service = CommonTaskService(session)
            return common_task_service.update_task(task_id, **kwargs)

    def update_task_start_status(self, **kwargs):
        with session_manager.with_session() as session:
            common_task_service = CommonTaskService(session)
            return common_task_service.update_task_start_status(**kwargs)
