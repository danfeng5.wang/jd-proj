#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.crm.log import CRMLogService


class CRMLogApi(object):

    def get_task_last_log(self, **kwargs):
        with session_manager.with_session() as session:
            crm_log_service = CRMLogService(session)
            return crm_log_service.get_task_last_log(**kwargs)

    def get_task_log(self, **kwargs):
        with session_manager.with_session() as session:
            crm_log_service = CRMLogService(session)
            return crm_log_service.get_task_log(**kwargs)

    def add_crm_log_info(self, **kwargs):
        with session_manager.with_session() as session:
            crm_log_service = CRMLogService(session)
            return crm_log_service.add_crm_log_info(**kwargs)