#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.crm.task import CRMTaskService


class CRMTaskApi(object):

    def get_by_task_id(self, task_id):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.get_by_task_id(task_id)

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.feed_many_waiting_tasks(try_limit, feed_num)

    def recover_queued_to_waitting(self, task_id):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.recover_queued_to_waitting(task_id)

    def check_quota_usable(self, user_id, group_id, algo_name):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.check_quota_usable(user_id, group_id, algo_name)

    def task_start(self, **task_start_args):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.task_start(**task_start_args)

    def task_stop(self, task_id):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.task_stop(task_id)

    def add_task_info(self, **jsondata):
        with session_manager.with_session() as session:
            crm_task_service = CRMTaskService(session)
            return crm_task_service.add_task_info(**jsondata)
