#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.group import GroupService



class GroupApi(object):

    def create_or_update_group(self, **group_dict):
        with session_manager.with_session() as session:
            group_service = GroupService(session)
            return group_service.create_or_update_group(**group_dict)

    def create_or_update_belong(self, **belong_dict):
        with session_manager.with_session() as session:
            group_service = GroupService(session)
            return group_service.create_or_update_belong(**belong_dict)
