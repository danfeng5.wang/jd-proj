#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.group_quota import GroupQuotaService



class GroupQuotaApi(object):

    def create_or_update_quota(self, **quota_dict):
        with session_manager.with_session() as session:
            group_quota_service = GroupQuotaService(session)
            return group_quota_service.create_or_update_quota(**quota_dict)
