#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.user import UserService



class UserApi(object):

    def create_or_update_user(self, **user_info_dict):
        with session_manager.with_session() as session:
            user_service = UserService(session)
            return user_service.create_or_update_user(**user_info_dict)

    def check_user_token(self, user_id):
        with session_manager.with_session() as session:
            user_service = UserService(session)
            return user_service.check_user_token(user_id)

    def get_user_above_info(self, user_id):
        with session_manager.with_session() as session:
            user_service = UserService(session)
            return user_service.get_user_above_info(user_id)
