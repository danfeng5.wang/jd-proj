#!/usr/bin/env python3
# Time: 2017/12/31 17:39

__author__ = 'wanggangshan@jd.com'


from db.db import db_session_manager as session_manager
from src.services.white_list import WhiteListService



class WhiteListApi(object):

    def get_white_user(self, user_id):
        with session_manager.with_session() as session:
            white_service = WhiteListService(session)
            return white_service.get_white_user(user_id)


