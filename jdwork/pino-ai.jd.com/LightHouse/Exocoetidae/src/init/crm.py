#!/usr/bin/env python3
# Time: 2017/12/19 15:52

__author__ = 'wanggangshan@jd.com'


import sys
from utils.config_helper import crm_config
from utils.util import md5_passwd
from src.api.user import UserApi
from src.api.group import GroupApi
from src.api.group_quota import GroupQuotaApi
import PinoCore.services as PinoCoreApi

from db.models import UserSourceType
import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)


class CrmInit(object):

    def __init__(self):
        self.__user_api = UserApi()
        self.__group_api = GroupApi()
        self.__group_quota_api = GroupQuotaApi()
        self.crm_user = crm_config.get('user')
        self.__core_api = PinoCoreApi

    def init(self):

        func_list = [
            self.init_user,
            self.init_group,
            self.init_belong,
            self.init_quota,
            self.init_k8s_namespace,
            self.init_k8s_quota
        ]
        for func in func_list:

            res = func()

            logging.info('%s end, return: %s', func.__name__, res)
            if not res:
                logging.info('init CRM user Faild, %s return: %s', func.__name__, res)
                sys.exit(0)

    def init_user(self):
        password = self.crm_user.get('password', 0)
        if password:
            password, password_salt = md5_passwd(password)
        else:
            password, password_salt = ('', '')
        user_info_dict = {
            'user_id': self.crm_user.get('user_id', 0),
            'username': self.crm_user.get('username', ''),
            'password': password,
            'password_salt': password_salt,
            'mobile': self.crm_user.get('mobile', ''),
            'email': self.crm_user.get('email', ''),
            'source_id': '',
            'source_type': UserSourceType.System,
            'source_describe': 'crm unique user'
        }
        user_response = self.__user_api.create_or_update_user(**user_info_dict)
        return user_response.status

    def init_group(self):
        group_dict = {
            'group_id': self.crm_user.get('group_id', 0),
            'group_name': self.crm_user.get('group_name', ''),
            'group_email': self.crm_user.get('email', ''),
        }
        group_info = self.__group_api.create_or_update_group(
            **group_dict)
        return group_info

    def init_belong(self):
        belong_dict = {
            'user_id': self.crm_user.get('user_id', 0),
            'group_id': self.crm_user.get('group_id', 0),
        }
        group_info = self.__group_api.create_or_update_belong(
            **belong_dict)
        return group_info

    def init_quota(self):
        quota = self.crm_user.get('resources')
        quota_dict = {
            'group_id': self.crm_user.get('group_id', 0),
            'cpus': float(quota.get('cpus', 0)),
            'gpus': float(quota.get('gpus', 0)),
            'mems': float(quota.get('mems', 0)),
            'disk': float(quota.get('disk', 0)),
            'over_use_rate': self.crm_user.get('resources.over_use_rate', 1.0),
        }
        quota = self.__group_quota_api.create_or_update_quota(**quota_dict)
        return quota

    def init_k8s_namespace(self):
        namespace = self.crm_user.get('group_name', '')
        res = self.__core_api.ns_create_or_update(namespace)
        return res

    def init_k8s_quota(self):
        namespace = self.crm_user.get('group_name', 0)
        quota = self.crm_user.get('resources')
        quota_dict = {
            'cpus': float(quota.get('cpus', 0)),
            'gpus': float(quota.get('gpus', 0)),
            'mems': float(quota.get('mems', 0)),
            'disk': float(quota.get('disk', 0)),
        }
        res = self.__core_api.quota_create_or_update(namespace, **quota_dict)
        return res