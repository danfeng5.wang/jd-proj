#!/usr/bin/env python3
# Time: 2017/11/20 15:56

__author__ = 'wanggangshan@jd.com'


from .base import BaseLogic
from .algo import AlgoManager
from .algo_auth import AlgoAuthManager
from .algo_history import AlgoHistoryManager
from .cloud import CloudManager
from .group import GroupManager
from .group_quota import GroupQuotaManager
from .task import TaskManager
from .user import UserManager
from .white_list import WhiteListManager
