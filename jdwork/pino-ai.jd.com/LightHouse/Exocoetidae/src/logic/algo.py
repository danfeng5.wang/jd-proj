#!/usr/bin/env python3
# Time: 2017/11/23 11:27

__author__ = 'wanggangshan@jd.com'


from db.models import AlgoList, AlgoJobHistory
from src.logic.base import BaseLogic
from utils.util import get_uuid


class AlgoManager(BaseLogic):
    """
        算法库管理类    
    """

    def __init__(self, db_session):
        super(AlgoManager, self).__init__(db_session)

    def add_algo(self, **kwargs):
        """
            添加算法实例
            
        :param kwargs: dict, 算法参数 e.g: {'uid':'', 'algo_name':'', 'description':''}
        :return: bool
        """
        algo = AlgoList(author_uid=kwargs.get('uid', 0),
                        author_name=kwargs.get('author_name', ''),
                        algo_name=kwargs.get('algo_name', ''),
                        description=kwargs.get('description', '')
                        )
        self.db.add(algo)
        self.db.commit()
        return algo

    def get_by_uid_and_name(self, author_uid, algo_name):
        info = self.db.query(AlgoList)\
            .filter_by(algo_name=algo_name, author_uid=author_uid)\
            .first()
        return info

    def get_algo_by_id(self, algo_id):
        info = self.db.query(AlgoList)\
            .filter(id=algo_id)\
            .first()
        return info

    def get_algo_by_ids(self, algo_id_list):
        info = self.db.query(AlgoList)\
            .filter(AlgoList.id.in_(algo_id_list))\
            .all()
        return info

    def get_algos_by_author_id(self, author_id):
        infos = self.db.query(AlgoList)\
            .filter_by(author_uid=author_id)\
            .all()
        return infos

    def update_algo(self, algo_id, **kwargs):
        self.db.query(AlgoList).filter_by(id=algo_id)\
            .update(kwargs, synchronize_session=False)
        self.db.commit()

    def delete_algo(self, algo_id):
        self.db.query(AlgoList).filter_by(id=algo_id).delete()
        self.db.commit()
        return True
