#!/usr/bin/env python3
# Time: 2017/11/23 14:13

__author__ = 'wanggangshan@jd.com'


from collections import defaultdict
from db.models import AlgoAuth
from src.logic.base import BaseLogic
from src.logic.algo import AlgoManager
from src.logic.group import GroupManager

class AlgoAuthManager(BaseLogic):
    def __init__(self, db_session):
        super(AlgoAuthManager, self).__init__(db_session)
        self.__algo_manager = AlgoManager(db_session)

    def get_user_algo_auth(self, uid, algo_id):
        algo_auth = self.db.query(AlgoAuth) \
            .filter_by(algo_id=algo_id,
                       group_or_user=0,
                       gid_or_uid=uid) \
            .first()
        return algo_auth

    def _get_user_algos_auth(self, uid):
        algo_auths = self.db.query(AlgoAuth)\
            .filter_by(group_or_user=0, gid_or_uid=uid)\
            .all()
        return algo_auths


    def get_group_algo_auth(self, gid, algo_id):
        algo_auth = self.db.query(AlgoAuth) \
            .filter_by(algo_id=algo_id,
                       group_or_user=1,
                       gid_or_uid=gid) \
            .first()
        return algo_auth

    def get_group_algos_auth(self, gid):
        """
            获取组所拥有的算法集合的权限集合
            
        :param gid: 
        :return: 
        """
        algo_auths = self.db.query(AlgoAuth) \
            .filter_by(group_or_user=1, gid_or_uid=gid) \
            .all()
        return algo_auths

    def __trans_auth_code_to_dict(self, auth_code):
        auth_info = {
            'read': int(auth_code[0]),
            'download': int(auth_code[1]),
            'write': int(auth_code[2]),
            'exec': int(auth_code[3])
        }
        return auth_info

    def __combine_two_auth(self, auth_code_one, auth_code_two):
        code = ''
        for i in range(len(auth_code_one)):
            code += str(max(int(auth_code_one[i], int(auth_code_two[i]))))
        return code

    def combine_group_auths(self, group_auths):
        auth_code_dict = defaultdict(dict)
        for group_auth in group_auths:
            if group_auth.algo_id in auth_code_dict:
                auth_code = self.__combine_two_auth(
                    auth_code_dict[group_auth.algo_id],
                    group_auth.auth_code
                )
                auth_code_dict[group_auth.algo_id] = auth_code
            else:
                auth_code_dict[group_auth.algo_id] = group_auth.auth_code
        for key in auth_code_dict:
            auth_code_dict[key] = self.__trans_auth_code_to_dict(auth_code_dict[key])
        return auth_code_dict

    def get_user_algos_auth(self, uid):
        as_author_algos = self.__algo_manager.get_algos_by_author_id(uid)
        auth_form_author = {algo.id: '1111' for algo in as_author_algos}
        auths_from_user = self._get_user_algos_auth(uid)

        auths_user_dict = {auth.algo_id: self.__trans_auth_code_to_dict(auth.auth_code) for auth in auths_from_user}
        auths_user_dict.update(auth_form_author)
        group_manager = GroupManager()
        gids = group_manager.get_user_belong_gids(uid)
        if not gids:
            return auths_user_dict
        auths_from_groups = []
        for gid in gids:
            group_auths = self.get_group_algos_auth(gid)
            auths_from_groups.extend(group_auths)
        if not auths_from_groups:
            return auths_user_dict

        auths_group_dict = self.combine_group_auths(auths_from_groups)

        for algo_id in auths_group_dict:
            if algo_id not in auths_user_dict:
                auths_user_dict[algo_id] = auths_group_dict[algo_id]
        return auths_user_dict

    def verify_user_algo_auth(self, user_id, algo_id, auth='read'):
        """
            验证用户算法权限
            验证用户对该算法是否拥有读/下载/写/执行的权限
        
        :param user_id:
        :param algo_id: 
        :param auth: 
        :return: 
        """

        algo_info = self.get_user_algo_auth(user_id, algo_id)
        auth_code_info = self.__trans_auth_code_to_dict(algo_info.auth_code)
        return True if auth_code_info[type] else False
