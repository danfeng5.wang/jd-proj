#!/usr/bin/env python3
# Time: 2017/11/23 11:27

__author__ = 'wanggangshan@jd.com'


from db.models import AlgoList, AlgoJobHistory
from src.logic.base import BaseLogic
from utils.util import get_uuid


class AlgoHistoryManager(BaseLogic):
    """
        算法库管理类    
    """

    def __init__(self, db_session):
        super(AlgoHistoryManager, self).__init__(db_session)

    def get_algo_history_by_job_id(self):
        pass

    def get_by_author_and_algo_name(self):
        pass

    def add_algo_job_history(self, **kwargs):
        job_id = get_uuid()
        history = AlgoJobHistory(
            job_id=job_id,
            author_uid=kwargs.get('author', 0),
            algo_name=kwargs.get('algo_name', ''),
            action_type=kwargs.get('action', ''),
            status=kwargs.get('status', ''),
            directory=kwargs.get('directory', ''),
            description=kwargs.get('message', ''),
            created_id=kwargs.get('author', '')
        )
        self.db.add(history)
        self.db.commit()
        return history

    def update_algo_job_history(self, job_id, **kwargs):
        self.db.query(AlgoJobHistory)\
            .filter_by(job_id=job_id)\
            .update(kwargs, synchronize_session=False)
        self.db.commit()
