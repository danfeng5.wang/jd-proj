#!/usr/bin/env python3
# Time: 2017/11/27 21:01

__author__ = 'wanggangshan@jd.com'


from db.models import GroupInfo
from src.logic.base import BaseLogic


class CloudManager(BaseLogic):
    """
        PinoCloud 管理类
    """
    def __init__(self, db_session):
        super(CloudManager, self).__init__(db_session)
