#!/usr/bin/env python3
# Time: 2017/12/7 14:55
from utils.config_helper import config

__author__ = 'hejingjian@jd.com'

from db.models import CommonTaskList
from src.logic.base import BaseLogic

import climate
logging = climate.get_logger(__name__)

class CommonTaskManager(BaseLogic):
    def __init__(self, db_session):
        self.limit_try_num = config.get('limit_try_num')
        super(CommonTaskManager, self).__init__(db_session)


    def add_common_task(self, **kwargs):
        """
            添加一个任务

        :param kwargs: dict， 任务表字段内容
        :return: bool
        """
        try:
            task_info = CommonTaskList(**kwargs)
            self.db.add(task_info)
            self.db.commit()
            return task_info
        except Exception as e:
            logging.info('mysql error:', e)
            self.db.commit()
            return False

    def update_common_task(self, **kwargs):
        """
            通过task_id更新用户任务列表

        :param kwargs: dict， 任务表字段内容
        :return: bool
        """
        self.db.query(CommonTaskList) \
            .filter_by(task_id=kwargs['task_id']) \
            .update(kwargs)
        self.db.commit()

    def update_by_topic_period(self, **kwargs):
        """
            通过task_id更新用户任务列表

        :param kwargs: dict， {'topic_type':xxx, 'period':xxxx, 'limit':xxxx...}
        :return: bool
        """
        result = []
        try:
            result = self.db.query(CommonTaskList).with_for_update(read=False) \
                .filter_by(topic_type=kwargs['topic_type'],
                           status=0,
                           period=kwargs['period']
                           )\
                .filter(CommonTaskList.try_num <= kwargs['try_limit'],)\
                .order_by(CommonTaskList.created_at.desc())\
                .limit(kwargs['limit']).all()
        except:
            self.db.rollback()
        for task in result:
            try_num = task.try_num + 1
            self.db.query(CommonTaskList).filter_by(id=task.id).update({'period': kwargs['to_period'],
                                                                        'try_num': try_num})
        self.db.commit()
        return result

    def add_one(self, **kwargs):
        """
        新增一条（增）
        
        args:
            common_task: 

        returns:
            True / False
        """
        try:
            task_info = CommonTaskList(**kwargs)
            self.db.add(task_info)
            self.db.commit()
            return True
        except Exception as e:
            logging.info('mysql error:', e)
            self.db.commit()
            return False


    def add_many(self, **kwargs):
        """
        新增多条（增）
        
        args:
            common_task_list: [
            ]

        returns:
            True / False
        """
        try:
            for data in kwargs:
                task_info = CommonTaskList(**data)
                self.db.add(task_info)
            self.db.commit()
            return True
        except Exception as e:
            logging.info('mysql error:', e)
            self.db.commit()
            return False

    def query_one(self, **kwargs):
        """
        只查询，返回结果列表（查）
        
        args:
            filters: {
                topic_type:
            }

        returns:
            common_task_list: []
        """
        task_info = self.db.query(CommonTaskList). \
            filter_by(**kwargs).first()
        self.db.commit()
        return task_info

    def query_all(self, **kwargs):
        """
        只查询，返回结果列表（查）

        args:
            filters: {
                topic_type:
            }

        returns:
            common_task_list: []
        """
        task_info = self.db.query(CommonTaskList). \
            filter_by(**kwargs).all()
        self.db.commit()
        return task_info


    def query_with_update(self, **kwargs):
        """
        查询并更新，返回结果列表（改）
        
        args:
            filters: {
                topic_type:
                status:
                periods:
            }
            updates: {
                periods:
            }

        returns:
            common_task_list: []
        """
        try:
            result = self.db.query(CommonTaskList).with_for_update(read=False) \
                .filter_by(topic_type=kwargs['topic_type'],
                           status=kwargs['status'],
                           period=kwargs['period'],
                           ).all()
        except:
            self.db.rollback()
        self.db.query(CommonTaskList)\
            .filter_by(topic_type=kwargs['topic_type'],
                       status=kwargs['status'],
                       period=kwargs['period'])\
            .update({'period': kwargs['to_period']})
        self.db.commit()
        return result

    def update(self, **kwargs):
        result = []
        try:
            result = self.db.query(CommonTaskList).with_for_update(read=False) \
                .filter_by(**kwargs['filters']).all()
        except:
            self.db.rollback()
        for r in result:
            new_error_msg = kwargs['updates'].get('error_msg', '')
            if r.error_msg and new_error_msg:
                kwargs['updates']['error_msg'] = r.error_msg + '|' + new_error_msg
            self.db.query(CommonTaskList) \
                .filter_by(id=r.id) \
                .update(kwargs['updates'])
        self.db.commit()
        return True
        # self.db.query(CommonTaskList) \
        #     .filter_by(**kwargs['filters']) \
        #     .update(kwargs['updates'])
        # self.db.commit()
        # return True

    def update_failed(self, **kwargs):
        try:
            result = self.db.query(CommonTaskList).with_for_update(read=False) \
                .filter_by(task_id=kwargs['task_id']).first()
        except:
            self.db.rollback()
        error_msg = result.error_msg + '|' + kwargs['error_msg']
        self.db.query(CommonTaskList) \
            .filter_by(id=result.id) \
            .update({'status': kwargs['status'],
                     'error_msg': error_msg})
        self.db.commit()
        return True

    def delete(self, **kwargs):
        self.db.query(CommonTaskList).filter_by(**kwargs).delete()
        self.db.commit()
        return True