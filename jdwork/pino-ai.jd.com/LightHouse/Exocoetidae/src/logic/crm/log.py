
#!/usr/bin/env python3
# Time: 2017/12/10 16:25
from db.crm.models import CRMLogTable
from src.logic import BaseLogic

__author__ = 'hejingjian@jd.com'


import climate
logging = climate.get_logger(__name__)

class CRMLogManager(BaseLogic):
    def __init__(self, db_session):
        super(CRMLogManager, self).__init__(db_session)


    def add_crm_log(self, **kwargs):
        """
            添加日志

        :param kwargs: dict， 日志表字段内容
        :return: bool
        """
        try:
            new_log = CRMLogTable()
            [setattr(new_log, key, value) for key, value in kwargs.items() if hasattr(new_log, key)]
            self.db.add(new_log)
            self.db.commit()
            return new_log
        except Exception as e:
            logging.info('mysql error:', e)
            self.db.commit()
            return False

    def update_crm_log(self, **kwargs):
        """
            通过id更新日志

        :param kwargs: dict， 日志表字段内容
        :return: bool
        """
        self.db.query(CRMLogTable) \
            .filter_by(id=kwargs['id']) \
            .update(kwargs)
        self.db.commit()

    def query_one(self, **kwargs):
        log_info = self.db.query(CRMLogTable). \
            filter_by(**kwargs).first()
        self.db.commit()
        return log_info

    def query_all(self, **kwargs):
        log_info = self.db.query(CRMLogTable). \
            filter_by(**kwargs).all()
        self.db.commit()
        return log_info

    def query_train_task_log(self, **kwargs):
        log_info = self.db.query(CRMLogTable).\
            filter(CRMLogTable.model_id == kwargs['model_id'],
                   CRMLogTable.train_id == kwargs['train_id'],
                   CRMLogTable.role.in_(kwargs['role']),
                   CRMLogTable.batches == kwargs['batches'],).all()
        self.db.commit()
        return log_info

    def update(self, **kwargs):
        self.db.query(CRMLogTable) \
            .filter_by(kwargs['filters']) \
            .update(kwargs['updates'])
        self.db.commit()
        return True

    def delete(self, **kwargs):
        self.db.query(CRMLogTable).filter_by(**kwargs).delete()
        self.db.commit()
        return True

    def get_task_last_log(self, **kwargs):
        log_info = self.db.query(CRMLogTable)\
            .filter_by(**kwargs)\
            .order_by(CRMLogTable.cur_time.desc())\
            .first()
        self.db.commit()
        return log_info

    def get_task_collect_log(self, **kwargs):
        log_info = self.db.query(CRMLogTable)\
            .filter(CRMLogTable.model_id == kwargs['model_id'])\
            .filter(CRMLogTable.is_active == kwargs['is_active'])\
            .filter(CRMLogTable.batches == kwargs['batches'])\
            .filter(CRMLogTable.role.in_(kwargs['role']))\
            .all()
        self.db.commit()
        return log_info
