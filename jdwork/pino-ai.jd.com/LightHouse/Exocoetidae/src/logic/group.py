#!/usr/bin/env python3
# Time: 2017/11/23 11:27

__author__ = 'wanggangshan@jd.com'

from src.logic.base import BaseLogic
from db.models import (
    GroupInfo,
    UserBelongGroup,
    AlgoAuth,
    GroupQuota
)


class GroupManager(BaseLogic):
    """
        用户组管理类
    """
    def __init__(self, db_session):
        super(GroupManager, self).__init__(db_session)

    def require_group_dict(self, **kwargs):
        group_dict = {
            'group_name': kwargs.get('group_name', ''),
            'owner_uid': kwargs.get('owner_uid', 0),
            'owner_name': kwargs.get('owner_name', ''),
            'email': kwargs.get('email', ''),
            'is_delete': kwargs.get('is_delete', 0),
        }
        if 'id' in kwargs:
            group_dict.update({'id': kwargs.get('id')})
            self.db.commit()
        return group_dict

    def require_belong_dict(self, **kwargs):
        belong_dict = {
            'uid': kwargs.get('user_id', 0),
            'gid': kwargs.get('group_id', 0),
            'is_delete': kwargs.get('is_delete', 0),
            'created_uid': kwargs.get('created_uid', 0),
            'updated_uid': kwargs.get('updated_uid', 0),
        }
        return belong_dict

    def get_user_belong_gids(self, user_id, is_delete=0):
        """
            获取用户所属的组id列表  
            
        :param uid: int， 用户id 
        :param is_delete: int， 过滤已删除的记录
        :return: list, e.g: [gid, gid]
        """
        orm_gids = self.db.query(UserBelongGroup.gid)\
            .filter_by(uid=user_id, is_delete=is_delete)\
            .all()
        gids = []
        if orm_gids:
            gids = [gid[0] for gid in orm_gids]
        self.db.commit()
        return gids

    def get_group_by_name(self, group_name):
        group_info = self.db.query(GroupInfo) \
            .filter_by(group_name=group_name) \
            .filter_by(is_delete=0) \
            .first()
        self.db.commit()
        return group_info

    def get_group_by_id(self, group_id, is_delete=0):
        group_info = self.db.query(GroupInfo) \
            .filter_by(id=group_id,
                       is_delete=is_delete) \
            .first()
        self.db.commit()
        return group_info

    def get_group_record(self, group_id):
        group_info = self.db.query(GroupInfo) \
            .filter_by(id=group_id) \
            .first()
        self.db.commit()
        return group_info

    def get_record_by_name(self, group_name):
        group_info = self.db.query(GroupInfo) \
            .filter_by(group_name=group_name) \
            .first()
        return group_info

    def get_group_infos_by_gids(self, group_ids, is_delete=0):
        """
            通过组id列表获取组信息列表
            
        :param gids: list， 组id列表
        :param is_delete: int， 过滤已删除的记录
        :return: list, e.g: [object, object]
        """
        group_infos = self.db.query(GroupInfo)\
            .filter(GroupInfo.id.in_(group_ids))\
            .filter_by(is_delete=is_delete)\
            .all()
        self.db.commit()
        return group_infos

    def get_group_infos_by_uid(self, user_id, is_delete=0):
        """
            通过用户id获取用户所有所属组的信息列表
            
        :param uid: int, 用户id
        :param is_delete: int, 是否过滤已删除数据
        :return: list, 所属组信息列表， e.g: [object, object]
        """
        gids = self.get_user_belong_gids(user_id, is_delete)
        if not gids:
            return []
        group_infos = self.get_group_infos_by_gids(gids, is_delete)
        return group_infos

    def create_group(self, **kwargs):
        """
            添加一个组
        
        :param kwargs: dict， 组信息表字段内容
        :return: bool
        """
        try:
            group_info = GroupInfo(**kwargs)
            self.db.add(group_info)
            self.db.commit()
            return True
        except:
            self.db.commit()
            return False

    def add_user_belong_group(self, **kwargs):
        """
            添加用户属于某个组
        
        :param kwargs: dict，关系相关信息， e.g: {'uid':xxx, 'gid': yyy}
        :return:bool 
        """
        try:
            info = UserBelongGroup(**kwargs)
            self.db.add(info)
            self.db.commit()
            return True
        except:
            self.db.commit()
            return False


    def delete_group(self, op_uid, gid=None, group_name=''):
        """
            删除一个组(连带删除组属用户关系和组算法权限)
        
        :param op_uid: int, 操作人的uid
        :param gid: int, 组id
        :return: bool
        """
        try:
            if not gid and group_name:
                info = self.db.query(GroupInfo).filter_by(group_name=group_name).first()
                gid=info.id
            self.db.query(GroupInfo).filter_by(id=gid).update({'is_delete': 1})
            self.db.query(UserBelongGroup).filter_by(gid=gid)\
                .update({'is_delete': 1, 'updated_uid': op_uid})
            self.db.query(AlgoAuth).filter_by(gid=gid).delete()
            self.db.query(GroupQuota).filter_by(gid=gid).delete()
            self.db.commit()
            return True
        except:
            self.db.rollback()
            self.db.commit()
            return False

    def delete_user_belong_group(self, op_uid, uid, gid):
        """
            删除用户所属组关系
        
        :param op_uid: int, 操作人的uid
        :param uid: int, 用户id
        :param gid: int, 组id
        :return: bool
        """
        try:
            self.db.query(UserBelongGroup)\
                .filter_by(uid=uid, gid=gid)\
                .update({'is_delete': 1, 'updated_uid': op_uid})
            self.db.commit()
            return True
        except:
            self.db.commit()
            return False

    def get_belong_record(self, uid, gid):
        info = self.db.query(UserBelongGroup) \
            .filter_by(uid=uid, gid=gid) \
            .first()
        self.db.commit()
        return info

    def is_user_group(self, uid, gid):
        """
            判断用户是否属于某个组
        
        :param uid: int, 用户id
        :param gid: int, 组id
        :return: bool
        """
        info = self.db.query(UserBelongGroup)\
                .filter_by(uid=uid, gid=gid, is_delete=0)\
                .first()
        self.db.commit()
        return True if info else False

    def update_group_info(self, gid, **kwargs):
        self.db.query(GroupInfo)\
            .filter_by(id=gid)\
            .update(kwargs)
        self.db.commit()
        return True

    def update_belong(self, user_id, group_id, **kwargs):
        self.db.query(UserBelongGroup)\
            .filter_by(uid=user_id,gid=group_id)\
            .update(kwargs)
        self.db.commit()
        return True

    def create_or_update_group(self, **kwargs):
        group_dict = self.require_group_dict(**kwargs)
        group_id = group_dict.get('id')
        group_name = group_dict.get('group_name')
        update_flag = False
        has_exist = False
        if group_id:
            if self.get_group_record(group_id):
                has_exist = True
        if group_name:
            group = self.get_group_record(group_name)
            if group:
                if group.id != group_id:
                    return False
                else:
                    has_exist = True
        if has_exist:
            self.update_group_info(group_id, **group_dict)
            update_flag = True
        if not update_flag:
            self.create_group(**group_dict)
        group = self.get_group_by_id(group_id)
        return group

    def create_or_update_belong(self, **kwargs):
        belong_dict = self.require_belong_dict(**kwargs)
        user_id = belong_dict.get('uid')
        group_id = belong_dict.get('gid')
        if self.get_belong_record(user_id, group_id):
            self.update_belong(user_id, group_id, **belong_dict)
        else:
            self.add_user_belong_group(**belong_dict)
        belong = self.get_belong_record(user_id, group_id)
        return belong
