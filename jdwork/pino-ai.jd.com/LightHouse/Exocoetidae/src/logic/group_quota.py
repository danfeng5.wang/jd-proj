#!/usr/bin/env python3
# Time: 2017/11/29 17:27

__author__ = 'wanggangshan@jd.com'

from src.logic.base import BaseLogic
from db.models import GroupQuota, GroupInfo


class GroupQuotaManager(BaseLogic):
    """
        用户组-资源管理类
    """
    def __init__(self, db_session):
        super(GroupQuotaManager, self).__init__(db_session)

    def trans_quota(self, kwargs):
        try:
            quota_info = {
                'cpus': float(kwargs.get('cpus')),
                'gpus': float(kwargs.get('gpus')),
                'mems': float(kwargs.get('mems')),
                'disk': float(kwargs.get('disk'))
            }
        except Exception as e:
            return {}
        return quota_info

    def require_qupta_dict(self, **kwargs):
        quota_trans = self.trans_quota(kwargs)
        quota_dict = {
            'group_id': kwargs.get('group_id', 0),
            'over_use_rate': kwargs.get('over_use_rate', 1.0)
        }
        quota_dict.update(quota_trans)
        return quota_dict

    def get_all(self):

        quotas = self.db.query(GroupQuota).all()
        return quotas

    def get_quota_by_gid(self, gid):
        quota = self.db.query(GroupQuota)\
            .filter_by(group_id=gid)\
            .first()
        return quota

    def add_group(self, gid,  **kwargs):
        kwargs.update({'group_id': gid})
        quota = GroupQuota(**kwargs)
        self.db.add(quota)
        self.db.commit()
        return quota

    def update_gid_quota(self, gid, **kwargs):

        self.db.query(GroupQuota) \
            .filter_by(group_id=gid) \
            .update(kwargs)
        self.db.commit()
        return True

    def delete_gid_quota(self, gid):
        self.db.query(GroupQuota) \
            .filter_by(group_id=gid) \
            .delete()
        self.db.commit()
        return

    def create_or_update_quota(self, **kwargs):
        quota_dict = self.require_qupta_dict(**kwargs)
        group_id = quota_dict.get('group_id')
        if self.get_quota_by_gid(group_id):
            self.update_gid_quota(group_id, **quota_dict)
        else:
            self.add_group(group_id, **quota_dict)
        quota = self.get_quota_by_gid(group_id)
        return quota
