from db.models import MlTaskHistory, CommonTaskList
from src.logic.base import BaseLogic

import climate
logging = climate.get_logger(__name__)

class TaskManager(BaseLogic):
    def __init__(self, db_session):
        super(TaskManager, self).__init__(db_session)

    def get_task_list_all(self):
        """
            获取任务列表

        :param None
        :return: list
        """
        task_list = self.db.query(MlTaskHistory).all()
        self.db.commit()
        return task_list

    def get_task_list_by_uid(self, uid):
        """
            通过uid获取用户任务列表

        :param uid: int， 用户唯一标识
        :return: list
        """
        task_list = self.db.query(MlTaskHistory).filter(MlTaskHistory.owner_uid == uid,
                                                        MlTaskHistory.is_clean == 0,
                                                        MlTaskHistory.is_release == 0,
                                                        ).order_by(MlTaskHistory.created_at.desc()).all()
        self.db.commit()
        return task_list

    def get_task_list_by_gid(self, gid):
        """
            通过gid获取任务列表

        :param gid: int， 用户组唯一标识
        :return: list
        """
        task_list = self.db.query(MlTaskHistory).filter(MlTaskHistory.gid == gid,
                                                        MlTaskHistory.is_clean == 0,
                                                        MlTaskHistory.is_release == 0,
                                                        ).order_by(MlTaskHistory.created_at.desc()).all()
        self.db.commit()
        return task_list

    def get_task_list_by_gids(self, gids):
        """
            通过gids获取任务列表

        :param gids: list， 用户组集合
        :return: list
        """
        task_list = self.db.query(MlTaskHistory).filter(MlTaskHistory.gid.in_(gids)).all()
        self.db.commit()
        return task_list

    def update_task_info(self, **kwargs):
        """
            通过task_id更新用户任务列表

        :param kwargs: dict， 任务表字段内容
        :return: bool
        """

        self.db.query(MlTaskHistory)\
            .filter_by(task_id=kwargs['task_id'])\
            .update(kwargs)
        self.db.commit()

    def add_task_info(self, **kwargs):
        """
            添加一个任务

        :param kwargs: dict， 任务表字段内容
        :return: bool
        """
        try:
            task_info = MlTaskHistory(**kwargs)
            self.db.add(task_info)
            self.db.commit()
            return True
        except Exception as e:
            logging.info('mysql error:', e)
            self.db.commit()
            return False

    def get_task_id(self, task_id):
        """
            通过task_id获取用户任务列表

        :param task_id: int， 任务唯一标识
        :return: list
        """
        task_info = self.db.query(MlTaskHistory).\
            filter_by(task_id=task_id).first()
        self.db.commit()
        return task_info

    def delete_task_by_task_id(self, task_id):
        """
            通过task_id删除任务

        :param task_id: int， 任务唯一标识
        :return: list
        """
        self.db.query(MlTaskHistory).filter_by(task_id=task_id) \
            .update({'is_clean': 1, 'status': -2})
        self.db.commit()