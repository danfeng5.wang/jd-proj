#!/usr/bin/env python3
# Time: 2017/11/27 15:28
from utils.config_helper import crm_config

__author__ = 'wanggangshan@jd.com'


from db.models import UserInfo, UserSource
from src.logic.base import BaseLogic

import climate
logging = climate.get_logger(__name__)

class UserManager(BaseLogic):
    """
        用户信息管理类
    """
    def __init__(self, db_session):
        self.crm_name = crm_config.get('user.username')
        super(UserManager, self).__init__(db_session)

    def create_or_update_user_info(self, **kwargs):
        """
        
        :param kwargs: dict-{'id':901, 'mobile': 'wgs@jd.com' ...}
        :return: 
        """
        user_id = kwargs.get('id')
        update_flag = False
        if user_id:
            if self.get_user_info_by_uid(user_id):
                self.update_user_info(user_id, **kwargs)
                update_flag = True
        if not update_flag:
            self.add_user_info(**kwargs)
        user_info = self.get_user_info_by_uid(user_id)
        return user_info

    def create_or_update_user_source(self, **kwargs):
        user_id = kwargs.get('uid')
        update_flag = False
        if user_id:
            if self.get_user_source_by_uid(user_id):
                self.update_user_source(user_id, **kwargs)
                update_flag = True
        if not update_flag:
            self.add_user_source_info(**kwargs)
        user_source = self.get_user_source_by_uid(user_id)
        return user_source

    def update_user_info(self, user_id, **kwargs):
        self.db.query(UserInfo)\
            .filter_by(id=user_id)\
            .update(kwargs)
        self.db.commit()
        return True

    def update_user_source(self, user_id, **kwargs):
        self.db.query(UserSource)\
            .filter_by(uid=user_id)\
            .update(kwargs)
        self.db.commit()
        return True

    def add_user_info(self, **kwargs):
        user_info = UserInfo(**kwargs)
        self.db.add(user_info)
        self.db.commit()
        return user_info

    def add_user_source_info(self, **kwargs):
        user_source = UserSource(**kwargs)
        self.db.add(user_source)
        self.db.commit()
        return user_source

    def get_user_source_by_uid(self, uid):
        """
            获取用户账号来源信息
        :param uid: 用户唯一id
        :return: object
        """
        user_source_info = self.db.query(UserSource).filter_by(uid=uid).first()
        return user_source_info

    def get_source_by_username(self, username, source_type):
        user_source_info = self.db.query(UserSource)\
            .filter_by(username=username,
                       source_type=source_type)\
            .first()
        return user_source_info

    def get_source_by_sourceId(self, sourceId, source_type):
        user_source_info = self.db.query(UserSource)\
            .filter_by(source_type=source_type,
                       source_id=sourceId)\
            .first()
        return user_source_info

    def get_user_info_by_uid(self, uid):
        """
            获取统一账户的基础信息
        :param uid: 用户唯一id
        :return: object
        """
        user_info = self.db.query(UserInfo).filter_by(id=uid).first()
        return user_info

    def get_user_all_info(self, uid):
        """
            获取用户来源信息及用户基础信息
        :param uid: 用户唯一id
        :return: (object,object)
        """
        try:
            all_info = self.db.query(UserInfo, UserSource)\
                .join(UserSource, UserInfo.id == UserSource.uid)\
                .filter(UserSource.uid == uid)\
                .first()
        except:
            return None, None
        if all_info:
            user_info, user_source = all_info
            return user_info, user_source
        else:
            return None, None

    def query_crm_user(self):
        """
            获取crm用户信息
        :return: object
        """
        user_info = self.db.query(UserSource).filter_by(source_type=-1,
                                                        username=self.crm_name).first()
        return user_info