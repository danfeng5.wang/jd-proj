#!/usr/bin/env python3
# Time: 2017/11/27 15:28

__author__ = 'wanggangshan@jd.com'


from src.logic.base import BaseLogic
from db.models import WhiteListUser


class WhiteListManager(BaseLogic):

    def __init__(self, db_session):
        super(WhiteListManager, self).__init__(db_session)

    def get_all_white_users(self):
        infos = self.db.query(WhiteListUser).all()
        return infos

    def get_white_user(self, uid):
        info = self.db.query(WhiteListUser) \
            .filter_by(uid=uid)\
            .first()
        return info

    def add_white_user(self, uid):
        info = WhiteListUser(uid=uid)
        self.db.add(info)
        self.db.commit()
        return info

    def delete_white_user(self, uid):
        info = self.db.query(WhiteListUser)\
            .filter_by(uid=uid)\
            .delete()
        self.db.commit()
        return True
