#!/usr/bin/env python3
# Time: 2017/11/20 15:56

__author__ = 'wanggangshan@jd.com'


from .base import BaseService
from .algo import AlgoService
from .cloud import CloudService
from .group import GroupService
from .group_quota import GroupQuotaService
from .task import TaskService
from .user import UserService
from .white_list import WhiteListService
from .just_as_test import TestService, test

from . import crm
from . import common


