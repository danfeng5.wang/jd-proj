#!/usr/bin/env python3
# Time: 2017/11/20 18:50
import uuid

from libs.flask import make_response, send_file
from src.logic.algo_auth import AlgoAuthManager

__author__ = 'wanggangshan@jd.com'


import PinoCore.services as PinoCoreApi
from src.services.base import BaseService
from src.logic.algo import AlgoManager
from src.logic.algo_history import AlgoHistoryManager
from utils.util import dict_processing, make_zip


class AlgoService(BaseService):
    """ 算法库操作接口
    """
    def __init__(self, db_session):
        super(AlgoService, self).__init__(db_session)
        self.__algo_manager = AlgoManager(db_session)
        self.__algo_auth_manager = AlgoAuthManager(db_session)
        self.__algo_history_manager = AlgoHistoryManager(db_session)

    def __verify_user_algo_auth(self, type='name', **kwargs):
        """ 验证用户算法权限

            验证该算法名(name)是否已存在，和该用户对该算法是否拥有读/下载/写/执行的权限

        :param type: str, e.g: 'name'|'read'|'download'|'write'|'exec'
        :param kwargs: dict, e.g: {'algo_name': xxx, 'username': xxx}
        :return: bool, e.g: True|False
        """

        algo_name = kwargs.get('algo_name')
        username = kwargs.get('username')
        algo_manager = AlgoManager()
        algo_info = algo_manager.get_algo_by_name(algo_name)
        if type == 'name':
            return True if not algo_info else False
        auth_code_info = algo_info.get_auth_info()
        return True if auth_code_info[type] else False

    def get_algo_version(self):
        """ 获取算法库版本信息
        
        :return: str, e.g: '0.1'
        """
        request_info = PinoCoreApi.AlgoLibRequest()
        info = PinoCoreApi.pinoalgo_version_v2(request_info)
        format_info = {
            'status': info.success,
            'result': info.version,
            'message': info.error_message
        }
        response = self.set_response(**format_info)
        return response

    def get_algo_info(self, **kwargs):
        """ 获取算法库信息

        :param kwargs: dict, e.g: {'uid': xxx, 'algo_name': xxx}
        :return: status: True/False
        :return: message: str
        :return: result: Collection information, dict
        """
        algo_verify = self.__verify_user_algo_auth('read', **kwargs)
        if not algo_verify:
            response = self.set_response({'status': False,
                                          'result': {},
                                          'message': 'The user does not have the permission of the algorithm'})
            return response
        request_info = PinoCoreApi.AlgoLibRequest(
            author=kwargs['uid'],
            algo_name=kwargs['algo_name'],
        )
        request_obj = PinoCoreApi.query_algo_info_v2(request_info)
        format_info = {
            'status': request_obj.success,
            'result': request_obj.info,
            'message': request_obj.error_message
        }
        response = self.set_response(**format_info)
        return response

    def delete_algo(self, **kwargs):
        """ 删除算法

        :param kwargs: dict, e.g: {'uid': xxx, 'algo_name': xxx}
        :return: status: True/False
        :return: message: str
        :return: result: {}
        """
        algo_verify = self.__verify_user_algo_auth('write', **kwargs)
        if not algo_verify:
            response = self.set_response({'status': False,
                                          'result': {},
                                          'message': 'The user does not have the permission of the algorithm'})
            return response
        request_info = PinoCoreApi.AlgoLibRequest(
            author=kwargs['uid'],
            algo_name=kwargs['algo_name'],
        )
        request_obj = PinoCoreApi.delete_algo_v2(request_info)
        format_info = {
            'status': request_obj.success,
            'result': request_obj.info,
            'message': request_obj.error_message
        }
        response = self.set_response(**format_info)
        if not response.status:
            return response
        self.__algo_manager.add_algo_history(**{'job_id': uuid.uuid1().hex,
                                                'author_uid': kwargs['uid'],
                                                'algo_name': kwargs['algo_name'],
                                                'action_type': '4',
                                                'status': '1',
                                                'directory': kwargs['local_dir'],
                                                'description': response.result['description'],
                                                'created_uid': kwargs['uid'],
                                                })
        self.__algo_manager.delete_algo(kwargs['uid'], kwargs['algo_name'])
        return response


    def upload_algo(self, **kwargs):
        """ 删除算法

        :param kwargs: dict, e.g: {'uid': xxx, 'algo_name': xxx, 'local_dir': xxx}
        :return: status: True/False
        :return: message: str
        :return: result: Collection information, dict
        """
        get_algo = self.__algo_manager.get_by_uid_and_name(kwargs['uid'], kwargs['algo_name'])
        if get_algo:
            format_info = {
                'status': False,
                'result': {},
                'message': '该算法已存在，是否需要重新上传覆盖？'
            }
            response = self.set_response(**format_info)
            return response
        request_info = PinoCoreApi.AlgoLibRequest(
            author=kwargs['uid'],
            algo_name=kwargs['algo_name'],
            local_dir=kwargs['local_dir'],
        )
        request_obj = PinoCoreApi.upload_algo_v2(request_info)
        message = '' if request_obj.success else request_obj.error_message

        format_info = {
            'status': request_obj.success,
            'result': '',
            'message': message
        }
        response = self.set_response(**format_info)
        if not response.status:
            return response
        self.__algo_history_manager.add_algo_job_history(**{'job_id': uuid.uuid1().hex,
                                                'author_uid': kwargs['uid'],
                                                'algo_name': kwargs['algo_name'],
                                                'action_type': '2',
                                                'status': '1',
                                                'directory': kwargs['local_dir'],
                                                'description': '',
                                                'created_uid': kwargs['uid'],
                                                })
        self.__algo_manager.add_algo(**{'uid': kwargs['uid'],
                                        'algo_name': kwargs['algo_name'],
                                        'description': response.result['description'],
                                        })
        return response

    def get_algo_list(self, user_id, all=False):

        if all:
            # 所有的 判断是否是管理员
            pass
        algo_id_list = self.__algo_auth_manager.get_user_algos_auth(user_id)
        algo_info_list = self.__algo_manager.get_algo_by_ids(algo_id_list)
        need_keys = ['id', 'author_uid', 'author_name', 'algo_name',
                    'description', 'created_at', 'updated_at']
        algo_list = []
        for algo in algo_info_list:
            algo_dict = {key: algo.__dict__[key] for key in need_keys}
            algo_list.append(algo_dict)

        message = '查询成功' if len(algo_list) > 0 else '没有可查看算法实例'
        result = {'data': algo_list, 'total': len(algo_list)}
        response_info = {
            'status': True,
            'result': result,
            'message': message
        }
        response = self.set_response(**response_info)
        return response


    def download_algo(self, **kwargs):
        """ 下载算法

        :param kwargs: dict, e.g: {'uid': xxx, 'algo_name': xxx, 'local_dir': xxx}
        :return: bool: True/False
        """
        algo_verify = self.__verify_user_algo_auth('download', **kwargs)
        if not algo_verify:
            response = self.set_response({'status': False,
                                          'result': {},
                                          'message': 'The user does not have the permission of the algorithm'})
            return response
        request_info = PinoCoreApi.AlgoLibRequest(
            author=kwargs['uid'],
            algo_name=kwargs['algo_name'],
            local_dir=kwargs['local_dir'])
        response = PinoCoreApi.download_algo_v2(request_info)
        format_info = {
            'status': response.success,
            'result': '',
            'message': ''
        }
        if response.success:
            zip_file_name = response.downloaded_dir + '.zip'
            make_zip(response.downloaded_dir, zip_file_name)
            file_name = str(response.downloaded_dir).split('/')[-1] + '.zip'
            file_response = make_response(send_file(response.downloaded_dir+'.zip'))
            file_response.headers["Content-Disposition"] = "attachment; filename=" + file_name + ";"
            format_info['result'] = file_response
            # return file_response
            # return jsonify({'status': 'success', 'result': {"downloaded_dir": response.downloaded_dir}, 'message': ''})
        else:
            format_info['result'] = response.error_message

        response = self.set_response(**format_info)
        return response
