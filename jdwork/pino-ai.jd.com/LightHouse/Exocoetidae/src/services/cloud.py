#!/usr/bin/env python3
# Time: 2017/11/20 18:50

__author__ = 'wanggangshan@jd.com'


import json
import PinoCore.services as PinoCoreApi
from src.services.base import BaseService
from src.logic.task import TaskManager


class CloudService(BaseService):
    """ 
        Pino Cloud模块服务接口
    """
    def __init__(self, db_session):
        super(CloudService, self).__init__(db_session)
        self.__task_manager=TaskManager(db_session)

    def get_cloud_version(self):

        info = PinoCoreApi.service_getVersionInfo()
        format_info = {
            'status': info.success,
            'result': info.version,
            'message': info.errmsg
        }
        response = self.set_response(**format_info)
        return response

    def query_group_running_usage(self, group_id):
        """ （待定）显示全部，已使用，剩余，可超额"""
        pass

    def query_task_running_usage(self, task_id):
        """ （待定）显示全部，已使用，剩余，可超额"""
        pass

    def query_group_quota(self, group_id):
        """ （待定）显示全部，已使用，剩余，可超额"""
        pass

    def query_task_nodes(self, task_id):
        task_info = self.__task_manager.get_task_id(task_id)
        if not task_info:
            format_info = {
                'status': False,
                'result': None,
                'message': '该任务不存在: %s' % task_id
            }
        else:
            nodes = json.loads(task_info.cloud_node_info)
            format_info = {
                'status': True,
                'result': nodes,
                'message': '请求成功'
            }
        response = self.set_response(**format_info)
        return response

    def cloud_nodes(self, user_info, task_id):
        response = PinoCoreApi.service_listTaskNode(user_info['username'], task_id)
        # response = PinoCoreApi.service_listTaskNode_v2(user_info, task_id)
        nodes = [n.__dict__ for n in response.nodes]
        format_info = {
            'status': response.success,
            'result': {'nodes': nodes},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        if not response.status:
            return response
        return response

    def cloud_nodes_by_namespace(self, namespace, task_id):
        response = PinoCoreApi.service_listTaskNode_v2(namespace, task_id)
        # response = PinoCoreApi.service_listTaskNode_v2(user_info, task_id)
        nodes = [n.__dict__ for n in response.nodes]
        format_info = {
            'status': response.success,
            'result': {'nodes': nodes},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        if not response.status:
            return response
        return response