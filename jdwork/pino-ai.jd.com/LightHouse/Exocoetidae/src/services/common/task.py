#!/usr/bin/env python3
# Time: 2017/12/12 21:18
from collections import defaultdict

from db.models import TaskPeriod
from src.logic.common_task import CommonTaskManager
from src.services import TaskService
from src.services.base import BaseService
#from src.services.common.task import CommonTaskService

import climate
logging = climate.get_logger(__name__)
__author__ = 'hejingjian@jd.com'


class CommonTaskService(BaseService):
    def __init__(self, db_session):
        super(CommonTaskService, self).__init__(db_session)
        self.__common_task_manager = CommonTaskManager(db_session)
        self.__task_service = TaskService(db_session)

    # XXX(penghao) Below methods are used in runner.

    def set_enqueue_many(self, **kwargs):
        return self.__common_task_manager.query_with_update(
            filters=dict(
                topic_type=self.topic_type,
                status=0,
                period=0),
            updates=dict(
                period=1))

    def set_submit(self, **kwargs):
        return self.__common_task_manager.query_with_update(
            filters=dict(
                topic_type=self.topic_type,
                task_id=kwargs.get('task_id'),
                updates=dict(
                    period=2)))

    def set_running(self, **kwargs):
        return self.__common_task_manager.query_with_update(
            filters=dict(
                topic_type=self.topic_type,
                task_id=kwargs.get('task_id'),
                updates=dict(
                    period=3)))

    def set_success(self, **kwargs):
        return self.__common_task_manger.query_with_update(
            filters=dict(
                topic_type=self.topic_type,
                task_id=kwargs.get('task_id'),
                updates=dict(
                    status=1,
                    period=4)))

    def set_failed(self, **kwargs):
        return self.__common_task_manager.query_with_update(
            filters=dict(
                topic_type=self.topic_type,
                task_id=kwargs.get('task_id'),
                updates=dict(
                    status=-1,
                    period=4)))

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        kwargs = {'topic_type': 3,
                  'period': TaskPeriod.Waiting,
                  'to_period': 1,
                  'try_limit': try_limit,
                  'limit': feed_num,
                  }
        result = self.__common_task_manager.update_by_topic_period(**kwargs)
        return result

    def recover_queued_to_waitting(self, task_id):
        kwargs = {'filters': {'task_id': task_id},
                  'updates': {'period': TaskPeriod.Queued},
                  }
        result = self.__common_task_manager.update(**kwargs)
        return result

    def get_by_task_id(self, task_id):
        kwargs = {'task_id': task_id}
        result = self.__common_task_manager.query_one(**kwargs)
        return result

    def update_task_start_status(self, **kwargs):
        status = kwargs['status']
        task_id = kwargs['task_id']
        error_msg = kwargs['error_msg']
        namespace = kwargs['namespace']
        task_info = self.__common_task_manager.query_one(**{'task_id': task_id})
        db_kwargs = defaultdict(list)
        db_kwargs['filters'] = {'task_id': task_id}
        if status:
            db_kwargs['updates'] = {'period': TaskPeriod.Running}
        else:
            # if task_info.try_num > 5:
            #     db_kwargs['updates'] = {'status': -1}
            #     self.__common_task_manager.update(**db_kwargs)
            #     return True
            db_kwargs['updates'] = {'status': -1, 'error_msg': error_msg}
        self.__common_task_manager.update(**db_kwargs)
        if task_info.topic_type == '1':
            response = self.__task_manager.task_start_result(**kwargs)
        return True

    def update_task(self, task_id, **kwargs):
        db_kwargs = defaultdict(list)
        db_kwargs['filters'] = {'task_id': task_id}
        db_kwargs['updates'] = kwargs
        self.__common_task_manager.update(**db_kwargs)
        return True

    def update_task_status(self, **kwargs):
        task_id = kwargs['task_id']
        db_kwargs = defaultdict(list)
        db_kwargs['filters'] = {'task_id': task_id}
        if kwargs['status']:
            db_kwargs['updates'] = {'status': 1,
                                    'period': 4}
        else:
            db_kwargs['updates'] = {'status': -1}
        namespace = kwargs['namespace']
        self.__common_task_manager.update(**db_kwargs)
        return True

    def update_task_failed(self, **kwargs):
        task_id = kwargs['task_id']
        status = kwargs['status']
        error_msg = kwargs['error_msg']
        self.__common_task_manager.update_failed(**{'task_id': task_id,
                                                    'status': status,
                                                    'error_msg': error_msg,
                                                    })
        return True
