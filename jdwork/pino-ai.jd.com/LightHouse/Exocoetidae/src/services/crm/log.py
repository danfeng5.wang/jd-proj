#!/usr/bin/env python3
# Time: 2017/12/10 16:43
import math

from src.logic.crm.log import CRMLogManager
from src.services import BaseService

__author__ = 'hejingjian@jd.com'

import climate
logging = climate.get_logger(__name__)

class CRMLogService(BaseService):
    def __init__(self, db_session):
        super(CRMLogService, self).__init__(db_session)
        self.__crm_log_manager = CRMLogManager(db_session)

    def add_crm_log_info(self, **kwargs):
        """
            添加crm日志

        :param kwargs: dict， 日志信息字段
        :return: bool
        """
        try:
            pop_list = []
            for key, value in kwargs.items():
                if isinstance(value, float) and (math.isnan(value) or math.isinf(value)):
                    kwargs[key] = 0.0
                if value == 'None' or not value:
                    pop_list.append(key)
            for key in pop_list:
                kwargs.pop(key)
            result = self.__crm_log_manager.add_crm_log(**kwargs)
            return self.set_response(**{'status': True,
                                        'result': {'id': result.id},
                                        'message': ''
                                        })
        except Exception as e:
            logging.error('[MysqlError] insert crm_log_table:%s; kwargs:%s', e, kwargs)
            return self.set_response(**{'status': False,
                                        'result': {},
                                        'message': str(e)
                                        })

    def get_task_log(self, **kwargs):
        try:
            if kwargs.get('task_type', None) == 'train':
                kwargs.pop('task_type')
                log_info = self.__crm_log_manager.query_train_task_log(**kwargs)
            elif kwargs.get('task_type', None) == 'test':
                kwargs.pop('task_type')
                log_info = self.__crm_log_manager.query_all(**kwargs)
            return log_info
        except Exception as e:
            return False

    def get_task_last_log(self, **kwargs):
        try:
            log_info = self.__crm_log_manager.get_task_last_log(**kwargs)
            return log_info
        except Exception as e:
            return False

    def get_task_collect_log(self, **kwargs):
        try:
            log_info = self.__crm_log_manager.query_all(**kwargs)
            return log_info
        except Exception as e:
            return False