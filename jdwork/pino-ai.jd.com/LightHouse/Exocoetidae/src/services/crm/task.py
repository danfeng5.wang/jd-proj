import json
import os
import uuid
from collections import defaultdict

import climate

import services as PinoCoreApi
# from common.session_object import user_session_manager, PinoSession, PinoUser
from PinoCore.uniKubePino.pysrc.server import PinoSession, PinoUser
from db.models import TaskPeriod
from src.logic import UserManager
from src.logic import GroupManager
from src.logic.common_task import CommonTaskManager
from src.logic.task import TaskManager
from src.services import CloudService, UserService
from src.services.base import BaseService
from src.services.common.task import CommonTaskService
from utils.config_helper import config, crm_config

logging = climate.get_logger(__name__)


def _create_crm_k8s_group():
    namespace = crm_config.get('user.group_name')
    result = PinoCoreApi.create_namespace(namespace)
    if not result:
        return result
    name = PinoCoreApi.get_namespace(namespace)
    return name


def _create_crm_k8s_quota():
    namespace = crm_config.get('user.group_name')
    resources = crm_config.get('user.resources')
    result = PinoCoreApi.create_quota(namespace, **resources)
    return result


class CRMTaskService(BaseService):
    def __init__(self, db_session):
        super(CRMTaskService, self).__init__(db_session)
        self.__common_task_manager = CommonTaskManager(db_session)
        self.__user_service = UserService(db_session)
        self.__common_task_service = CommonTaskService(db_session)
        self.__user_manager = UserManager(db_session)
        self.__group_manager = GroupManager(db_session)
        self.__task_manager = TaskManager(db_session)
        self.user_id = self.__user_manager.query_crm_user().uid
        self.user_info = self.__user_service.get_user_above_info(self.user_id).result

    def update_task(self, task_id, kwargs):
        self.__common_task_service.update_task(task_id, kwargs)
        return True

    def update_task_status(self, **kwargs):
        self.__common_task_service.update_task_status(**kwargs)
        return True

    def update_task_start_status(self, **kwargs):
        self.__common_task_service.update_task_start_status(**kwargs)
        return True

    def update_task_error(self, **kwargs):
        self.__common_task_service.update_task_error(**kwargs)
        return True

    def feed_many_waiting_tasks(self, try_limit, feed_num):
        result = self.__common_task_service.feed_many_waiting_tasks(try_limit, feed_num)
        return result

    def recover_queued_to_waitting(self, task_id):
        result = self.__common_task_service.recover_queued_to_waitting(task_id)
        return result

    def get_by_task_id(self, task_id):
        result = self.__common_task_service.get_by_task_id(task_id)
        return result

    def add_task_info(self, **kwargs):
        """
            添加一个任务

        :param kwargs: dict， 任务信息字段
        :return: bool
        """
        try:
            jsondata = kwargs
            task_info = defaultdict(list)
            task_info['topic_type'] = 3
            task_info['task_type'] = jsondata.get('task', 'N/A')
            # task_info['algo_author_uid'] = 
            # task_info['algo_name'] = 
            # task_info['engine_type'] = 
            # task_info['status'] = 0
            task_info['period'] = TaskPeriod.Waiting
            task_info['owner_uid'] = self.user_info['user_id']
            task_info['owner_name'] = self.user_info['username']
            task_info['gid'] = self.user_info['groups'][0]['group_id']
            task_info['group_name'] = self.user_info['groups'][0]['group_name']
            task_info['predict_id'] = jsondata.get('predict_id', -1)
            task_info['test_id'] = jsondata.get('task_id', -1)
            task_info['model_id'] = jsondata.get('model_id', -1)
            task_info['train_id'] = jsondata.get('train_id', -1)

            task_id = uuid.uuid1().hex
            task_info['task_id'] = task_id

            mfs_path = crm_config.get('mfs.base_path')
            file_path = os.path.join(mfs_path, str(task_info['model_id']) + '_' + str(task_info['train_id']))
            task_info['mfs_path'] = file_path

            result = self.__common_task_manager.add_common_task(**task_info)
            if task_info['task_type'] == 'train':
                file_name = file_path + '/train_request.json'
            elif task_info['task_type'] == 'test':
                file_name = file_path + '/test_%s_request.json' % (task_info['test_id'], )
            else:
                file_name = file_path + '/predict_%s_request.json' % (task_info['predict_id'], )
            file_backup_path = file_path + '/.request_backup'
            logging.info('mfs_file_name:' + file_name)
            if os.path.isdir(file_path):
                pass
            else:
                os.mkdir(file_path)
                # if there is file_path, there must be file_backup_path
                os.mkdir(file_backup_path)
                cmd = 'chmod -R 777 %s' % file_path
                os.system(cmd)
            with open(file_name, 'w') as json_file:
                json.dump(jsondata, json_file, indent=4)

            return self.set_response(**{'status': True,
                                        'result': {'id': result.id},
                                        'message': ''
                                        })
        except Exception as e:
            return self.set_response(**{'status': False,
                                        'result': {},
                                        'message': e
                                        })

    def task_start(self, **kwargs):
        logging.info('train kwargs: ' + str(kwargs))
        user_session = PinoSession(uuid.uuid1().hex)
        if kwargs.get('user_id', None):
            user_info = self.__user_service.get_user_above_info(kwargs.get('user_id', None)).result
        else:
            user_info = self.user_info
        user = PinoUser(**user_info)
        user_session.update(user=user)

        # project_type 由algo值决定 待定。
        algo = kwargs.get('algo', None)
        dir = kwargs.get('dir', None)
        config = kwargs.get('config', None)
        other_args_dict = kwargs.get('other_args_dict', None)

        request_info = PinoCoreApi.StartTrainRequest(
            algoType=algo,
            uniName=kwargs.get('task_id', None),
            project=dir,
            remove=False,
            extra={
                'configure_path': config,
                'keywords': other_args_dict,
            },
        )

        response = PinoCoreApi.service_startTrainNew(user_session, request_info)
        # if response.success:
        #     self.__common_task_manager.update(**{'filters': {'task_id': kwargs.get('task_id', None)},
        #                                          'updates': {'period': TaskPeriod.Submitted}})
        format_info = {
            'status': response.success,
            'result': response.uniName,
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response

    def task_check(self, user_info, task_id):
        """
            校验任务是否有效

        :param owner_uid: int， 用户id
        :param task_id: int， 任务id
        :return: dict
        """
        task_info = self.__task_manager.get_task_id(task_id)
        if not task_info:
            return {'status': 'fail', 'result': {}, 'message': 'task with uniName of ' + task_id + ' not found.'}

        if user_info['user_id'] != task_info.owner_uid and not user_info['is_super']:
            return {'status': 'fail', 'result': {}, 'message': 'task owner is not you.'}
        return {'status': 'success', 'result': {}, 'message': ''}

    def task_stop(self, task_id):
        """
            停止任务

        :param user_info: dict， 用户信息
        :param task_id: int， 任务id
        :return: dict
        """
        check_info = self.task_check(self.user_info, task_id)
        if not check_info['status']:
            return check_info
        if self.user_info['groups']:
            namespace = self.user_info['groups'][0].get('group_name', None)
        else:
            return self.set_response(**{
                                        'status': False,
                                        'result': '',
                                        'message': 'user no groups'
                                    })
        response = PinoCoreApi.service_deleteTask_v2(namespace, task_id)
        format_info = {
            'status': response.success,
            'result': {'task_id': task_id},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        # self.__common_task_manager.delete(**{'task_id': task_id})
        return response

    def check_quota_usable(self, uid, gid, algo_name):
        # cpus单位（个）, mems单位G
        group = self.__group_manager.get_group_infos_by_gids([gid])[0]
        code, k8s_quota = PinoCoreApi.get_quota(group.group_name)
        if not code:
            return False
        all_quota = k8s_quota.get('all')
        used_quota = k8s_quota.get('used')
        usable_cpus = all_quota.get('cpus') - used_quota.get('cpus')
        usable_mems = all_quota.get('mems') - used_quota.get('mems')

        res_info = PinoCoreApi.query_algo_resource_v2\
            (PinoCoreApi.AlgoLibRequest(author=uid, algo_name=algo_name))
        if not res_info.success:
            return False
        res_list = res_info.result.get('resource').get('roles')

        req_cpus, req_mems = (0.0, 0.0)
        for res_role in res_list:
            req_cpus += res_role.get('resources').get('requests').get('cpu')
            req_mems += res_role.get('resources').get('requests').get('memory')

        if usable_cpus > req_cpus and usable_mems > req_mems:
            return True
        else:
            return False

