#!/usr/bin/env python3
# Time: 2017/11/29 11:13

__author__ = 'wanggangshan@jd.com'


import json
import copy
from src.services.base import BaseService
from src.logic.user import UserManager
from src.logic.group import GroupManager
from src.logic.group_quota import GroupQuotaManager
import PinoCore.services as PinoCoreApi
import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)


class GroupService(BaseService):
    def __init__(self, db_session):
        super(GroupService, self).__init__(db_session)
        self.__user_manager = UserManager(db_session)
        self.__group_manager = GroupManager(db_session)
        self.__group_quota = GroupQuotaManager(db_session)


    def trans_quota(self, kwargs):
        try:
            quota_info = {
                'cpus': float(kwargs.get('cpus')),
                'gpus': float(kwargs.get('gpus')),
                'mems': float(kwargs.get('mems')),
                'disk': float(kwargs.get('disk'))
            }
        except Exception as e:
            logging.error(
                'Create K8s quota Failed Because Of Data Type: %s' % e)
            return {}
        return quota_info


    def delete_group(self, op_uid, gid=None, group_name=None):

        if gid and (not group_name):
            info = self.__group_manager.get_group_infos_by_gids([gid])[0]
            group_name = info.group_name
        self.__group_manager.delete_group(op_uid, gid=gid, group_name=None)
        PinoCoreApi.delete_namespace(group_name)
        PinoCoreApi.delete_quota(group_name)

        return True


    def create_group(self, **kwargs):
        group_name = kwargs.get('group_name')
        if not group_name:
            return False, 0
        if self.__group_manager.get_group_by_name(group_name):
            return False, 1
        group_info = {
            'group_name': group_name,
            'owner_uid': kwargs.get('owner_uid', 0),
            'owner_name': kwargs.get('group_name', ''),
            'email': kwargs.get('email', ''),
        }

        quota_info = {}
        k8s_quota_req = {}
        quota = kwargs.get('quota', {})
        if quota:
            quota_info = self.trans_quota(kwargs)
            if not quota_info:
                return False

        k8s_group = PinoCoreApi.create_namespace(group_name)
        if not k8s_group:
            logging.error('Create K8s namespace Failed: %s' % kwargs)
            return False, 3
        group = self.__group_manager.create_group(**group_info)
        if quota:
            k8s_group_quota = PinoCoreApi.create_quota(group_name, **k8s_quota_req)
            if not k8s_group_quota:
                logging.error('Create K8s quota Failed: %s' % kwargs)
                PinoCoreApi.delete_namespace(group_name)
            self.__group_quota.add_group(group.id, **quota_info)

        return group


    def get_user_groups(self, user_id):
        group_infos = self.__group_manager.get_group_infos_by_uid(user_id)
        group_list = [
            {
                'group_id': group.id,
                'group_name': group.group_name,
                'owner_uid': group.owner_uid,
                'owner_name': group.owner_name
            } for group in group_infos
        ]
        if len(group_list) > 0:
            response_info = {
                'status': True,
                'result': group_list,
                'message': ''
            }
        else:
            response_info = {
                'status': False,
                'result': [],
                'message': '用户当前不属于任何组，请先添加组'
            }
        response = self.set_response(**response_info)
        return response

    def get_user_all_group_infos(self, user_id):
        group_infos = self.__group_manager.get_group_infos_by_uid(user_id)
        if not group_infos:
            response_info = {
                'status': False,
                'result': None,
                'message': '用户组不属于任何组'
            }
        else:
            group_list = []
            for group_info in group_infos:
                quota_info = self.__group_quota.get_quota_by_gid(group_info.id)
                quota_dict = {
                    'cpus': quota_info.cpus,
                    'gpus': quota_info.gpus,
                    'mems': quota_info.mems,
                    'disk': quota_info.cpus,
                    'over_use_rate': quota_info.over_use_rate,
                }
                group_dict = {
                    'groud_id': group_info.id,
                    'group_name': group_info.group_name,
                    'owner_uid': group_info.owner_uid,
                    'owner_name': group_info.owner_name,
                    'quota': quota_dict
                }
                group_list.append(group_dict)
            response_info = {
                'status': True,
                'result': group_list,
                'message': ''
            }
        response = self.set_response(**response_info)
        return response



    def get_group_info_by_gid(self, gid):
        group_info = self.__group_manager.get_group_infos_by_gids([gid])[0]
        if not group_info:
            response_info = {
                'status': False,
                'result': None,
                'message': '用户组不存在: %s' % gid
            }
        else:
            quota_info = self.__group_quota.get_quota_by_gid(gid)
            quota_dict = {
                'cpus': quota_info.cpus,
                'gpus': quota_info.gpus,
                'mems': quota_info.mems,
                'disk': quota_info.cpus,
                'over_use_rate': quota_info.over_use_rate,
            }
            group_dict = {
                'groud_id': group_info.id,
                'group_name': group_info.group_name,
                'owner_uid': group_info.owner_uid,
                'owner_name': group_info.owner_name,
                'quota': quota_dict
            }
            response_info = {
                'status': True,
                'result': group_dict,
                'message': ''
            }
        response = self.set_response(**response_info)
        return response


    def create_or_update_group(self, **kwargs):
        group_dict = {
            'id': kwargs.get('group_id', ''),
            'group_name': kwargs.get('group_name', ''),
            'owner_uid': kwargs.get('owner_uid', 0),
            'owner_name': kwargs.get('owner_name', ''),
            'email': kwargs.get('email', ''),
        }
        group_info = self.__group_manager.create_or_update_group(**group_dict)
        return group_info

    def create_or_update_belong(self, **kwargs):
        belong_dict = {
            'user_id': kwargs.get('user_id', 0),
            'group_id': kwargs.get('group_id', 0),
            'is_delete': kwargs.get('is_delete', 0),
            'created_uid': kwargs.get('created_uid', 0),
            'updated_uid': kwargs.get('updated_uid', 0),
        }
        belong = self.__group_manager.create_or_update_belong(**belong_dict)
        return belong
