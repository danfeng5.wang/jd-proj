#!/usr/bin/env python3
# Time: 2017/11/29 11:13

__author__ = 'wanggangshan@jd.com'


import climate
climate.enable_default_logging()
logging = climate.get_logger(__name__)
import copy
from src.services.base import BaseService
from src.logic.user import UserManager
from src.logic.group import GroupManager
from src.logic.group_quota import GroupQuotaManager
import PinoCore.services as PinoCoreApi


class GroupQuotaService(BaseService):
    def __init__(self, db_session):
        super(GroupQuotaService, self).__init__(db_session)
        self.__group_quota = GroupQuotaManager(db_session)
        self.__group_manager = GroupManager(db_session)
        self.__user_manager = UserManager(db_session)

    def require_qupta_dict(self, **kwargs):
        quota_trans = self.trans_quota(kwargs)
        quota_dict = {
            'group_id': kwargs.get('group_id', 0),
            'over_use_rate': kwargs.get('over_use_rate', 1.0)
        }
        quota_dict.update(quota_trans)
        return quota_dict

    def trans_quota(self, kwargs):
        try:
            quota_info = {
                'cpus': float(kwargs.get('cpus')),
                'gpus': float(kwargs.get('gpus')),
                'mems': float(kwargs.get('mems')),
                'disk': float(kwargs.get('disk'))
            }
        except Exception as e:
            logging.error(
                'Create K8s quota Failed Because Of Data Type: %s' % e)
            return {}
        return quota_info

    def get_group_quota(self, gid):
        group = self.__group_manager.get_user_belong_gids([gid])[0]
        if not group:
            return False
        code, k8s_quota_dict = PinoCoreApi.get_quota(group.group_name)
        return k8s_quota_dict

    def update_group_quota(self, gid, **kwargs):
        group = self.__group_manager.get_user_belong_gids([gid])[0]
        if not group:
            return False
        code, k8s_quota_dict = PinoCoreApi.get_quota(group.group_name)
        if not code:
            return False
        quota_info = self.trans_quota(kwargs)
        if not quota_info:
            return False
        PinoCoreApi.update_quota(group.group_name, **quota_info)
        self.__group_quota.update_gid_quota(gid, **quota_info)
        return True

    def delete_group_quota(self, gid):
        group = self.__group_manager.get_user_belong_gids([gid])[0]
        if not group:
            return True
        self.__group_quota.delete_gid_quota(gid)
        PinoCoreApi.delete_quota(group.group_name)
        return True

    def add_group_quota(self, gid, **kwargs):
        group = self.__group_manager.get_user_belong_gids([gid])[0]
        if not group:
            return False
        quota_info = self.trans_quota(kwargs)
        if not quota_info:
            return False
        code, k8s_quota_dict = PinoCoreApi.get_quota(group.group_name)
        if code:
            return False
        PinoCoreApi.create_quota(group.group_name, **quota_info)
        info = self.__group_quota.add_group(gid, **quota_info)
        return info

    def get_group_quota_usage(self, uid, gid):
        gids = self.__group_manager.get_user_belong_gids(uid)
        if gid not in gids:
            user = self.__user_manager.get_user_info_by_uid(uid)
            if not user.is_super:
                response_info = {
                    'status': False,
                    'result': None,
                    'message': '您没有权限查询该组资源信息。'
                }
                response = self.set_response(**response_info)
                return response

        quota = self.__group_quota.get_quota_by_gid(gid)
        if not quota:
            response_info = {
                'status': False,
                'result': None,
                'message': '用户组不存在: %s' % gid
            }
        else:
            # has_use_quota =PinoCoreApi.get_usage_by_gid(gid)
            all_quota_str = 'cpus: %s个, gpus: %s个, mems: %sG, disk: %s个'\
                            % (quota.cpus, quota.gpus, quota.mems, quota.disk)
            used_quota_str = 'cpus: %s个, gpus: %s个, mems: %sG, disk: %s个' % (0, 0, 0, 0)
            free_quota_str = 'cpus: %s个, gpus: %s个, mems: %sG, disk: %s个' \
                            % (quota.cpus, quota.gpus, quota.mems, quota.disk)
            quota_dict = {
                'all_quotas': all_quota_str,
                'used_quota': used_quota_str,
                'free_quota': free_quota_str,
            }
            response_info = {
                'status': True,
                'result': quota_dict,
                'message': '查询成功'
            }

        response = self.set_response(**response_info)
        return response


    def create_or_update_quota(self, **kwargs):
        quota_dict = {
            'group_id': kwargs.get('group_id', 0),
            'cpus': kwargs.get('cpus', 0),
            'gpus': kwargs.get('gpus', 0),
            'mems': kwargs.get('mems', 0),
            'disk': kwargs.get('disk', 0),
            'over_use_rate': kwargs.get('over_use_rate', 1.0),
        }
        quota = self.__group_quota.create_or_update_quota(**quota_dict)
        return quota