#!/usr/bin/env python3
# Time: 2017/12/3 17:56

__author__ = 'wanggangshan@jd.com'



import PinoCore.services as PinoCoreApi
from src.services.base import BaseService
from db.db import db_session_manager

class TestService(BaseService):
    def __init__(self, *args):
        print('接收到的初始化类参数: ', args)
        super(TestService, self).__init__(db_session_manager.get_session)

    def test(self, *args, x=1):
        print('函数接收到用户输入参数: ', args)
        return '测试成功'


def test(*args, x=2):
    print('函数接收到用户输入参数: ', args)
    return '测试成功'