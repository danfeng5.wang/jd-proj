from collections import defaultdict

import climate

import PinoCore.services as PinoCoreApi
from PinoCore.uniKubePino.pysrc.server import PinoUser
from common.session_object import user_session_manager
from src.logic.common_task import CommonTaskManager
from src.logic.task import TaskManager
from src.services import CloudService
from src.services.base import BaseService

logging = climate.get_logger(__name__)


class TaskService(BaseService):
    def __init__(self, db_session):
        super(TaskService, self).__init__(db_session)
        self.__task_manager = TaskManager(db_session)
        self.__common_task_manager = CommonTaskManager(db_session)
        self.__cloud_service = CloudService(db_session)

    def get_task_list_by_uid(self, uid):
        """
            通过uid获取用户任务列表

        :param uid: int， 用户唯一标识
        :return: list
        """
        tasklist = self.__task_manager.get_task_list_by_uid(uid)
        need_keys = ['task_id', 'owner_uid', 'owner_name', 'type', 'group_name',
                     'status', 'is_start', 'created_at', 'updated_at']
        task_list = []
        for task in tasklist:
            task_dict = {key: task.__dict__[key] for key in need_keys}
            task_list.append(task_dict)

        message = '查询成功' if len(task_list) > 0 else '任务列表为空'
        result = {'data': task_list, 'total': len(task_list)}
        response_info = {
            'status': True,
            'result': result,
            'message': message
        }
        response = self.set_response(**response_info)
        return response

    def get_task_list_by_gid(self, gid):
        """
            通过gid获取任务列表

        :param gid: int， 用户组id
        :return: list
        """
        tasklist = self.__task_manager.get_task_list_by_gid(gid)
        need_keys = ['task_id', 'owner_uid', 'type', 'group_name',
                     'status', 'created_at', 'updated_at']
        task_list = []
        for task in tasklist:
            task_dict = {key: task.__dict__[key] for key in need_keys}
            task_list.append(task_dict)

        message = '查询成功' if len(task_list) > 0 else '没有可查看算法实例'
        result = {'data': task_list, 'total': len(task_list)}
        response_info = {
            'status': True,
            'result': result,
            'message': message
        }
        response = self.set_response(**response_info)
        return response

    def get_task_list_by_gids(self, gids):
        """
            通过gids获取任务列表

        :param gids: list， 用户组id集合
        :return: list
        """
        task_list = self.__task_manager.get_task_list_by_gids(gids)
        return task_list

    def get_task_list_all(self):
        """
            获取任务列表

        :param None
        :return: list
        """
        task_list = self.__task_manager.get_task_list_all()
        return task_list

    def update_task_info(self, **kwargs):
        """
            更新任务信息

        :param kwargs: dict， 任务信息字段
        :return: bool
        """
        task_list = self.__task_manager.update_task_info(**kwargs)
        return task_list

    def add_task_info(self, **kwargs):
        """
            添加一个任务

        :param kwargs: dict， 任务信息字段
        :return: bool
        """
        try:
            task_info = self.__task_manager.add_task_info(**kwargs)
            return True
        except:
            return False

    def task_start(self, **kwargs):
        logging.info('train kwargs: ' + str(kwargs))
        project = kwargs.get('project_dir')
        socket_uuid = kwargs.get('socket_uuid')
        user_session = user_session_manager.get_user_session(socket_uuid)
        # kwargs['user_info']['token'] = '111111'
        user = PinoUser(**kwargs['user_info'])
        user_session.update(user=user)

        # project_type 由algo值决定 待定。
        algoType = kwargs.get('algo')
        config_path = kwargs.get('config_path')
        other_args_dict = kwargs.get('other_args_dict')

        # [start args]
        logging.info('========start args========')
        logging.info('algoType:' + str(algoType))
        logging.info('project:' + str(project))
        logging.info('configure_path:' + str(config_path))
        logging.info('keywords:' + str(other_args_dict))
        logging.info('==========================')
        # 登记task任务
        add_kwargs = defaultdict(list)
        add_kwargs['task_id'] = user_session.uniName
        add_kwargs['gid'] = kwargs['group_id']
        add_kwargs['group_name'] = kwargs['group_name']
        add_kwargs['type'] = 0
        add_kwargs['status'] = 0
        add_kwargs['owner_uid'] = kwargs['user_info']['user_id']
        add_kwargs['owner_name'] = kwargs['user_info']['username']
        add_kwargs['owner_email'] = kwargs['user_info']['email']
        logging.info('add_task_info:' + str(add_kwargs))
        self.__task_manager.add_task_info(**add_kwargs)

        request_info = PinoCoreApi.StartTrainRequest(
            algoType=algoType,
            uniName=user_session.uniName,
            project=project,
            extra={
                'configure_path': config_path,
                'keywords': other_args_dict,
            },
        )
        response = PinoCoreApi.service_startTrainNew(user_session, request_info)
        format_info = {
            'status': response.success,
            'result': response.uniName,
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response


    def task_check(self, user_info, task_id):
        """
            校验任务是否有效

        :param owner_uid: int， 用户id
        :param task_id: int， 任务id
        :return: dict
        """
        task_info = self.__task_manager.get_task_id(task_id)
        if not task_info:
            return {'status': 'fail', 'result': {}, 'message': 'task with uniName of ' + task_id + ' not found.'}

        if user_info['user_id'] != task_info.owner_uid and not user_info['is_super']:
            return {'status': 'fail', 'result': {}, 'message': 'task owner is not you.'}
        return {'status': 'success', 'result': {}, 'message': ''}

    def task_stop(self, user_info, task_id):
        """
            停止任务

        :param user_info: dict， 用户信息
        :param task_id: int， 任务id
        :return: dict
        """
        check_info = self.task_check(user_info, task_id)
        if not check_info['status']:
            return check_info
        response = PinoCoreApi.service_deleteTask_v2(user_info, task_id)
        format_info = {
            'status': response.success,
            'result': {'task_id': task_id},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        if not response.status:
            self.__task_manager.delete_task_by_task_id(task_id)
            return response
        self.__task_manager.delete_task_by_task_id(task_id)
        return response


    def task_view(self, user_info, task_id):
        """
            查看任务信息

        :param user_info: dict， 用户信息
        :param task_id: int， 任务id
        :return: dict
        """
        check_info = self.task_check(user_info, task_id)
        if not check_info['status']:
            return check_info
        response = PinoCoreApi.service_viewTask(user_info['user_id'], task_id)
        format_info = {
            'status': response.success,
            'result': {'task_detail': response.taskDetail},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response

    def task_logs(self, **kwargs):
        """
            查看任务日志

        :param kwargs: dict， 请求参数
        :return: dict
        """
        check_info = self.task_check(kwargs['user_info'], kwargs['task_id'])
        if not check_info['status']:
            return check_info
        request_info = PinoCoreApi.LogsRequest(taskID=kwargs['task_id'],
                                               nodeName=kwargs['node_id'],
                                               filepath=kwargs['logfile'],
                                               verb=kwargs['mode'])
        response = PinoCoreApi.service_logs(kwargs['session'], request_info)
        format_info = {
            'status': response.success,
            'result': {},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response


    def task_tty(self, **kwargs):
        """
            tty任务

        :param owner_uid: int， 用户id
        :param task_id: int， 任务id
        :return: dict
        """
        check_info = self.task_check(kwargs['session'].owner_uid, kwargs['task_id'])
        if not check_info['status']:
            return check_info
        request_info = PinoCoreApi.TTYRequest(taskID=kwargs['task_id'], nodeName=kwargs['node_id'])
        response = PinoCoreApi.service_TTY(kwargs['session'], request_info)
        format_info = {
            'status': response.success,
            'result': {},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response

    def task_list(self, **kwargs):
        """
            task_list任务

        :param owner_uid: int， 用户id
        :param task_id: int， 任务id
        :return: dict
        """
        check_info = self.task_check(kwargs['session'].owner_uid, kwargs['task_id'])
        if not check_info['status']:
            return check_info
        request_info = PinoCoreApi.TTYRequest(taskID=kwargs['task_id'], nodeName=kwargs['node_id'])
        response = PinoCoreApi.service_TTY(kwargs['session'], request_info)
        format_info = {
            'status': response.success,
            'result': {},
            'message': response.errmsg
        }
        response = self.set_response(**format_info)
        return response

    def task_start_result(self, **kwargs):
        status = kwargs['status']
        task_id = kwargs['task_id']
        namespace = kwargs['namespace']
        db_kwargs = defaultdict(list)
        db_kwargs['is_start'] = 1 if status else 0
        db_kwargs['task_id'] = task_id
        if status:
            response = PinoCoreApi.service_listTaskNode_v2(namespace, task_id)
            if not response.success:
                return self.set_response(**{'status': response.success,
                                            'result': {},
                                            'message': response.errmsg
                                            })
            db_kwargs['cloud_node_info'] = str([n.__dict__ for n in response.nodes])
            db_kwargs['cloud_cpus'] = 0
            db_kwargs['cloud_gpus'] = 0
            db_kwargs['cloud_mems'] = 0
            for resource in response.nodes:
                db_kwargs['cloud_cpus'] += resource.usage['cpu']
                db_kwargs['cloud_gpus'] += resource.usage['gpu']
                db_kwargs['cloud_mems'] += resource.usage['memory']
            self.__task_manager.update_task_info(**db_kwargs)
        else:
            self.__task_manager.update_task_info(**db_kwargs)
        return self.set_response(**{'status': True,
                                    'result': {},
                                    'message': ''
                                    })
