#!/usr/bin/env python3
# Time: 2017/11/29 11:13

__author__ = 'wanggangshan@jd.com'


from src.services.base import BaseService
from src.logic.user import UserManager
from src.logic.group import GroupManager
from utils.config_helper import config
from utils.util import get_uuid
from db.models import UserSourceType


class UserService(BaseService):
    def __init__(self, db_session):
        super(UserService, self).__init__(db_session)
        self.__user_manager = UserManager(db_session)
        self.__group_manager = GroupManager(db_session)

    def check_user_token(self, user_id, token):
        user_info = self.__user_manager.get_user_info_by_uid(user_id)
        return True if user_info.token == token else False


    def register_user(self, **kwargs):
        user_dict = {
            'is_super': kwargs.get('is_super', 0),
            'mobile': kwargs.get('mobile', ''),
            'email': kwargs.get('email', ''),
            'token': get_uuid()
        }
        user_info = self.__user_manager.add_user_info(**user_dict)
        user_source_dict = {
            'uid': user_info.id,
            'source_id': kwargs.get('source_id', 0),
            'source_type': kwargs.get('source_type', 0),
            'username': kwargs.get('username', ''),
            'password': kwargs.get('password', ''),
            'password_salt': kwargs.get('password_salt', ''),
            'source_describe': kwargs.get('source_describe', ''),
            'source_auth_code': kwargs.get('source_auth_code', ''),
        }
        self.__user_manager.add_user_source_info(**user_source_dict)
        response = self.get_user_above_info(user_info.id)
        return response

    def add_user(self, ):
        pass

    def get_source_by_sourceId(self, source_id, source_type=UserSourceType.Web):
        user_source = self.__user_manager.get_source_by_sourceId(source_id, source_type)
        return user_source

    def get_source_by_username(self, username, source_type=UserSourceType.Web):
        user_source = self.__user_manager.get_source_by_username(username, source_type)
        return user_source

    def get_user_info_by_uid(self, uid):
        user_info = self.__user_manager.get_user_info_by_uid(uid)
        return user_info

    def get_user_above_info(self, uid):
        user_info, user_source = self.__user_manager.get_user_all_info(uid)
        if (not user_info) or (not user_source):
            response_info = {
                'status': False,
                'result': None,
                'message': '用户不存在: %s' % uid
            }
        else:
            now_version = config.get('now_version')
            api_token = config.get('api_token').get(now_version)
            group_infos = self.__group_manager.get_group_infos_by_uid(uid)
            group_list = [
                {
                    'group_id': group.id,
                    'group_name': group.group_name,
                    'name': group.group_name,
                    'owner_uid': group.owner_uid,
                    'owner_name': group.owner_name
                } for group in group_infos
            ]
            user_dict = {
                'user_id': user_info.id,
                'username': user_source.username,
                'email': user_info.email,
                'mobile': user_info.mobile,
                'groups': group_list,
                'is_super': user_info.is_super,
                'source_type': user_source.source_type,
                'token': user_info.token,
            }
            response_info = {
                'status': True,
                'result': user_dict,
                'message': ''
            }

        response = self.set_response(**response_info)
        return response

    def create_or_update_user(self, **kwargs):
        user_dict = {
            'id': kwargs.get('user_id', 0),
            'is_super': kwargs.get('is_super', 0),
            'mobile': kwargs.get('mobile', ''),
            'email': kwargs.get('email', ''),
            'token': get_uuid()
        }
        user_info = self.__user_manager.create_or_update_user_info(**user_dict)

        user_source_dict = {
            'uid': user_info.id,
            'source_id': kwargs.get('source_id', 0),
            'source_type': kwargs.get('source_type', 0),
            'username': kwargs.get('username', ''),
            'password': kwargs.get('password', ''),
            'password_salt': kwargs.get('password_salt', ''),
            'source_describe': kwargs.get('source_describe', ''),
            'source_auth_code': kwargs.get('source_auth_code', ''),
        }
        self.__user_manager.create_or_update_user_source(**user_source_dict)
        response = self.get_user_above_info(user_info.id)
        return response