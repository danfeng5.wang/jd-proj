#!/usr/bin/env python3
# Time: 2017/11/27 16:04

__author__ = 'wanggangshan@jd.com'

from src.services.base import BaseService
from src.logic.user import UserManager
from src.logic.white_list import WhiteListManager


class WhiteListService(BaseService):
    def __init__(self, db_session):
        super(WhiteListService, self).__init__(db_session)
        self.__user_manager = UserManager(db_session)
        self.__white_manager = WhiteListManager(db_session)

    def add_white(self, uid):
        if self.get_white_user(uid):
            return True
        else:
            self.__white_manager.add_white_user(uid)
        return True

    def get_all_white_users(self):
        self.__white_manager.get_all_white_users()

    def get_white_user(self, uid):
        info = self.__white_manager.get_white_user(uid)
        return info

    def add_users_by_uids_or_erps(self, type='erp', users=[]):
        fail_not_exist = []
        fail_has_add = []
        success = []

        for tag in users:
            source = None
            if type == 'uid':
                user, source = self.__user_manager.get_user_all_info(tag)
            if type == 'erp':
                source = self.__user_manager.get_user_source_by_erp(tag)
            if not source:
                fail_not_exist.append(tag)
            else:
                uid = source.uid
                white = self.get_white_user(uid)
                if white:
                    fail_has_add.append(tag)

                else:
                    white_info = self.__white_manager.add_white_user(uid)
                    success.append(tag)
        status = False if fail_not_exist else True
        reponse_info = {
            'status': status,
            'result': {'fail_not_exist': fail_not_exist,
                       'fail_has_add': fail_has_add,
                       'success': success},
            'message': '添加完成'
        }
        response = self.set_response(**reponse_info)
        return response

    def delete_white(self, uid):
        self.__user_manager.delete_white_user(uid)
        format_info = {
            'status': True,
            'result': None,
            'message': '删除成功'
        }
        response = self.set_response(**format_info)
        return response
