#!/bin/sh

script_dir=$(cd `dirname $0`; pwd)
log_dir="${script_dir}/logs"
python_bin="/export/App/training_platform/anaconda3/bin/python"

ip="10.181.54.64"
port="5004"
exec_url="http://es.pino-ai.jd.com/1.0"

if [ $# = 2 ] ;then
    ip=$1
    port=$2
elif [ $# = 3 ] ;then
    ip=$1
    port=$2
    exec_url=$3
fi

if [ ! -d "$log_dir" ] ;then
    mkdir "$log_dir"
fi


cur_time=$(date +%Y%m%d%H%M%S)
log_file="${log_dir}/${ip}_${port}_${cur_time}.log"

lsof -i:${port}|grep -v 'COMMAND'| awk '{print $2}'|xargs -i kill -9 {}

$python_bin app_server.py -i $ip -p $port  > $log_file 2>&1 &
#$python_bin server.py $ip $port $exec_url > $log_file 2>&1 &
