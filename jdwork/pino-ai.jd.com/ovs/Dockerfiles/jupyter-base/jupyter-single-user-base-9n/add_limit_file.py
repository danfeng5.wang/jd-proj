import notebook
from shutil import copyfile

def get_notebook_base_path():
    note_file = notebook.__file__.split('/')
    note_file.pop()
    return '/'.join(note_file)

nb_base_path = get_notebook_base_path()

zmq_path = nb_base_path + ('/base/zmqhandlers.py')

copyfile('./zmqhandlers.py', zmq_path)
