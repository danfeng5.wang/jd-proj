#!/bin/bash

set -e

wget -t 3 ftp://mfsb.jd.com:/mnt/mfsb/pino/keytabs/${JUPYTERHUB_USER}.keytab -O /etc/kerberos/9n-user.keytab >/dev/null 2>&1 || {
    echo "Fail to get keytab. Delete Pyspark kernel"
    \rm -rf /usr/share/jupyter/kernels
    \rm -f /etc/jupyter/00-pyspark-setup.py
    \rm -f /tmp/getkeytab_before_start_notebook.sh
    exec tini -- $*
    exit 0
}

sed -E -i "s/__JUPYTERHUB_USER__/${JUPYTERHUB_USER}/g" /usr/share/jupyter/kernels/pyspark/kernel.json

ipython profile create pyspark
cp /etc/jupyter/00-pyspark-setup.py ~/.ipython/profile_pyspark/startup/

\rm -f /tmp/getkeytab_before_start_notebook.sh

exec tini -- $*
